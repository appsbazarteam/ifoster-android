package com.ifoster;

import android.app.Application;
import android.content.Context;
import com.ifoster.common.utility.TypeFaceUtil;
import com.ifoster.mvp.login.LoginModel;

public class App extends Application {

    private static App sInstance;
    public static Context context;
    private boolean isShowParentTab;
    private LoginModel.Eid eid;
    private String profileImageURL = "";
    private boolean isSingleStudent;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        context = getApplicationContext();
        TypeFaceUtil.setDefaultFont(this, "MONOSPACE", "fonts/Quicksand-Bold.otf");
    }

    public static App getApplicationInstance() {
        return sInstance;
    }

    public boolean isShowParentTab() {
        return isShowParentTab;
    }

    public void setShowParentTab(boolean showParentTab) {
        isShowParentTab = showParentTab;
    }

    public LoginModel.Eid getEid() {
        return eid;
    }

    public void setEid(LoginModel.Eid eid) {
        this.eid = eid;
    }

    public String getProfileImageURL() {
        return profileImageURL;
    }

    public void setProfileImageURL(String profileImageURL) {
        this.profileImageURL = profileImageURL;
    }

    public boolean isSingleStudent() {
        return isSingleStudent;
    }
    public void setSingleStudent(boolean singleStudent) {
        isSingleStudent = singleStudent;
    }
}