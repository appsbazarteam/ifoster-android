package com.ifoster.common.utility;

import android.widget.ImageView;

import com.ifoster.App;
import com.ifoster.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by A1VSZHT2 on 7/18/2018.
 */

public class BitmapUtils {

    public static void loadImageCircular(ImageView imageView, String imageURL) {
        if (imageURL != null && imageURL.length() > 0) {
            Glide.with(App.context).load(imageURL)
                    .error(R.drawable.user)
                    .placeholder(R.drawable.user)
                    .thumbnail(0.5f)
                    .crossFade()
                    .transform(new CircleTransform(App.context))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);

        } else {
            imageView.setBackgroundResource(R.drawable.user);
        }
    }
}
