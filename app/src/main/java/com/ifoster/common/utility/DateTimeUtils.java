package com.ifoster.common.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by A1VSZHT2 on 7/19/2018.
 */

public class DateTimeUtils {

    public static DateFormat DATE_FORMAT_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");
    public static DateFormat DATE_FORMAT_DD_MM_YYYY = new SimpleDateFormat("dd-MM-yyyy");
    public static DateFormat DATE_FORMAT_MMMM = new SimpleDateFormat("MMMM");
    public static DateFormat DATE_FORMAT_MMM = new SimpleDateFormat("MMM");
    public static DateFormat DATE_FORMAT_MM = new SimpleDateFormat("MM");
    public static DateFormat DATE_FORMAT_DD = new SimpleDateFormat("dd");

    public static String convertDate(String dateData,DateFormat dateFormat1, DateFormat dateFormat2){
        String formattedDate = "";
        try{
            Date date = dateFormat1.parse(dateData);
            formattedDate = dateFormat2.format(date);
        }catch (Exception e){
            e.printStackTrace();
        }
        return formattedDate;
    }

}
