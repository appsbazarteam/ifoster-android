package com.ifoster.common.utility;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Braintech on 9/12/2016.
 */
public class Utils {

    public static void hideKeyboardIfOpen(Activity activity) {
        View view = activity.getCurrentFocus();

        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String convertDate(String date) {
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date newDate = null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss");
        date = spf.format(newDate);
        return date;
    }

    public static String convertToSimpleDateFormate(String dateOfWeight) {

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = null;
            try {
                date = simpleDateFormat.parse(dateOfWeight);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            SimpleDateFormat newFormatDate = new SimpleDateFormat("yyyy-MM-dd");

            return newFormatDate.format(date);
        } catch (Exception ex) {
            ex.printStackTrace();

            return dateOfWeight;
        }
    }

    public static String convertToDateFormate(String dateOfWeight) {

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//
            Date date = null;
            try {
                date = simpleDateFormat.parse(dateOfWeight);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            SimpleDateFormat newFormatDate = new SimpleDateFormat("dd/MM/yyyy");

            return newFormatDate.format(date);
        } catch (Exception ex) {
            ex.printStackTrace();
            return dateOfWeight;
        }
    }


    public static String convertToSimpleDateFormateNew(String dateOfWeight) {

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = null;
            try {
                date = simpleDateFormat.parse(dateOfWeight);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            SimpleDateFormat newFormatDate = new SimpleDateFormat("dd MMM yyyy hh:mm aa", Locale.US);

            return newFormatDate.format(date);
        } catch (Exception ex) {
            ex.printStackTrace();

            return dateOfWeight;
        }
    }

    public static Date getCurrentDateWithDateFormate() {

        Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(new Date()); // sets calendar time/date
        Date currentDate = cal.getTime(); // returns new date object, one hour in the future
        return currentDate;
    }

    public static Date getCurrentDateWith15MinExtra() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE, 15);
        Date currentDateWith15MinExtra = cal.getTime();
        return currentDateWith15MinExtra;
    }

    public static boolean compareDate(Date currentDate, Date currentDateWith15MinExtra, Date doctorAvailDate) {
        if ((((currentDate.compareTo(doctorAvailDate) == 0)) || ((currentDate.compareTo(doctorAvailDate) < 0)))
                && (((currentDateWith15MinExtra.compareTo(doctorAvailDate) == 0)) || ((currentDateWith15MinExtra.compareTo(doctorAvailDate) > 0)))
                ) {
            return true;

        } else {
            return false;
        }
    }

    public static String getCurrentDay() {
        Date now = new Date();
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
        return simpleDateformat.format(now);
    }

    public static String getCurrentMonth() {
        Calendar cal = Calendar.getInstance();
        String month = new SimpleDateFormat("MMM").format(cal.getTime());
        return month;
    }


    public static String getCurrentMonthForFee() {
        Calendar cal = Calendar.getInstance();
        String month = new SimpleDateFormat("MMMM").format(cal.getTime());
        return month;
    }

    public static Date convertToSimpleDateToDateFormate(String dateOfWeight) {
        Date date = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                date = simpleDateFormat.parse(dateOfWeight);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return date;
        } catch (Exception ex) {
            ex.printStackTrace();
            return date;
        }
    }

    public static String getMonthFromDate(String dateOfWeight) {
        Date date = null;
        SimpleDateFormat simpleDateformat = null;

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                date = simpleDateFormat.parse(dateOfWeight);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            simpleDateformat = new SimpleDateFormat("MMM"); // the day of the week spelled out completely
            return simpleDateformat.format(date);
        } catch (Exception ex) {
            ex.printStackTrace();
            return simpleDateformat.format(date);
        }
    }

    public static String getDateFromDate(String dateOfWeight) {
        Date date = null;
        SimpleDateFormat simpleDateformat = null;

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                date = simpleDateFormat.parse(dateOfWeight);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            simpleDateformat = new SimpleDateFormat("dd");
            return simpleDateformat.format(date);
        } catch (Exception ex) {
            ex.printStackTrace();
            return simpleDateformat.format(date);
        }
    }


    public static String convertToSimpleDateTime(String dateOfWeight) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = null;
            try {
                date = simpleDateFormat.parse(dateOfWeight);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            SimpleDateFormat newFormatDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            return newFormatDate.format(date);
        } catch (Exception ex) {
            ex.printStackTrace();
            return dateOfWeight;
        }
    }

    public static boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public static Boolean isEmptyOrNull(String value) {
        if (value == null || value.isEmpty())
            return true;
        else
            return false;
    }

    public static String convertDateFormat(String date, String inputFormat, String outputFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM,yyyy");
        Date testDate = null;
        String newFormat = "N/A";
        try {
            testDate = sdf.parse(date);
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            newFormat = formatter.format(testDate);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return newFormat;
    }

    public static String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        String date = dateFormat.format(cal.getTime());
        return date;
    }

    public static Boolean isVisibleOrNull(Fragment fragment) {
        if (fragment != null) {
            if (fragment.isVisible())
                return true;
            else
                return false;
        } else
            return false;
    }

    public static Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path) throws IOException {

        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }


    public static String getRealPathFromURI(String contentURI, Context context) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static Bitmap resizeImageForImageView(Bitmap bitmap, float rotation) {

        Bitmap resizedBitmap = null;
        int scaleSize = 1024;
        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();
        int newWidth = -1;
        int newHeight = -1;
        float multFactor = -1.0F;
        if (originalHeight > originalWidth) {
            newHeight = scaleSize;
            multFactor = (float) originalWidth / (float) originalHeight;
            newWidth = (int) (newHeight * multFactor);
        } else if (originalWidth > originalHeight) {
            newWidth = scaleSize;
            multFactor = (float) originalHeight / (float) originalWidth;
            newHeight = (int) (newWidth * multFactor);
        } else if (originalHeight == originalWidth) {
            newHeight = scaleSize;
            newWidth = scaleSize;
        }

        Log.e("rotation", "" + rotation);

        Matrix matrix = new Matrix();
        matrix.postRotate(rotation);
        return resizedBitmap;
    }

    public static Bitmap rotateImage(Bitmap bitmap, float rotation) {

        Bitmap resizedBitmap = null;
        int scaleSize = 1024;
        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();

        Log.e("rotation", "" + rotation);

        Matrix matrix = new Matrix();
        matrix.postRotate(rotation);

        return Bitmap.createBitmap(bitmap, 0, 0, originalWidth, originalHeight, matrix, true);
    }

    public static String formatCurrency(Double amount) {
        String s = null;
        try {
            NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
            s = n.format(amount);

        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        return s;
    }

    public static String currencyFormat(String sum) {
        Double doubleSum = Double.parseDouble(sum);
        String resultSum = null;

        if (doubleSum > 0) {
            DecimalFormat decimalFormat = new DecimalFormat("#.00");
            resultSum = "Rs. " + decimalFormat.format(doubleSum);
        } else {
            resultSum = "Rs. 0.00";
        }
        return resultSum;
    }

    public static Spanned fromHtml(String text) {
        Spanned result;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(text);
        }
        return result;
    }

    public static String dateFormat(String dateTime) {
        String date = "";
        SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date newDate = null;
        try {
            newDate = spf.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("dd MMM yyyy hh:mm aa", Locale.US);
        date = spf.format(newDate);

        return date;
    }

    public static Calendar getDateAndTime(String dateTime) {
        String date = "";
        SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
        Date newDate = null;
        try {
            newDate = spf.parse(dateTime);

            Calendar cal = Calendar.getInstance();
            cal.setTime(newDate);

            return cal;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public static void setCrossTextView(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public static String getBase64FromString(String text) {
        byte[] data = null;
        try {
            data = text.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        return base64;
    }
}
