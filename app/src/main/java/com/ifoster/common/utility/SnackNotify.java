package com.ifoster.common.utility;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.ifoster.App;
import com.ifoster.R;
import com.ifoster.interfaces.OnClickInterface;


/**
 * Created by Braintech on 30-Oct-15.
 */
public class SnackNotify {

    private static final int TIME_DELAY = 2000;
    private static long back_pressed;

    public static void checkConnection(final OnClickInterface onRetryClick, View coordinatorLayout) {

        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "No internet connection!.", Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onRetryClick.onClick();
                    }
                });

        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(App.getApplicationInstance(), R.color.color_white));

        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.RED);
        snackbar.show();
    }

    public static void showSnakeBarForBackPress(View coordinatorLayout) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Please click BACK again to exit.", Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(App.getApplicationInstance(), R.color.colorPrimary));
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);

        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            System.exit(0);
        } else {
            snackbar.show();
        }
        back_pressed = System.currentTimeMillis();
    }

    public static void showMessage(String msg, View coordinatorLayout) {

        if (coordinatorLayout != null) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "", Snackbar.LENGTH_LONG);

            Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
            layout.setBackgroundColor(ContextCompat.getColor(App.getApplicationInstance(), android.R.color.transparent));

            TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
            textView.setVisibility(View.INVISIBLE);

            LayoutInflater mInflater = LayoutInflater.from(App.getApplicationInstance());
            View snackView = mInflater.inflate(R.layout.layout_snackbar, null);

            TextView txtViewMsg = (TextView) snackView.findViewById(R.id.txtViewMsg);
            txtViewMsg.setText(msg);
            txtViewMsg.setTextColor(Color.WHITE);

            layout.addView(snackView, 0);
            snackbar.show();
        }
    }
}
