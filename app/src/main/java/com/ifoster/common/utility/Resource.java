package com.ifoster.common.utility;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import com.ifoster.App;

public class Resource {

    static Resources resources;
    static final String PACKAGE_NAME;

    static {
        resources = App.context.getResources();
        PACKAGE_NAME = App.context.getPackageName();
    }

    public static int toColor(int colorId) {
        return resources.getColor(colorId);
    }

    public static float toDimen(int dimenId) {
        return resources.getDimension(dimenId);
    }

    public static int toInt(int resId) {
        return resources.getInteger(resId);
    }

    public static String toString(int stringId) {
        return resources.getString(stringId);
    }

    public static Spanned toHtmlSpan(int stringId) {
        return Html.fromHtml(resources.getString(stringId));
    }

    public static VectorDrawableCompat toVectorDrawable(int drawableId) {
        return VectorDrawableCompat.create(resources, drawableId, App.context.getTheme());
    }

    public static Drawable toDrawable(int drawableId) {
        return ResourcesCompat.getDrawable(resources, drawableId, App.context.getTheme());
    }

    public static Drawable toDrawable(String iconName) {
        if(TextUtils.isEmpty(iconName)) return null;

        return toDrawable(toResId(iconName, "drawable"));
    }

    public static int toResId(String identifier, String defType) {
        if(TextUtils.isEmpty(identifier)) return 0;

        return resources.getIdentifier(identifier, defType, App.context.getPackageName());
    }

    public static int toDrawableResId(String identifier) {
        return toResId(identifier, "drawable");
    }

    public static LayerDrawable layer(int... drawableIds) {
        Drawable[] drawables = new Drawable[drawableIds.length];
        for(int i = 0; i < drawableIds.length; i++) {
            drawables[i] = toDrawable(drawableIds[i]);
        }

        return layer(drawables);
    }

    public static LayerDrawable layer(Drawable... drawables) {
        return new LayerDrawable(drawables);
    }

    public static LayerDrawable layer(int drawableId, Drawable baseDrawable) {
        return layer(baseDrawable, toDrawable(drawableId));
    }

    /*public static ShapeDrawable toShapeDrawable(int baseColor, DrawableConfig.Shape shapeType) {
        Shape shape;

        switch(shapeType) {
            case RECTANGLE:
                shape = new RectShape();
                break;

            case CIRCLE:
            default:
                shape = new OvalShape();
                break;

        }

        ShapeDrawable shapeDrawable = new ShapeDrawable(shape);
        shapeDrawable.getPaint().setColor(toColor(baseColor));
        return shapeDrawable;
    }*/

    public static String toString(int... stringIds) {
        StringBuilder builder = new StringBuilder();

        for(int i = 0; i < stringIds.length; i++) {
            builder.append(toString(stringIds[i]));
        }
        return builder.toString();
    }

    public static String[] toStringArray(int arrayId) {
        return resources.getStringArray(arrayId);
    }

    public static int[] toIntArray(int arrayId) {
        return resources.getIntArray(arrayId);
    }

    public static TypedArray getStyle(int styleId, int[] attrs) {
        return App.context.obtainStyledAttributes(styleId, attrs);
    }

    public static int getPixelSizeForDimen(int dimenId){
        return resources.getDimensionPixelSize(dimenId);
    }

    public static String toStringFormat(int stringId, Object... args) {
        return resources.getString(stringId, args);
    }

    /* toPluralString : basically format string+returns string based on quantity */
    public static String toPluralString(int pluralStringId, int quantity, Object... args) {
        return resources.getQuantityString(pluralStringId, quantity, args);
    }
}
