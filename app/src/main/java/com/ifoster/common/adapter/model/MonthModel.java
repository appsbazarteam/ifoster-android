package com.ifoster.common.adapter.model;

/**
 * Created by Alam on 23-Jun-18.
 */

public class MonthModel {

    private String monthName;
    private boolean isSelected;

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
