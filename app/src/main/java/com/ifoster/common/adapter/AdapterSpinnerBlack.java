package com.ifoster.common.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.ifoster.R;
import com.ifoster.common.requestresponse.Const;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Braintech on 10/12/2015.
 */
public class AdapterSpinnerBlack extends ArrayAdapter<String> {

    private ArrayList<HashMap<String, String>> list;
    private Context context;

    public AdapterSpinnerBlack(Context context, int txtViewResourceId,
                               ArrayList<HashMap<String, String>> list) {
        super(context, txtViewResourceId);
        this.context = context;
        this.list = list;

    }

    @Override
    public String getItem(int index) {
        return list.get(index).get(Const.KEY_NAME);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        return getCustomViewDropdown(position, cnvtView, prnt);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) View.inflate(context, R.layout.layout_spinner_dropdown, null);

        textView.setTextColor(ContextCompat.getColor(context, R.color.color_black));
        HashMap<String, String> events = list.get(position);

        if (events.get(Const.KEY_NAME) != null)
            textView.setText(events.get(Const.KEY_NAME));

        return textView;
    }

    public View getCustomViewDropdown(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) View.inflate(context, R.layout.layout_spinner_dropdown_view, null);

        HashMap<String, String> events = list.get(position);
        if (events.get(Const.KEY_NAME) != null)
            textView.setText(events.get(Const.KEY_NAME));

        return textView;
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
