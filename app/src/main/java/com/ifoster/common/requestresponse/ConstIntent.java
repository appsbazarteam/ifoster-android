package com.ifoster.common.requestresponse;

/**
 * Created by Braintech on 9/16/2016.
 */
public class ConstIntent {

    public static String KEY_CATEGORY_NAME = "key_category_name";
    public static String KEY_ABSENT_LIST = "key_absent_list";

    public static String KEY_PRESENT_LIST = "key_present_list";

    public static String KEY_EXAM_NAME = "key_exam_name";

    public static String KEY_TASK_DETAIL = "key_task_detail";

    public static String KEY_USER_TYPE = "key_user_type";

    public static String KEY_TASK_TYPE = "key_task_type";

    public static String KEY_USER_LIST = "key_user_list";

    public static String KEY_CLASS_LIST = "key_class_list";

    public static String KEY_CATEGORY_ID = "key_category_id";

    public static String KEY_ID = "key_id";

    public static String KEY_IS_STUDENT = "key_is_student";

    public static String KEY_IS_REJECT_LIST = "key_is_reject_list";

    public static final String KEY_IMAGE = "image";

}
