package com.ifoster.common.requestresponse;

import com.ifoster.mvp.achievement.model.AchievementResponseModel;
import com.ifoster.mvp.attendance.model.AttendanceDetailModel;
import com.ifoster.mvp.attendance.model.MonthDetailModel;
import com.ifoster.mvp.attendance.model.PresentModel;
import com.ifoster.mvp.attendance_teacher.model.ClassListModel;
import com.ifoster.mvp.attendance_teacher.model.ClassStudentListModel;
import com.ifoster.mvp.attendance_teacher.model.MonthDetailTeacherModel;
import com.ifoster.mvp.attendance_teacher.model.TakeAttendanceResponseModel;
import com.ifoster.mvp.change_mobile_no.ChangeMobileNoModel;
import com.ifoster.mvp.class_time_table.ClassTimeTableModel;
import com.ifoster.mvp.exam_schedule.ExamScheduleModel;
import com.ifoster.mvp.fee.model.FeeResponseModel;
import com.ifoster.mvp.forgot_password.ForgotPasswordModel;
import com.ifoster.mvp.gallery.model.GalleryDetailModel;
import com.ifoster.mvp.gallery.model.GalleryModel;
import com.ifoster.mvp.holiday_calender.HoliDayCalenderModel;
import com.ifoster.mvp.leave.leave_approval.fragment.model.EmployeeApprovelListModel;
import com.ifoster.mvp.leave.leave_approval.fragment.model.StudentApprovelListModel;
import com.ifoster.mvp.leave.leave_approve_detail.model.EmployeeApprovedLeaveModel;
import com.ifoster.mvp.leave.leave_dashboard.fragment.model.LeaveApplylModel;
import com.ifoster.mvp.leave.leave_dashboard.fragment.model.LeaveListHistoryModel;
import com.ifoster.mvp.leave.leave_dashboard.fragment.model.LeaveListTeacherModel;
import com.ifoster.mvp.login.LoginModel;
import com.ifoster.mvp.medical.MedicalIdModel;
import com.ifoster.mvp.medical_teacher.UpdateFamilyIdlModel;
import com.ifoster.mvp.navigation.model.LogoutModel;
import com.ifoster.mvp.notice_board.model.EventListModel;
import com.ifoster.mvp.notice_board.model.NoticeModel;
import com.ifoster.mvp.notification.model.NotificationModel;
import com.ifoster.mvp.reset_password.ResetPasswordModel;
import com.ifoster.mvp.show_student_list.ShowStudentListModel;
import com.ifoster.mvp.task.task_dashboard.model.CurrentTaskModel;
import com.ifoster.mvp.task.task_detail.TaskDetailModel;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Braintech on 9/12/2016.
 */
public interface ApiService {

    //<-----------------------------Api's for Admin-------------------------------->

    //<-----------------------------Login Api-------------------------------->

    @POST("login_v1")
    Call<LoginModel> login(@Header("Content-Type")
                                   String contentType,
                           @Header("Cache-Control")
                                   String cache,
                           @Body RequestBody params);

    @POST("testNotification")
    Call<LoginModel> login1(@Header("Content-Type")
                                    String contentType,
                            @Header("Cache-Control")
                                    String cache,
                            @Body RequestBody params);

    //<-----------------------------Reset Password Api-------------------------------->
    @POST("set-new-password")
    Call<ResetPasswordModel> resetPassword(@Header("Content-Type")
                                                   String contentType,
                                           @Header("Cache-Control")
                                                   String cache,
                                           @Body RequestBody params);

    //<-----------------------------get student list-------------------------------->
    @POST("getStudentNewList")
    Call<ShowStudentListModel> getStudentList(@Header("Content-Type")
                                                      String contentType,
                                              @Header("Cache-Control")
                                                      String cache,
                                              @Body RequestBody params);


    //<-----------------------------get current task list-------------------------------->
    @POST("homeworkListCurrent")
    Call<CurrentTaskModel> getCutrrentTask(@Header("Content-Type")
                                                   String contentType,
                                           @Header("Cache-Control")
                                                   String cache,
                                           @Body RequestBody params);

    //<-----------------------------get current task list-------------------------------->
    @POST("homeworkListArchive")
    Call<CurrentTaskModel> getArchiveTask(@Header("Content-Type")
                                                  String contentType,
                                          @Header("Cache-Control")
                                                  String cache,
                                          @Body RequestBody params);

    //<-----------------------------get current detail-------------------------------->
    @POST("homeworkDetailsCurrent")
    Call<TaskDetailModel> getCurrentTaskDetail(@Header("Content-Type")
                                                       String contentType,
                                               @Header("Cache-Control")
                                                       String cache,
                                               @Body RequestBody params);

    //<-----------------------------get current detail-------------------------------->
    @POST("homeworkDetailsArchive")
    Call<TaskDetailModel> getArchiveTaskDetail(@Header("Content-Type")
                                                       String contentType,
                                               @Header("Cache-Control")
                                                       String cache,
                                               @Body RequestBody params);


    //<-----------------------------get teacher attendance list-------------------------------->
    @POST("techerAttList")
    Call<ClassListModel> getTeacherAttendance(@Header("Content-Type")
                                                      String contentType,
                                              @Header("Cache-Control")
                                                      String cache,
                                              @Body RequestBody params);


    //<-----------------------------get class student list-------------------------------->
    @POST("getAttendanceStudentList")
    Call<ClassStudentListModel> getClassStudentList(@Header("Content-Type")
                                                            String contentType,
                                                    @Header("Cache-Control")
                                                            String cache,
                                                    @Body RequestBody params);

    //<-----------------------------get notice list-------------------------------->
    @POST("getNoticeList")
    Call<NoticeModel> getNoticeBoard(@Header("Content-Type")
                                             String contentType,
                                     @Header("Cache-Control")
                                             String cache,
                                     @Body RequestBody params);

    @POST("getClasswiseNoticeList")
    Call<NoticeModel> getClasswiseNoticeList(@Header("Content-Type")
                                             String contentType,
                                     @Header("Cache-Control")
                                             String cache,
                                     @Body RequestBody params);

    @POST("getGlobalNoticeList")
    Call<NoticeModel> getGlobalNoticeList(@Header("Content-Type")
                                                     String contentType,
                                             @Header("Cache-Control")
                                                     String cache,
                                             @Body RequestBody params);


    //<-----------------------------get notice list-------------------------------->
    @POST("getNoticeEmployeeList")
    Call<NoticeModel> getEmpNoticeBoard(@Header("Content-Type")
                                                String contentType,
                                        @Header("Cache-Control")
                                                String cache,
                                        @Body RequestBody params);

    //<-----------------------------get event list-------------------------------->
    @POST("getEventList")
    Call<EventListModel> getEventList(@Header("Content-Type")
                                              String contentType,
                                      @Header("Cache-Control")
                                              String cache,
                                      @Body RequestBody params);

    @POST("getClasswiseEventList")
    Call<EventListModel> getClasswiseEventList(@Header("Content-Type")
                                              String contentType,
                                      @Header("Cache-Control")
                                              String cache,
                                      @Body RequestBody params);

    @POST("getGlobalEventList")
    Call<EventListModel> getGlobalEventList(@Header("Content-Type")
                                              String contentType,
                                      @Header("Cache-Control")
                                              String cache,
                                      @Body RequestBody params);

    //<-----------------------------get event list-------------------------------->
    @POST("getEventEmployeeList")
    Call<EventListModel> getEmpEventList(@Header("Content-Type")
                                                 String contentType,
                                         @Header("Cache-Control")
                                                 String cache,
                                         @Body RequestBody params);


    //<-----------------------------Change Mobile No Api-------------------------------->
    @POST("changeMobile")
    Call<ChangeMobileNoModel> changeMobileNo(@Header("Content-Type")
                                                     String contentType,
                                             @Header("Cache-Control")
                                                     String cache,
                                             @Body RequestBody params);


    //<-----------------------------Attendance Detail Api-------------------------------->
    @POST("studentAttendancedata")
    Call<AttendanceDetailModel> getAttendanceDetail(@Header("Content-Type")
                                                            String contentType,
                                                    @Header("Cache-Control")
                                                            String cache,
                                                    @Body RequestBody params);

    //<----------------------------- Teacher Attendance Detail Api-------------------------------->
    @POST("employeeAttendancedata")
    Call<AttendanceDetailModel> getTeacherAttendanceDetail(@Header("Content-Type")
                                                                   String contentType,
                                                           @Header("Cache-Control")
                                                                   String cache,
                                                           @Body RequestBody params);


    //<-----------------------------Month Detail Api-------------------------------->
    @POST("studentAttendancedetailsdata")
    Call<MonthDetailModel> getMonthDetail(@Header("Content-Type")
                                                  String contentType,
                                          @Header("Cache-Control")
                                                  String cache,
                                          @Body RequestBody params);

    @POST("studentAttendancePresent")
    Call<PresentModel> getStudentAttendancePresent(@Header("Content-Type")
                                                  String contentType,
                                                   @Header("Cache-Control")
                                                  String cache,
                                                   @Body RequestBody params);

    @POST("employeeAttendancePresent")
    Call<PresentModel> getEmployeeAttendancePresent(@Header("Content-Type")
                                                  String contentType,
                                          @Header("Cache-Control")
                                                  String cache,
                                          @Body RequestBody params);

    //<-----------------------------Month Detail Api-------------------------------->
    @POST("employeeAttendancedetails")
    Call<MonthDetailTeacherModel> getTeacherMonthDetail(@Header("Content-Type")
                                                                String contentType,
                                                        @Header("Cache-Control")
                                                                String cache,
                                                        @Body RequestBody params);


    //<-----------------------------Exam Schedule Api-------------------------------->
    @POST("examSchedule")
    Call<ExamScheduleModel> getExamSchedule(@Header("Content-Type")
                                                    String contentType,
                                            @Header("Cache-Control")
                                                    String cache,
                                            @Body RequestBody params);

    //<-----------------------------Exam Schedule Api-------------------------------->
    @POST("examEmpSchedule")
    Call<ExamScheduleModel> getTeacherExamSchedule(@Header("Content-Type")
                                                           String contentType,
                                                   @Header("Cache-Control")
                                                           String cache,
                                                   @Body RequestBody params);


    //<-----------------------------Get Holiday Day Api-------------------------------->
    @POST("holidayCalendar")
    Call<HoliDayCalenderModel> getHolidayList(@Header("Content-Type")
                                                      String contentType,
                                              @Header("Cache-Control")
                                                      String cache,
                                              @Body RequestBody params);

    //<-----------------------------Class Time Table Api-------------------------------->
    @POST("classTimeTable")
    Call<ClassTimeTableModel> getClassTimeTable(@Header("Content-Type")
                                                        String contentType,
                                                @Header("Cache-Control")
                                                        String cache,
                                                @Body RequestBody params);

    //<-----------------------------Teacher Class Time Table Api-------------------------------->
    @POST("empTimeTable")
    Call<ClassTimeTableModel> getTeacherClassTimeTable(@Header("Content-Type")
                                                               String contentType,
                                                       @Header("Cache-Control")
                                                               String cache,
                                                       @Body RequestBody params);

    //<-----------------------------Student Medical Api-------------------------------->
    @POST("medicalID")
    Call<MedicalIdModel> getMedicalId(@Header("Content-Type")
                                              String contentType,
                                      @Header("Cache-Control")
                                              String cache,
                                      @Body RequestBody params);

    //<-----------------------------Teacher Medical Api-------------------------------->
    @POST("empMedicalID")
    Call<MedicalIdModel> getTeacherMedicalId(@Header("Content-Type")
                                                     String contentType,
                                             @Header("Cache-Control")
                                                     String cache,
                                             @Body RequestBody params);


    //<-----------------------------Exam Schedule Api-------------------------------->
    @POST("logout")
    Call<LogoutModel> logout(@Header("Content-Type")
                                     String contentType,
                             @Header("Cache-Control")
                                     String cache,
                             @Body RequestBody params);

    //<-----------------------------get student leave list-------------------------------->
    @POST("studentleaveList")
    Call<LeaveListTeacherModel> getLeaveList(@Header("Content-Type")
                                                     String contentType,
                                             @Header("Cache-Control")
                                                     String cache,
                                             @Body RequestBody params);


    //<-----------------------------get teacher leave list-------------------------------->
    @POST("employeeleaveList")
    Call<LeaveListTeacherModel> getTeacherLeaveList(@Header("Content-Type")
                                                            String contentType,
                                                    @Header("Cache-Control")
                                                            String cache,
                                                    @Body RequestBody params);


    //<-----------------------------get student leave history list-------------------------------->
    @POST("studentleaveHistory")
    Call<LeaveListHistoryModel> getLeaveListHistory(@Header("Content-Type")
                                                            String contentType,
                                                    @Header("Cache-Control")
                                                            String cache,
                                                    @Body RequestBody params);

    //<-----------------------------get Teacher leave history list-------------------------------->
    @POST("employeeleaveHistory")
    Call<LeaveListHistoryModel> getTeacherLeaveListHistory(@Header("Content-Type")
                                                                   String contentType,
                                                           @Header("Cache-Control")
                                                                   String cache,
                                                           @Body RequestBody params);

    //<-----------------------------Apply Leave-------------------------------->
    @POST("studentLeaveApply_v1")
    Call<LeaveApplylModel> applyLeave(@Header("Content-Type")
                                              String contentType,
                                      @Header("Cache-Control")
                                              String cache,
                                      @Body RequestBody params);

    //<-----------------------------Teacher Apply Leave-------------------------------->
    @POST("employeeLeaveApply_v1")
    Call<LeaveApplylModel> applyTeacherLeave(@Header("Content-Type")
                                                     String contentType,
                                             @Header("Cache-Control")
                                                     String cache,
                                             @Body RequestBody params);

    //<-----------------------------Update Teacher Medical-------------------------------->
    @POST("empMedicalUpdateID")
    Call<UpdateFamilyIdlModel> updateTeacherMedicalId(@Header("Content-Type")
                                                              String contentType,
                                                      @Header("Cache-Control")
                                                              String cache,
                                                      @Body RequestBody params);

    //<-----------------------------Update Teacher Medical-------------------------------->
    @POST("medicalUpdateID")
    Call<UpdateFamilyIdlModel> updateStudentMedicalId(@Header("Content-Type")
                                                              String contentType,
                                                      @Header("Cache-Control")
                                                              String cache,
                                                      @Body RequestBody params);

    //<-----------------------------Update Teacher Medical-------------------------------->
    @POST("studentFeemonthly")
    Call<FeeResponseModel> getStudentFee(@Header("Content-Type")
                                                 String contentType,
                                         @Header("Cache-Control")
                                                 String cache,
                                         @Body RequestBody params);

    //<-----------------------------Take Attendance-------------------------------->
    @POST("studentAttendanceabsentdata_v1")
    Call<TakeAttendanceResponseModel> takeAttendance(@Header("Content-Type")
                                                             String contentType,
                                                     @Header("Cache-Control")
                                                             String cache,
                                                     @Body RequestBody params);


    //<-----------------------------Get Approvel Api-------------------------------->
    @POST("selectachievements")
    Call<AchievementResponseModel> getAchievement(@Header("Content-Type")
                                                          String contentType,
                                                  @Header("Cache-Control")
                                                          String cache,
                                                  @Body RequestBody params);

    //<-----------------------------Get Approvel Api-------------------------------->
    @POST("gallerydata")
    Call<GalleryModel> getGalleryData(@Header("Content-Type")
                                              String contentType,
                                      @Header("Cache-Control")
                                              String cache,
                                      @Body RequestBody params);

    //<-----------------------------Get Approvel Api-------------------------------->
    @POST("gallerydetails")
    Call<GalleryDetailModel> getGalleryDetailData(@Header("Content-Type")
                                                          String contentType,
                                                  @Header("Cache-Control")
                                                          String cache,
                                                  @Body RequestBody params);


    //<-----------------------------Get Employee Approvel List Api-------------------------------->

    /*------------------------------Start-----------------------------------*/
    @POST("employeeleaveApproverList")
    Call<EmployeeApprovelListModel> getEmployeeApprovelList(@Header("Content-Type")
                                                                    String contentType,
                                                            @Header("Cache-Control")
                                                                    String cache,
                                                            @Body RequestBody params);

    @POST("studentleaveApproverList")
    Call<StudentApprovelListModel> getStudentApprovelList(@Header("Content-Type")
                                                                  String contentType,
                                                          @Header("Cache-Control")
                                                                  String cache,
                                                          @Body RequestBody params);

    /*------------------------------End-----------------------------------*/

    //<-----------------------------Get Employee Approvel List Api-------------------------------->

    /*------------------------------Start-----------------------------------*/
    @POST("employeeleaveApproverDetails")
    Call<EmployeeApprovelListModel> getEmployeeApprovelDetail(@Header("Content-Type")
                                                                      String contentType,
                                                              @Header("Cache-Control")
                                                                      String cache,
                                                              @Body RequestBody params);


    @POST("studentleaveApproverDeatils")
    Call<StudentApprovelListModel> getStudentApprovelDetail(@Header("Content-Type")
                                                                    String contentType,
                                                            @Header("Cache-Control")
                                                                    String cache,
                                                            @Body RequestBody params);

    /*------------------------------End-----------------------------------*/


    //<-----------------------------Get Employee Approvel List Api-------------------------------->

    /*------------------------------Start-----------------------------------*/
    @POST("employeeApprovedleave_v1")
    Call<EmployeeApprovedLeaveModel> employeeApprovedLeave(@Header("Content-Type")
                                                                   String contentType,
                                                           @Header("Cache-Control")
                                                                   String cache,
                                                           @Body RequestBody params);

    @POST("studentApprovedleave_v1")
    Call<EmployeeApprovedLeaveModel> studentApprovedLeave(@Header("Content-Type")
                                                                  String contentType,
                                                          @Header("Cache-Control")
                                                                  String cache,
                                                          @Body RequestBody params);

     /*------------------------------End-----------------------------------*/


    /*------------------------------Start-----------------------------------*/
    @POST("employeeleaveapprovedRejectList")
    Call<EmployeeApprovelListModel> employeeRejectLeave(@Header("Content-Type")
                                                                String contentType,
                                                        @Header("Cache-Control")
                                                                String cache,
                                                        @Body RequestBody params);

    @POST("studentleaveApproverRejectList")
    Call<StudentApprovelListModel> studentRejectLeave(@Header("Content-Type")
                                                              String contentType,
                                                      @Header("Cache-Control")
                                                              String cache,
                                                      @Body RequestBody params);

     /*------------------------------End-----------------------------------*/


    //<-----------------------------Forgot Pass Api-------------------------------->
    @POST("smsForgot")
    Call<ForgotPasswordModel> forgotPassword(@Header("Content-Type")
                                                     String contentType,
                                             @Header("Cache-Control")
                                                     String cache,
                                             @Body RequestBody params);

    @POST("getNotifications")
    Call<NotificationModel> getNotificationList(@Header("Content-Type")
                                                        String contentType,
                                                @Header("Cache-Control")
                                                        String cache,
                                                @Body RequestBody params);

    @POST("readNotification")
    Call<ResponseBody> readNotification(@Header("Content-Type")
                                                String contentType,
                                        @Header("Cache-Control")
                                                String cache,
                                        @Body RequestBody params);

}




