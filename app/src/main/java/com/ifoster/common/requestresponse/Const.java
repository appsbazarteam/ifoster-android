package com.ifoster.common.requestresponse;

/**
 * Created by Braintech on 9/12/2016.
 */
public class Const {


    //public static final String Base_URL = " http://ifoster.in/school_api/public/api/";
    public static final String Base_URL = "http://ifoster.in/iFosterDemo/api/public/api/";
    //public static final String Base_URL = "http://consultantforum.in/demo/demo-laravel/public/api/";


    public static final String APP_KEY = "appkey";
    public static final String APP_KEY_VALUE = "858cd14clifesaver14dejfkrf";

    public static final String KEY_NAME = "name";
    public static final String KEY_ID = "id";

    public static final String KEY_DEVICE_ID = "device_id";
    public static final String KEY_DEVICE_TOKEN = "device_token";
    public static final String KEY_DEVICE_TYPE = "device_type";
    public static final String KEY_DEVICE_TYPE_VALUE = "A";

    /*User Type*/
    public static final String KEY_PARENTS = "parents";
    public static final String KEY_SCHOOL = "school";

    //<-------------Params for user Login------------------------>
    public static final String PARAM_MOBILE_NO = "mobileNumber";
    public static final String PARAM_PASSWORD = "password";

    //<-------------Params for Reset Password------------------------>
    public static final String PARAM_TXT_PASSWORD = "txtPassword";

    //<-------------Params for Get Student List------------------------>
    public static final String PARAM_USER_TOKEN = "token";

    //<-------------Params for Get Task------------------------>
    public static final String PARAM_ID = "id";
    public static final String PARAM_STUDENT_ID = "student_id";
    public static final String PARAM_TEACHER_ID = "employee_id";
    public static final String PARAM_HOMWRORK_ID = "homework_id";

    //<-------------Params for GetStudentList------------------------>
    public static final String PARAM_LOGIN_USER_TYPE = "student_id";

    //<-------------Params for GetTeacherAttendace List------------------------>
    public static final String PARAM_EMPLOYEE_ID = "employee_id";

    //<-------------Params for GetClassStudent List------------------------>
    public static final String PARAM_CLASS_NAME = "stuclass";
    public static final String PARAM_SECTION = "section";

    //<-------------Params for GetNoticeBoard List------------------------>
    public static final String PARAM_NOTICE_DATE = "notice_date";
    public static final String PARAM_EVENT_DATE = "event_date";


    //<-------------Params for Change Mobile No------------------------>
    public static final String PARAM_OLD_MOBILE_NO = "oldmobile";
    public static final String PARAM_NEW_MOBILE_NO = "newmobile";

    //<-------------Params for Attendance Deytail------------------------>
    public static final String PARAM_MONTH = "month";
    public static final String PARAM_ATTENDANCE = "attedance";
    public static final String PARAM_HALF_DAY = "halfday";
    public static final String STUDENT_ID = "student_id";
    public static final String LOGED_DATA = "loged_data";

    //<-------------Params for Class Timeable------------------------>
    public static final String PARAM_DAY = "day";

    //<-------------Params for Apply Leave------------------------>
    public static final String PARAM_LEAVE_TYPE = "leave_type";
    public static final String PARAM_START_DATE = "start_date";
    public static final String PARAM_END_DATE = "end_date";
    public static final String PARAM_NO_OF_DAY = "no_ofdays";
    public static final String PARAM_REASON = "reason";
    public static final String PARAM_ATTACHMENT = "attchment";

    //<-------------Params for Update Medical Id------------------------>
    public static final String PARAM_HEIGHT = "height";
    public static final String PARAM_WEIGHT = "weight";
    public static final String PARAM_BLOOD_GROUP = "blood_group";
    public static final String PARAM_CONTACT = "contact";
    public static final String PARAM_LEFT_EYE = "eye_left";
    public static final String PARAM_RIGHT_EYE = "eye_right";
    public static final String PARAM_ALLERGY_TYPE = "allergy_type";
    public static final String PARAM_ALLERGY = "allergy";

    //<-------------Take Attendance------------------------>
    public static final String PARAM_ABSENT_STUDENYT_IDS = "absentstudent_id";
    public static final String PARAM_PRESENT_STUDENYT_IDS = "presentstudent_id";
    public static final String PARAM_ATTENDANCE_DATE = "attendnce_date";

    //<-------------Gallery------------------------>
    public static final String PARAM_CATEGORY_ID = "category_id";


    //<-------------Approvel------------------------>
    public static final String PARAM_LOGED_IN_USER_ID = "loged_userid";
    public static final String PARAM_LEAVE_ID = "leave_id";
    public static final String PARAM_LEAVE_STATUS = "leave_status";
    public static final String PARAM_COMMENT = "comments";


    //-------------Notification---------------
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_USER_TYPE = "user_type";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_NOTIFICATION_ID = "notification_id";

}

