package com.ifoster.common.push;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.ifoster.R;
import com.ifoster.common.session.FcmSession;
import com.ifoster.mvp.navigation.NavigationActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


/**
 * Created by Braintech on 8/8/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    int notificationId = 0;

    FcmSession fcmSession;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        try {

            //Displaying data in log
            //It is optional
            Log.e(TAG, "From: " + remoteMessage.getFrom());
            Log.e(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());

            fcmSession = new FcmSession(this);

            //Calling method to generate notification
            sendNotification(remoteMessage.getNotification().getBody());

            sendBroadcast();

        } catch (Exception ee) {
            ee.printStackTrace();

            Log.e(TAG, "Notification Receive From : " + remoteMessage.getFrom());
        }
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void sendNotification(String messageBody) {

        notificationId = fcmSession.getFcmCount();
        notificationId++;
        fcmSession.saveFcmCount(notificationId);

        Log.e("notification", "" + notificationId);
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, notificationId, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_app_icon))
                .setSmallIcon(R.mipmap.ic_app_icon)
                .setContentTitle("iFoster")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    public void sendBroadcast() {

       /* Intent intent = new Intent(ConstIntent.KEY_APPOINTMENT_ACCEPTED);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);*/

    }
}
