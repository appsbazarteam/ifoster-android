package com.ifoster.common.show_image;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ConstIntent;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowImageActivity extends Activity {

    @BindView(R.id.imageView)
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent.hasExtra(ConstIntent.KEY_IMAGE)) {
            Glide.with(this).load(intent.getStringExtra(ConstIntent.KEY_IMAGE))
                    .error(R.drawable.user)
                    .placeholder(R.drawable.user)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        }
    }
}
