package com.ifoster.common.session;

import android.content.SharedPreferences;

import com.ifoster.App;
import com.ifoster.mvp.login.LoginModel;
import com.google.gson.Gson;


/**
 * Created by Braintech on 7/22/2015.
 */
public class UserSession {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "user_pref";

    // All Shared Preferences Keys
    private static final String KEY_IS_USER_LOGGED_IN = "is_user_logged_in";
    private static final String KEY_TOKEN = "token";
    private static final String KEY_MOBILE_NO = "mobile_no";
    private static final String KEY_LOGIN_TYPE = "user_login_type";
    private static final String KEY_EMPLOYEE_ID = "employee_id";
    private static final String KEY_LOGIN_RESPONSE_RESULT = "login_response_result";
    private static final String KEY_EMPLOYEE_NAME = "employee_name";
    private static final String KEY_EMPLOYEE_IMAGE_PATH = "employee_imagepath";
    private static final String KEY_STUDENT_ID = "student_id";
    private static final String KEY_STUDENT_NAME = "student_name";
    private static final String KEY_STUDENT_CLASS= "student_class";
    private static final String KEY_STUDENT_SECTION = "student_section";
    private static final String KEY_EMPTY = "";

    private static UserSession userSession;

    public static UserSession getInstance(){
        if(userSession == null)
            userSession = new UserSession();
        return userSession;
    }

    private UserSession() {
        pref = App.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createUserSession(String mobileNo, String userToken, int loginType) {
        editor.putString(KEY_MOBILE_NO, mobileNo);
        editor.putString(KEY_TOKEN, userToken);
        editor.putBoolean(KEY_IS_USER_LOGGED_IN, true);
        editor.putInt(KEY_LOGIN_TYPE, loginType);
        editor.commit();
    }

    /**
     * Create login session
     */
    public void saveEmployeeDetails(int empId, String empName, String imagePath) {
        editor.putInt(KEY_EMPLOYEE_ID, empId);
        editor.putString(KEY_EMPLOYEE_NAME, empName);
        editor.putString(KEY_EMPLOYEE_IMAGE_PATH, imagePath);
        editor.commit();
    }

    public void saveStudentId(int studentId, String studentName,String schoolClass, String section) {
        editor.putInt(KEY_STUDENT_ID, studentId);
        editor.putString(KEY_STUDENT_NAME, studentName);
        editor.putString(KEY_STUDENT_CLASS,schoolClass);
        editor.putString(KEY_STUDENT_SECTION, section);
        editor.commit();
    }

    // Clearing all data from Shared
    public void clearUserSession() {
        editor.clear();
        editor.commit();
    }

    // Get Login State
    public boolean isUserLoggedIn() {
        return pref.getBoolean(KEY_IS_USER_LOGGED_IN, false);
    }

    // get user token
    public String getUserToken() {
        return pref.getString(KEY_TOKEN, KEY_EMPTY);
    }

    public int getEmployeeId() {
        return pref.getInt(KEY_EMPLOYEE_ID, 0);
    }

    // get mobile no
    public String getMobileNo() {
        return pref.getString(KEY_MOBILE_NO, KEY_EMPTY);
    }

    // get login type
    public int getLoginType() {
        return pref.getInt(KEY_LOGIN_TYPE, 0);
    }

    public int getStudentId() {
        return pref.getInt(KEY_STUDENT_ID, 0);
    }

    public String getStudentName() {
        return pref.getString(KEY_STUDENT_NAME, KEY_EMPTY);
    }

    public String getStudentSection() {
        return pref.getString(KEY_STUDENT_SECTION, KEY_EMPTY);
    }

    public String getStudentClass() {
        return pref.getString(KEY_STUDENT_CLASS, KEY_EMPTY);
    }

    public void saveLoginModelObject(LoginModel.Result result){
        Gson gson = new Gson();
        String json = gson.toJson(result);
        editor.putString(KEY_LOGIN_RESPONSE_RESULT, json);
        editor.commit();
    }

    public LoginModel.Result getLoginModelObject(){
        Gson gson = new Gson();
        String json = pref.getString(KEY_LOGIN_RESPONSE_RESULT, "");
        if(json.equalsIgnoreCase(""))
            return null;
        return gson.fromJson(json, LoginModel.Result.class);
    }
}
