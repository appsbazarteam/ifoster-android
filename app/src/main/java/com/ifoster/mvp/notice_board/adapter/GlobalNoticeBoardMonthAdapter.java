package com.ifoster.mvp.notice_board.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.adapter.model.MonthModel;
import com.ifoster.mvp.notice_board.fragment.GlobalNoticeFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class GlobalNoticeBoardMonthAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<MonthModel> listData;

    GlobalNoticeFragment globalNoticeFragment;

    public GlobalNoticeBoardMonthAdapter(Context context, ArrayList<MonthModel> listData, GlobalNoticeFragment circularAndNoticeFragment) {

        this.context = context;
        this.listData = listData;
        this.globalNoticeFragment = circularAndNoticeFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_circle_month, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ((ViewHolderList) holder).txtViewMonthTitle.setText(listData.get(position).getMonthName());


        if (listData.get(position).isSelected()) {

            ((ViewHolderList) holder).txtViewMonthTitle.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_small_circle_drawable));
            ((ViewHolderList) holder).txtViewMonthTitle.setTextColor(ContextCompat.getColor(context, R.color.color_white));

        } else {

            ((ViewHolderList) holder).txtViewMonthTitle.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_circle_gray_drawable));
            ((ViewHolderList) holder).txtViewMonthTitle.setTextColor(ContextCompat.getColor(context, R.color.color_black));
        }


        // view more click event
        ((ViewHolderList) holder).relLayMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listData.get(position).setSelected(true);

                for (int i = 0; i < listData.size(); i++) {

                    if (i != position) {
                        listData.get(i).setSelected(false);
                    }
                }

                notifyDataSetChanged();

                // call fragment method
                globalNoticeFragment.getData(listData.get(position).getMonthName());

            }
        });
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayMonth)
        RelativeLayout relLayMonth;

        @BindView(R.id.txtViewMonthTitle)
        TextView txtViewMonthTitle;

        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
