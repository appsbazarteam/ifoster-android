package com.ifoster.mvp.notice_board.view_more_text;

/**
 * Created by Braintech on 03-04-2017.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.ifoster.BuildConfig;
import com.ifoster.R;


public class ExpandableTextViewWith10Line extends AppCompatTextView {

    private int maxNoOfline = 10;
    private int minNoOfline = 1;
    private boolean isCollapsed = true;

    public ExpandableTextViewWith10Line(Context context) {
        this(context, null);
    }

    public ExpandableTextViewWith10Line(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ExpandableTextView);
        typedArray.recycle();

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCollapsed) {
                    expand(v);
                } else {
                    collapse(v);
                }
            }
        });
    }



    public void collapse(final View v) {
        isCollapsed = true;
        ((TextView) v).setSingleLine(true);
        ((TextView) v).setEllipsize(TextUtils.TruncateAt.END);
        ((TextView) v).setMaxLines(minNoOfline);
    }

    public void expand(final View v) {
        isCollapsed = false;
        ((TextView) v).setSingleLine(false);
        ((TextView) v).setEllipsize(TextUtils.TruncateAt.END);
        ((TextView) v).setMaxLines(maxNoOfline);
    }

    public boolean getIsCollapsed() {
        return isCollapsed;
    }

}