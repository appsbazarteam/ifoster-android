package com.ifoster.mvp.notice_board.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NoticeModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Result")
    @Expose
    private ArrayList<Result> result = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }


    public class Result {

        @SerializedName("notice_date")
        @Expose
        private String noticeDate;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("short_description")
        @Expose
        private String shortDescription;
        @SerializedName("long_description")
        @Expose
        private String longDescription;
        @SerializedName("attachment_text1")
        @Expose
        private String attachmentText;

        @SerializedName("attched_path")
        @Expose
        private String attchedPath;

        public int coloType;

        public int getColoType() {
            return coloType;
        }

        public void setColoType(int coloType) {
            this.coloType = coloType;
        }

        public String getNoticeDate() {
            return noticeDate;
        }

        public void setNoticeDate(String noticeDate) {
            this.noticeDate = noticeDate;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getShortDescription() {
            return shortDescription;
        }

        public void setShortDescription(String shortDescription) {
            this.shortDescription = shortDescription;
        }

        public String getLongDescription() {
            return longDescription;
        }

        public void setLongDescription(String longDescription) {
            this.longDescription = longDescription;
        }

        public String getAttachmentText() {
            return attachmentText;
        }

        public void setAttachmentText(String attachmentText) {
            this.attachmentText = attachmentText;
        }

        public String getAttchedPath() {
            return attchedPath;
        }

        public void setAttchedPath(String attchedPath) {
            this.attchedPath = attchedPath;
        }
    }
}

