package com.ifoster.mvp.notice_board.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.utility.Resource;
import com.ifoster.common.utility.Utils;
import com.ifoster.mvp.notice_board.model.NoticeModel;
import com.ifoster.mvp.notice_board.view.NoticeView;
import com.ifoster.mvp.notice_board.view_more_text.ExpandableTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class NoticeboardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<NoticeModel.Result> listData;
    private int type;
    private NoticeView noticeView;

    public NoticeboardAdapter(Context context, ArrayList<NoticeModel.Result> listData, int type, NoticeView noticeView) {
        this.context = context;
        this.listData = listData;
        this.type = type;
        this.noticeView = noticeView;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_notice_board, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        //((ViewHolderList) holder).txtViewMore.setTag(position);

        final NoticeModel.Result result = listData.get(position);
        if(result == null )
            return;

        ((ViewHolderList) holder).txtViewNoticeTitle.setText(result.getTitle());
        ((ViewHolderList) holder).txtViewNoticShortDesc.setText(result.getShortDescription());
        ((ViewHolderList) holder).txtViewNoticeDesc.setText(result.getLongDescription());

        if (result.getNoticeDate() != null && result.getNoticeDate().length() > 0) {

            String date = Utils.getDateFromDate(result.getNoticeDate());
            String month = Utils.getMonthFromDate(result.getNoticeDate());

            ((ViewHolderList) holder).txtViewCircleDate.setText(date);
            ((ViewHolderList) holder).txtViewCircleMonth.setText(month);
        }


        if (type == 0) {
            ((ViewHolderList) holder).cardViewPayNow.setVisibility(View.GONE);
        } else {
            ((ViewHolderList) holder).cardViewPayNow.setVisibility(View.VISIBLE);
        }



        // set color combination
        if (result.getColoType() == 0) {

            // set color on position zero
            ((NoticeboardAdapter.ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color1_text));

        } else if (result.getColoType() == 1) {

            // set color on position one
            ((NoticeboardAdapter.ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color2_text));

        } else if (result.getColoType() == 2) {

            // set color on position two
            ((NoticeboardAdapter.ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color3_text));

        } else if (result.getColoType() == 3) {

            // set color on position three
            ((NoticeboardAdapter.ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color4_text));
        }

        // view more click event
        ((ViewHolderList) holder).txtViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // int pos = (Integer) v.getTag();

                ((ViewHolderList) holder).txtViewNoticeDesc.performClick();

                if (((ViewHolderList) holder).txtViewNoticeDesc.getIsCollapsed()) {

                    ((ViewHolderList) holder).txtViewMore.setText("View More");

                } else {

                    ((ViewHolderList) holder).txtViewMore.setText("View Less");
                }
            }
        });


        showAttachment(result,(ViewHolderList) holder);
        // download attachment
        ((ViewHolderList) holder).txtViewAttach1Download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(result.getAttchedPath() != null && result.getAttchedPath().length()>0)
                    noticeView.onAttachmentDownload(result.getAttchedPath());
            }
        });

    }

    private void showAttachment(NoticeModel.Result result, ViewHolderList viewHolderList){
        if (result.getAttchedPath() != null && result.getAttchedPath().length() > 0) {
            viewHolderList.linLayAttach1.setVisibility(View.VISIBLE);
            if (result.getAttachmentText() != null && result.getAttachmentText().length() > 0)
                viewHolderList.txtViewAttach1Title.setText(result.getAttachmentText());
            else
                viewHolderList.txtViewAttach1Title.setText(Resource.toString(R.string.attachment1));
        } else {
            viewHolderList.linLayAttach1.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.cardViewRoundType)
        CardView cardViewRoundType;

        @BindView(R.id.txtViewCircleDate)
        TextView txtViewCircleDate;

        @BindView(R.id.txtViewCircleMonth)
        TextView txtViewCircleMonth;

        @BindView(R.id.cardViewPayNow)
        CardView cardViewPayNow;

        /*@BindView(R.id.txtViewType)
        TextView txtViewType;*/

        @BindView(R.id.txtViewNoticeTitle)
        TextView txtViewNoticeTitle;

        @BindView(R.id.txtViewNoticShortDesc)
        TextView txtViewNoticShortDesc;

        @BindView(R.id.txtViewNoticeDesc)
        ExpandableTextView txtViewNoticeDesc;

        @BindView(R.id.txtViewMore)
        TextView txtViewMore;

        @BindView(R.id.linLayAttach1)
        LinearLayout linLayAttach1;

        @BindView(R.id.txtViewAttach1Title)
        TextView txtViewAttach1Title;

        @BindView(R.id.txtViewAttach1Download)
        TextView txtViewAttach1Download;

        @BindView(R.id.txtViewAttachFileTitle)
        TextView txtViewAttachFileTitle;


        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            FontHelper.applyFont(context, txtViewAttachFileTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            txtViewAttachFileTitle.setPaintFlags(txtViewAttachFileTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }
    }


}
