package com.ifoster.mvp.notice_board;

/**
 * Created by Alam on 20-Oct-17.
 */

public class NoticeboardModel {

    public String type;
    public String shortDescription;
    public String longDescription;


    public int coloType;

    public int getColoType() {
        return coloType;
    }

    public void setColoType(int coloType) {
        this.coloType = coloType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }
}
