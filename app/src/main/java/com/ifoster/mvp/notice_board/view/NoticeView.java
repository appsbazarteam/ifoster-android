package com.ifoster.mvp.notice_board.view;

import com.ifoster.mvp.notice_board.model.EventListModel;
import com.ifoster.mvp.notice_board.model.NoticeModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface NoticeView {

    public void onSuccessNoticeList(ArrayList<NoticeModel.Result> message);

    public void onUnsuccessNoticeList(String message);

    public void onNoticeListInternetError();


    public void onSuccessEventList(ArrayList<EventListModel.Result> message);

    public void onUnsuccessEventList(String message);

    public void onAttachmentDownload(String path);

    public void onEventListInternetError();
}
