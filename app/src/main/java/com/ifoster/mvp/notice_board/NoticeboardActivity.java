package com.ifoster.mvp.notice_board;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ifoster.App;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.helpers.AlertDialogHelper;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.notice_board.fragment.ClassWiseNoticeFragment;
import com.ifoster.mvp.notice_board.fragment.GlobalNoticeFragment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class NoticeboardActivity extends BaseActivity {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @BindView(R.id.tv_title)
    TextView mTitle;

    @BindView(R.id.ll_global)
    LinearLayout mLayoutGlobal;

    private static final int DOWNLOAD_ATTACHMENT_REQ_CODE = 2909;
    private String path;
    private Dialog dialog;
    private ProgressBar pb;
    private TextView cur_val;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticeboard);

        //ButterKnife.bind(this);

        setToolBar();

        // create view pager
        createViewPager();

        // setup view pager
        tabLayout.setupWithViewPager(viewPager);

        setFont();

        // create tab icon
        createTabIcons();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }


    private void createTabIcons() {

        if(App.getApplicationInstance().isShowParentTab()){
            TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabOne.setText("Classwise Notice");
            tabLayout.getTabAt(0).setCustomView(tabOne);

            TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabTwo.setText("Global Notice");
            // tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_parent, 0, 0, 0);

            tabLayout.getTabAt(1).setCustomView(tabTwo);

            FontHelper.applyFont(this, tabOne, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(this, tabTwo, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        }else{


            TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabTwo.setText("Global Notice");
            // tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_parent, 0, 0, 0);

            tabLayout.getTabAt(0).setCustomView(tabTwo);

            FontHelper.applyFont(this, tabTwo, FontHelper.FontType.FONT_QUICKSAND_BOLD);

            mLayoutGlobal.setVisibility(View.VISIBLE);
            tabLayout.setVisibility(View.GONE);
            FontHelper.applyFont(this, mTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        }

    }

    private void createViewPager() {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        if(App.getApplicationInstance().isShowParentTab()){
            adapter.addFrag(new ClassWiseNoticeFragment(), "Classwise Notice");
            adapter.addFrag(new GlobalNoticeFragment(), "Global Notice");
        }else{
            adapter.addFrag(new GlobalNoticeFragment(), "Global Notice");
        }

        //adapter.addFrag(new EventAndProgramFragment(), "Global Notice");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @OnClick(R.id.imgViewBack)
    public void goBack() {
        finish();
    }

    private void setToolBar() {

        txtViewToolbarTitle.setText(getString(R.string.title_notice_board));
    }

    void showProgress() {
        dialog = new Dialog(NoticeboardActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.myprogressdialog);
        dialog.setTitle("Download Progress");

        TextView text = (TextView) dialog.findViewById(R.id.tv1);
        text.setText("Downloading file ... ");
        cur_val = (TextView) dialog.findViewById(R.id.cur_pg_tv);
        cur_val.setText("Starting download...");
        dialog.show();

        pb = (ProgressBar) dialog.findViewById(R.id.progress_bar);
        pb.setProgress(0);
        pb.setProgressDrawable(getResources().getDrawable(R.drawable.green_progress));
    }

    public void downloadAttachment(final String path){
        this.path = path;
        if (setRunTimePermission()) {
            showProgress();
            new Thread(new Runnable() {
                public void run() {
                    downloadFile(path);
                }
            }).start();
        } else {
            // request runtime permission
            if (!Settings.System.canWrite(this)) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, DOWNLOAD_ATTACHMENT_REQ_CODE);
            }
        }
    }


    private boolean setRunTimePermission() {

        int permissionCheckRead = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if ((permissionCheckRead == -1) || (permissionCheckWrite == -1)) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case DOWNLOAD_ATTACHMENT_REQ_CODE : {
                int permissionCheckRead = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE);

                int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);

                if ((permissionCheckRead == 0) && (permissionCheckWrite == 0)) {
                    downloadAttachment(path);
                } else {
                    AlertDialogHelper.alertEnableAppSetting(this);
                }
                return;
            }

        }
    }

    void downloadFile(String urlDown) {

        if (urlDown != null && urlDown.length() > 0) {

            int downloadedSize = 0;
            int totalSize = 0;

            try {
                URL url = new URL(urlDown);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);

                //connect
                urlConnection.connect();

                //set the path where we want to save the file
                File SDCardRoot = Environment.getExternalStorageDirectory();
                //create a new file, to save the downloaded file
                final File file = new File(SDCardRoot, "downloaded_file.png");

                FileOutputStream fileOutput = new FileOutputStream(file);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                totalSize = urlConnection.getContentLength();

                final int finalTotalSize = totalSize;
                runOnUiThread(new Runnable() {
                    public void run() {
                        pb.setMax(finalTotalSize);
                    }
                });

                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;

                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    fileOutput.write(buffer, 0, bufferLength);
                    downloadedSize += bufferLength;
                    // update the progressbar //
                    final int finalDownloadedSize = downloadedSize;
                    final int finalTotalSize1 = totalSize;
                    runOnUiThread(new Runnable() {
                        public void run() {
                            pb.setProgress(finalDownloadedSize);
                            float per = ((float) finalDownloadedSize / finalTotalSize1) * 100;
                            cur_val.setText("Downloaded " + finalDownloadedSize + "KB / " + finalTotalSize1 + "KB (" + (int) per + "%)");
                        }
                    });
                }
                //close the output stream when complete //
                fileOutput.close();
                runOnUiThread(new Runnable() {
                    public void run() {
                        //pb.dismiss(); // if you want close it..
                        dialog.dismiss();
                        openFileNew(file);
                    }
                });

            } catch (final MalformedURLException e) {
                showError("Error : MalformedURLException " + e);
                e.printStackTrace();
            } catch (final IOException e) {
                showError("Error : IOException " + e);
                e.printStackTrace();
            } catch (final Exception e) {
                showError("Error : Please check your internet connection " + e);
            }
        } else {
            SnackNotify.showMessage("File url not found.", coordinateLayout);
        }
    }

    private void openFileNew(File url) {

        MimeTypeMap map = MimeTypeMap.getSingleton();
        String ext = MimeTypeMap.getFileExtensionFromUrl(url.getName());
        String type = map.getMimeTypeFromExtension(ext);
        if (type == null)
            type = "*/*";

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // only for gingerbread and newer versions
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            Uri photoUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", url);
            intent.setDataAndType(photoUri, type);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        } else {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri data = Uri.fromFile(url);
            intent.setDataAndType(data, type);
            startActivity(intent);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //   mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    void showError(final String err) {
        runOnUiThread(new Runnable() {
            public void run() {
                SnackNotify.showMessage(err, coordinateLayout);
            }
        });
    }
}
