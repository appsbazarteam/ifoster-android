package com.ifoster.mvp.notice_board.presenter;

import android.app.Activity;
import android.util.Log;

import com.ifoster.App;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.notice_board.model.EventListModel;
import com.ifoster.mvp.notice_board.model.NoticeModel;
import com.ifoster.mvp.notice_board.view.NoticeView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class NoticePresenterImpl implements NoticePresenter {

    private Activity activity;
    private NoticeView noticeView;
    private JSONObject jsonObject;

    public NoticePresenterImpl(Activity activity, NoticeView noticeView) {
        this.activity = activity;
        this.noticeView = noticeView;
    }

    @Override
    public void getClassWiseNoticeList(String date, int userLogedInType) {
        try {
            ApiAdapter.getInstance(activity);
            callClassWiseNoticeListApi(date, userLogedInType);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            noticeView.onNoticeListInternetError();
        }
    }

    @Override
    public void getGlobalNoticeList(String date, int userLogedInType) {
        try {
            ApiAdapter.getInstance(activity);
            callGlobalNoticeListApi(date, userLogedInType);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            noticeView.onNoticeListInternetError();
        }
    }

    @Override
    public void getEventList(String date, int userLogedInType) {
        try {
            ApiAdapter.getInstance(activity);
            callEventListApi(date, userLogedInType);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            noticeView.onNoticeListInternetError();
        }
    }

    private void callClassWiseNoticeListApi(String date, int userLogedInType) {

        //Progress.start(activity);

        ///String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        // String deviceToken = new FcmSession(activity).getFcmToken();


        String token = UserSession.getInstance().getUserToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.STUDENT_ID, UserSession.getInstance().getStudentId()+"");
            jsonObject.put(Const.PARAM_MONTH, date);
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<NoticeModel> getLoginOutput = null;

        getLoginOutput = ApiAdapter.getApiService().getClasswiseNoticeList("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<NoticeModel>() {
            @Override
            public void onResponse(Call<NoticeModel> call, Response<NoticeModel> response) {
                try {

                    //getting whole data from response
                    NoticeModel noticeModel = response.body();
                    String message = noticeModel.getMessage();

                    if (noticeModel.getStatus()) {
                        noticeView.onSuccessNoticeList(noticeModel.getResult());
                    } else {
                        noticeView.onUnsuccessNoticeList(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    noticeView.onUnsuccessNoticeList(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<NoticeModel> call, Throwable t) {
                noticeView.onUnsuccessNoticeList(activity.getString(R.string.error_server));
            }
        });
    }

    private void callGlobalNoticeListApi(String date, int userLogedInType) {

        String token = UserSession.getInstance().getUserToken();
        String logedData = "";

        if(App.getApplicationInstance().isShowParentTab()){
            logedData = "1";
        }else{
            logedData = "2";
        }

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.LOGED_DATA, logedData);
            jsonObject.put(Const.PARAM_MONTH, date);
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<NoticeModel> getLoginOutput = null;

        getLoginOutput = ApiAdapter.getApiService().getGlobalNoticeList("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<NoticeModel>() {
            @Override
            public void onResponse(Call<NoticeModel> call, Response<NoticeModel> response) {
                try {

                    //getting whole data from response
                    NoticeModel noticeModel = response.body();
                    String message = noticeModel.getMessage();

                    if (noticeModel.getStatus()) {
                        noticeView.onSuccessNoticeList(noticeModel.getResult());
                    } else {
                        noticeView.onUnsuccessNoticeList(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    noticeView.onUnsuccessNoticeList(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<NoticeModel> call, Throwable t) {
                noticeView.onUnsuccessNoticeList(activity.getString(R.string.error_server));
            }
        });
    }


    private void callEventListApi(String date, int userLogedInType) {

        String token = UserSession.getInstance().getUserToken();

        Log.e("date", "--->" + date);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_MONTH, date);
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<EventListModel> getLoginOutput = null;

        if (userLogedInType == 1) {
            getLoginOutput = ApiAdapter.getApiService().getEventList("application/json", "no-cache", body);
        } else {
            getLoginOutput = ApiAdapter.getApiService().getEmpEventList("application/json", "no-cache", body);
        }

        getLoginOutput.enqueue(new Callback<EventListModel>() {
            @Override
            public void onResponse(Call<EventListModel> call, Response<EventListModel> response) {

                try {

                    //getting whole data from response
                    EventListModel eventListModel = response.body();
                    String message = eventListModel.getMessage();

                    if (eventListModel.getStatus()) {
                        noticeView.onSuccessEventList(eventListModel.getResult());
                    } else {
                        noticeView.onUnsuccessEventList(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    noticeView.onUnsuccessEventList(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<EventListModel> call, Throwable t) {
                noticeView.onUnsuccessEventList(activity.getString(R.string.error_server));
            }
        });
    }


}
