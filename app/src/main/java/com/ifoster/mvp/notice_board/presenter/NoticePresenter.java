package com.ifoster.mvp.notice_board.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface NoticePresenter {

    public void getClassWiseNoticeList(String date, int userLogedInType);

    public void getGlobalNoticeList(String date, int userLogedInType);

    public void getEventList(String date, int userLogedInType);
}
