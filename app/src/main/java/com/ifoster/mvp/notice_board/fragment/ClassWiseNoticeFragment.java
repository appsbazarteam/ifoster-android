package com.ifoster.mvp.notice_board.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ifoster.R;
import com.ifoster.common.adapter.model.MonthModel;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.notice_board.NoticeboardActivity;
import com.ifoster.mvp.notice_board.adapter.NoticeBoardMonthAdapter;
import com.ifoster.mvp.notice_board.adapter.NoticeboardAdapter;
import com.ifoster.mvp.notice_board.model.EventListModel;
import com.ifoster.mvp.notice_board.model.NoticeModel;
import com.ifoster.mvp.notice_board.presenter.NoticePresenterImpl;
import com.ifoster.mvp.notice_board.view.NoticeView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alam on 10-Oct-17.
 */

public class ClassWiseNoticeFragment extends Fragment implements NoticeView {


    @BindView(R.id.recyclerViewMonth)
    RecyclerView recyclerViewMonth;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.relLayMainContainer)
    RelativeLayout relLayMainContainer;

    /*---------Class Presebter--------*/
    NoticePresenterImpl noticePresenterImpl;


    String currentMonth;

    /*--------UserSession-----*/


    int scrollPosition;

    int userLogedInType = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_circular_and_notice, container, false);
        ButterKnife.bind(this, view);

        // Inflate the layout for getActivity() fragment
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        noticePresenterImpl = new NoticePresenterImpl(getActivity(), this);



        getIntentData();

        currentMonth = Utils.getCurrentMonth();

        getData(currentMonth);

        bindMonthToRecyclerView();

    }

    private void bindMonthToRecyclerView() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        recyclerViewMonth.setLayoutManager(linearLayoutManager);
        recyclerViewMonth.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMonth.setNestedScrollingEnabled(false);

        ArrayList<MonthModel> list = getMonthList();

        NoticeBoardMonthAdapter noticeboardAdapter = new NoticeBoardMonthAdapter(getActivity(), list, this);
        recyclerViewMonth.setAdapter(noticeboardAdapter);

        recyclerViewMonth.scrollToPosition(scrollPosition);
    }

    private void getIntentData() {

        Intent intent = getActivity().getIntent();

        if (intent.hasExtra(ConstIntent.KEY_USER_TYPE)) {

            userLogedInType = intent.getIntExtra(ConstIntent.KEY_USER_TYPE, 0);

            if (userLogedInType == 0) {
                userLogedInType = UserSession.getInstance().getLoginType();
            }
        } else {
            userLogedInType = UserSession.getInstance().getLoginType();
        }
    }


    public void getData(String date) {
        ((NoticeboardActivity)getActivity()).showProgressBar();
        noticePresenterImpl.getClassWiseNoticeList(date, userLogedInType);
    }

    @Override
    public void onSuccessNoticeList(ArrayList<NoticeModel.Result> results) {
        ((NoticeboardActivity)getActivity()).hideProgressBar();
        if (results != null && results.size() > 0) {

            int colorCodeType = 0;

            for (int i = 0; i < results.size(); i++) {
                results.get(i).setColoType(colorCodeType);

                // increase color clde value
                colorCodeType++;

                // reset color form strating again
                if (colorCodeType == 4) {
                    colorCodeType = 0;
                }
            }


        } else {

            results = new ArrayList<>();

            SnackNotify.showMessage("No record found.", relLayMainContainer);
        }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        NoticeboardAdapter noticeboardAdapter = new NoticeboardAdapter(getActivity(), results, 0,this);
        recyclerView.setAdapter(noticeboardAdapter);

    }

    @Override
    public void onUnsuccessNoticeList(String message) {
        ((NoticeboardActivity)getActivity()).hideProgressBar();
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    @Override
    public void onNoticeListInternetError() {
        ((NoticeboardActivity)getActivity()).hideProgressBar();
        SnackNotify.checkConnection(onRetryNotice, relLayMainContainer);
    }

    OnClickInterface onRetryNotice = new OnClickInterface() {
        @Override
        public void onClick() {
            getData(currentMonth);
        }
    };

    @Override
    public void onSuccessEventList(ArrayList<EventListModel.Result> message) {
        ((NoticeboardActivity)getActivity()).hideProgressBar();
        /*----------No Used----------*/
    }

    @Override
    public void onUnsuccessEventList(String message) {
        ((NoticeboardActivity)getActivity()).hideProgressBar();
        /*----------No Used----------*/
    }

    @Override
    public void onAttachmentDownload(String path) {
        NoticeboardActivity noticeboardActivity = (NoticeboardActivity)getActivity();
        if(noticeboardActivity != null)
            noticeboardActivity.downloadAttachment(path);
    }

    @Override
    public void onEventListInternetError() {
        ((NoticeboardActivity)getActivity()).hideProgressBar();
        /*----------No Used----------*/
    }

    private ArrayList<MonthModel> getMonthList() {


        ArrayList<MonthModel> monthList = new ArrayList<>();

        MonthModel monthModel1 = new MonthModel();
        monthModel1.setMonthName("Jan");

        MonthModel monthModel2 = new MonthModel();
        monthModel2.setMonthName("Feb");

        MonthModel monthModel3 = new MonthModel();
        monthModel3.setMonthName("Mar");

        MonthModel monthModel4 = new MonthModel();
        monthModel4.setMonthName("Apr");

        MonthModel monthModel5 = new MonthModel();
        monthModel5.setMonthName("May");

        MonthModel monthModel6 = new MonthModel();
        monthModel6.setMonthName("Jun");

        MonthModel monthModel7 = new MonthModel();
        monthModel7.setMonthName("Jul");

        MonthModel monthModel8 = new MonthModel();
        monthModel8.setMonthName("Aug");

        MonthModel monthModel9 = new MonthModel();
        monthModel9.setMonthName("Sep");

        MonthModel monthModel10 = new MonthModel();
        monthModel10.setMonthName("Oct");

        MonthModel monthModel11 = new MonthModel();
        monthModel11.setMonthName("Nov");

        MonthModel monthModel12 = new MonthModel();
        monthModel12.setMonthName("Dec");


        monthList.add(monthModel1);
        monthList.add(monthModel2);
        monthList.add(monthModel3);
        monthList.add(monthModel4);
        monthList.add(monthModel5);
        monthList.add(monthModel6);
        monthList.add(monthModel7);
        monthList.add(monthModel8);
        monthList.add(monthModel9);
        monthList.add(monthModel10);
        monthList.add(monthModel11);
        monthList.add(monthModel12);

        for (int i = 0; i < monthList.size(); i++) {

            if (currentMonth.equalsIgnoreCase(monthList.get(i).getMonthName())) {

                monthList.get(i).setSelected(true);

                scrollPosition = i;

                break;
            }
        }

        return monthList;
    }
}

