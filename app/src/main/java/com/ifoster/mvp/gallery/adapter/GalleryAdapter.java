package com.ifoster.mvp.gallery.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.mvp.gallery.activity.GalleryDetailActivity;
import com.ifoster.mvp.gallery.model.GalleryModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class GalleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<GalleryModel.Result> listData;

    public GalleryAdapter(Context context, ArrayList<GalleryModel.Result> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_gallery, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final GalleryModel.Result result = listData.get(position);

        Glide.with(context).load(result.getImgPath())
                .error(R.drawable.user)
                .placeholder(R.drawable.user)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(((ViewHolderList) holder).imgViewGallery);

        if (result.getTitle() != null) {
            ((ViewHolderList) holder).txtViewGalleryTitle.setText(result.getTitle());
        }

        if (result.getCategory() != null && result.getCategory().length() > 0) {
            ((ViewHolderList) holder).txtViewGalleryCategory.setText(result.getCategory());
        }

        ((ViewHolderList) holder).relLayMainContainer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, GalleryDetailActivity.class);
                intent.putExtra(ConstIntent.KEY_CATEGORY_ID, Integer.parseInt(result.getId()));
                intent.putExtra(ConstIntent.KEY_CATEGORY_NAME, result.getCategory());
                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayMainContainer)
        LinearLayout relLayMainContainer;

        @BindView(R.id.imgViewGallery)
        ImageView imgViewGallery;

        @BindView(R.id.txtViewGalleryTitle)
        TextView txtViewGalleryTitle;

        @BindView(R.id.txtViewGalleryCategory)
        TextView txtViewGalleryCategory;

        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
