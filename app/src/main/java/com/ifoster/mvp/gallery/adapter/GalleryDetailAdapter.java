package com.ifoster.mvp.gallery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.mvp.gallery.model.GalleryDetailModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class GalleryDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<GalleryDetailModel.Result> listData;

    public GalleryDetailAdapter(Context context, ArrayList<GalleryDetailModel.Result> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_gallery, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final GalleryDetailModel.Result result = listData.get(position);

        Glide.with(context).load(result.getImgPath())
                .error(R.drawable.user)
                .placeholder(R.drawable.user)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(((ViewHolderList) holder).imgViewGallery);


        ((ViewHolderList) holder).relLayMainContainer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


            }
        });

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayMainContainer)
        LinearLayout relLayMainContainer;

        @BindView(R.id.imgViewGallery)
        ImageView imgViewGallery;

        @BindView(R.id.txtViewGalleryTitle)
        TextView txtViewGalleryTitle;

        @BindView(R.id.txtViewGalleryCategory)
        TextView txtViewGalleryCategory;


        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            txtViewGalleryTitle.setVisibility(View.GONE);
            txtViewGalleryCategory.setVisibility(View.GONE);
        }
    }


}
