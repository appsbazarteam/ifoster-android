package com.ifoster.mvp.gallery.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.gallery.adapter.GalleryDetailAdapter;
import com.ifoster.mvp.gallery.model.GalleryDetailModel;
import com.ifoster.mvp.gallery.model.GalleryModel;
import com.ifoster.mvp.gallery.presenter.GalleryPresenterImpl;
import com.ifoster.mvp.gallery.view.GalleryView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GalleryDetailActivity extends BaseActivity implements GalleryView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    /*----------Class Presenter----------*/
    GalleryPresenterImpl galleryPresenterImpl;
    int categoryId = 0;
    private String categoryName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_detail);
        ButterKnife.bind(this);
        setToolbar();

        galleryPresenterImpl = new GalleryPresenterImpl(this, this);
        getIntentData();
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent.hasExtra(ConstIntent.KEY_CATEGORY_ID)) {
            categoryId = intent.getIntExtra(ConstIntent.KEY_CATEGORY_ID, 0);
            getGalleryData();
        }

        txtViewToolbarTitle.setText(getString(R.string.gallery));
        if (intent.hasExtra(ConstIntent.KEY_CATEGORY_NAME)) {
            categoryName = intent.getStringExtra(ConstIntent.KEY_CATEGORY_NAME);
            if(categoryName != null && !categoryName.equalsIgnoreCase("null") && categoryName.length()>0)
                txtViewToolbarTitle.setText(getString(R.string.gallery) +" - "+categoryName);

        }
    }

    private void setToolbar() {
        txtViewToolbarTitle.setText(getString(R.string.gallery));
    }

    @OnClick(R.id.imgViewBack)
    public void onBackClick() {
        finish();
    }

    @Override
    public void onSuccessGalleryList(ArrayList<GalleryModel.Result> results) {
        hideProgressBar();
    }

    @Override
    public void onUnsuccessGalleryList(String message) {
        hideProgressBar();
    }

    @Override
    public void onGalleryListInternetError() {
        hideProgressBar();
    }

    @Override
    public void onSuccessGalleryDetail(ArrayList<GalleryDetailModel.Result> results) {
        hideProgressBar();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        if (results != null && results.size() > 0) {
            GalleryDetailAdapter galleryAdapter = new GalleryDetailAdapter(this, results);
            recyclerView.setAdapter(galleryAdapter);
        } else {
            SnackNotify.showMessage(getString(R.string.no_record_found), coordinateLayout);
        }
    }

    @Override
    public void onUnsuccessGalleryDetail(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onGalleryDetailInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryGallery, coordinateLayout);
    }

    OnClickInterface onRetryGallery = new OnClickInterface() {
        @Override
        public void onClick() {
            getGalleryData();
        }
    };

    private void getGalleryData() {
        showProgressBar();
        galleryPresenterImpl.getGalleryDetail(categoryId);
    }
}
