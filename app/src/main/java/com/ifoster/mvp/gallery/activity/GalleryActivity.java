package com.ifoster.mvp.gallery.activity;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.gallery.adapter.GalleryAdapter;
import com.ifoster.mvp.gallery.model.GalleryDetailModel;
import com.ifoster.mvp.gallery.model.GalleryModel;
import com.ifoster.mvp.gallery.presenter.GalleryPresenterImpl;
import com.ifoster.mvp.gallery.view.GalleryView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class GalleryActivity extends BaseActivity implements GalleryView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    /*----------Class Presenter----------*/
    GalleryPresenterImpl galleryPresenterImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        setToolbar();

        galleryPresenterImpl = new GalleryPresenterImpl(this, this);
        getGalleryData();
    }

    @OnClick(R.id.imgViewBack)
    public void onBackClick() {
        finish();
    }

    private void setToolbar() {
        txtViewToolbarTitle.setText(getString(R.string.gallery));
    }

    @Override
    public void onSuccessGalleryList(ArrayList<GalleryModel.Result> results) {
        hideProgressBar();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        if (results != null && results.size() > 0) {

            GalleryAdapter galleryAdapter = new GalleryAdapter(this, results);
            recyclerView.setAdapter(galleryAdapter);

        } else {
            SnackNotify.showMessage(getString(R.string.no_record_found), coordinateLayout);
        }
    }

    @Override
    public void onUnsuccessGalleryList(String message) {
         hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onGalleryListInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryGallery, coordinateLayout);
    }

    OnClickInterface onRetryGallery = new OnClickInterface() {
        @Override
        public void onClick() {
            getGalleryData();
        }
    };

    private void getGalleryData() {
        showProgressBar();
        galleryPresenterImpl.getGalleryData();
    }

    @Override
    public void onSuccessGalleryDetail(ArrayList<GalleryDetailModel.Result> results) {
        hideProgressBar();
        /*-------------No Use-------------*/
    }

    @Override
    public void onUnsuccessGalleryDetail(String message) {
        hideProgressBar();
        /*-------------No Use-------------*/
    }

    @Override
    public void onGalleryDetailInternetError() {
        hideProgressBar();
        /*-------------No Use-------------*/
    }
}