package com.ifoster.mvp.gallery.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface GalleryPresenter {

    public void getGalleryData();

    public void getGalleryDetail(int categoryId);
}
