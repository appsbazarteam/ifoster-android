package com.ifoster.mvp.gallery.presenter;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.mvp.gallery.model.GalleryDetailModel;
import com.ifoster.mvp.gallery.model.GalleryModel;
import com.ifoster.mvp.gallery.view.GalleryView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class GalleryPresenterImpl implements GalleryPresenter {

    Activity activity;
    GalleryView galleryView;
    JSONObject jsonObject;

    public GalleryPresenterImpl(Activity activity, GalleryView galleryView) {
        this.activity = activity;
        this.galleryView = galleryView;
    }

    @Override
    public void getGalleryData() {

        try {
            ApiAdapter.getInstance(activity);
            callGalleryApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            galleryView.onGalleryListInternetError();
        }
    }

    @Override
    public void getGalleryDetail(int categoryId) {

        try {
            ApiAdapter.getInstance(activity);
            callGalleryDetailApi(categoryId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            galleryView.onGalleryDetailInternetError();
        }
    }


    private void callGalleryApi() {

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), "");

        Call<GalleryModel> getLoginOutput = ApiAdapter.getApiService().getGalleryData("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<GalleryModel>() {
            @Override
            public void onResponse(Call<GalleryModel> call, Response<GalleryModel> response) {

                try {
                    //getting whole data from response
                    GalleryModel holiDayCalenderModel = response.body();
                    String message = holiDayCalenderModel.getMessage();

                    if (holiDayCalenderModel.getStatus()) {
                        galleryView.onSuccessGalleryList(holiDayCalenderModel.getResult());
                    } else {
                        galleryView.onUnsuccessGalleryList(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    galleryView.onUnsuccessGalleryList(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<GalleryModel> call, Throwable t) {
                galleryView.onUnsuccessGalleryList(activity.getString(R.string.error_server));
            }
        });
    }


    private void callGalleryDetailApi(int categoryId) {

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CATEGORY_ID, categoryId);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

        Call<GalleryDetailModel> getLoginOutput = ApiAdapter.getApiService().getGalleryDetailData("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<GalleryDetailModel>() {
            @Override
            public void onResponse(Call<GalleryDetailModel> call, Response<GalleryDetailModel> response) {

                try {
                    //getting whole data from response
                    GalleryDetailModel holiDayCalenderModel = response.body();
                    String message = holiDayCalenderModel.getMessage();

                    if (holiDayCalenderModel.getStatus()) {
                        galleryView.onSuccessGalleryDetail(holiDayCalenderModel.getResult());
                    } else {
                        galleryView.onUnsuccessGalleryDetail(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    galleryView.onUnsuccessGalleryDetail(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<GalleryDetailModel> call, Throwable t) {
                galleryView.onUnsuccessGalleryDetail(activity.getString(R.string.error_server));
            }
        });
    }


}
