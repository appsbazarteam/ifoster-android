package com.ifoster.mvp.gallery.view;

import com.ifoster.mvp.gallery.model.GalleryDetailModel;
import com.ifoster.mvp.gallery.model.GalleryModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface GalleryView {

    // Gallery
    public void onSuccessGalleryList(ArrayList<GalleryModel.Result> results);

    public void onUnsuccessGalleryList(String message);

    public void onGalleryListInternetError();


    // Gallery detail
    public void onSuccessGalleryDetail(ArrayList<GalleryDetailModel.Result> results);

    public void onUnsuccessGalleryDetail(String message);

    public void onGalleryDetailInternetError();
}
