package com.ifoster.mvp.attendance;

import java.util.ArrayList;

/**
 * Created by Alam on 03-Oct-17.
 */

public class MonthModel {

    public String monthName;
    public String getTotalAbsent;
    public String getTotalPresent;
    public String getTotalLeave;

    public ArrayList<String> absentList;
    public ArrayList<String> presentList;
    public ArrayList<String> leaveList;

    public ArrayList<String> absentHalfDayList;
    public ArrayList<String> presentHalfDayList;
    public ArrayList<String> leaveHalfDayList;

    boolean isMonthExpand;
    boolean isPresentExpand;
    boolean isAbsentExpand;
    boolean isLeaveExpand;

    boolean isPresentAvailable;
    boolean isAbsentAvailable;
    boolean isLeaveAvailable;

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public String getGetTotalAbsent() {
        return getTotalAbsent;
    }

    public void setGetTotalAbsent(String getTotalAbsent) {
        this.getTotalAbsent = getTotalAbsent;
    }

    public String getGetTotalPresent() {
        return getTotalPresent;
    }

    public void setGetTotalPresent(String getTotalPresent) {
        this.getTotalPresent = getTotalPresent;
    }

    public String getGetTotalLeave() {
        return getTotalLeave;
    }

    public void setGetTotalLeave(String getTotalLeave) {
        this.getTotalLeave = getTotalLeave;
    }

    public ArrayList<String> getAbsentList() {
        return absentList;
    }

    public void setAbsentList(ArrayList<String> absentList) {
        this.absentList = absentList;
    }

    public ArrayList<String> getPresentList() {
        return presentList;
    }

    public void setPresentList(ArrayList<String> presentList) {
        this.presentList = presentList;
    }

    public ArrayList<String> getLeaveList() {
        return leaveList;
    }

    public void setLeaveList(ArrayList<String> leaveList) {
        this.leaveList = leaveList;
    }

    public ArrayList<String> getAbsentHalfDayList() {
        return absentHalfDayList;
    }

    public void setAbsentHalfDayList(ArrayList<String> absentHalfDayList) {
        this.absentHalfDayList = absentHalfDayList;
    }

    public ArrayList<String> getPresentHalfDayList() {
        return presentHalfDayList;
    }

    public void setPresentHalfDayList(ArrayList<String> presentHalfDayList) {
        this.presentHalfDayList = presentHalfDayList;
    }

    public ArrayList<String> getLeaveHalfDayList() {
        return leaveHalfDayList;
    }

    public void setLeaveHalfDayList(ArrayList<String> leaveHalfDayList) {
        this.leaveHalfDayList = leaveHalfDayList;
    }

    public boolean isMonthExpand() {
        return isMonthExpand;
    }

    public void setMonthExpand(boolean monthExpand) {
        isMonthExpand = monthExpand;
    }

    public boolean isPresentExpand() {
        return isPresentExpand;
    }

    public void setPresentExpand(boolean presentExpand) {
        isPresentExpand = presentExpand;
    }

    public boolean isAbsentExpand() {
        return isAbsentExpand;
    }

    public void setAbsentExpand(boolean absentExpand) {
        isAbsentExpand = absentExpand;
    }

    public boolean isLeaveExpand() {
        return isLeaveExpand;
    }

    public void setLeaveExpand(boolean leaveExpand) {
        isLeaveExpand = leaveExpand;
    }

    public boolean isPresentAvailable() {
        return isPresentAvailable;
    }

    public void setPresentAvailable(boolean presentAvailable) {
        isPresentAvailable = presentAvailable;
    }

    public boolean isAbsentAvailable() {
        return isAbsentAvailable;
    }

    public void setAbsentAvailable(boolean absentAvailable) {
        isAbsentAvailable = absentAvailable;
    }

    public boolean isLeaveAvailable() {
        return isLeaveAvailable;
    }

    public void setLeaveAvailable(boolean leaveAvailable) {
        isLeaveAvailable = leaveAvailable;
    }
}
