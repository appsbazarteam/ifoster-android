package com.ifoster.mvp.attendance.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.custom_gridview.CustomGridView;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.mvp.attendance.AttendanceDetailActivity;
import com.ifoster.mvp.attendance.MonthModel;
import com.ifoster.mvp.attendance.model.AttendanceDetailModel;
import com.ifoster.mvp.attendance.presenter.AttendanceDetailPresenterImpl;
import com.ifoster.mvp.attendance.view.AttendanceDetailView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class AttendanceDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AttendanceDetailView {
    private Context context;
    private ArrayList<MonthModel> listData;
    private int pos = 0;
    private int attendanceType;
    private AttendanceDetailPresenterImpl attendanceDetailPresenter;

    public AttendanceDetailAdapter(Context context, ArrayList<MonthModel> listData) {
        this.context = context;
        this.listData = listData;
        attendanceDetailPresenter = new AttendanceDetailPresenterImpl(context, this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_month_list, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final MonthModel monthModel = listData.get(position);

        if(monthModel == null)
            return;
        
        ((ViewHolderList) holder).txtViewMonthName.setText(monthModel.getMonthName());
        ((ViewHolderList) holder).txtViewTotalAbsent.setText(monthModel.getGetTotalAbsent());
        ((ViewHolderList) holder).txtViewTotalPresent.setText(monthModel.getGetTotalPresent());
        ((ViewHolderList) holder).txtViewTotalLeave.setText(monthModel.getGetTotalLeave());

        // hide/show month list
        if (monthModel.isMonthExpand()) {
            ((ViewHolderList) holder).imgViewPlus.setImageResource(R.mipmap.ic_minus);
            ((ViewHolderList) holder).llAbsentPresent.setVisibility(View.VISIBLE);
        } else {
            ((ViewHolderList) holder).llAbsentPresent.setVisibility(View.GONE);
            ((ViewHolderList) holder).imgViewPlus.setImageResource(R.mipmap.ic_plus);
        }

        // hide/show absent list
        if (monthModel.isAbsentExpand()) {
            ((ViewHolderList) holder).imgViewAbsentPlus.setImageResource(R.mipmap.ic_minus);
            ((ViewHolderList) holder).llAbsentContainer.setVisibility(View.VISIBLE);
        } else {
            ((ViewHolderList) holder).imgViewAbsentPlus.setImageResource(R.mipmap.ic_plus);
            ((ViewHolderList) holder).llAbsentContainer.setVisibility(View.GONE);
        }

        // hide/show present list
        if (monthModel.isPresentExpand()) {
            ((ViewHolderList) holder).imgViewPresentPlus.setImageResource(R.mipmap.ic_minus);
            ((ViewHolderList) holder).llPresentContainer.setVisibility(View.VISIBLE);
        } else {
            ((ViewHolderList) holder).llPresentContainer.setVisibility(View.GONE);
            ((ViewHolderList) holder).imgViewPresentPlus.setImageResource(R.mipmap.ic_plus);
        }

        // hide/show leave list
        if (monthModel.isLeaveExpand()) {
            ((ViewHolderList) holder).imgViewLeavePlus.setImageResource(R.mipmap.ic_minus);
            ((ViewHolderList) holder).llLeaveContainer.setVisibility(View.VISIBLE);
        } else {
            ((ViewHolderList) holder).llLeaveContainer.setVisibility(View.GONE);
            ((ViewHolderList) holder).imgViewLeavePlus.setImageResource(R.mipmap.ic_plus);
        }


        // add absent child
        if (monthModel.getAbsentList().size() > 0) {

            String[] stockArr = new String[monthModel.getAbsentList().size()];
            stockArr = monthModel.getAbsentList().toArray(stockArr);

            GridViewAdapter gridViewAdapter = new GridViewAdapter(((AttendanceDetailActivity) context), stockArr);
            ((ViewHolderList) holder).gridViewAbsent.setAdapter(gridViewAdapter);
        }

        // add present child
        if (monthModel.getPresentList().size() > 0) {

            String[] stockArr = new String[monthModel.getPresentList().size()];
            stockArr = monthModel.getPresentList().toArray(stockArr);

            GridViewAdapter gridViewAdapter = new GridViewAdapter(((AttendanceDetailActivity) context), stockArr);
            ((ViewHolderList) holder).gridViewPresent.setAdapter(gridViewAdapter);
        }

        // add leave child
        if (monthModel.getLeaveList().size() > 0) {

            String[] stockArr = new String[monthModel.getLeaveList().size()];
            stockArr = monthModel.getLeaveList().toArray(stockArr);

            GridViewAdapter gridViewAdapter = new GridViewAdapter(((AttendanceDetailActivity) context), stockArr);
            ((ViewHolderList) holder).gridViewLeave.setAdapter(gridViewAdapter);
        }

        // hide/show text
        if (monthModel.isAbsentAvailable())
            ((ViewHolderList) holder).txtViewAbsentNoFound.setVisibility(View.GONE);
        else
            ((ViewHolderList) holder).txtViewAbsentNoFound.setVisibility(View.VISIBLE);

        if (monthModel.isPresentAvailable())
            ((ViewHolderList) holder).txtViewPresentNoFound.setVisibility(View.GONE);
        else
            ((ViewHolderList) holder).txtViewPresentNoFound.setVisibility(View.VISIBLE);

        if (monthModel.isLeaveAvailable())
            ((ViewHolderList) holder).txtViewLeaveNoFound.setVisibility(View.GONE);
        else
            ((ViewHolderList) holder).txtViewLeaveNoFound.setVisibility(View.VISIBLE);

        // parent item click
        ((ViewHolderList) holder).cardViewMonthName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String monthName = monthModel.getMonthName();
                pos = position;
                boolean isCallAPI;

                if (monthModel.isMonthExpand()) {
                    monthModel.setMonthExpand(false);

                    for (int i = 0; i < listData.size(); i++) {
                        listData.get(i).setMonthExpand(false);
                        listData.get(i).setAbsentExpand(false);
                        listData.get(i).setPresentExpand(false);
                        listData.get(i).setLeaveExpand(false);
                    }
                    isCallAPI = false;

                } else {
                    monthModel.setMonthExpand(true);

                    for (int i = 0; i < listData.size(); i++) {
                        if (position != i) {
                            ((ViewHolderList) holder).imgViewPlus.setImageResource(R.mipmap.ic_plus);
                            listData.get(i).setMonthExpand(false);
                            listData.get(i).setAbsentExpand(false);
                            listData.get(i).setPresentExpand(false);
                            listData.get(i).setLeaveExpand(false);
                        }
                    }

                    isCallAPI = true;
                }

                notifyDataSetChanged();

                if (isCallAPI) {
                    callAttendenceDetailApi(monthName);
                }
            }
        });

        // absent container click
        ((ViewHolderList) holder).relLayAbsent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String monthName = monthModel.getMonthName();
                boolean isCallAPI;

                if (monthModel.isAbsentExpand()) {
                    isCallAPI = false;
                    monthModel.setAbsentExpand(false);
                    monthModel.setPresentExpand(false);
                    monthModel.setLeaveExpand(false);

                    for (int i = 0; i < listData.size(); i++) {
                        listData.get(i).setAbsentExpand(false);
                        listData.get(i).setPresentExpand(false);
                        listData.get(i).setLeaveExpand(false);
                    }

                } else {
                    isCallAPI = true;
                    monthModel.setAbsentExpand(true);
                    monthModel.setPresentExpand(false);
                    monthModel.setLeaveExpand(false);

                    for (int i = 0; i < listData.size(); i++) {
                        if (position != i) {
                            listData.get(i).setAbsentExpand(false);
                            listData.get(i).setPresentExpand(false);
                            listData.get(i).setLeaveExpand(false);
                        }
                    }
                }

                notifyDataSetChanged();

                if (isCallAPI) {
                    callMonthDetailApi(monthName, 1);
                }
            }
        });

        // present container click
        ((ViewHolderList) holder).relLayPresent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String monthName = monthModel.getMonthName();
                boolean isCallAPI;

                if (monthModel.isPresentExpand()) {
                    isCallAPI = false;
                    monthModel.setPresentExpand(false);
                    monthModel.setAbsentExpand(false);
                    monthModel.setLeaveExpand(false);

                    for (int i = 0; i < listData.size(); i++) {
                        listData.get(i).setPresentExpand(false);
                        listData.get(i).setAbsentExpand(false);
                        listData.get(i).setLeaveExpand(false);
                    }

                } else {
                    isCallAPI = true;
                    monthModel.setPresentExpand(true);
                    monthModel.setAbsentExpand(false);
                    monthModel.setLeaveExpand(false);

                    for (int i = 0; i < listData.size(); i++) {
                        if (position != i) {
                            listData.get(i).setPresentExpand(false);
                            listData.get(i).setAbsentExpand(false);
                            listData.get(i).setLeaveExpand(false);
                        }
                    }
                }

                notifyDataSetChanged();

                if(isCallAPI){
                    callMonthDetailApi(monthName, 0);
                }
            }
        });

        // leave container click
        ((ViewHolderList) holder).relLayLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String monthName = monthModel.getMonthName();
                boolean isCallAPI;

                if (monthModel.isLeaveExpand()) {
                    isCallAPI = false;
                    monthModel.setLeaveExpand(false);
                    monthModel.setAbsentExpand(false);
                    monthModel.setPresentExpand(false);

                    for (int i = 0; i < listData.size(); i++) {
                        listData.get(i).setLeaveExpand(false);
                        listData.get(i).setAbsentExpand(false);
                        listData.get(i).setPresentExpand(false);
                    }

                } else {
                    isCallAPI = true;
                    monthModel.setLeaveExpand(true);
                    monthModel.setAbsentExpand(false);
                    monthModel.setPresentExpand(false);

                    for (int i = 0; i < listData.size(); i++) {
                        if (position != i) {
                            listData.get(i).setLeaveExpand(false);
                            listData.get(i).setAbsentExpand(false);
                            listData.get(i).setPresentExpand(false);
                        }
                    }
                }

                notifyDataSetChanged();

                if(isCallAPI){
                    callMonthDetailApi(monthName, 2);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    private void callAttendenceDetailApi(String monthName) {

        attendanceDetailPresenter.getAttendanceDetail1(monthName);
    }

    private void callMonthDetailApi(String monthName, int attendanceType) {

        this.attendanceType = attendanceType;
        attendanceDetailPresenter.getAttendanceDetail2(monthName, attendanceType);
    }

    @Override
    public void onSuccessAttendanceDetail(AttendanceDetailModel attendanceDetailModel) {
        if (attendanceDetailModel != null) {
            listData.get(pos).setGetTotalAbsent(String.valueOf(attendanceDetailModel.getAbsentcount()));
            listData.get(pos).setGetTotalPresent(String.valueOf(attendanceDetailModel.getPresentcount()));
            listData.get(pos).setGetTotalLeave(attendanceDetailModel.getLeave());
            notifyDataSetChanged();
        }
    }

    @Override
    public void onUnsuccessAttendanceDetail(String message) {

    }

    @Override
    public void onAttendanceDetailInternetError() {

    }

    @Override
    public void onSuccessMonthDetail(String data) {

        String[] dateArray;
        if (data != null) {

            if (data.contains(",")) {
                dateArray = data.split(",");
            } else {
                dateArray = new String[]{data};
            }

            if (dateArray.length > 0) {

                // remove first item from array
                int n = dateArray.length - 1;
                String[] newArray = new String[n];
                System.arraycopy(dateArray, 1, newArray, 0, n);

                // after removing date convert to list
                List<String> list = Arrays.asList(newArray);

                // now convert list to ArrayList
                ArrayList<String> finalArrayList = new ArrayList<String>(list);

                if (attendanceType == 0) { // Present
                    listData.get(pos).setPresentList(finalArrayList);

                    if (finalArrayList.size() > 0) {
                        listData.get(pos).setPresentAvailable(true);
                    } else {
                        listData.get(pos).setPresentAvailable(false);
                    }

                } else if (attendanceType == 1) { // Absent
                    listData.get(pos).setAbsentList(finalArrayList);

                    if (finalArrayList.size() > 0) {
                        listData.get(pos).setAbsentAvailable(true);
                    } else {
                        listData.get(pos).setAbsentAvailable(false);
                    }

                } else if (attendanceType == 2) { // Leave
                    listData.get(pos).setLeaveList(finalArrayList);

                    if (finalArrayList.size() > 0) {
                        listData.get(pos).setLeaveAvailable(true);
                    } else {
                        listData.get(pos).setLeaveAvailable(false);
                    }
                }

                notifyDataSetChanged();
            }
        }

    }

    @Override
    public void onUnsuccessMonthDetail(String message) {
        //SnackNotify.showMessage(message, App.getApplicationInstance().);
    }

    @Override
    public void onMonthDetailInternetError() {

    }

    @Override
    public void onMonthSelect(String month) {

    }

    @Override
    public void onSuccessAttendancePresent(String data) {

    }

    @Override
    public void onUnSuccessAttendancePresent(String data) {

    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        // parent view
        @BindView(R.id.cardViewMonthName)
        CardView cardViewMonthName;

        @BindView(R.id.txtViewMonthName)
        TextView txtViewMonthName;

        @BindView(R.id.imgViewPlus)
        ImageView imgViewPlus;

        @BindView(R.id.imgViewAbsentPlus)
        ImageView imgViewAbsentPlus;

        @BindView(R.id.imgViewPresentPlus)
        ImageView imgViewPresentPlus;

        @BindView(R.id.imgViewLeavePlus)
        ImageView imgViewLeavePlus;

        // child view
        @BindView(R.id.llAbsentPresent)
        LinearLayout llAbsentPresent;

        @BindView(R.id.relLayAbsent)
        RelativeLayout relLayAbsent;

        @BindView(R.id.relLayPresent)
        RelativeLayout relLayPresent;

        @BindView(R.id.relLayLeave)
        RelativeLayout relLayLeave;

        @BindView(R.id.txtViewTotalAbsent)
        TextView txtViewTotalAbsent;

        @BindView(R.id.txtViewTotalPresent)
        TextView txtViewTotalPresent;

        @BindView(R.id.txtViewTotalLeave)
        TextView txtViewTotalLeave;

        /*Absent Left Container*/
        @BindView(R.id.llAbsentContainer)
        LinearLayout llAbsentContainer;

        @BindView(R.id.gridViewAbsent)
        CustomGridView gridViewAbsent;

        /*Present Left Container*/
        @BindView(R.id.llPresentContainer)
        LinearLayout llPresentContainer;

        @BindView(R.id.gridViewPresent)
        CustomGridView gridViewPresent;

        /*Leave Left Container*/
        @BindView(R.id.llLeaveContainer)
        LinearLayout llLeaveContainer;

        @BindView(R.id.gridViewLeave)
        CustomGridView gridViewLeave;

        @BindView(R.id.txtAbsent)
        TextView txtAbsent;

        @BindView(R.id.txtPresent)
        TextView txtPresent;

        @BindView(R.id.txtLeave)
        TextView txtLeave;

        @BindView(R.id.txtViewLeaveDateAre)
        TextView txtViewLeaveDateAre;

        @BindView(R.id.txtViewPresentDateAre)
        TextView txtViewPresentDateAre;

        @BindView(R.id.txtViewAbsentDAteAre)
        TextView txtViewAbsentDAteAre;

        @BindView(R.id.txtViewLeaveNoFound)
        TextView txtViewLeaveNoFound;

        @BindView(R.id.txtViewPresentNoFound)
        TextView txtViewPresentNoFound;

        @BindView(R.id.txtViewAbsentNoFound)
        TextView txtViewAbsentNoFound;


        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            FontHelper.applyFont(context, txtViewMonthName,FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewTotalAbsent,FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewTotalPresent,FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewTotalLeave,FontHelper.FontType.FONT_QUICKSAND_BOLD);

            FontHelper.applyFont(context, txtAbsent,FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtPresent,FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtLeave,FontHelper.FontType.FONT_QUICKSAND_BOLD);

            FontHelper.applyFont(context, txtViewAbsentDAteAre,FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewPresentDateAre,FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewLeaveDateAre,FontHelper.FontType.FONT_QUICKSAND_BOLD);

            FontHelper.applyFont(context, txtViewLeaveNoFound,FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewPresentNoFound,FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewAbsentNoFound,FontHelper.FontType.FONT_QUICKSAND_BOLD);
        }
    }


}
