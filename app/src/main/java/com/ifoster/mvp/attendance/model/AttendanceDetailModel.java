package com.ifoster.mvp.attendance.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttendanceDetailModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("MonthRange")
    @Expose
    private String monthRange;
    @SerializedName("absentcount")
    @Expose
    private int absentcount;
    @SerializedName("presentcount")
    @Expose
    private int presentcount;
    @SerializedName("Leave")
    @Expose
    private String leave;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMonthRange() {
        return monthRange;
    }

    public int getAbsentcount() {
        return absentcount;
    }

    public int getPresentcount() {
        return presentcount;
    }

    public String getLeave() {
        return leave;
    }

    public void setLeave(String leave) {
        this.leave = leave;
    }

}