package com.ifoster.mvp.attendance.presenter;

import android.content.Context;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.attendance.model.AttendanceDetailModel;
import com.ifoster.mvp.attendance.model.MonthDetailModel;
import com.ifoster.mvp.attendance.model.PresentModel;
import com.ifoster.mvp.attendance.view.AttendanceDetailView;
import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class AttendanceDetailPresenterImpl implements AttendanceDetailPresenter {

    private Context activity;
    private AttendanceDetailView attendanceDetailView;
    private JSONObject jsonObject;

    public AttendanceDetailPresenterImpl(Context activity, AttendanceDetailView attendanceDetailView) {
        this.activity = activity;
        this.attendanceDetailView = attendanceDetailView;
    }

    @Override
    public void getAttendanceDetail1(String month) {
        try {
            ApiAdapter.getInstance(activity);
            callAttendanceDetailApi(month);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            attendanceDetailView.onAttendanceDetailInternetError();
        }
    }

    public void getAttendanceDetail2(String month, int attendanceType) {
        try {
            ApiAdapter.getInstance(activity);
            callMonthDetailApi(month, attendanceType);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            attendanceDetailView.onAttendanceDetailInternetError();
        }

    }

    public void getStudentPresentData() {
        try {
            ApiAdapter.getInstance(activity);
            callStudentPresentApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            attendanceDetailView.onAttendanceDetailInternetError();
        }
    }

    private void callAttendanceDetailApi(String month) {
        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_STUDENT_ID, UserSession.getInstance().getStudentId());
            jsonObject.put(Const.PARAM_MONTH, month);
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<AttendanceDetailModel> getLoginOutput = ApiAdapter.getApiService().getAttendanceDetail("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<AttendanceDetailModel>() {
            @Override
            public void onResponse(Call<AttendanceDetailModel> call, Response<AttendanceDetailModel> response) {
                try {
                    //getting whole data from response
                    AttendanceDetailModel attendanceDetailModel = response.body();
                    String message = attendanceDetailModel.getMessage();

                    if (attendanceDetailModel.getStatus()) {
                        attendanceDetailView.onSuccessAttendanceDetail(attendanceDetailModel);
                    } else {
                        attendanceDetailView.onUnsuccessAttendanceDetail(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    attendanceDetailView.onUnsuccessAttendanceDetail(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<AttendanceDetailModel> call, Throwable t) {
                attendanceDetailView.onUnsuccessAttendanceDetail(activity.getString(R.string.error_server));
            }
        });
    }


    private void callMonthDetailApi(String month, int attendanceType) {
        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_STUDENT_ID, UserSession.getInstance().getStudentId());
            jsonObject.put(Const.PARAM_MONTH, month);
            jsonObject.put(Const.PARAM_ATTENDANCE, attendanceType);
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<MonthDetailModel> getLoginOutput = ApiAdapter.getApiService().getMonthDetail("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<MonthDetailModel>() {
            @Override
            public void onResponse(Call<MonthDetailModel> call, Response<MonthDetailModel> response) {
                try {
                    MonthDetailModel attendanceDetailModel = response.body();
                    String message = attendanceDetailModel.getMessage();

                    if (attendanceDetailModel.getStatus()) {
                        attendanceDetailView.onSuccessMonthDetail(attendanceDetailModel.getDate());
                    } else {
                        attendanceDetailView.onUnsuccessMonthDetail(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    attendanceDetailView.onUnsuccessMonthDetail(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<MonthDetailModel> call, Throwable t) {
                attendanceDetailView.onUnsuccessMonthDetail(activity.getString(R.string.error_server));
            }
        });
    }

    private void callStudentPresentApi() {
        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<PresentModel> getLoginOutput = ApiAdapter.getApiService().getStudentAttendancePresent("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<PresentModel>() {
            @Override
            public void onResponse(Call<PresentModel> call, Response<PresentModel> response) {

                try {
                    //getting whole data from response
                    PresentModel attendanceDetailModel = response.body();
                    String message = attendanceDetailModel.getMessage();

                    if (attendanceDetailModel.getStatus()) {
                        attendanceDetailView.onSuccessAttendancePresent(attendanceDetailModel.getResult());
                    } else {
                        attendanceDetailView.onUnSuccessAttendancePresent(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    attendanceDetailView.onUnSuccessAttendancePresent(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<PresentModel> call, Throwable t) {
                attendanceDetailView.onUnSuccessAttendancePresent(activity.getString(R.string.error_server));
            }
        });
    }

}
