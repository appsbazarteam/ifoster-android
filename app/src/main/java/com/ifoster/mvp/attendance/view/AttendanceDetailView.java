package com.ifoster.mvp.attendance.view;

import com.ifoster.mvp.attendance.model.AttendanceDetailModel;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface AttendanceDetailView {

    public void onSuccessAttendanceDetail(AttendanceDetailModel attendanceDetailModel);

    public void onUnsuccessAttendanceDetail(String message);

    public void onAttendanceDetailInternetError();


    public void onSuccessMonthDetail(String data);

    public void onUnsuccessMonthDetail(String message);

    public void onMonthDetailInternetError();

    public void onMonthSelect(String month);

    public void onSuccessAttendancePresent(String data);
    public void onUnSuccessAttendancePresent(String data);
}
