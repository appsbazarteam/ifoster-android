package com.ifoster.mvp.leave.leave_approval;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.utility.Resource;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.leave.leave_approval.fragment.approvel.HistoryFragment;
import com.ifoster.mvp.leave.leave_approval.fragment.approvel.PendingFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class LeaveApprovalActivity extends BaseActivity {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_approval);

        setFont();
        setToolBar();
        createViewPager();
        tabLayout.setupWithViewPager(viewPager);
        createTabIcons();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    private void createTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText(Resource.toString(R.string.pending));
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText(Resource.toString(R.string.history));
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        FontHelper.applyFont(this, tabOne, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, tabTwo, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    private void createViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new PendingFragment(), Resource.toString(R.string.pending));
        adapter.addFrag(new HistoryFragment(), Resource.toString(R.string.history));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @OnClick(R.id.imgViewBack)
    public void goBack() {
        finish();
    }

    private void setToolBar() {
        txtViewToolbarTitle.setText(getString(R.string.title_leave_approval));
    }

}
