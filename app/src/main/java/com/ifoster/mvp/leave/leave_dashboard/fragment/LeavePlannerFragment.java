package com.ifoster.mvp.leave.leave_dashboard.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.adapter.AdapterSpinner;
import com.ifoster.common.helpers.AlertDialogHelper;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.leave.leave_dashboard.LeaveDashboardActivity;
import com.ifoster.mvp.leave.leave_dashboard.fragment.presenter.LeaveApplyPresenterImpl;
import com.ifoster.mvp.leave.leave_dashboard.fragment.view.LeaveApplyView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Alam on 10-Oct-17.
 */

public class LeavePlannerFragment extends Fragment implements LeaveApplyView {

    @BindView(R.id.relLayMainContainer)
    RelativeLayout relLayMainContainer;

    @BindView(R.id.spinnerTypeOfLeave)
    AppCompatSpinner spinnerTypeOfLeave;

    @BindView(R.id.edtTextNoOfDays)
    AppCompatEditText edtTextNoOfDays;

    @BindView(R.id.edtTextReason)
    AppCompatEditText edtTextReason;

    @BindView(R.id.txtViewFrom)
    TextView txtViewFrom;

    @BindView(R.id.txtViewTo)
    TextView txtViewTo;

    @BindView(R.id.txtApply)
    TextView txtApply;

    @BindView(R.id.txtCancel)
    TextView txtCancel;

    private LeaveApplyPresenterImpl leaveApplyPresenterImpl;
    private DatePicker datePicker;
    private Calendar calendar;
    private int year, month, day;
    private ArrayList<HashMap<String, String>> arrayListLeaveType;
    private boolean isParent = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_leave_planner, container, false);
        ButterKnife.bind(this, view);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return view;
    }

    private void setFont() {
        FontHelper.applyFont(getActivity(), edtTextNoOfDays, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), edtTextReason, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewFrom, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewTo, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), edtTextReason, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), edtTextReason, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtApply, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtCancel, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        leaveApplyPresenterImpl = new LeaveApplyPresenterImpl(getActivity(), this);
        getIntentData();
        if (isParent) {
            bindStudentLeaveTypeSpinner();
        } else {
            bindTeacherLeaveTypeSpinner();
        }


        edtTextReason.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edtTextReason.setOnEditorActionListener(
                new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                        if(actionId==EditorInfo.IME_ACTION_DONE){
                            //do something
                            onLeaveApply();
                        }
                        return true;
                    }
                });
    }

    private void getIntentData() {
        Intent intent = getActivity().getIntent();
        if (intent.hasExtra(ConstIntent.KEY_USER_TYPE)) {
            if (intent.getStringExtra(ConstIntent.KEY_USER_TYPE).equals(Const.KEY_PARENTS)) {
                isParent = true;
            } else {
                isParent = false;
            }
        }
    }

    private void bindStudentLeaveTypeSpinner() {
        arrayListLeaveType = new ArrayList<>();

        // add spinner index
        HashMap<String, String> index = new HashMap<>();
        index.put(Const.KEY_ID, "0");
        index.put(Const.KEY_NAME, "Select leave type");
        arrayListLeaveType.add(index);

        //add spinner item
        HashMap<String, String> hashMap1 = new HashMap<>();
        hashMap1.put(Const.KEY_ID, "1");
        hashMap1.put(Const.KEY_NAME, "Medical");

        arrayListLeaveType.add(hashMap1);

        HashMap<String, String> hashMap2 = new HashMap<>();
        hashMap2.put(Const.KEY_ID, "2");
        hashMap2.put(Const.KEY_NAME, "Personal");

        arrayListLeaveType.add(hashMap2);

        HashMap<String, String> hashMap3 = new HashMap<>();
        hashMap3.put(Const.KEY_ID, "3");
        hashMap3.put(Const.KEY_NAME, "Emergency");

        arrayListLeaveType.add(hashMap3);

        HashMap<String, String> hashMap4 = new HashMap<>();
        hashMap4.put(Const.KEY_ID, "4");
        hashMap4.put(Const.KEY_NAME, "Others");
        arrayListLeaveType.add(hashMap4);

        // bind spiiner item
        bindLeaveTypeToSpinner(arrayListLeaveType);
    }

    private void bindTeacherLeaveTypeSpinner() {

        arrayListLeaveType = new ArrayList<>();

        // add spinner index
        HashMap<String, String> index = new HashMap<>();
        index.put(Const.KEY_ID, "0");
        index.put(Const.KEY_NAME, "Select leave type");
        arrayListLeaveType.add(index);

        //add spinner item
        HashMap<String, String> hashMap1 = new HashMap<>();
        hashMap1.put(Const.KEY_ID, "1");
        hashMap1.put(Const.KEY_NAME, "Sick Leave");

        arrayListLeaveType.add(hashMap1);

        HashMap<String, String> hashMap2 = new HashMap<>();
        hashMap2.put(Const.KEY_ID, "2");
        hashMap2.put(Const.KEY_NAME, "Casual Leave");

        arrayListLeaveType.add(hashMap2);

        HashMap<String, String> hashMap3 = new HashMap<>();
        hashMap3.put(Const.KEY_ID, "3");
        hashMap3.put(Const.KEY_NAME, "Privillage Leave");

        arrayListLeaveType.add(hashMap3);

        HashMap<String, String> hashMap4 = new HashMap<>();
        hashMap4.put(Const.KEY_ID, "4");
        hashMap4.put(Const.KEY_NAME, "Others");
        arrayListLeaveType.add(hashMap4);

        // bind spiiner item
        bindLeaveTypeToSpinner(arrayListLeaveType);
    }

    private void bindLeaveTypeToSpinner(ArrayList<HashMap<String, String>> leaveTypes) {

        AdapterSpinner adapterSpinner = new AdapterSpinner(getActivity(), R.layout.layout_spinner_dropdown, leaveTypes);
        spinnerTypeOfLeave.setAdapter(adapterSpinner);
    }


    @OnClick(R.id.relLayApplyLeave)
    public void onLeaveApply() {

        Utils.hideKeyboardIfOpen(getActivity());

        String fromDate = txtViewFrom.getText().toString();
        String toDate = txtViewTo.getText().toString();
        String noOfDays = edtTextNoOfDays.getText().toString();
        String reason = edtTextReason.getText().toString();
        String leaveType = arrayListLeaveType.get(spinnerTypeOfLeave.getSelectedItemPosition()).get(Const.KEY_NAME);

        if (isValid(fromDate, toDate, String.valueOf(noOfDays), leaveType+"jhghjgs", reason)) {
            ((LeaveDashboardActivity)getActivity()).showProgressBar();
            leaveApplyPresenterImpl.applyLeave(leaveType, reason, fromDate, toDate, noOfDays, null, isParent);
        }
    }


    private boolean isValid(String fromDate, String toDate, String noOfDays, String leaveType, String reason) {

        if (fromDate.equals("From")) {
            showMsg(getActivity().getString(R.string.error_from_date));
            return false;
        } else if (toDate.equals("To")) {
            showMsg(getActivity().getString(R.string.error_to_date));
            return false;
        } else if (TextUtils.isEmpty(noOfDays)) {
            showMsg(getActivity().getString(R.string.error_no_of_days));
            return false;
        } else if (leaveType.equals("Select leave type")) {
            showMsg(getActivity().getString(R.string.error_leave_type));
            return false;
        }

        return true;
    }

    private void showMsg(String msg) {

        SnackNotify.showMessage(msg, relLayMainContainer);
    }

    @OnClick(R.id.relLayCancel)
    public void onLeaveCancel() {
        getActivity().finish();

    }

    @OnClick(R.id.llFromDate)
    public void openFromDatePicker() {

        // open date picker dialog
        setCurrentDate(R.id.txtViewFrom);
    }

    @OnClick(R.id.llToDate)
    public void openToDatePicker() {

        // open date picker dialog
        setCurrentDate(R.id.txtViewTo);
    }

    private void setCurrentDate(final int txtViewType) {

        Utils.hideKeyboardIfOpen(getActivity());

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if (R.id.txtViewFrom == txtViewType) {
                            txtViewFrom.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        } else if (R.id.txtViewTo == txtViewType) {
                            txtViewTo.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        }
                    }
                }, year, month, day);
        datePickerDialog.show();
    }

    @Override
    public void onSuccessLeaveApply(String messages) {
        ((LeaveDashboardActivity)getActivity()).hideProgressBar();
        AlertDialogHelper.showAlertAndFinishedActivity(getActivity(), messages);
    }

    @Override
    public void onUnsuccessLeaveApply(String message) {
        ((LeaveDashboardActivity)getActivity()).hideProgressBar();
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    @Override
    public void onLeaveApplyInternetError() {
        ((LeaveDashboardActivity)getActivity()).hideProgressBar();
        SnackNotify.checkConnection(onRetryApplyLeave, relLayMainContainer);
    }

    OnClickInterface onRetryApplyLeave = new OnClickInterface() {
        @Override
        public void onClick() {
            onLeaveApply();
        }
    };

}

