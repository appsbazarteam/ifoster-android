package com.ifoster.mvp.leave.leave_approval.fragment.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.mvp.leave.leave_approval.fragment.model.ApprovelModel;
import com.ifoster.mvp.leave.leave_approve_detail.LeaveApproveDetailActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class PendingLeaveAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<ApprovelModel.Result> listData;

    public PendingLeaveAdapter(Context context, ArrayList<ApprovelModel.Result> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_pending_leave, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ((ViewHolderList) holder).txtViewName.setText(listData.get(position).getEmpName());
        ((ViewHolderList) holder).txtViewClass.setText(listData.get(position).getLeaveType());
        ((ViewHolderList) holder).txtViewDate.setText(listData.get(position).getStartDate());

        ((ViewHolderList) holder).coordinateLayoutContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, LeaveApproveDetailActivity.class);

                Bundle bundle = new Bundle();
                bundle.putSerializable(ConstIntent.KEY_USER_LIST, listData.get(position));
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.coordinateLayoutContainer)
        CoordinatorLayout coordinateLayoutContainer;

        @BindView(R.id.imgProfile)
        ImageView imgProfile;

        @BindView(R.id.txtViewName)
        TextView txtViewName;

        @BindView(R.id.txtViewClass)
        TextView txtViewClass;

        @BindView(R.id.txtViewDate)
        TextView txtViewDate;

        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            FontHelper.applyFont(context, txtViewName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewClass, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewDate, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        }
    }
}
