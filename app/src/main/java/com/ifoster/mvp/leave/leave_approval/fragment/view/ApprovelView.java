package com.ifoster.mvp.leave.leave_approval.fragment.view;

import com.ifoster.mvp.leave.leave_approval.fragment.model.EmployeeApprovelListModel;
import com.ifoster.mvp.leave.leave_approval.fragment.model.StudentApprovelListModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ApprovelView {

    /*------------Employee Approvel List------------*/
    public void onSuccessEmployeeLeaveList(ArrayList<EmployeeApprovelListModel.Result> results);

    public void onUnsuccessEmployeeLeaveList(String message);

    public void onEmployeeLeaveListInternetError();


    /*------------Student Approvel List------------*/
    public void onSuccessStudentLeaveList(ArrayList<StudentApprovelListModel.Result> results);

    public void onUnsuccessStudentLeaveList(String message);

    public void onStudentLeaveListInternetError();

    /*------------Student Reject Leave List------------*/
    public void onSuccessStudentReject(ArrayList<StudentApprovelListModel.Result> results);

    public void onUnsuccessStudentReject(String message);

    public void onStudentRejectInternetError();


    /*------------Student Reject Leave List------------*/
    public void onSuccessEmpReject(ArrayList<EmployeeApprovelListModel.Result> results);

    public void onUnsuccessEmpReject(String message);

    public void onStudentEmpInternetError();


}
