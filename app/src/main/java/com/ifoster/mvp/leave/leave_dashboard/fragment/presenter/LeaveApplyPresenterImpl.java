package com.ifoster.mvp.leave.leave_dashboard.fragment.presenter;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.Utils;
import com.ifoster.mvp.leave.leave_dashboard.fragment.model.LeaveApplylModel;
import com.ifoster.mvp.leave.leave_dashboard.fragment.view.LeaveApplyView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class LeaveApplyPresenterImpl implements LeaveApplyPresenter {

    Activity activity;
    LeaveApplyView leaveApplyView;
    JSONObject jsonObject;

    public LeaveApplyPresenterImpl(Activity activity, LeaveApplyView leaveApplyView) {
        this.activity = activity;
        this.leaveApplyView = leaveApplyView;
    }

    @Override
    public void applyLeave(String leaveType, String reason, String startData, String endDate,
                           String noOfDays, String attachment, boolean isApplyLeaveForParent) {
        try {
            ApiAdapter.getInstance(activity);
            callApplyLeaveApi(leaveType, reason, startData, endDate, noOfDays, attachment, isApplyLeaveForParent);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            leaveApplyView.onLeaveApplyInternetError();
        }
    }

    private void callApplyLeaveApi(String leaveType, String reason, String startData, String endDate,
                                   String noOfDays, String attachment, boolean isApplyLeaveForParent) {


        String token = UserSession.getInstance().getUserToken();
        int studentId = UserSession.getInstance().getStudentId();
        int employeeId = UserSession.getInstance().getEmployeeId();

        try {
            jsonObject = new JSONObject();

            if (isApplyLeaveForParent) {
                jsonObject.put(Const.PARAM_STUDENT_ID, studentId);
            } else {
                jsonObject.put(Const.PARAM_EMPLOYEE_ID, employeeId);
            }

            jsonObject.put(Const.PARAM_USER_TOKEN, token);
            jsonObject.put(Const.PARAM_LEAVE_TYPE, leaveType);
            jsonObject.put(Const.PARAM_START_DATE, Utils.convertToSimpleDateFormate(startData));
            jsonObject.put(Const.PARAM_END_DATE, Utils.convertToSimpleDateFormate(endDate));
            jsonObject.put(Const.PARAM_NO_OF_DAY, noOfDays);
            jsonObject.put(Const.PARAM_REASON, reason);
            jsonObject.put(Const.PARAM_ATTACHMENT, "info.png");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<LeaveApplylModel> getLoginOutput;

        if (isApplyLeaveForParent) {
            getLoginOutput = ApiAdapter.getApiService().applyLeave("application/json", "no-cache", body);
        } else {
            getLoginOutput = ApiAdapter.getApiService().applyTeacherLeave("application/json", "no-cache", body);
        }

        getLoginOutput.enqueue(new Callback<LeaveApplylModel>() {
            @Override
            public void onResponse(Call<LeaveApplylModel> call, Response<LeaveApplylModel> response) {

                try {

                    //getting whole data from response
                    LeaveApplylModel LeaveListModel = response.body();
                    String message = LeaveListModel.getMessage();

                    if (LeaveListModel.getStatus()) {
                        leaveApplyView.onSuccessLeaveApply(message);
                    } else {
                        leaveApplyView.onUnsuccessLeaveApply(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    leaveApplyView.onUnsuccessLeaveApply(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<LeaveApplylModel> call, Throwable t) {
                leaveApplyView.onUnsuccessLeaveApply(activity.getString(R.string.error_server));
            }
        });
    }


}
