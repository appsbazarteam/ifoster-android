package com.ifoster.mvp.leave.leave_dashboard.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ifoster.R;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.leave.leave_dashboard.LeaveDashboardActivity;
import com.ifoster.mvp.leave.leave_dashboard.fragment.adapter.LeaveListAdapter;
import com.ifoster.mvp.leave.leave_dashboard.fragment.model.LeaveListHistoryModel;
import com.ifoster.mvp.leave.leave_dashboard.fragment.model.LeaveListTeacherModel;
import com.ifoster.mvp.leave.leave_dashboard.fragment.presenter.LeavePresenterImpl;
import com.ifoster.mvp.leave.leave_dashboard.fragment.view.LeaveView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alam on 10-Oct-17.
 */

public class LeaveListFragment extends Fragment implements LeaveView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.relLayMainContainer)
    RelativeLayout relLayMainContainer;

    private LeavePresenterImpl leavePresenterImpl;
    private boolean isParent = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_leavelist, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        leavePresenterImpl = new LeavePresenterImpl(getActivity(), this);
        getIntentData();
        getData();
    }


    private void getIntentData() {
        Intent intent = getActivity().getIntent();
        if (intent.hasExtra(ConstIntent.KEY_USER_TYPE)) {
            if (intent.getStringExtra(ConstIntent.KEY_USER_TYPE).equals(Const.KEY_PARENTS)) {
                isParent = true;
            } else {
                isParent = false;
            }
        }
    }

    private void getData() {
        ((LeaveDashboardActivity)getActivity()).showProgressBar();
        leavePresenterImpl.getLeaveList(isParent);
    }

    @Override
    public void onSuccessLeaveList(LeaveListTeacherModel leaveListModel) {
        ((LeaveDashboardActivity)getActivity()).hideProgressBar();
        ArrayList<LeaveListTeacherModel.Result> results = leaveListModel.getResult();

        if (results != null && results.size() > 0) {

            int colorCodeType = 0;

            for (int i = 0; i < results.size(); i++) {
                results.get(i).setColorType(colorCodeType);

                // increase color clde value
                colorCodeType++;

                // reset color form strating again
                if (colorCodeType == 4) {
                    colorCodeType = 0;
                }
            }

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setNestedScrollingEnabled(false);
        } else {
            SnackNotify.showMessage("No record found.", relLayMainContainer);
        }

        LeaveListAdapter leaveListAdapter = new LeaveListAdapter(getActivity(), results);
        recyclerView.setAdapter(leaveListAdapter);
    }

    @Override
    public void onUnsuccessLeaveList(String message) {
        ((LeaveDashboardActivity)getActivity()).hideProgressBar();
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    @Override
    public void onLeaveListInternetError() {
        ((LeaveDashboardActivity)getActivity()).hideProgressBar();
        SnackNotify.checkConnection(onRetryLeaveList, relLayMainContainer);
    }

    OnClickInterface onRetryLeaveList = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    @Override
    public void onSuccessLeaveListHistory(ArrayList<LeaveListHistoryModel.Result> message) {
        ((LeaveDashboardActivity)getActivity()).hideProgressBar();
    }

    @Override
    public void onUnsuccessLeaveListHistory(String message) {
        ((LeaveDashboardActivity)getActivity()).hideProgressBar();
    }

    @Override
    public void onLeaveListHistoryInternetError() {
        ((LeaveDashboardActivity)getActivity()).hideProgressBar();
    }
}
