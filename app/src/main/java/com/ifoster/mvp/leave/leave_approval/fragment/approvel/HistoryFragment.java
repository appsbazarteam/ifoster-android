package com.ifoster.mvp.leave.leave_approval.fragment.approvel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifoster.R;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.leave.leave_approval.LeaveApprovalActivity;
import com.ifoster.mvp.leave.leave_approval.fragment.adapter.EmployeeApprovelListAdapter;
import com.ifoster.mvp.leave.leave_approval.fragment.adapter.StudentApprovelListAdapter;
import com.ifoster.mvp.leave.leave_approval.fragment.model.EmployeeApprovelListModel;
import com.ifoster.mvp.leave.leave_approval.fragment.model.StudentApprovelListModel;
import com.ifoster.mvp.leave.leave_approval.fragment.presenter.ApprovelPresenterImpl;
import com.ifoster.mvp.leave.leave_approval.fragment.view.ApprovelView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Alam on 10-Oct-17.
 */

public class HistoryFragment extends Fragment implements ApprovelView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.relLayMainContainer)
    RelativeLayout relLayMainContainer;

    @BindView(R.id.txtViewStudent)
    TextView txtViewStudent;

    @BindView(R.id.txtViewEmployee)
    TextView txtViewEmployee;

    private ApprovelPresenterImpl approvelPresenterImpl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_approved, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        approvelPresenterImpl = new ApprovelPresenterImpl(getActivity(), this);
        setLayoutManager();
        getStudentApproverList();
    }

    private void setLayoutManager() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
    }

    @OnClick(R.id.txtViewStudent)
    public void getStudentApproverList() {
        txtViewEmployee.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.drawable_round_green));
        txtViewStudent.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.drawable_round_green_fill));
        txtViewEmployee.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_black));
        txtViewStudent.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_white));
        ((LeaveApprovalActivity)getActivity()).showProgressBar();
        approvelPresenterImpl.getStudentRejectLeave();
    }

    @OnClick(R.id.txtViewEmployee)
    public void getEmployeeApproverList() {
        txtViewEmployee.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.drawable_round_green_fill));
        txtViewStudent.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.drawable_round_green));
        txtViewEmployee.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_white));
        txtViewStudent.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_black));
        ((LeaveApprovalActivity)getActivity()).showProgressBar();
        approvelPresenterImpl.getEmployeeRejectLeave();
    }

    @Override
    public void onSuccessEmployeeLeaveList(ArrayList<EmployeeApprovelListModel.Result> results) {
        ((LeaveApprovalActivity)getActivity()).hideProgressBar();
    }

    @Override
    public void onUnsuccessEmployeeLeaveList(String message) {
        ((LeaveApprovalActivity)getActivity()).hideProgressBar();
    }

    @Override
    public void onEmployeeLeaveListInternetError() {
        ((LeaveApprovalActivity)getActivity()).hideProgressBar();
    }

    @Override
    public void onSuccessStudentLeaveList(ArrayList<StudentApprovelListModel.Result> results) {
        ((LeaveApprovalActivity)getActivity()).hideProgressBar();
    }

    @Override
    public void onUnsuccessStudentLeaveList(String message) {
        ((LeaveApprovalActivity)getActivity()).hideProgressBar();
    }

    @Override
    public void onStudentLeaveListInternetError() {
        ((LeaveApprovalActivity)getActivity()).hideProgressBar();
    }

    @Override
    public void onSuccessStudentReject(ArrayList<StudentApprovelListModel.Result> results) {
        ((LeaveApprovalActivity)getActivity()).hideProgressBar();
        if (results != null && results.size() > 0) {

            StudentApprovelListAdapter studentApprovelListAdapter = new StudentApprovelListAdapter(getActivity(), results, true);
            recyclerView.setAdapter(studentApprovelListAdapter);

        } else {

            StudentApprovelListAdapter studentApprovelListAdapter = new StudentApprovelListAdapter(getActivity(), new ArrayList<StudentApprovelListModel.Result>(), true);
            recyclerView.setAdapter(studentApprovelListAdapter);

            SnackNotify.showMessage(getActivity().getString(R.string.no_record_found), relLayMainContainer);
        }
    }

    @Override
    public void onUnsuccessStudentReject(String message) {
        ((LeaveApprovalActivity)getActivity()).hideProgressBar();
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    @Override
    public void onStudentRejectInternetError() {
        ((LeaveApprovalActivity)getActivity()).hideProgressBar();
        SnackNotify.checkConnection(onRetryStudentReject, relLayMainContainer);
    }

    @Override
    public void onSuccessEmpReject(ArrayList<EmployeeApprovelListModel.Result> results) {
        ((LeaveApprovalActivity)getActivity()).hideProgressBar();
        if (results != null && results.size() > 0) {

            EmployeeApprovelListAdapter employeeApprovelListAdapter = new EmployeeApprovelListAdapter(getActivity(), results,true);
            recyclerView.setAdapter(employeeApprovelListAdapter);

        } else {

            EmployeeApprovelListAdapter employeeApprovelListAdapter = new EmployeeApprovelListAdapter(getActivity(), new ArrayList<EmployeeApprovelListModel.Result>(),true);
            recyclerView.setAdapter(employeeApprovelListAdapter);

            SnackNotify.showMessage(getActivity().getString(R.string.no_record_found), relLayMainContainer);
        }
    }

    @Override
    public void onUnsuccessEmpReject(String message) {
        ((LeaveApprovalActivity)getActivity()).hideProgressBar();
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    @Override
    public void onStudentEmpInternetError() {
        ((LeaveApprovalActivity)getActivity()).hideProgressBar();
        SnackNotify.checkConnection(onRetryEmpReject, relLayMainContainer);
    }

    OnClickInterface onRetryEmpReject = new OnClickInterface() {
        @Override
        public void onClick() {
            getEmployeeApproverList();
        }
    };

    OnClickInterface onRetryStudentReject = new OnClickInterface() {
        @Override
        public void onClick() {
            getStudentApproverList();
        }
    };
}

