package com.ifoster.mvp.leave.leave_dashboard.fragment.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface LeavePresenter {

    public void getLeaveList(boolean isLeaveListForParent);

    public void getLeaveListHistory(boolean isLeaveListForParent);
}
