package com.ifoster.mvp.leave.leave_dashboard.fragment.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface LeaveApplyPresenter {

    public void applyLeave(String leaveType, String reason, String startData, String endDate,
                           String noOfDays, String attachment, boolean isApplyLeaveForParent);

}
