package com.ifoster.mvp.leave.leave_approve_detail.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ApprovelDetailPresenter {


    void getStudentApprovelDetail(int studentId);

    void getEmployeeApprovelDetail(int empId);

    void approveOrRejectEmpLeave(int leaveId, String leaveStatus, String comments, boolean isStudent);
}
