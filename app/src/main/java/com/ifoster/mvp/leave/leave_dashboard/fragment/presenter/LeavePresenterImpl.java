package com.ifoster.mvp.leave.leave_dashboard.fragment.presenter;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.leave.leave_dashboard.fragment.model.LeaveListHistoryModel;
import com.ifoster.mvp.leave.leave_dashboard.fragment.model.LeaveListTeacherModel;
import com.ifoster.mvp.leave.leave_dashboard.fragment.view.LeaveView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class LeavePresenterImpl implements LeavePresenter {

    Activity activity;
    LeaveView leaveView;
    JSONObject jsonObject;

    public LeavePresenterImpl(Activity activity, LeaveView leaveView) {
        this.activity = activity;
        this.leaveView = leaveView;
    }

    @Override
    public void getLeaveList(boolean isLeaveListForParent) {
        try {
            ApiAdapter.getInstance(activity);
            callLeaveListApi(isLeaveListForParent);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            leaveView.onLeaveListInternetError();
        }
    }

    @Override
    public void getLeaveListHistory(boolean isLeaveListForParent) {
        try {
            ApiAdapter.getInstance(activity);
            callLeaveListHistoryApi(isLeaveListForParent);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            leaveView.onLeaveListHistoryInternetError();
        }
    }

    private void callLeaveListApi(boolean isLeaveListForParent) {
        String token = UserSession.getInstance().getUserToken();
        int studentId = UserSession.getInstance().getStudentId();
        int empId = UserSession.getInstance().getEmployeeId();

        try {
            jsonObject = new JSONObject();
            if (isLeaveListForParent) {
                jsonObject.put(Const.PARAM_STUDENT_ID, studentId);
            } else {
                jsonObject.put(Const.PARAM_EMPLOYEE_ID, empId);
            }
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<LeaveListTeacherModel> getLoginOutput = null;

        if (isLeaveListForParent) {
            getLoginOutput = ApiAdapter.getApiService().getLeaveList("application/json", "no-cache", body);
        } else {
            getLoginOutput = ApiAdapter.getApiService().getTeacherLeaveList("application/json", "no-cache", body);
        }

        getLoginOutput.enqueue(new Callback<LeaveListTeacherModel>() {
            @Override
            public void onResponse(Call<LeaveListTeacherModel> call, Response<LeaveListTeacherModel> response) {
                try {

                    //getting whole data from response
                    LeaveListTeacherModel leaveListModel = response.body();
                    String message = leaveListModel.getMessage();

                    if (leaveListModel.getStatus()) {
                        leaveView.onSuccessLeaveList(leaveListModel);
                    } else {
                        leaveView.onUnsuccessLeaveList(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    leaveView.onUnsuccessLeaveList(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<LeaveListTeacherModel> call, Throwable t) {
                leaveView.onUnsuccessLeaveList(activity.getString(R.string.error_server));
            }
        });
    }

    private void callLeaveListHistoryApi(boolean isLeaveListForParent) {

        String token = UserSession.getInstance().getUserToken();
        int studentId = UserSession.getInstance().getStudentId();
        int empId = UserSession.getInstance().getEmployeeId();

        try {
            jsonObject = new JSONObject();
            if (isLeaveListForParent) {
                jsonObject.put(Const.PARAM_STUDENT_ID, studentId);
            } else {
                jsonObject.put(Const.PARAM_EMPLOYEE_ID, empId);
            }
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<LeaveListHistoryModel> getLoginOutput = null;

        if (isLeaveListForParent) {
            getLoginOutput = ApiAdapter.getApiService().getLeaveListHistory("application/json", "no-cache", body);
        } else {
            getLoginOutput = ApiAdapter.getApiService().getTeacherLeaveListHistory("application/json", "no-cache", body);
        }


        getLoginOutput.enqueue(new Callback<LeaveListHistoryModel>() {
            @Override
            public void onResponse(Call<LeaveListHistoryModel> call, Response<LeaveListHistoryModel> response) {

                try {

                    //getting whole data from response
                    LeaveListHistoryModel LeaveListModel = response.body();
                    String message = LeaveListModel.getMessage();

                    if (LeaveListModel.getStatus()) {
                        leaveView.onSuccessLeaveListHistory(LeaveListModel.getResult());
                    } else {
                        leaveView.onUnsuccessLeaveListHistory(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    leaveView.onUnsuccessLeaveListHistory(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<LeaveListHistoryModel> call, Throwable t) {
                leaveView.onUnsuccessLeaveListHistory(activity.getString(R.string.error_server));
            }
        });
    }


}
