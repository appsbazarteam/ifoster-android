package com.ifoster.mvp.leave.leave_approve_detail;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.AlertDialogHelper;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.utility.CircleTransform;
import com.ifoster.common.utility.Resource;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.leave.leave_approval.fragment.model.EmployeeApprovelListModel;
import com.ifoster.mvp.leave.leave_approval.fragment.model.PendingLeaveModel;
import com.ifoster.mvp.leave.leave_approval.fragment.model.StudentApprovelListModel;
import com.ifoster.mvp.leave.leave_approve_detail.presenter.ApprovelDetailPresenterImpl;
import com.ifoster.mvp.leave.leave_approve_detail.view.ApprovelDetailView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class LeaveApproveDetailActivity extends BaseActivity implements ApprovelDetailView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @BindView(R.id.txtViewName)
    TextView txtViewName;

    @BindView(R.id.txtViewClass)
    TextView txtViewClass;

    @BindView(R.id.txtFrom)
    TextView txtFrom;

    @BindView(R.id.txtViewFrom)
    TextView txtViewFrom;

    @BindView(R.id.txtTo)
    TextView txtTo;

    @BindView(R.id.txtViewTo)
    TextView txtViewTo;

    @BindView(R.id.txtTypeOfLeave)
    TextView txtTypeOfLeave;

    @BindView(R.id.txtViewTypeOfLeave)
    TextView txtViewTypeOfLeave;

    @BindView(R.id.txtReason)
    TextView txtReason;

    @BindView(R.id.txtViewReason)
    TextView txtViewReason;

    @BindView(R.id.txtComment)
    TextView txtComment;

    @BindView(R.id.ll_comment)
    LinearLayout ll_comment;

    @BindView(R.id.txtViewComment)
    TextView txtViewComment;

    @BindView(R.id.edtTextComment)
    AppCompatEditText edtTextComment;

    @BindView(R.id.txtViewApprove)
    TextView txtViewApprove;

    @BindView(R.id.txtViewReject)
    TextView txtViewReject;

    @BindView(R.id.linLayComment)
    LinearLayout linLayComment;

    @BindView(R.id.cardViewBottom)
    CardView cardViewBottom;

    @BindView(R.id.imgProfile)
    ImageView imgProfile;

    @BindView(R.id.txtViewStatus)
    TextView txtViewStatus;

    @BindView(R.id.cardViewStatus)
    CardView cardViewStatus;


    String leaveStatus = "";

    // intent data
    int id = 0;
    boolean isStudent = false;
    boolean isRejectList = false;

    // class presenter
    ApprovelDetailPresenterImpl approvelDetailPresenterImpl;

    // result
    EmployeeApprovelListModel.Result result;
    StudentApprovelListModel.Result studentResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_approve_detail);

        approvelDetailPresenterImpl = new ApprovelDetailPresenterImpl(this, this);

        setFont();

        getIntentData();

        // set toolbar
        setToolBar();
    }



    private void getIntentData() {

        Intent intent = getIntent();

        if (intent.hasExtra(ConstIntent.KEY_USER_LIST)) {

            PendingLeaveModel pendingLeaveModel = (PendingLeaveModel) intent.getSerializableExtra(ConstIntent.KEY_USER_LIST);

            if (pendingLeaveModel != null) {

                txtViewName.setText(pendingLeaveModel.getName());
                txtViewFrom.setText(pendingLeaveModel.getDate());
                txtViewTo.setText(pendingLeaveModel.getDate());
                txtViewTypeOfLeave.setText(pendingLeaveModel.getLeaveType());
                txtViewReason.setText(pendingLeaveModel.getDescription());
            }
        } else if (intent.hasExtra(ConstIntent.KEY_ID)) {

            String empId = intent.getStringExtra(ConstIntent.KEY_ID);
            isStudent = intent.getBooleanExtra(ConstIntent.KEY_IS_STUDENT, false);
            isRejectList = intent.getBooleanExtra(ConstIntent.KEY_IS_REJECT_LIST, false);

            // hide/show view
            if (isRejectList) {
                ll_comment.setVisibility(View.VISIBLE);
                linLayComment.setVisibility(View.GONE);
                cardViewBottom.setVisibility(View.GONE);
            } else {
                ll_comment.setVisibility(View.GONE);
                linLayComment.setVisibility(View.VISIBLE);
                cardViewBottom.setVisibility(View.VISIBLE);
            }

            if (empId != null && empId.length() > 0) {

                id = Integer.parseInt(empId);

                if (isStudent) {

                    // call employee detail api
                    callStudentDetail();

                } else {
                    // call employee detail api
                    callEmpDetailId();
                }
            }
        }
    }

    private void setProfileImage(String url) {
        if (url != null && url.length() > 0) {
            Glide.with(this).load(url)
                    .error(R.drawable.user)
                    .placeholder(R.drawable.user)
                    .thumbnail(0.5f)
                    .crossFade()
                    .transform(new CircleTransform(this))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfile);

        } else {
            imgProfile.setBackgroundResource(R.drawable.user);
        }

    }

    private void callEmpDetailId() {
        showProgressBar();
        approvelDetailPresenterImpl.getEmployeeApprovelDetail(id);
    }

    private void callStudentDetail() {
        showProgressBar();
        approvelDetailPresenterImpl.getStudentApprovelDetail(id);
    }


    @OnClick(R.id.imgViewBack)
    public void goBack() {
        finish();
    }

    private void setToolBar() {

        txtViewToolbarTitle.setText(getString(R.string.title_leave_approval));
        txtReason.setPaintFlags(txtReason.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtComment.setPaintFlags(txtComment.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    public void onSuccessEmployeeLeaveDetail(ArrayList<EmployeeApprovelListModel.Result> results) {
        hideProgressBar();
        if (results != null && results.size() > 0) {

            result = results.get(0);

            txtViewName.setText(result.getEmpName());
            txtViewFrom.setText(result.getStartDate());
            txtViewTo.setText(result.getEndDate());
            txtViewTypeOfLeave.setText(result.getLeaveType());
            txtViewReason.setText(result.getReason());
            txtViewComment.setText(result.getApprovelComment());
            setProfileImage(result.getEmpProfile());
            leaveStatus(result.getLeaveStatus());

        } else {
            SnackNotify.showMessage(getString(R.string.no_record_found), coordinateLayout);
        }
    }

    @Override
    public void onUnsuccessEmployeeLeaveDetail(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onEmployeeLeaveDetailInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryEmpDetail, coordinateLayout);
    }


    OnClickInterface onRetryEmpDetail = new OnClickInterface() {
        @Override
        public void onClick() {

            callEmpDetailId();
        }
    };

    @Override
    public void onSuccessStudentLeaveDetail(ArrayList<StudentApprovelListModel.Result> results) {
        hideProgressBar();
        if (results != null && results.size() > 0) {

            studentResult = results.get(0);

            txtViewName.setText(studentResult.getStudentName());
            txtViewFrom.setText(studentResult.getStartDate());
            txtViewTo.setText(studentResult.getEndDate());
            txtViewTypeOfLeave.setText(studentResult.getLeaveType());
            txtViewReason.setText(studentResult.getReason());
            //.txtViewComment.setText(studentResult.getap);

            setProfileImage(studentResult.getProfileImage());
            leaveStatus(studentResult.getLeaveStatus());

        } else {

            SnackNotify.showMessage(getString(R.string.no_record_found), coordinateLayout);
        }

    }

    @Override
    public void onUnsuccessStudentLeaveDetail(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onStudentLeaveDetailInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryStudentDetail, coordinateLayout);
    }

    OnClickInterface onRetryStudentDetail = new OnClickInterface() {
        @Override
        public void onClick() {
            callStudentDetail();
        }
    };


    @Override
    public void onSuccessApproved(String message) {
        hideProgressBar();
        AlertDialogHelper.showAlertAndFinishedActivity(this, message);
    }

    @Override
    public void onUnsuccessApproved(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onApprovedInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryApprovedLeave, coordinateLayout);
    }

    OnClickInterface onRetryApprovedLeave = new OnClickInterface() {
        @Override
        public void onClick() {

        }
    };


    @OnClick(R.id.relLayApprove)
    public void onApproveClick() {

        Utils.hideKeyboardIfOpen(this);

        leaveStatus = "Approve";

        approvedEmployeeLeaveApi();
    }

    @OnClick(R.id.relLayReject)
    public void onRejectClick() {

        Utils.hideKeyboardIfOpen(this);

        leaveStatus = "Reject";

        approvedEmployeeLeaveApi();
    }

    private void approvedEmployeeLeaveApi() {

        if (isStudent) {
            approvelDetailPresenterImpl.approveOrRejectEmpLeave(Integer.parseInt(studentResult.getId()),
                    leaveStatus, edtTextComment.getText().toString(), isStudent);
        } else {
            approvelDetailPresenterImpl.approveOrRejectEmpLeave(Integer.parseInt(result.getId()),
                    leaveStatus, edtTextComment.getText().toString(), isStudent);
        }
    }


    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewClass, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtFrom, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewFrom, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtTo, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewTo, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtTypeOfLeave, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewTypeOfLeave, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtReason, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewReason, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, edtTextComment, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewApprove, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewReject, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewStatus, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        FontHelper.applyFont(this, txtComment, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewComment, FontHelper.FontType.FONT_QUICKSAND_BOLD);

    }

    private void leaveStatus(String leaveStatus){
        cardViewStatus.setVisibility(View.VISIBLE);
        txtViewStatus.setText(leaveStatus);
        if(Resource.toString(R.string.pending).toLowerCase().contains(leaveStatus.toLowerCase())){
            cardViewStatus.setCardBackgroundColor(Color.YELLOW);
            txtViewStatus.setText(Resource.toString(R.string.pending));
            txtViewStatus.setTextColor(Resource.toColor(R.color.color_black));
        }else if(Resource.toString(R.string.approved).toLowerCase().contains(leaveStatus.toLowerCase())){
            cardViewStatus.setCardBackgroundColor(Resource.toColor(R.color.color_32cd32));
            txtViewStatus.setText(Resource.toString(R.string.approved));
            txtViewStatus.setTextColor(Resource.toColor(R.color.color_black));
        }else if(Resource.toString(R.string.reject).toLowerCase().contains(leaveStatus.toLowerCase())){
            cardViewStatus.setCardBackgroundColor(Color.RED);
            txtViewStatus.setText(Resource.toString(R.string.reject));
            txtViewStatus.setTextColor(Resource.toColor(R.color.color_white));
        }
    }

}
