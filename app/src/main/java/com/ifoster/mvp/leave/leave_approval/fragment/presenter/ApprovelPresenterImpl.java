package com.ifoster.mvp.leave.leave_approval.fragment.presenter;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.leave.leave_approval.fragment.model.EmployeeApprovelListModel;
import com.ifoster.mvp.leave.leave_approval.fragment.model.StudentApprovelListModel;
import com.ifoster.mvp.leave.leave_approval.fragment.view.ApprovelView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class ApprovelPresenterImpl implements ApprovelPresenter {

    Activity activity;
    ApprovelView approvelView;
    JSONObject jsonObject;

    public ApprovelPresenterImpl(Activity activity, ApprovelView approvelView) {
        this.activity = activity;
        this.approvelView = approvelView;
    }

    @Override
    public void getStudentApprovelData() {

        try {
            ApiAdapter.getInstance(activity);
            callStudentApprovelApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            approvelView.onStudentLeaveListInternetError();
        }

    }

    @Override
    public void getEmployeeApprovelData() {

        try {
            ApiAdapter.getInstance(activity);
            callEmployeeApprovelApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            approvelView.onEmployeeLeaveListInternetError();
        }

    }

    @Override
    public void getStudentRejectLeave() {

        try {
            ApiAdapter.getInstance(activity);
            callStudentRejectApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            approvelView.onStudentRejectInternetError();
        }
    }

    @Override
    public void getEmployeeRejectLeave() {

        try {
            ApiAdapter.getInstance(activity);
            callEmpRejectApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            approvelView.onStudentEmpInternetError();
        }
    }

    private void callStudentApprovelApi() {

        String token = UserSession.getInstance().getUserToken();
        int employeeId = UserSession.getInstance().getEmployeeId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
            jsonObject.put(Const.PARAM_EMPLOYEE_ID, employeeId);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

        Call<StudentApprovelListModel> getLoginOutput = ApiAdapter.getApiService().getStudentApprovelList("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<StudentApprovelListModel>() {
            @Override
            public void onResponse(Call<StudentApprovelListModel> call, Response<StudentApprovelListModel> response) {

                try {
                    //getting whole data from response
                    StudentApprovelListModel holiDayCalenderModel = response.body();
                    String message = holiDayCalenderModel.getMessage();

                    if (holiDayCalenderModel.getStatus()) {
                        approvelView.onSuccessStudentLeaveList(holiDayCalenderModel.getResult());
                    } else {
                        approvelView.onUnsuccessStudentLeaveList(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    approvelView.onUnsuccessStudentLeaveList(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<StudentApprovelListModel> call, Throwable t) {
                approvelView.onUnsuccessStudentLeaveList(activity.getString(R.string.error_server));
            }
        });
    }

    private void callEmployeeApprovelApi() {

        String token = UserSession.getInstance().getUserToken();
        int employeeId = UserSession.getInstance().getEmployeeId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
            jsonObject.put(Const.PARAM_EMPLOYEE_ID, employeeId);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

        Call<EmployeeApprovelListModel> getLoginOutput = ApiAdapter.getApiService().getEmployeeApprovelList("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<EmployeeApprovelListModel>() {
            @Override
            public void onResponse(Call<EmployeeApprovelListModel> call, Response<EmployeeApprovelListModel> response) {

                try {
                    //getting whole data from response
                    EmployeeApprovelListModel holiDayCalenderModel = response.body();
                    String message = holiDayCalenderModel.getMessage();

                    if (holiDayCalenderModel.getStatus()) {
                        approvelView.onSuccessEmployeeLeaveList(holiDayCalenderModel.getResult());
                    } else {
                        approvelView.onUnsuccessEmployeeLeaveList(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    approvelView.onUnsuccessEmployeeLeaveList(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<EmployeeApprovelListModel> call, Throwable t) {
                approvelView.onUnsuccessEmployeeLeaveList(activity.getString(R.string.error_server));
            }
        });
    }

    private void callStudentRejectApi() {

        String token = UserSession.getInstance().getUserToken();
        int employeeId = UserSession.getInstance().getEmployeeId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
            jsonObject.put(Const.PARAM_EMPLOYEE_ID, employeeId);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

        Call<StudentApprovelListModel> getLoginOutput = ApiAdapter.getApiService().studentRejectLeave("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<StudentApprovelListModel>() {
            @Override
            public void onResponse(Call<StudentApprovelListModel> call, Response<StudentApprovelListModel> response) {

                try {
                    //getting whole data from response
                    StudentApprovelListModel holiDayCalenderModel = response.body();
                    String message = holiDayCalenderModel.getMessage();

                    if (holiDayCalenderModel.getStatus()) {
                        approvelView.onSuccessStudentReject(holiDayCalenderModel.getResult());
                    } else {
                        approvelView.onUnsuccessStudentReject(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    approvelView.onUnsuccessStudentReject(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<StudentApprovelListModel> call, Throwable t) {
                approvelView.onUnsuccessStudentReject(activity.getString(R.string.error_server));
            }
        });
    }

    private void callEmpRejectApi() {

        String token = UserSession.getInstance().getUserToken();
        int employeeId = UserSession.getInstance().getEmployeeId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
            jsonObject.put(Const.PARAM_EMPLOYEE_ID, employeeId);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

        Call<EmployeeApprovelListModel> getLoginOutput = ApiAdapter.getApiService().employeeRejectLeave("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<EmployeeApprovelListModel>() {
            @Override
            public void onResponse(Call<EmployeeApprovelListModel> call, Response<EmployeeApprovelListModel> response) {

                try {
                    //getting whole data from response
                    EmployeeApprovelListModel holiDayCalenderModel = response.body();
                    String message = holiDayCalenderModel.getMessage();

                    if (holiDayCalenderModel.getStatus()) {
                        approvelView.onSuccessEmpReject(holiDayCalenderModel.getResult());
                    } else {
                        approvelView.onUnsuccessEmpReject(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    approvelView.onUnsuccessEmpReject(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<EmployeeApprovelListModel> call, Throwable t) {
                approvelView.onUnsuccessEmpReject(activity.getString(R.string.error_server));
            }
        });
    }
}
