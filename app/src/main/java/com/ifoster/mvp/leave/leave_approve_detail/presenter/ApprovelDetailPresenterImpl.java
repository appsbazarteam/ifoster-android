package com.ifoster.mvp.leave.leave_approve_detail.presenter;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.leave.leave_approval.fragment.model.EmployeeApprovelListModel;
import com.ifoster.mvp.leave.leave_approval.fragment.model.StudentApprovelListModel;
import com.ifoster.mvp.leave.leave_approve_detail.model.EmployeeApprovedLeaveModel;
import com.ifoster.mvp.leave.leave_approve_detail.view.ApprovelDetailView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class ApprovelDetailPresenterImpl implements ApprovelDetailPresenter {

    Activity activity;
    ApprovelDetailView approvelDetailView;
    JSONObject jsonObject;

    public ApprovelDetailPresenterImpl(Activity activity, ApprovelDetailView approvelDetailView) {
        this.activity = activity;
        this.approvelDetailView = approvelDetailView;
    }

    @Override
    public void getStudentApprovelDetail(int studentId) {

        try {
            ApiAdapter.getInstance(activity);
            callStudentApprovelDetailApi(studentId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            approvelDetailView.onStudentLeaveDetailInternetError();
        }

    }

    @Override
    public void getEmployeeApprovelDetail(int empId) {

        try {
            ApiAdapter.getInstance(activity);
            callEmployeeApprovelApi(empId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            approvelDetailView.onEmployeeLeaveDetailInternetError();
        }
    }

    @Override
    public void approveOrRejectEmpLeave(int leaveId, String leaveStatus, String comments, boolean isStudent) {

        try {
            ApiAdapter.getInstance(activity);
            callApproveOrRejectEmpLeave(leaveId, leaveStatus, comments, isStudent);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            approvelDetailView.onEmployeeLeaveDetailInternetError();
        }
    }


    private void callStudentApprovelDetailApi(int id) {

        String token = UserSession.getInstance().getUserToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
            jsonObject.put(Const.KEY_ID, id);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

        Call<StudentApprovelListModel> getLoginOutput = ApiAdapter.getApiService().getStudentApprovelDetail("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<StudentApprovelListModel>() {
            @Override
            public void onResponse(Call<StudentApprovelListModel> call, Response<StudentApprovelListModel> response) {

                try {
                    //getting whole data from response
                    StudentApprovelListModel holiDayCalenderModel = response.body();
                    String message = holiDayCalenderModel.getMessage();

                    if (holiDayCalenderModel.getStatus()) {
                        approvelDetailView.onSuccessStudentLeaveDetail(holiDayCalenderModel.getResult());
                    } else {
                        approvelDetailView.onUnsuccessStudentLeaveDetail(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    approvelDetailView.onUnsuccessStudentLeaveDetail(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<StudentApprovelListModel> call, Throwable t) {
                approvelDetailView.onUnsuccessStudentLeaveDetail(activity.getString(R.string.error_server));
            }
        });
    }

    private void callEmployeeApprovelApi(int id) {

        String token = UserSession.getInstance().getUserToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
            jsonObject.put(Const.KEY_ID, id);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

        Call<EmployeeApprovelListModel> getLoginOutput = ApiAdapter.getApiService().getEmployeeApprovelDetail("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<EmployeeApprovelListModel>() {
            @Override
            public void onResponse(Call<EmployeeApprovelListModel> call, Response<EmployeeApprovelListModel> response) {
                try {
                    //getting whole data from response
                    EmployeeApprovelListModel holiDayCalenderModel = response.body();
                    String message = holiDayCalenderModel.getMessage();

                    if (holiDayCalenderModel.getStatus()) {
                        approvelDetailView.onSuccessEmployeeLeaveDetail(holiDayCalenderModel.getResult());
                    } else {
                        approvelDetailView.onUnsuccessEmployeeLeaveDetail(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    approvelDetailView.onUnsuccessEmployeeLeaveDetail(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<EmployeeApprovelListModel> call, Throwable t) {
                approvelDetailView.onUnsuccessEmployeeLeaveDetail(activity.getString(R.string.error_server));
            }
        });
    }

    private void callApproveOrRejectEmpLeave(int leaveId, String leaveStatus, String comments, boolean isStudent) {

        String token = UserSession.getInstance().getUserToken();
        int employeeId = UserSession.getInstance().getEmployeeId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
            jsonObject.put(Const.PARAM_LEAVE_ID, leaveId);
            jsonObject.put(Const.PARAM_LEAVE_STATUS, leaveStatus);
            jsonObject.put(Const.PARAM_COMMENT, comments);
            jsonObject.put(Const.PARAM_LOGED_IN_USER_ID, employeeId);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

        Call<EmployeeApprovedLeaveModel> getLoginOutput = null;

        if (isStudent) {
            getLoginOutput = ApiAdapter.getApiService().studentApprovedLeave("application/json", "no-cache", body);
        } else {
            getLoginOutput = ApiAdapter.getApiService().employeeApprovedLeave("application/json", "no-cache", body);
        }

        getLoginOutput.enqueue(new Callback<EmployeeApprovedLeaveModel>() {
            @Override
            public void onResponse(Call<EmployeeApprovedLeaveModel> call, Response<EmployeeApprovedLeaveModel> response) {
                try {
                    //getting whole data from response
                    EmployeeApprovedLeaveModel holiDayCalenderModel = response.body();
                    String message = holiDayCalenderModel.getMessage();

                    if (holiDayCalenderModel.getStatus()) {
                        approvelDetailView.onSuccessApproved(message);
                    } else {
                        approvelDetailView.onUnsuccessApproved(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    approvelDetailView.onUnsuccessApproved(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<EmployeeApprovedLeaveModel> call, Throwable t) {
                approvelDetailView.onUnsuccessApproved(activity.getString(R.string.error_server));
            }
        });
    }
}
