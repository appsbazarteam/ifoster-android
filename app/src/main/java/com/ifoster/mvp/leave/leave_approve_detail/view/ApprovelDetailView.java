package com.ifoster.mvp.leave.leave_approve_detail.view;

import com.ifoster.mvp.leave.leave_approval.fragment.model.EmployeeApprovelListModel;
import com.ifoster.mvp.leave.leave_approval.fragment.model.StudentApprovelListModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ApprovelDetailView {


    /*------------Employee Approvel List------------*/
    public void onSuccessEmployeeLeaveDetail(ArrayList<EmployeeApprovelListModel.Result> results);

    public void onUnsuccessEmployeeLeaveDetail(String message);

    public void onEmployeeLeaveDetailInternetError();


    /*------------Student Approvel List------------*/
    public void onSuccessStudentLeaveDetail(ArrayList<StudentApprovelListModel.Result> results);

    public void onUnsuccessStudentLeaveDetail(String message);

    public void onStudentLeaveDetailInternetError();


    /*------------Employee Approved Result------------*/
    public void onSuccessApproved(String message);

    public void onUnsuccessApproved(String message);

    public void onApprovedInternetError();

}
