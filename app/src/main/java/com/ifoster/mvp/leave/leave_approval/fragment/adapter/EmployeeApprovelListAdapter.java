package com.ifoster.mvp.leave.leave_approval.fragment.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.utility.CircleTransform;
import com.ifoster.common.utility.Resource;
import com.ifoster.mvp.leave.leave_approval.fragment.model.EmployeeApprovelListModel;
import com.ifoster.mvp.leave.leave_approve_detail.LeaveApproveDetailActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class EmployeeApprovelListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<EmployeeApprovelListModel.Result> listData;
    boolean isRejectList;

    public EmployeeApprovelListAdapter(Context context, ArrayList<EmployeeApprovelListModel.Result> listData, boolean isRejectList) {
        this.context = context;
        this.listData = listData;
        this.isRejectList = isRejectList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_pending_leave, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ((ViewHolderList) holder).txtViewName.setText(listData.get(position).getEmpName());
        ((ViewHolderList) holder).txtViewClass.setText(listData.get(position).getLeaveType());
        ((ViewHolderList) holder).txtViewDate.setText(listData.get(position).getStartDate() +" to "+listData.get(position).getEndDate());

        leaveStatus(((EmployeeApprovelListAdapter.ViewHolderList) holder).cardViewStatus,
                ((EmployeeApprovelListAdapter.ViewHolderList) holder).txtViewStatus,
                listData.get(position).getLeaveStatus());

        ((ViewHolderList) holder).coordinateLayoutContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, LeaveApproveDetailActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString(ConstIntent.KEY_ID, listData.get(position).getId());
                bundle.putBoolean(ConstIntent.KEY_IS_STUDENT, false);
                bundle.putBoolean(ConstIntent.KEY_IS_REJECT_LIST, isRejectList);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });

        Glide.with(context).load(listData.get(position).getEmpProfile())
                .error(R.drawable.user)
                .placeholder(R.drawable.user)
                .thumbnail(0.5f)
                .crossFade()
                .transform(new CircleTransform(context))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(((EmployeeApprovelListAdapter.ViewHolderList) holder).imgProfile);

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.coordinateLayoutContainer)
        CoordinatorLayout coordinateLayoutContainer;

        @BindView(R.id.imgProfile)
        ImageView imgProfile;

        @BindView(R.id.txtViewName)
        TextView txtViewName;

        @BindView(R.id.txtViewClass)
        TextView txtViewClass;

        @BindView(R.id.txtViewDate)
        TextView txtViewDate;

        @BindView(R.id.txtViewStatus)
        TextView txtViewStatus;

        @BindView(R.id.cardViewStatus)
        CardView cardViewStatus;

        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            FontHelper.applyFont(context, txtViewName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewClass, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewDate, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewStatus, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        }
    }

    private void leaveStatus(CardView cardViewStatus, TextView txtViewStatus, String leaveStatus){
        txtViewStatus.setText(leaveStatus);
        if(Resource.toString(R.string.pending).toLowerCase().contains(leaveStatus.toLowerCase())){
            cardViewStatus.setCardBackgroundColor(Color.YELLOW);
            txtViewStatus.setText(Resource.toString(R.string.pending));
            txtViewStatus.setTextColor(Resource.toColor(R.color.color_black));
        }else if(Resource.toString(R.string.approved).toLowerCase().contains(leaveStatus.toLowerCase())){
            cardViewStatus.setCardBackgroundColor(Resource.toColor(R.color.color_32cd32));
            txtViewStatus.setText(Resource.toString(R.string.approved));
            txtViewStatus.setTextColor(Resource.toColor(R.color.color_black));
        }else if(Resource.toString(R.string.reject).toLowerCase().contains(leaveStatus.toLowerCase())){
            cardViewStatus.setCardBackgroundColor(Color.RED);
            txtViewStatus.setText(Resource.toString(R.string.reject));
            txtViewStatus.setTextColor(Resource.toColor(R.color.color_white));
        }
    }

}
