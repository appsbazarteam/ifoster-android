package com.ifoster.mvp.leave.leave_dashboard;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.leave.leave_dashboard.fragment.LeaveHistoryFragment;
import com.ifoster.mvp.leave.leave_dashboard.fragment.LeaveListFragment;
import com.ifoster.mvp.leave.leave_dashboard.fragment.LeavePlannerFragment;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;

public class LeaveDashboardActivity extends BaseActivity {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.relLayLeaveList)
    RelativeLayout relLayLeaveList;

    @BindView(R.id.relLayLeaveHistory)
    RelativeLayout relLayLeaveHistory;

    @BindView(R.id.relLayLeavePlanner)
    RelativeLayout relLayLeavePlanner;

    @BindView(R.id.relLayLeaveListIndecator)
    RelativeLayout relLayLeaveListIndecator;

    @BindView(R.id.relLayLeaveHistryIndecator)
    RelativeLayout relLayLeaveHistryIndecator;

    @BindView(R.id.relLayLeavePlanerIndecator)
    RelativeLayout relLayLeavePlanerIndecator;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @BindView(R.id.txtViewLeaveList)
    TextView txtViewLeaveList;

    @BindView(R.id.txtViewLeaveHistory)
    TextView txtViewLeaveHistory;

    @BindView(R.id.txtViewLeavePlanner)
    TextView txtViewLeavePlanner;

    @BindView(R.id.relLayTop)
    RelativeLayout relLayTop;

    @BindView(R.id.txtViewLeaveBalance)
    TextView txtViewLeaveBalance;

    //    Sick Leave
    @BindView(R.id.txtViewSickLeaveTaken)
    TextView txtViewSickLeaveTaken;

    @BindView(R.id.txtViewSickLeaveOutOf)
    TextView txtViewSickLeaveOutOf;

    @BindView(R.id.txtViewSickLeaveTotal)
    TextView txtViewSickLeaveTotal;

    @BindView(R.id.txtViewSickLeave)
    TextView txtViewSickLeave;

    //    Casual Leave
    @BindView(R.id.txtViewCasualLeaveTaken)
    TextView txtViewCasualLeaveTaken;

    @BindView(R.id.txtViewCasualLeaveOutOf)
    TextView txtViewCasualLeaveOutOf;

    @BindView(R.id.txtViewCasualLeaveTotal)
    TextView txtViewCasualLeaveTotal;

    @BindView(R.id.txtViewCasualLeave)
    TextView txtViewCasualLeave;

    @BindView(R.id.txtViewPrivillageLeaveTaken)
    TextView txtViewPrivillageLeaveTaken;

    @BindView(R.id.txtViewPrivillageLeaveOutOF)
    TextView txtViewPrivillageLeaveOutOF;

    @BindView(R.id.txtViewPrivillageLeaveTotal)
    TextView txtViewPrivillageLeaveTotal;

    @BindView(R.id.txtViewPrevillageLeave)
    TextView txtViewPrevillageLeave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_dashboard);

        getIntentData();
        setToolBar();
        createViewPager();
        txtViewLeaveBalance.setPaintFlags(txtViewLeaveBalance.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    private void getIntentData() {
        Intent intent = getIntent();

        if (intent.hasExtra(ConstIntent.KEY_USER_TYPE)) {
            if (intent.getStringExtra(ConstIntent.KEY_USER_TYPE).equals(Const.KEY_PARENTS)) {
                relLayTop.setVisibility(View.GONE);
            } else {
                relLayTop.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        FontHelper.applyFont(this, txtViewSickLeaveTaken, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewSickLeaveOutOf, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewSickLeaveTotal, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewSickLeave, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        FontHelper.applyFont(this, txtViewCasualLeaveTaken, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewCasualLeaveOutOf, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewCasualLeaveTotal, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewCasualLeave, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        FontHelper.applyFont(this, txtViewPrivillageLeaveTaken, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewPrivillageLeaveOutOF, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewPrivillageLeaveTotal, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewPrevillageLeave, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        FontHelper.applyFont(this, txtViewLeaveBalance, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewLeaveList, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewLeaveHistory, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewLeavePlanner, FontHelper.FontType.FONT_QUICKSAND_BOLD);


    }

    private void setToolBar() {
        txtViewToolbarTitle.setText(getString(R.string.title_leave_toolbar));
    }

    @OnClick(R.id.relLayLeaveList)
    public void leaveList() {
        viewPager.setCurrentItem(0, true);
        setTabIndecator(0);
    }

    @OnClick(R.id.relLayLeaveHistory)
    public void liveHistory() {
        viewPager.setCurrentItem(1, true);
        setTabIndecator(1);
    }

    @OnClick(R.id.relLayLeavePlanner)
    public void leavePlanner() {
        viewPager.setCurrentItem(2, true);
        setTabIndecator(2);
    }

    @OnClick(R.id.imgViewBack)
    public void back() {
        finish();
    }

    private void setTabIndecator(int pos) {

        if (pos == 0) {
            relLayLeaveListIndecator.setVisibility(View.VISIBLE);
            relLayLeaveHistryIndecator.setVisibility(View.GONE);
            relLayLeavePlanerIndecator.setVisibility(View.GONE);
        } else if (pos == 1) {
            relLayLeaveListIndecator.setVisibility(View.GONE);
            relLayLeaveHistryIndecator.setVisibility(View.VISIBLE);
            relLayLeavePlanerIndecator.setVisibility(View.GONE);
        } else if (pos == 2) {
            relLayLeaveListIndecator.setVisibility(View.GONE);
            relLayLeaveHistryIndecator.setVisibility(View.GONE);
            relLayLeavePlanerIndecator.setVisibility(View.VISIBLE);
        }
    }

    private void createViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new LeaveListFragment(), "Leave List");
        adapter.addFrag(new LeaveHistoryFragment(), "Leave History");
        adapter.addFrag(new LeavePlannerFragment(), "Leave Planner");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setTabIndecator(position);
            }

            @Override
            public void onPageSelected(int position) {
                setTabIndecator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
