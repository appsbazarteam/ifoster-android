package com.ifoster.mvp.leave.leave_dashboard.fragment.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeaveListModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Result")
    @Expose
    private ArrayList<Result> result = null;
    @SerializedName("Total_leave")
    @Expose
    private ArrayList<TotalLeave> totalLeave = null;
    @SerializedName("leave")
    @Expose
    private Integer leave;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }

    public ArrayList<TotalLeave> getTotalLeave() {
        return totalLeave;
    }

    public void setTotalLeave(ArrayList<TotalLeave> totalLeave) {
        this.totalLeave = totalLeave;
    }

    public Integer getLeave() {
        return leave;
    }

    public void setLeave(Integer leave) {
        this.leave = leave;
    }

    public class Result {

        @SerializedName("emp_name")
        @Expose
        private String empName;
        @SerializedName("leave_type")
        @Expose
        private String leaveType;
        @SerializedName("start_date")
        @Expose
        private String startDate;
        @SerializedName("end_date")
        @Expose
        private String endDate;
        @SerializedName("no_ofdays")
        @Expose
        private String noOfdays;
        @SerializedName("reason")
        @Expose
        private String reason;
        @SerializedName("attachment")
        @Expose
        private String attachment;
        @SerializedName("leave_status")
        @Expose
        private String leaveStatus;

        int colorType;

        public int getColorType() {
            return colorType;
        }

        public void setColorType(int colorType) {
            this.colorType = colorType;
        }

        public String getEmpName() {
            return empName;
        }

        public void setEmpName(String empName) {
            this.empName = empName;
        }

        public String getLeaveType() {
            return leaveType;
        }

        public void setLeaveType(String leaveType) {
            this.leaveType = leaveType;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getNoOfdays() {
            return noOfdays;
        }

        public void setNoOfdays(String noOfdays) {
            this.noOfdays = noOfdays;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getAttachment() {
            return attachment;
        }

        public void setAttachment(String attachment) {
            this.attachment = attachment;
        }

        public String getLeaveStatus() {
            return leaveStatus;
        }

        public void setLeaveStatus(String leaveStatus) {
            this.leaveStatus = leaveStatus;
        }

    }

    public class TotalLeave {

        @SerializedName("sick_leave")
        @Expose
        private String sickLeave;
        @SerializedName("casual_leave")
        @Expose
        private String casualLeave;
        @SerializedName("privilege_leave")
        @Expose
        private String privilegeLeave;

        public String getSickLeave() {
            return sickLeave;
        }

        public void setSickLeave(String sickLeave) {
            this.sickLeave = sickLeave;
        }

        public String getCasualLeave() {
            return casualLeave;
        }

        public void setCasualLeave(String casualLeave) {
            this.casualLeave = casualLeave;
        }

        public String getPrivilegeLeave() {
            return privilegeLeave;
        }

        public void setPrivilegeLeave(String privilegeLeave) {
            this.privilegeLeave = privilegeLeave;
        }
    }
}
