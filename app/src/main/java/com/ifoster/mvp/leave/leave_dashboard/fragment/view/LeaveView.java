package com.ifoster.mvp.leave.leave_dashboard.fragment.view;

import com.ifoster.mvp.leave.leave_dashboard.fragment.model.LeaveListHistoryModel;
import com.ifoster.mvp.leave.leave_dashboard.fragment.model.LeaveListTeacherModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface LeaveView {

    public void onSuccessLeaveList(LeaveListTeacherModel leaveListModel);

    public void onUnsuccessLeaveList(String message);

    public void onLeaveListInternetError();


    public void onSuccessLeaveListHistory(ArrayList<LeaveListHistoryModel.Result> message);

    public void onUnsuccessLeaveListHistory(String message);

    public void onLeaveListHistoryInternetError();
}
