package com.ifoster.mvp.leave.leave_approval.fragment.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ApprovelPresenter {

    void getStudentApprovelData();

    void getEmployeeApprovelData();

    void getStudentRejectLeave();

    void getEmployeeRejectLeave();
}
