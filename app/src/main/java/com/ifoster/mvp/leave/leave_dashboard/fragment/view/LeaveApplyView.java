package com.ifoster.mvp.leave.leave_dashboard.fragment.view;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface LeaveApplyView {

    public void onSuccessLeaveApply(String messages);

    public void onUnsuccessLeaveApply(String message);

    public void onLeaveApplyInternetError();

}
