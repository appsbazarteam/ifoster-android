package com.ifoster.mvp.leave.leave_dashboard.fragment.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.utility.Utils;
import com.ifoster.mvp.leave.leave_dashboard.fragment.model.LeaveListHistoryModel;
import com.ifoster.mvp.notice_board.view_more_text.ExpandableTextView;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class LeaveListHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<LeaveListHistoryModel.Result> listData;

    public LeaveListHistoryAdapter(Context context, ArrayList<LeaveListHistoryModel.Result> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_leave_list, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        LeaveListHistoryModel.Result result = listData.get(position);

        Date date = Utils.convertToSimpleDateToDateFormate(result.getStartDate());

        String dayOfTheWeek = (String) DateFormat.format("EEEE", date.getTime()); // Thursday
        String day = (String) DateFormat.format("dd", date); // 20
        String monthString = (String) DateFormat.format("MMM", date); // Jun
        String monthNumber = (String) DateFormat.format("MM", date); // 06
        String year = (String) DateFormat.format("yyyy", date); // 2013

        ((ViewHolderList) holder).txtViewDate.setText(day);
        ((ViewHolderList) holder).txtViewDay.setText(monthString);
        ((ViewHolderList) holder).txtViewLeaveTitle.setText(result.getLeaveType());
        ((ViewHolderList) holder).txtViewLeaveDesc.setText(result.getReason());
        ((ViewHolderList) holder).txtViewStatus.setText(result.getLeaveStatus());

        ((ViewHolderList) holder).txtViewStatus.setText(result.getLeaveStatus());

        String dateRange = Utils.convertToDateFormate(result.getStartDate()) + " to " + Utils.convertToDateFormate(result.getEndDate());
        ((ViewHolderList) holder).txtViewDateRange.setText(dateRange);

        // set color combination
        if (result.getColoType() == 0) {

            // set color on position zero
            ((ViewHolderList) holder).cardViewRoundDate.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color1_text));

        } else if (result.getColoType() == 1) {

            // set color on position one
            ((ViewHolderList) holder).cardViewRoundDate.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color2_text));

        } else if (result.getColoType() == 2) {

            // set color on position two
            ((ViewHolderList) holder).cardViewRoundDate.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color3_text));

        } else if (result.getColoType() == 3) {

            // set color on position three
            ((ViewHolderList) holder).cardViewRoundDate.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color4_text));
        }

        // view more click event
        ((ViewHolderList) holder).txtViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // int pos = (Integer) v.getTag();

                ((ViewHolderList) holder).txtViewLeaveDesc.performClick();
                if (((ViewHolderList) holder).txtViewLeaveDesc.getIsCollapsed()) {
                    ((ViewHolderList) holder).txtViewMore.setText("View More");
                } else {
                    ((ViewHolderList) holder).txtViewMore.setText("View Less");
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.cardViewRoundDate)
        CardView cardViewRoundDate;

        @BindView(R.id.txtViewDate)
        TextView txtViewDate;

        @BindView(R.id.txtViewDay)
        TextView txtViewDay;

        @BindView(R.id.txtViewDateRange)
        TextView txtViewDateRange;

        @BindView(R.id.txtViewLeaveTitle)
        TextView txtViewLeaveTitle;

        @BindView(R.id.txtViewLeaveDesc)
        ExpandableTextView txtViewLeaveDesc;

        @BindView(R.id.txtViewStatus)
        TextView txtViewStatus;

        @BindView(R.id.txtViewMore)
        TextView txtViewMore;

        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            FontHelper.applyFont(context, txtViewDate, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewDay, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewLeaveTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewLeaveDesc, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
            FontHelper.applyFont(context, txtViewStatus, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        }
    }
}
