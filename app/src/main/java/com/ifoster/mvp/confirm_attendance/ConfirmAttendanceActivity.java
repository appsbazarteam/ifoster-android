package com.ifoster.mvp.confirm_attendance;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.attendance_teacher.adapter.AttendanceListAdapter;
import com.ifoster.mvp.attendance_teacher.model.ClassStudentListModel;
import com.ifoster.mvp.attendance_teacher.presenter.TakeAttendancePresenterImpl;
import com.ifoster.mvp.attendance_teacher.view.TakeAttendanceView;
import com.ifoster.mvp.navigation.NavigationActivity;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.OnClick;

public class ConfirmAttendanceActivity extends BaseActivity implements TakeAttendanceView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    AttendanceListAdapter attendanceListAdapter;

    ArrayList<ClassStudentListModel.StudentArrayList> absentList;
    ArrayList<ClassStudentListModel.StudentArrayList> presentList;

    ArrayList<String> absentIdList;
    ArrayList<String> presentIdList;

    /*-------------------Class Presenter-------------------*/
    TakeAttendancePresenterImpl takeAttendancePresenterImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_attendance);

        takeAttendancePresenterImpl = new TakeAttendancePresenterImpl(this, this);
        absentIdList = new ArrayList<>();
        presentIdList = new ArrayList<>();

        setFont();

        setToolBar();

        getIntentData();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }


    @OnClick(R.id.cardViewBtnContinue)
    public void onSubmitClick() {

        ArrayList<ClassStudentListModel.StudentArrayList> absentList = attendanceListAdapter.getAbsentList();
        ArrayList<ClassStudentListModel.StudentArrayList> presentList = attendanceListAdapter.getPresentList();

        this.presentList.addAll(presentList);

        // absent id list
        if (absentList != null && absentList.size() > 0) {
            for (int i = 0; i < absentList.size(); i++) {

                absentIdList.add(absentList.get(i).getStudentId());
            }
        }

        // present id list
        if (this.presentList != null && this.presentList.size() > 0) {
            for (int i = 0; i < this.presentList.size(); i++) {

                presentIdList.add(this.presentList.get(i).getStudentId());
            }
        }

        String absentArray = Arrays.toString(absentIdList.toArray()).replace("[", "").replace("]", "");
        String presentArray = Arrays.toString(presentIdList.toArray()).replace("[", "").replace("]", "");

        // call take attendance api
        showProgressBar();
        takeAttendancePresenterImpl.takeAttendance(absentArray, presentArray, Utils.getCurrentDate());
    }

    @OnClick(R.id.imgViewBack)
    public void goBack() {
        finish();
    }

    private void getIntentData() {

        Intent intent = getIntent();

        if (intent.hasExtra(ConstIntent.KEY_ABSENT_LIST)) {

            absentList = (ArrayList<ClassStudentListModel.StudentArrayList>) intent.getSerializableExtra(ConstIntent.KEY_ABSENT_LIST);
            presentList = (ArrayList<ClassStudentListModel.StudentArrayList>) intent.getSerializableExtra(ConstIntent.KEY_PRESENT_LIST);

            setData(absentList);
        }
    }

    private void setData(ArrayList<ClassStudentListModel.StudentArrayList> absentList) {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        attendanceListAdapter = new AttendanceListAdapter(this, absentList);
        recyclerView.setAdapter(attendanceListAdapter);
    }

    private void setToolBar() {

        txtViewToolbarTitle.setText(getString(R.string.title_confirm_attendace));

    }

    @Override
    public void onSuccessClassStudent(ArrayList<ClassStudentListModel.StudentArrayList> result) {
        hideProgressBar();
        /*------------------No Use-------------------*/
    }

    @Override
    public void onUnsuccessClassStudent(String message) {
        hideProgressBar();
        /*------------------No Use-------------------*/
    }

    @Override
    public void onClassStudentInternetError() {
        hideProgressBar();
        /*------------------No Use-------------------*/
    }

    @Override
    public void onSuccessTakeAttendance(String message) {
        hideProgressBar();
        showAlert(message);
    }

    public void showAlert(String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(ConfirmAttendanceActivity.this, NavigationActivity.class);
                intent.putExtra(ConstIntent.KEY_USER_TYPE, 2);
                startActivity(intent);

                finishAffinity();
            }
        });

        alertDialog.show();

    }

    @Override
    public void onUnsuccessTakeAttendance(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onTakeAttendanceInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryAttendance, coordinateLayout);
    }

    OnClickInterface onRetryAttendance = new OnClickInterface() {
        @Override
        public void onClick() {
            onSubmitClick();
        }
    };
}
