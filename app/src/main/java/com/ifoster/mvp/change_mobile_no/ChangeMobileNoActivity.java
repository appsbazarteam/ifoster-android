package com.ifoster.mvp.change_mobile_no;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.login.LoginActivity;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class ChangeMobileNoActivity extends BaseActivity implements ChangeMobileNoView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.edtTextOldMobileNo)
    AppCompatEditText edtTextOldMobileNo;

    @BindView(R.id.edtTextNewMobileNo)
    AppCompatEditText edtTextNewMobileNo;

    @BindView(R.id.txtView_changeMobile_submitTxt)
    TextView txtView_changeMobile_submitTxt;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    private ChangeMobileNoPresenterImpl changeMobileNoPresenterImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_mobile_no);

        //ButterKnife.bind(this);

        changeMobileNoPresenterImpl = new ChangeMobileNoPresenterImpl(this, this);

        setFont();

        setToolbar();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, edtTextNewMobileNo, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, edtTextOldMobileNo, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtView_changeMobile_submitTxt, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    private void setToolbar() {

        txtViewToolbarTitle.setText(getString(R.string.title_change_mobile));
    }

    @OnEditorAction(R.id.edtTextNewMobileNo)
    public boolean setEdTextPassword(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            getData();
            return true;
        }
        return false;
    }

    @OnClick(R.id.cardViewBtnSubmit)
    public void btnSubmit() {
        getData();
    }

    @OnClick(R.id.imgViewBack)
    public void back() {
        finish();
    }

    private void getData() {

        Utils.hideKeyboardIfOpen(this);

        String oldMobileNo = edtTextOldMobileNo.getText().toString();
        String newMobileNo = edtTextNewMobileNo.getText().toString();

        if (isValid(oldMobileNo, "", newMobileNo, "")) {
            showProgressBar();
            changeMobileNoPresenterImpl.callingMobileNoApi(oldMobileNo, newMobileNo);
        }
    }

    private boolean isValid(String oldMobileNo, String oldPassword, String newMobileNo, String newPassword) {

        if (TextUtils.isEmpty(oldMobileNo)) {
            edtTextOldMobileNo.setError(getString(R.string.empty_old_mobile_no));
            edtTextOldMobileNo.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(newMobileNo)) {
            edtTextNewMobileNo.setError(getString(R.string.empty_new_mobile_no));
            edtTextNewMobileNo.requestFocus();
            return false;
        } else if (oldMobileNo.equals(newMobileNo)) {
            edtTextNewMobileNo.setError(getString(R.string.empty_old_and_new_mobile_diff));
            edtTextNewMobileNo.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onSuccessChangeMobileNo(String message) {
        hideProgressBar();
        changeMobileDialog(message);
    }

    public void changeMobileDialog(String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChangeMobileNoActivity.this);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                UserSession.getInstance().clearUserSession();

                Intent intent = new Intent(ChangeMobileNoActivity.this, LoginActivity.class);
                startActivity(intent);
                finishAffinity();
            }
        });

        alertDialog.show();

    }

    @Override
    public void onUnsuccessChangeMobileNo(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onChangeMobileNoInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryChangeMobileNo, coordinateLayout);
    }

    OnClickInterface onRetryChangeMobileNo = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };
}
