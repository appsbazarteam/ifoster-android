package com.ifoster.mvp.change_mobile_no;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ChangeMobileNoView {

    public void onSuccessChangeMobileNo(String message);

    public void onUnsuccessChangeMobileNo(String message);

    public void onChangeMobileNoInternetError();
}
