package com.ifoster.mvp.change_mobile_no;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ChangeMobileNoPresenter {
    public void callingMobileNoApi(String oldMobileNo, String newMobileNo);
}
