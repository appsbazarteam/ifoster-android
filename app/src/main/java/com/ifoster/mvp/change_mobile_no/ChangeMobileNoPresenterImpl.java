package com.ifoster.mvp.change_mobile_no;

import android.app.Activity;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class ChangeMobileNoPresenterImpl implements ChangeMobileNoPresenter {

    Activity activity;
    ChangeMobileNoView changeMobileNoView;
    JSONObject jsonObject;

    public ChangeMobileNoPresenterImpl(Activity activity, ChangeMobileNoView changeMobileNoView) {
        this.activity = activity;
        this.changeMobileNoView = changeMobileNoView;
    }

    @Override
    public void callingMobileNoApi(String oldMobileNo, String newMobileNo) {
        try {
            ApiAdapter.getInstance(activity);
            changeMobileNo(oldMobileNo, newMobileNo);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            changeMobileNoView.onChangeMobileNoInternetError();
        }
    }

    private void changeMobileNo(String oldMobileNo, String newMobileNo) {

        String token = UserSession.getInstance().getUserToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_OLD_MOBILE_NO, oldMobileNo);
            jsonObject.put(Const.PARAM_NEW_MOBILE_NO, newMobileNo);
            jsonObject.put(Const.PARAM_USER_TOKEN, token);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<ChangeMobileNoModel> getLoginOutput = ApiAdapter.getApiService().changeMobileNo("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<ChangeMobileNoModel>() {
            @Override
            public void onResponse(Call<ChangeMobileNoModel> call, Response<ChangeMobileNoModel> response) {

                try {
                    //getting whole data from response
                    ChangeMobileNoModel commonResponseModel = response.body();
                    String message = commonResponseModel.getMessage();

                    if (commonResponseModel.getStatus()) {
                        changeMobileNoView.onSuccessChangeMobileNo(message);
                    } else {
                        changeMobileNoView.onSuccessChangeMobileNo(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    changeMobileNoView.onSuccessChangeMobileNo(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ChangeMobileNoModel> call, Throwable t) {
                changeMobileNoView.onSuccessChangeMobileNo(activity.getString(R.string.error_server));
            }
        });
    }


}
