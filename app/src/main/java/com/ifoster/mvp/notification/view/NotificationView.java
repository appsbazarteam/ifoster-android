package com.ifoster.mvp.notification.view;

import com.ifoster.mvp.notification.model.NotificationModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface NotificationView {

    public void onSuccessNotificationList(ArrayList<NotificationModel.Datum> message);

    public void onUnsuccessNotificationList(String message);

    public void onNotificationListInternetError();


    public void onSuccessReadNotification(String message);

    public void onUnsuccessReadNotification(String message);

    public void onReadNotificationInternetError();
}
