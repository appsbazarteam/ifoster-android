package com.ifoster.mvp.notification.presenter;

import android.content.Context;

import com.ifoster.App;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.notification.model.NotificationModel;
import com.ifoster.mvp.notification.view.NotificationView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class NotificationPresenterImpl implements NotificationPresenter {

    private static final String PARAM_TEACHER = "Teacher ";
    private static final String PARAM_PARENT = "Parent";

    private Context activity;
    private NotificationView notificationView;
    private JSONObject jsonObject;

    public NotificationPresenterImpl(Context activity, NotificationView notificationView) {
        this.activity = activity;
        this.notificationView = notificationView;
    }

    @Override
    public void getNotificationList() {

        try {
            ApiAdapter.getInstance(activity);
            callNotificationApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            notificationView.onNotificationListInternetError();
        }

    }

    @Override
    public void readNotification(int notificationId) {

        try {
            ApiAdapter.getInstance(activity);
            readNotificationApi(notificationId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            notificationView.onReadNotificationInternetError();
        }
    }


    private void callNotificationApi() {

        try {
            jsonObject = new JSONObject();
            if(App.getApplicationInstance().isShowParentTab()){
                jsonObject.put(Const.PARAM_USER_ID, UserSession.getInstance().getStudentId());
                jsonObject.put(Const.PARAM_TYPE, PARAM_PARENT);
            }else{
                jsonObject.put(Const.PARAM_USER_ID, UserSession.getInstance().getEmployeeId());
                jsonObject.put(Const.PARAM_TYPE, PARAM_TEACHER);
            }

            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<NotificationModel> getLoginOutput = ApiAdapter.getApiService().getNotificationList("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<NotificationModel>() {
            @Override
            public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {
                try {

                    //getting whole data from response
                    NotificationModel eventListModel = response.body();
                    String message = eventListModel.getMessage();

                    if(eventListModel != null){
                        if (eventListModel.getStatus()) {
                            if(eventListModel.getData() != null && eventListModel.getData().size()>0){
                                notificationView.onSuccessNotificationList(eventListModel.getData());
                            }else{
                                notificationView.onUnsuccessNotificationList(message);
                            }
                        } else {
                            notificationView.onUnsuccessNotificationList(message);
                        }
                    }else{
                        notificationView.onUnsuccessNotificationList(activity.getString(R.string.error_server));
                    }

                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    notificationView.onUnsuccessNotificationList(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<NotificationModel> call, Throwable t) {
                notificationView.onUnsuccessNotificationList(activity.getString(R.string.error_server));
            }
        });
    }

    private void readNotificationApi(int notificationId) {

        String token = UserSession.getInstance().getUserToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_NOTIFICATION_ID, notificationId);
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<ResponseBody> getLoginOutput = ApiAdapter.getApiService().readNotification("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                //Progress.stop();

                /*try {

                    //getting whole data from response
                    EventListModel eventListModel = response.body();
                    String message = eventListModel.getMessage();

                    if (eventListModel.getStatus()) {
                        noticeView.onSuccessEventList(eventListModel.getResult());
                    } else {
                        noticeView.onUnsuccessEventList(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    noticeView.onUnsuccessEventList(activity.getString(R.string.error_server));
                }*/
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                notificationView.onUnsuccessReadNotification(activity.getString(R.string.error_server));
            }
        });
    }


}
