package com.ifoster.mvp.notification.adapter;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.mvp.notice_board.view_more_text.ExpandableTextViewWith10Line;
import com.ifoster.mvp.notification.model.NotificationModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alam on 15-May-18.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolderList> {

    Activity activity;
    ArrayList<NotificationModel.Datum> listData;

    public NotificationAdapter(Activity activity, ArrayList<NotificationModel.Datum> listData) {

        this.activity = activity;
        this.listData = listData;
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public ViewHolderList onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(activity).inflate(R.layout.item_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolderList holder, int position) {

        final NotificationModel.Datum data = listData.get(position);

        holder.txtViewNotification.setText(data.getMessage());
        holder.txtViewDate.setText(data.getCreatedAt());

        // view more click event
        holder.txtViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.txtViewNotification.performClick();
                if (holder.txtViewNotification.getIsCollapsed()) {
                    holder.txtViewMore.setText("View More");
                } else {
                    holder.txtViewMore.setText("View Less");
                }
            }
        });

        // set color combination
        if (data.getColoType() == 0) {

            // set color on position zero
            holder.relLayMainContainer.setBackgroundColor(ContextCompat.getColor(activity, R.color.color1));

        } else if (data.getColoType() == 1) {

            // set color on position one
            holder.relLayMainContainer.setBackgroundColor(ContextCompat.getColor(activity, R.color.color2));

        } else if (data.getColoType() == 2) {

            // set color on position two
            holder.relLayMainContainer.setBackgroundColor(ContextCompat.getColor(activity, R.color.color3));

        } else if (data.getColoType() == 3) {

            // set color on position three
            holder.relLayMainContainer.setBackgroundColor(ContextCompat.getColor(activity, R.color.color4));
        }

        holder.relLayMainContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* Intent intent = new Intent(activity, TaskDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(ConstIntent.KEY_TASK_DETAIL, user);
                bundle.putInt(ConstIntent.KEY_TASK_TYPE, 1);
                intent.putExtras(bundle);
                activity.startActivity(intent);*/
            }
        });
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayMainContainer)
        RelativeLayout relLayMainContainer;

        @BindView(R.id.txtViewDate)
        TextView txtViewDate;

        @BindView(R.id.txtViewNotification)
        ExpandableTextViewWith10Line txtViewNotification;

        @BindView(R.id.txtViewMore)
        TextView txtViewMore;

        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
