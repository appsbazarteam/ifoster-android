package com.ifoster.mvp.notification;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.notification.adapter.NotificationAdapter;
import com.ifoster.mvp.notification.model.NotificationModel;
import com.ifoster.mvp.notification.presenter.NotificationPresenterImpl;
import com.ifoster.mvp.notification.view.NotificationView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class NotificationActivity extends BaseActivity implements NotificationView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;


    /*-------Class Presenter------*/
    NotificationPresenterImpl notificationPresenterImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        txtViewToolbarTitle.setText("Notification");

        notificationPresenterImpl = new NotificationPresenterImpl(this, this);

        getNotificationData();
    }

    private void getNotificationData() {
        showProgressBar();
        notificationPresenterImpl.getNotificationList();
    }

    @OnClick(R.id.imgViewBack)
    public void back() {
        finish();
    }

    @Override
    public void onSuccessNotificationList(ArrayList<NotificationModel.Datum> result) {
        hideProgressBar();
        if (result != null) {

            int colorCodeType = 0;

            if (result.size() > 0) {

               for (NotificationModel.Datum user : result) {

                    // set color
                    user.setColoType(colorCodeType);

                    // increase color code value
                    colorCodeType++;

                    // reset color form strating again
                    if (colorCodeType == 4) {
                        colorCodeType = 0;
                    }
                }
            }

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            NotificationAdapter completedTaskAdapter = new NotificationAdapter(this, result);
            recyclerView.setAdapter(completedTaskAdapter);
        }

    }

    @Override
    public void onUnsuccessNotificationList(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onNotificationListInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryNotification, coordinateLayout);
    }

    OnClickInterface onRetryNotification = new OnClickInterface() {
        @Override
        public void onClick() {
            getNotificationData();
        }
    };

    @Override
    public void onSuccessReadNotification(String message) {
        hideProgressBar();
    }

    @Override
    public void onUnsuccessReadNotification(String message) {
        hideProgressBar();
    }

    @Override
    public void onReadNotificationInternetError() {
        hideProgressBar();
    }
}
