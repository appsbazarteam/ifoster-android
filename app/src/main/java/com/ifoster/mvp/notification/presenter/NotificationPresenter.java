package com.ifoster.mvp.notification.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface NotificationPresenter {

    public void getNotificationList();

    public void readNotification(int notificationId);
}
