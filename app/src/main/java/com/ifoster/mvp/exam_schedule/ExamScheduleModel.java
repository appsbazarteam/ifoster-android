package com.ifoster.mvp.exam_schedule;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExamScheduleModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Result")
    @Expose
    private ArrayList<Result> result = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }


    public class Result {

        @SerializedName("class")
        @Expose
        private String _class;
        @SerializedName("section")
        @Expose
        private String section;
        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("exam_date")
        @Expose
        private String examDate;
        @SerializedName("emp_name")
        @Expose
        private String empName;
        @SerializedName("Time")
        @Expose
        private String time;
        int colorType;

        public int getColorType() {
            return colorType;
        }

        public void setColorType(int colorType) {
            this.colorType = colorType;
        }

        public String getDate() {
            return examDate;
        }

        public void setDate(String date) {
            this.examDate = date;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

    }

}