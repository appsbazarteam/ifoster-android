package com.ifoster.mvp.exam_schedule;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.utility.DateTimeUtils;

import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class ExamScheduleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<ExamScheduleModel.Result> listData;

    int colorCodeType = 0;

    public ExamScheduleAdapter(Context context, ArrayList<ExamScheduleModel.Result> listData) {

        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_exam_schedule, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final ExamScheduleModel.Result result = listData.get(position);

        if (result.getDate() != null) {
            //String[] date = result.getDate().split(" ");


            String date = DateTimeUtils.convertDate(result.getDate(),DateTimeUtils.DATE_FORMAT_YYYY_MM_DD,DateTimeUtils.DATE_FORMAT_DD);
            String month = DateTimeUtils.convertDate(result.getDate(),DateTimeUtils.DATE_FORMAT_YYYY_MM_DD,DateTimeUtils.DATE_FORMAT_MMM);

            if (date.length() > 0) {
                ((ViewHolderList) holder).txtViewCircleDate.setText(date);
            }

            if (month.length() > 0) {
                ((ViewHolderList) holder).txtViewCircleMonth.setText(month);
            }
        }

        if (result.getSubject() != null) {
            ((ViewHolderList) holder).txtViewExamSubject.setText(result.getSubject());
        }

        if (result.getDate() != null) {
            ((ViewHolderList) holder).txtViewExamDate.setText(DateTimeUtils.convertDate(result.getDate(),DateTimeUtils.DATE_FORMAT_YYYY_MM_DD,
                    DateTimeUtils.DATE_FORMAT_DD_MM_YYYY));
        }

        if (result.getTime() != null) {
            ((ViewHolderList) holder).txtViewExamTime.setText(result.getTime());
        }

        // set color combination
        if (result.getColorType() == 0) {
            // set color on position zero
            ((ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color1_text));

        } else if (result.getColorType() == 1) {
            // set color on position one
            ((ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color2_text));

        } else if (result.getColorType() == 2) {
            // set color on position two
            ((ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color3_text));

        } else if (result.getColorType() == 3) {
            // set color on position three
            ((ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color4_text));
        }

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.cardViewRoundType)
        CardView cardViewRoundType;

        @BindView(R.id.txtViewCircleDate)
        TextView txtViewCircleDate;

        @BindView(R.id.txtViewCircleMonth)
        TextView txtViewCircleMonth;

        @BindView(R.id.txtViewExamSubject)
        TextView txtViewExamSubject;

        @BindView(R.id.txtViewExamTime)
        TextView txtViewExamTime;

        @BindView(R.id.txtViewExamDate)
        TextView txtViewExamDate;


        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            // set font
            FontHelper.applyFont(context, txtViewCircleDate, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewCircleMonth, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewExamSubject, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewExamTime, FontHelper.FontType.FONT_QUICKSAND_REGULAR);

        }
    }
}
