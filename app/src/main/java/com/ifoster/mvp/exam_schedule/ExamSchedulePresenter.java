package com.ifoster.mvp.exam_schedule;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ExamSchedulePresenter {

    public void getExamSchedule();
}
