package com.ifoster.mvp.exam_schedule;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class ExamScheduleActivity extends BaseActivity implements ExamScheduleView {

    //    Getting the id for toolbar
    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;


    /*-----------Class Presenter---------*/
    ExamSchedulePresenterImpl examSchedulePresenterImpl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_schedule);

        examSchedulePresenterImpl = new ExamSchedulePresenterImpl(this, this);

        setFont();

        setToolbar();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        showProgressBar();
        examSchedulePresenterImpl.getExamSchedule();
    }


    @OnClick(R.id.imgViewBack)
    public void back() {
        finish();
    }

    @Override
    public void onSuccessGetExamSchedule(ArrayList<ExamScheduleModel.Result> results) {
        hideProgressBar();
        if (results != null && results.size() > 0) {

            int colorCodeType = 0;

            // if (results.size() > 0) {

            for (ExamScheduleModel.Result user : results) {

                // set color
                user.setColorType(colorCodeType);

                // increase color code value
                colorCodeType++;

                // reset color form strating again
                if (colorCodeType == 4) {
                    colorCodeType = 0;
                }
            }
            //  }

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setNestedScrollingEnabled(false);

            ExamScheduleAdapter examScheduleAdapter = new ExamScheduleAdapter(this, results);
            recyclerView.setAdapter(examScheduleAdapter);
        }
        else {

            SnackNotify.showMessage(getString(R.string.no_record_found),coordinateLayout);
        }
    }

    @Override
    public void onUnsuccessGetExamSchedule(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onGetExamScheduleInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryGetExamSchedule, coordinateLayout);
    }

    OnClickInterface onRetryGetExamSchedule = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    private void setToolbar() {

        txtViewToolbarTitle.setText(getString(R.string.title_activity_exam_schedule));
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }
}
