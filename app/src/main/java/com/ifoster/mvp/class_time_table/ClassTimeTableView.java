package com.ifoster.mvp.class_time_table;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ClassTimeTableView {

    public void onSuccessTimeTable(ArrayList<ClassTimeTableModel.Result> results);

    public void onUnsuccessTimeTable(String message);

    public void onTimeTableInternetError();
}
