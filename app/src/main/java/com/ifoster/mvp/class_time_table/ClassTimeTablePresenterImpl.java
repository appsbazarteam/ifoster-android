package com.ifoster.mvp.class_time_table;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class ClassTimeTablePresenterImpl implements ClassTimeTablePresenter {

    Activity activity;
    ClassTimeTableView classTimeTableView;
    JSONObject jsonObject;

    public ClassTimeTablePresenterImpl(Activity activity, ClassTimeTableView classTimeTableView) {
        this.activity = activity;
        this.classTimeTableView = classTimeTableView;
    }

    @Override
    public void getTimeTable(String day) {
        try {
            ApiAdapter.getInstance(activity);
            callClassTimeTableApi(day);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            classTimeTableView.onTimeTableInternetError();
        }
    }

    private void callClassTimeTableApi(String day) {

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_STUDENT_ID, UserSession.getInstance().getStudentId());
            jsonObject.put(Const.PARAM_DAY, day);
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<ClassTimeTableModel> getLoginOutput = ApiAdapter.getApiService().getClassTimeTable("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<ClassTimeTableModel>() {
            @Override
            public void onResponse(Call<ClassTimeTableModel> call, Response<ClassTimeTableModel> response) {

                try {
                    //getting whole data from response
                    ClassTimeTableModel classTimeTableModel = response.body();
                    String message = classTimeTableModel.getMessage();

                    if (classTimeTableModel.getStatus()) {
                        classTimeTableView.onSuccessTimeTable(classTimeTableModel.getResult());
                    } else {
                        classTimeTableView.onUnsuccessTimeTable(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    classTimeTableView.onUnsuccessTimeTable(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ClassTimeTableModel> call, Throwable t) {
                classTimeTableView.onUnsuccessTimeTable(activity.getString(R.string.error_server));
            }
        });
    }


}
