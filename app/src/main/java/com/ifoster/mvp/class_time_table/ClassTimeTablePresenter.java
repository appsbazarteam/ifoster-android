package com.ifoster.mvp.class_time_table;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ClassTimeTablePresenter {

    public void getTimeTable(String day);
}
