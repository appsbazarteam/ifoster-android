package com.ifoster.mvp.class_time_table;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class ClassTimeTableActivity extends BaseActivity implements ClassTimeTableView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.relLayMonday)
    RelativeLayout relLayMonday;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @BindView(R.id.relLayTuesday)
    RelativeLayout relLayTuesday;

    @BindView(R.id.relLayWednesday)
    RelativeLayout relLayWednessday;

    @BindView(R.id.relLayThursday)
    RelativeLayout relLayThursday;

    @BindView(R.id.relLayFriday)
    RelativeLayout relLayFriday;

    @BindView(R.id.relLaySaturday)
    RelativeLayout relLaySaturday;

   /* @BindView(R.id.relLaySunday)
    RelativeLayout relLaySunday;*/

    @BindView(R.id.txtViewMonday)
    TextView txtViewMonday;

    @BindView(R.id.txtViewTuesday)
    TextView txtViewTuesday;

    @BindView(R.id.txtViewWednesday)
    TextView txtViewWednesday;

    @BindView(R.id.txtViewThursday)
    TextView txtViewThursday;

    @BindView(R.id.txtViewFriday)
    TextView txtViewFriday;

    @BindView(R.id.txtViewSaturday)
    TextView txtViewSaturday;

   /* @BindView(R.id.txtViewSunday)
    TextView txtViewSunday;*/

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    String day;

    /*----------Class Presenter----------*/
    ClassTimeTablePresenterImpl classTimeTablePresenterImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_time_table);

        classTimeTablePresenterImpl = new ClassTimeTablePresenterImpl(this, this);

        setToolbar();
        setFont();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // get current day
        day = Utils.getCurrentDay();
        getData();

        // select round button
        if (day.equals("Monday")) {
            changeSelectedColor(R.id.txtViewMonday);
        } else if (day.equals("Tuesday")) {
            changeSelectedColor(R.id.txtViewTuesday);
        } else if (day.equals("Wednesday")) {
            changeSelectedColor(R.id.txtViewWednesday);
        } else if (day.equals("Thursday")) {
            changeSelectedColor(R.id.txtViewThursday);
        } else if (day.equals("Friday")) {
            changeSelectedColor(R.id.txtViewFriday);
        } else if (day.equals("Saturday")) {
            changeSelectedColor(R.id.txtViewSaturday);
        } /*else if (day.equals("Sunday")) {
            changeSelectedColor(R.id.txtViewSunday);
        }*/
    }

    @OnClick(R.id.relLayMonday)
    public void onMondayClick() {

        changeSelectedColor(R.id.txtViewMonday);
        day = "Monday";

        getData();
    }

    @OnClick(R.id.relLayTuesday)
    public void onTuesdayClick() {

        changeSelectedColor(R.id.txtViewTuesday);
        day = "Tuesday";

        getData();
    }

    @OnClick(R.id.relLayWednesday)
    public void onWednesdayClick() {

        changeSelectedColor(R.id.txtViewWednesday);
        day = "Wednesday";

        getData();
    }

    @OnClick(R.id.relLayThursday)
    public void onThursdayClick() {

        changeSelectedColor(R.id.txtViewThursday);
        day = "Thursday";

        getData();
    }

    @OnClick(R.id.relLayFriday)
    public void onFridayClick() {

        changeSelectedColor(R.id.txtViewFriday);
        day = "Friday";

        getData();
    }

    @OnClick(R.id.relLaySaturday)
    public void onSaturdayClick() {

        changeSelectedColor(R.id.txtViewSaturday);
        day = "Saturday";

        getData();
    }

    private void changeSelectedColor(int id) {

        switch (id) {

            case R.id.txtViewMonday:

                txtViewMonday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_small_circle_drawable));
                txtViewTuesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewWednesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewThursday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewFriday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewSaturday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                //txtViewSunday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));

                txtViewMonday.setTextColor(ContextCompat.getColor(this, R.color.color_white));
                txtViewTuesday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewWednesday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewThursday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewFriday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewSaturday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                //txtViewSunday.setTextColor(ContextCompat.getColor(this, R.color.color_black));

                break;

            case R.id.txtViewTuesday:

                txtViewMonday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewTuesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_small_circle_drawable));
                txtViewWednesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewThursday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewFriday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewSaturday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                //txtViewSunday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));

                txtViewMonday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewTuesday.setTextColor(ContextCompat.getColor(this, R.color.color_white));
                txtViewWednesday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewThursday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewFriday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewSaturday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                //txtViewSunday.setTextColor(ContextCompat.getColor(this, R.color.color_black));

                break;

            case R.id.txtViewWednesday:

                txtViewMonday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewTuesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewWednesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_small_circle_drawable));
                txtViewThursday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewFriday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewSaturday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                //txtViewSunday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));

                txtViewMonday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewTuesday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewWednesday.setTextColor(ContextCompat.getColor(this, R.color.color_white));
                txtViewThursday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewFriday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewSaturday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                //txtViewSunday.setTextColor(ContextCompat.getColor(this, R.color.color_black));

                break;

            case R.id.txtViewThursday:

                txtViewMonday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewTuesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewWednesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewThursday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_small_circle_drawable));
                txtViewFriday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewSaturday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                //txtViewSunday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));

                txtViewMonday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewTuesday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewWednesday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewThursday.setTextColor(ContextCompat.getColor(this, R.color.color_white));
                txtViewFriday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewSaturday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                //txtViewSunday.setTextColor(ContextCompat.getColor(this, R.color.color_black));

                break;

            case R.id.txtViewFriday:

                txtViewMonday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewTuesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewWednesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewThursday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewFriday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_small_circle_drawable));
                txtViewSaturday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                //txtViewSunday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));

                txtViewMonday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewTuesday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewWednesday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewThursday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewFriday.setTextColor(ContextCompat.getColor(this, R.color.color_white));
                txtViewSaturday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                //txtViewSunday.setTextColor(ContextCompat.getColor(this, R.color.color_black));

                break;

            case R.id.txtViewSaturday:

                txtViewMonday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewTuesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewWednesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewThursday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewFriday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewSaturday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_small_circle_drawable));
                //txtViewSunday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));

                txtViewMonday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewTuesday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewWednesday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewThursday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewFriday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewSaturday.setTextColor(ContextCompat.getColor(this, R.color.color_white));
                //txtViewSunday.setTextColor(ContextCompat.getColor(this, R.color.color_black));

                break;

            /*case R.id.txtViewSunday:

                txtViewMonday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewTuesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewWednesday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewThursday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewFriday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                txtViewSaturday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_circle_gray_drawable));
                //txtViewSunday.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_small_circle_drawable));

                txtViewMonday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewTuesday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewWednesday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewThursday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewFriday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                txtViewSaturday.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                //txtViewSunday.setTextColor(ContextCompat.getColor(this, R.color.color_white));

                break;*/
        }

    }

    @OnClick(R.id.imgViewBack)
    public void back() {
        finish();
    }

    @Override
    public void onSuccessTimeTable(ArrayList<ClassTimeTableModel.Result> results) {
        hideProgressBar();
        if (results != null && results.size() > 0) {

            int colorCodeType = 0;

            if (results.size() > 0) {

                for (ClassTimeTableModel.Result user : results) {

                    // set color
                    user.setColorType(colorCodeType);

                    // increase color code value
                    colorCodeType++;

                    // reset color form strating again
                    if (colorCodeType == 4) {
                        colorCodeType = 0;
                    }
                }
            }

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setNestedScrollingEnabled(false);

        } else {
            SnackNotify.showMessage(getString(R.string.no_record_found), coordinatorLayout);
        }

        ClassTimeTableAdapter classTimeTableAdapter = new ClassTimeTableAdapter(this, results);
        recyclerView.setAdapter(classTimeTableAdapter);
    }

    @Override
    public void onUnsuccessTimeTable(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinatorLayout);
    }

    @Override
    public void onTimeTableInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryTimeTable, coordinatorLayout);
    }

    OnClickInterface onRetryTimeTable = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };


    private void getData() {
        showProgressBar();
        classTimeTablePresenterImpl.getTimeTable(day);
    }

    private void setToolbar() {

        txtViewToolbarTitle.setText(getString(R.string.title_time_table));
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }
}
