package com.ifoster.mvp.medical_teacher;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TeacherMedicalIdModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Result")
    @Expose
    private ArrayList<Result> result = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("height")
        @Expose
        private String height;
        @SerializedName("weight")
        @Expose
        private String weight;
        @SerializedName("blood_group")
        @Expose
        private String bloodGroup;
        @SerializedName("contact")
        @Expose
        private String contact;
        @SerializedName("eye_vision")
        @Expose
        private String eyeVision;
        @SerializedName("allergy_type")
        @Expose
        private String allergyType;
        @SerializedName("allergy")
        @Expose
        private String allergy;

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getBloodGroup() {
            return bloodGroup;
        }

        public void setBloodGroup(String bloodGroup) {
            this.bloodGroup = bloodGroup;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getEyeVision() {
            return eyeVision;
        }

        public void setEyeVision(String eyeVision) {
            this.eyeVision = eyeVision;
        }

        public String getAllergyType() {
            return allergyType;
        }

        public void setAllergyType(String allergyType) {
            this.allergyType = allergyType;
        }

        public String getAllergy() {
            return allergy;
        }

        public void setAllergy(String allergy) {
            this.allergy = allergy;
        }

    }
}