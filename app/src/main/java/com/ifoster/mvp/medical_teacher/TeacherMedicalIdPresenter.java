package com.ifoster.mvp.medical_teacher;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface TeacherMedicalIdPresenter {

    void getMedicalIdDetail();

    void updateMedicalDetail(String height , String weight,String bloodGroup,String contact,
                             String leftEye,String rightEye,String allergyType,String allergy);
}
