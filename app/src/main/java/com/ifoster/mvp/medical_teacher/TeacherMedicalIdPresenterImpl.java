package com.ifoster.mvp.medical_teacher;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.medical.MedicalIdModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class TeacherMedicalIdPresenterImpl implements TeacherMedicalIdPresenter {

    Activity activity;
    TeacherMedicalIdView medicalIdView;
    JSONObject jsonObject;

    public TeacherMedicalIdPresenterImpl(Activity activity, TeacherMedicalIdView medicalIdView) {
        this.activity = activity;
        this.medicalIdView = medicalIdView;
    }

    @Override
    public void getMedicalIdDetail() {
        try {
            ApiAdapter.getInstance(activity);
            callMedicalDetailId();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            medicalIdView.onMedicalIdInternetError();
        }
    }

    @Override
    public void updateMedicalDetail(String height, String weight, String bloodGroup, String contact, String leftEye,
                                    String rightEye, String allergyType, String allergy) {

        try {
            ApiAdapter.getInstance(activity);
            callMedicalDetailIdUpdate(height, weight, bloodGroup, contact, leftEye, rightEye, allergyType, allergy);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            medicalIdView.onMedicalIdInternetError();
        }

    }

    private void callMedicalDetailId() {
        String token = UserSession.getInstance().getUserToken();
        int studentId = UserSession.getInstance().getEmployeeId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_TEACHER_ID, studentId);
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<MedicalIdModel> getLoginOutput = ApiAdapter.getApiService().getTeacherMedicalId("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<MedicalIdModel>() {
            @Override
            public void onResponse(Call<MedicalIdModel> call, Response<MedicalIdModel> response) {
                try {
                    //getting whole data from response
                    MedicalIdModel examScheduleModel = response.body();
                    String message = examScheduleModel.getMessage();

                    if (examScheduleModel.getStatus()) {
                        medicalIdView.onSuccessMedicalId(examScheduleModel.getResult());
                    } else {
                        medicalIdView.onUnsuccessMedicalId(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    medicalIdView.onUnsuccessMedicalId(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<MedicalIdModel> call, Throwable t) {
                medicalIdView.onUnsuccessMedicalId(activity.getString(R.string.error_server));
            }
        });
    }


    private void callMedicalDetailIdUpdate(String height, String weight, String bloodGroup, String contact, String leftEye, String rightEye, String allergyType, String allergy) {

        String token = UserSession.getInstance().getUserToken();
        int studentId = UserSession.getInstance().getEmployeeId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_TEACHER_ID, studentId);
            jsonObject.put(Const.PARAM_HEIGHT, height);
            jsonObject.put(Const.PARAM_WEIGHT, weight);
            jsonObject.put(Const.PARAM_BLOOD_GROUP, bloodGroup);
            jsonObject.put(Const.PARAM_CONTACT, contact);
            jsonObject.put(Const.PARAM_LEFT_EYE, leftEye);
            jsonObject.put(Const.PARAM_RIGHT_EYE, rightEye);
            jsonObject.put(Const.PARAM_ALLERGY_TYPE, allergyType);
            jsonObject.put(Const.PARAM_ALLERGY, allergy);
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<UpdateFamilyIdlModel> getLoginOutput = ApiAdapter.getApiService().updateTeacherMedicalId("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<UpdateFamilyIdlModel>() {
            @Override
            public void onResponse(Call<UpdateFamilyIdlModel> call, Response<UpdateFamilyIdlModel> response) {

                try {
                    //getting whole data from response
                    UpdateFamilyIdlModel examScheduleModel = response.body();
                    String message = examScheduleModel.getMessage();

                    if (examScheduleModel.getStatus()) {
                        medicalIdView.onSuccessMedicalIdUpdate(examScheduleModel.getMessage());
                    } else {
                        medicalIdView.onUnsuccessMedicalIdUpdate(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    medicalIdView.onUnsuccessMedicalIdUpdate(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<UpdateFamilyIdlModel> call, Throwable t) {
                medicalIdView.onUnsuccessMedicalIdUpdate(activity.getString(R.string.error_server));
            }
        });
    }

}
