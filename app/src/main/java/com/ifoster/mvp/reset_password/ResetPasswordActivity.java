package com.ifoster.mvp.reset_password;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.login.LoginActivity;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class ResetPasswordActivity extends BaseActivity implements ResetPasswordView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.edtTextPassword)
    AppCompatEditText edtTextPassword;

    @BindView(R.id.edtTextCnfrmPassword)
    AppCompatEditText edtTextCnfrmPassword;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;


    /*---------Class Presenter ------------*/
    ResetPasswordPresenterImpl resetPasswordPresenterImpl;

    /*--------UserSession---------*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        resetPasswordPresenterImpl = new ResetPasswordPresenterImpl(this, this);


        setFont();

        setToolbar();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    private void setToolbar() {

        txtViewToolbarTitle.setText(getString(R.string.title_change_password));
    }


    private void getData() {

        Utils.hideKeyboardIfOpen(this);

        //String mobileNo = edtTextMobileNo.getText().toString();
        String password = edtTextPassword.getText().toString();
        String cnfrmPass = edtTextCnfrmPassword.getText().toString();

        if (isValid(password, cnfrmPass)) {
            showProgressBar();
            resetPasswordPresenterImpl.callingResetPasswordApi(UserSession.getInstance().getMobileNo(), cnfrmPass);
        }

    }

    private boolean isValid(String password, String cnfrmPassword) {

        if (TextUtils.isEmpty(password)) {
            edtTextPassword.setError(getString(R.string.empty_pass));
            edtTextPassword.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(cnfrmPassword)) {
            edtTextCnfrmPassword.setError(getString(R.string.empty_cnfrm_pass));
            edtTextCnfrmPassword.requestFocus();
            return false;
        } else if (!password.equals(cnfrmPassword)) {
            edtTextCnfrmPassword.setError(getString(R.string.valid_pass_cpass_same));
            edtTextCnfrmPassword.requestFocus();
            return false;
        }

        return true;
    }

    @OnClick(R.id.cardViewBtnContinue)
    public void submit() {

        getData();
    }

    @OnClick(R.id.imgViewBack)
    public void back() {
        finish();
    }


    @OnEditorAction(R.id.edtTextCnfrmPassword)
    public boolean setEdTextPassword(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            getData();
            return true;
        }
        return false;
    }

    @Override
    public void onSuccessResetPassword(String message) {
        hideProgressBar();
        resetPassDialog(message);
    }

    public void resetPassDialog(String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ResetPasswordActivity.this);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                UserSession.getInstance().clearUserSession();

                Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                startActivity(intent);
                finishAffinity();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onUnsuccessResetPassword(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onResetPasswordInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryResetPassword, coordinateLayout);
    }

    OnClickInterface onRetryResetPassword = new OnClickInterface() {
        @Override
        public void onClick() {

            getData();
        }
    };
}
