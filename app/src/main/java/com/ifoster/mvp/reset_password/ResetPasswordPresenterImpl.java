package com.ifoster.mvp.reset_password;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class ResetPasswordPresenterImpl implements ResetPasswordPresenter {

    Activity activity;
    ResetPasswordView resetPasswordView;
    JSONObject jsonObject;

    public ResetPasswordPresenterImpl(Activity activity, ResetPasswordView resetPasswordView) {
        this.activity = activity;
        this.resetPasswordView = resetPasswordView;
    }

    @Override
    public void callingResetPasswordApi(String mobileNo, String newPassword) {
        try {
            ApiAdapter.getInstance(activity);
            resetPassword(mobileNo, newPassword);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            resetPasswordView.onResetPasswordInternetError();
        }
    }

    private void resetPassword(String mobileNo, String password) {

        String token = UserSession.getInstance().getUserToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_MOBILE_NO, mobileNo);
            jsonObject.put(Const.PARAM_TXT_PASSWORD, password);
            jsonObject.put(Const.PARAM_USER_TOKEN, token);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<ResetPasswordModel> getLoginOutput = ApiAdapter.getApiService().resetPassword("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<ResetPasswordModel>() {
            @Override
            public void onResponse(Call<ResetPasswordModel> call, Response<ResetPasswordModel> response) {

                try {
                    //getting whole data from response
                    ResetPasswordModel commonResponseModel = response.body();
                    String message = commonResponseModel.getMessage();

                    if (commonResponseModel.getStatus()) {

                        resetPasswordView.onSuccessResetPassword(message);
                    } else {
                        resetPasswordView.onUnsuccessResetPassword(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    resetPasswordView.onUnsuccessResetPassword(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordModel> call, Throwable t) {
                resetPasswordView.onUnsuccessResetPassword(activity.getString(R.string.error_server));
            }
        });
    }


}
