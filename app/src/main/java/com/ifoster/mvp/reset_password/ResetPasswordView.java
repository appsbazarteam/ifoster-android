package com.ifoster.mvp.reset_password;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ResetPasswordView {

    public void onSuccessResetPassword(String message);

    public void onUnsuccessResetPassword(String message);

    public void onResetPasswordInternetError();
}
