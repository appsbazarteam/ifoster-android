package com.ifoster.mvp.reset_password;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ResetPasswordPresenter {
    public void callingResetPasswordApi(String mobileNo, String newPassword);
}
