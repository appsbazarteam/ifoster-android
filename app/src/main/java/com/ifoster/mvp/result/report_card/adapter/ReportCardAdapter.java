package com.ifoster.mvp.result.report_card.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.mvp.result.report_card.ReportCardModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class ReportCardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<ReportCardModel> listData;

    public ReportCardAdapter(Context context, ArrayList<ReportCardModel> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_report_card, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ((ViewHolderList) holder).txtViewSubjectName.setText(listData.get(position).getSubjectName());

        // hide/show month list
        if (listData.get(position).isSubjectExpand()) {
            ((ViewHolderList) holder).linLayTopics.setVisibility(View.VISIBLE);
            ((ViewHolderList) holder).imgViewPlus.setImageResource(R.mipmap.ic_minus);
        } else {
            ((ViewHolderList) holder).linLayTopics.setVisibility(View.GONE);
            ((ViewHolderList) holder).imgViewPlus.setImageResource(R.mipmap.ic_plus);
        }


        // add absent child
        if (listData.get(position).getArrayListTopic().size() > 0) {

            // clear all previous children view
            ((ViewHolderList) holder).linLayTopicsContainer.removeAllViews();

            for (int i = 0; i < listData.get(position).getArrayListTopic().size(); i++) {

                LayoutInflater inflater = null;
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mLinearView = inflater.inflate(R.layout.item_topics, null);

                final TextView txtViewTopicName = (TextView) mLinearView.findViewById(R.id.txtViewTopicName);
                final TextView txtViewGrade = (TextView) mLinearView.findViewById(R.id.txtViewGrade);
                final AppCompatEditText edtTextRemark = (AppCompatEditText) mLinearView.findViewById(R.id.edtTextRemark);

                // set Font
                FontHelper.applyFont(context, txtViewTopicName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
                FontHelper.applyFont(context, txtViewGrade, FontHelper.FontType.FONT_QUICKSAND_BOLD);
                FontHelper.applyFont(context, edtTextRemark, FontHelper.FontType.FONT_QUICKSAND_BOLD);

                // set value
                txtViewTopicName.setText(listData.get(position).getArrayListTopic().get(i).getTopicName());
                txtViewGrade.setText(listData.get(position).getArrayListTopic().get(i).getGrade());

                if (listData.get(position).getArrayListTopic().get(i).getRemark() != null && listData.get(position).getArrayListTopic().get(i).getRemark().length() > 0) {

                    edtTextRemark.setHint(listData.get(position).getArrayListTopic().get(i).getRemark());
                    edtTextRemark.setVisibility(View.VISIBLE);

                } else {
                    edtTextRemark.setVisibility(View.GONE);
                }

                // click listener
                edtTextRemark.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        edtTextRemark.requestFocus();
                    }
                });

                ((ViewHolderList) holder).linLayTopicsContainer.addView(mLinearView);
            }
        }

        // parent item click
        ((ViewHolderList) holder).cardViewExamName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listData.get(position).isSubjectExpand()) {
                    listData.get(position).setSubjectExpand(false);

                    for (int i = 0; i < listData.size(); i++) {
                        listData.get(i).setSubjectExpand(false);
                    }

                } else {
                    listData.get(position).setSubjectExpand(true);

                    for (int i = 0; i < listData.size(); i++) {
                        if (position != i) {
                            listData.get(i).setSubjectExpand(false);
                        }
                    }
                }
                notifyDataSetChanged();
            }
        });


    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        // parent view
        @BindView(R.id.cardViewExamName)
        CardView cardViewExamName;

        @BindView(R.id.txtGrade)
        TextView txtGrade;

        @BindView(R.id.txtRemark)
        TextView txtRemark;

        @BindView(R.id.txtViewSubjectName)
        TextView txtViewSubjectName;

        @BindView(R.id.linLayTopics)
        LinearLayout linLayTopics;

        @BindView(R.id.linLayTopicsContainer)
        LinearLayout linLayTopicsContainer;

        @BindView(R.id.imgViewPlus)
        ImageView imgViewPlus;


        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            FontHelper.applyFont(context, txtViewSubjectName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtGrade, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtRemark, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewSubjectName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        }
    }


}
