package com.ifoster.mvp.result.exam_list.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.mvp.result.exam_list.ResultModel;
import com.ifoster.mvp.result.report_card.ReportCardActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class ResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<ResultModel> listData;

    public ResultAdapter(Context context, ArrayList<ResultModel> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_result, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ((ViewHolderList) holder).txtViewExamName.setText(listData.get(position).getExamName());
        ((ViewHolderList) holder).txtViewExamShortName.setText(listData.get(position).getExamShortName());

        // set color combination
        if (listData.get(position).getColorType() == 0) {

            // set color on position zero
            ((ViewHolderList) holder).cardViewRoundExamShortName.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color1_text));

        } else if (listData.get(position).getColorType() == 1) {

            // set color on position one
            ((ViewHolderList) holder).cardViewRoundExamShortName.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color2_text));

        } else if (listData.get(position).getColorType() == 2) {

            // set color on position two
            ((ViewHolderList) holder).cardViewRoundExamShortName.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color3_text));

        } else if (listData.get(position).getColorType() == 3) {

            // set color on position three
            ((ViewHolderList) holder).cardViewRoundExamShortName.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color4_text));
        }

        ((ViewHolderList) holder).relLayContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ReportCardActivity.class);
                intent.putExtra(ConstIntent.KEY_EXAM_NAME, listData.get(position).getExamName());
                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayContainer)
        RelativeLayout relLayContainer;

        @BindView(R.id.cardViewRoundExamShortName)
        CardView cardViewRoundExamShortName;

        @BindView(R.id.txtViewExamShortName)
        TextView txtViewExamShortName;

        @BindView(R.id.txtViewExamName)
        TextView txtViewExamName;


        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            FontHelper.applyFont(context, txtViewExamShortName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewExamName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        }
    }

}
