package com.ifoster.mvp.result.report_card;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.result.report_card.adapter.ReportCardAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class ReportCardActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;


    @BindView(R.id.txtViewName)
    TextView txtViewName;

    @BindView(R.id.txtViewClass)
    TextView txtViewClass;

    @BindView(R.id.txtViewExamName)
    TextView txtViewExamName;

    @BindView(R.id.txtViewResultStatusTitle)
    TextView txtViewResultStatusTitle;

    @BindView(R.id.txtViewResultStatus)
    TextView txtViewResultStatus;

    @BindView(R.id.txtViewRemarkTitle)
    TextView txtViewRemarkTitle;

    @BindView(R.id.txtViewRemark)
    TextView txtViewRemark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_card);

        setFont();

        setToolbar();

        getIntentData();

        // set underline to text
        // txtViewGradeDetailTitle.setPaintFlags(txtViewGradeDetailTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtViewExamName.setPaintFlags(txtViewExamName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewClass, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewExamName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewResultStatusTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewResultStatus, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewRemarkTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewRemark, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    private void getIntentData() {

        Intent intent = getIntent();

        if (intent.hasExtra(ConstIntent.KEY_EXAM_NAME)) {

            txtViewExamName.setText(intent.getStringExtra(ConstIntent.KEY_EXAM_NAME));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        ArrayList<ReportCardModel> examList = getSubjectList();

        if (examList.size() > 0) {

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setNestedScrollingEnabled(false);

            ReportCardAdapter resultAdapter = new ReportCardAdapter(this, examList);
            recyclerView.setAdapter(resultAdapter);

        }
    }

    private ArrayList<ReportCardModel> getSubjectList() {

        ArrayList<ReportCardModel> subjectList = new ArrayList<>();

        for (int i = 1; i <= 5; i++) {

            ArrayList<ReportCardModel.Topic> topics = new ArrayList<>();

            ReportCardModel reportCardModel = new ReportCardModel();

            if (i == 1) {

                reportCardModel.setSubjectName("Mathematics");

                for (int j = 0; j < 3; j++) {

                    ReportCardModel.Topic topic = reportCardModel.new Topic();
                    topic.setTopicName("Concept & Logic");
                    topic.setGrade("A");
                    // topic.setRemark("Remark");

                    if (j == 1) {
                        topic.setTopicName("Mental Ability");
                        topic.setGrade("B");
                    }

                    if (j == 2) {
                        topic.setTopicName("Solving skill");
                        topic.setGrade("B");
                        topic.setRemark("Remark");
                    }

                    topics.add(topic);

                    reportCardModel.setArrayListTopic(topics);
                }
            }
            if (i == 2) {
                reportCardModel.setSubjectName("English");

                for (int j = 0; j < 7; j++) {

                    ReportCardModel.Topic topic = reportCardModel.new Topic();
                    topic.setTopicName("Reading");
                    topic.setGrade("B");


                    if (j == 1) {
                        topic.setTopicName("Listening Skill");
                        topic.setGrade("C");
                    }

                    if (j == 2) {
                        topic.setTopicName("Conversation");
                        topic.setGrade("B");
                    }

                    if (j == 3) {
                        topic.setTopicName("Recitation");
                        topic.setGrade("A");
                    }

                    if (j == 4) {
                        topic.setTopicName("Handwriting");
                        topic.setGrade("C");
                    }


                    if (j == 5) {
                        topic.setTopicName("Grammar");
                        topic.setGrade("B");
                    }

                    if (j == 6) {
                        topic.setTopicName("Vocabulary");
                        topic.setGrade("B");
                        topic.setRemark("Remark");
                    }

                    topics.add(topic);

                    reportCardModel.setArrayListTopic(topics);
                }
            }

            if (i == 3) {
                reportCardModel.setSubjectName("Hindi");

                for (int j = 0; j < 7; j++) {

                    ReportCardModel.Topic topic = reportCardModel.new Topic();
                    topic.setTopicName("Reading");
                    topic.setGrade("B");


                    if (j == 1) {
                        topic.setTopicName("Listening Skill");
                        topic.setGrade("A");
                    }

                    if (j == 2) {
                        topic.setTopicName("Conversation");
                        topic.setGrade("B");
                    }

                    if (j == 3) {
                        topic.setTopicName("Recitation");
                        topic.setGrade("A");
                    }

                    if (j == 4) {
                        topic.setTopicName("Handwriting");
                        topic.setGrade("B");
                    }


                    if (j == 5) {
                        topic.setTopicName("Grammar");
                        topic.setGrade("B");
                    }

                    if (j == 6) {
                        topic.setTopicName("Vocabulary");
                        topic.setGrade("B");
                        topic.setRemark("Remark");
                    }

                    topics.add(topic);

                    reportCardModel.setArrayListTopic(topics);
                }
            }

            if (i == 4) {
                reportCardModel.setSubjectName("Environmental  Sensitivity");

                for (int j = 0; j < 2; j++) {

                    ReportCardModel.Topic topic = reportCardModel.new Topic();
                    topic.setTopicName("Reading");
                    topic.setGrade("B");

                    if (j == 0) {
                        topic.setTopicName("Activity / Project");
                        topic.setGrade("A");
                    }

                    if (j == 1) {
                        topic.setTopicName("Content Learning");
                        topic.setGrade("B");
                        topic.setRemark("Remark");
                    }


                    topics.add(topic);

                    reportCardModel.setArrayListTopic(topics);
                }
            }

            if (i == 5) {
                reportCardModel.setSubjectName("Co-curricular Activities");

                for (int j = 0; j < 4; j++) {

                    ReportCardModel.Topic topic = reportCardModel.new Topic();
                    topic.setTopicName("Art & Craft");
                    topic.setGrade("B");

                    if (j == 1) {
                        topic.setTopicName("Music");
                        topic.setGrade("A");
                    }

                    if (j == 2) {
                        topic.setTopicName("Dance");
                        topic.setGrade("A");
                    }

                    if (j == 3) {
                        topic.setTopicName("Physical Education");
                        topic.setGrade("B");
                        topic.setRemark("Remark");
                    }


                    topics.add(topic);

                    reportCardModel.setArrayListTopic(topics);
                }
            }

            subjectList.add(reportCardModel);
        }

        return subjectList;
    }


    private void setToolbar() {

        txtViewToolbarTitle.setText(getString(R.string.title_report_card));
    }

    @OnClick(R.id.imgViewBack)
    public void back() {
        finish();
    }

}
