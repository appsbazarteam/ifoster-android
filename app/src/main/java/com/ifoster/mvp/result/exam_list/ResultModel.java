package com.ifoster.mvp.result.exam_list;

/**
 * Created by Alam on 22-Oct-17.
 */

public class ResultModel {

    public String examName;
    public String examShortName;
    public int colorType;

    public int getColorType() {
        return colorType;
    }

    public void setColorType(int colorType) {
        this.colorType = colorType;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getExamShortName() {
        return examShortName;
    }

    public void setExamShortName(String examShortName) {
        this.examShortName = examShortName;
    }
}
