package com.ifoster.mvp.result.report_card;

import java.util.ArrayList;

/**
 * Created by Alam on 22-Oct-17.
 */

public class ReportCardModel {

    public String subjectName;

    public boolean isSubjectExpand;

    public boolean isSubjectExpand() {
        return isSubjectExpand;
    }

    public void setSubjectExpand(boolean subjectExpand) {
        isSubjectExpand = subjectExpand;
    }

    public ArrayList<Topic> arrayListTopic;

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public ArrayList<Topic> getArrayListTopic() {
        return arrayListTopic;
    }

    public void setArrayListTopic(ArrayList<Topic> arrayListTopic) {
        this.arrayListTopic = arrayListTopic;
    }

    public class Topic {

        public String topicName;
        public String grade;
        public String remark;

        public String getGrade() {
            return grade;
        }

        public void setGrade(String grade) {
            this.grade = grade;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getTopicName() {
            return topicName;
        }

        public void setTopicName(String topicName) {
            this.topicName = topicName;
        }
    }
}
