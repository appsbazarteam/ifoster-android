package com.ifoster.mvp.result.exam_list;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.result.exam_list.adapter.ResultAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class ResultActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        setFont();

        setToolbar();

    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    @Override
    public void onResume() {
        super.onResume();

        ArrayList<ResultModel> examList = getExamList();

        if (examList.size() > 0) {

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            ResultAdapter resultAdapter = new ResultAdapter(this, examList);
            recyclerView.setAdapter(resultAdapter);

        }
    }

    private ArrayList<ResultModel> getExamList() {

        int colorCodeType = 0;
        ArrayList<ResultModel> arrayListExam = new ArrayList<>();

        for (int i = 1; i <= 4; i++) {

            ResultModel resultModel = new ResultModel();
            resultModel.setExamName("Unit Test " + i);
            resultModel.setExamShortName("UT" + i);
            resultModel.setColorType(colorCodeType);


            if(i==2){
                resultModel.setExamName("Term 1");
                resultModel.setExamShortName("T1");
            }
            if(i==4){
                resultModel.setExamName("Term 2");
                resultModel.setExamShortName("T2");
            }

            // increase color clde value
            colorCodeType++;

            // reset color form strating again
            if (colorCodeType == 4) {
                colorCodeType = 0;
            }

            arrayListExam.add(resultModel);
        }

        return arrayListExam;
    }

    private void setToolbar() {

        txtViewToolbarTitle.setText(getString(R.string.title_exam_list));
    }

    @OnClick(R.id.imgViewBack)
    public void back() {
        finish();
    }

}
