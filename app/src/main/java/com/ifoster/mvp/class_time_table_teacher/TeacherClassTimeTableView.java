package com.ifoster.mvp.class_time_table_teacher;

import com.ifoster.mvp.class_time_table.ClassTimeTableModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface TeacherClassTimeTableView {

    public void onSuccessTimeTable(ArrayList<ClassTimeTableModel.Result> results);

    public void onUnsuccessTimeTable(String message);

    public void onTimeTableInternetError();
}
