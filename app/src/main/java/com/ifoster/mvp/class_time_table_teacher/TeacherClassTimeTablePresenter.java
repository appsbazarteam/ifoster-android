package com.ifoster.mvp.class_time_table_teacher;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface TeacherClassTimeTablePresenter {

    public void getTimeTable(String day);
}
