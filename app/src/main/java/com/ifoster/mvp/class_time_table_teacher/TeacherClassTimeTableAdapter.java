package com.ifoster.mvp.class_time_table_teacher;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.mvp.class_time_table.ClassTimeTableModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class TeacherClassTimeTableAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<ClassTimeTableModel.Result> listData;

    int colorCodeType = 0;

    public TeacherClassTimeTableAdapter(Context context, ArrayList<ClassTimeTableModel.Result> listData) {

        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_class_time_table, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final ClassTimeTableModel.Result result = listData.get(position);

        if (result.getSubject() != null) {
            ((ViewHolderList) holder).txtViewCircleDate.setText(String.valueOf(position + 1));
        }

        if (result.getSubject() != null) {
            ((ViewHolderList) holder).txtViewSubject.setText(result.getSubject());
        }

        if (result.getTime() != null) {
            ((ViewHolderList) holder).txtViewTime.setText(result.getTime());
        }

        if (result.getTeacherName() != null) {
            ((ViewHolderList) holder).txtViewTeacherName.setText(result.getTeacherName());
        }

        // set color combination
        if (result.getColorType() == 0) {
            // set color on position zero
            ((ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color1_text));

        } else if (result.getColorType() == 1) {
            // set color on position one
            ((ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color2_text));

        } else if (result.getColorType() == 2) {
            // set color on position two
            ((ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color3_text));

        } else if (result.getColorType() == 3) {
            // set color on position three
            ((ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color4_text));
        }

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.cardViewRoundType)
        CardView cardViewRoundType;

        @BindView(R.id.txtViewCircleDate)
        TextView txtViewCircleDate;

        /*@BindView(R.id.txtViewCircleMonth)
        TextView txtViewCircleMonth;*/

        @BindView(R.id.txtViewSubject)
        TextView txtViewSubject;

        @BindView(R.id.txtViewTime)
        TextView txtViewTime;

        @BindView(R.id.txtViewTeacherName)
        TextView txtViewTeacherName;


        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            // set font
            FontHelper.applyFont(context, txtViewCircleDate, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewSubject, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
            FontHelper.applyFont(context, txtViewTeacherName, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
            FontHelper.applyFont(context, txtViewTime, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        }
    }


}
