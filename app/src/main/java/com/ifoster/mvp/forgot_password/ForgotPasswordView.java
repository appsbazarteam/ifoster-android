package com.ifoster.mvp.forgot_password;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ForgotPasswordView {

    public void onSuccessForgotPassword(String message);

    public void onUnsuccessForgotPassword(String message);

    public void onForgotPasswordInternetError();

}
