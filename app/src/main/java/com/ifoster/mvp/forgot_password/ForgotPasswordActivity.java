package com.ifoster.mvp.forgot_password;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifoster.R;
import com.ifoster.common.helpers.AlertDialogHelper;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class ForgotPasswordActivity extends BaseActivity implements ForgotPasswordView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.edtTextMobileNo)
    AppCompatEditText edtTextMobileNo;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @BindView(R.id.txtView_forget_continueTxt)
    TextView txtView_forget_continueTxt;


    /*---------------Class Presenter---------------*/
    ForgotPasswordPresenterImpl forgotPasswordPresenterImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        forgotPasswordPresenterImpl = new ForgotPasswordPresenterImpl(this, this);

        setFont();

        setToolbar();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, edtTextMobileNo, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtView_forget_continueTxt, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    private void setToolbar() {

        txtViewToolbarTitle.setText(getString(R.string.title_forgot_pass));
    }

    @OnClick(R.id.cardViewBtnSubmit)
    public void submit() {

        getData();
    }

    @OnClick(R.id.imgViewBack)
    public void back() {
        finish();
    }

    private void getData() {

        Utils.hideKeyboardIfOpen(this);

        String mobileNo = edtTextMobileNo.getText().toString();

        if (isValid(mobileNo)) {
            showProgressBar();
            forgotPasswordPresenterImpl.forgotPassword(mobileNo);
        }
    }

    private boolean isValid(String mobileNo) {

        if (TextUtils.isEmpty(mobileNo)) {

            edtTextMobileNo.setError(getString(R.string.empty_mobile_no));

            return false;
        }

        return true;
    }


    @OnEditorAction(R.id.edtTextMobileNo)
    public boolean setEdTextPassword(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            getData();
            return true;
        }
        return false;
    }

    @Override
    public void onSuccessForgotPassword(String message) {
        hideProgressBar();
        AlertDialogHelper.showAlertAndFinishedActivity(this, message);
    }

    @Override
    public void onUnsuccessForgotPassword(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onForgotPasswordInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryForgotPass, coordinateLayout);
    }

    OnClickInterface onRetryForgotPass = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };
}
