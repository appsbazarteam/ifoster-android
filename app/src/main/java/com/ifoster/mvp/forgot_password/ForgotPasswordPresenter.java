package com.ifoster.mvp.forgot_password;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ForgotPasswordPresenter {

    public void forgotPassword(String mobileNo);
}
