package com.ifoster.mvp.forgot_password;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class ForgotPasswordPresenterImpl implements ForgotPasswordPresenter {

    Activity activity;
    ForgotPasswordView forgotPasswordView;
    JSONObject jsonObject;

    public ForgotPasswordPresenterImpl(Activity activity, ForgotPasswordView forgotPasswordView) {
        this.activity = activity;
        this.forgotPasswordView = forgotPasswordView;
    }

    @Override
    public void forgotPassword(String mobileNo) {

        try {
            ApiAdapter.getInstance(activity);
            callForgotPass(mobileNo);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            forgotPasswordView.onForgotPasswordInternetError();
        }
    }


    private void callForgotPass(String mobileNo) {

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_MOBILE_NO, mobileNo);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

        Call<ForgotPasswordModel> getLoginOutput = ApiAdapter.getApiService().forgotPassword("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<ForgotPasswordModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {

                try {
                    //getting whole data from response
                    ForgotPasswordModel holiDayCalenderModel = response.body();
                    String message = holiDayCalenderModel.getMessage();

                    if (holiDayCalenderModel.getStatus()) {
                        forgotPasswordView.onSuccessForgotPassword(message);
                    } else {
                        forgotPasswordView.onUnsuccessForgotPassword(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    forgotPasswordView.onUnsuccessForgotPassword(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                forgotPasswordView.onUnsuccessForgotPassword(activity.getString(R.string.error_server));
            }
        });
    }


}
