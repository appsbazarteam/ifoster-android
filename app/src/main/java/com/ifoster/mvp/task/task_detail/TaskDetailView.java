package com.ifoster.mvp.task.task_detail;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface TaskDetailView {

    // current task
    public void onSuccessTaskDetail(ArrayList<TaskDetailModel.Result> results, String message);

    public void onUnsuccessTaskDetail(String message);

    public void onTaskDetailInternetError();

}
