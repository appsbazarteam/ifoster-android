package com.ifoster.mvp.task.task_dashboard.model;

import java.io.Serializable;

/**
 * Created by Alam on 07-Oct-17.
 */

public class CurrentTaskModelOld implements Serializable {

    String date;
    String day;
    String taskTitle;
    String taskSubject;
    String taskDescription;
    String seperatorDetail;
    int itemType;
    int colorType;

    public int getColorType() {
        return colorType;
    }

    public void setColorType(int colorType) {
        this.colorType = colorType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public String getTaskSubject() {
        return taskSubject;
    }

    public void setTaskSubject(String taskSubject) {
        this.taskSubject = taskSubject;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getSeperatorDetail() {
        return seperatorDetail;
    }

    public void setSeperatorDetail(String seperatorDetail) {
        this.seperatorDetail = seperatorDetail;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }
}
