package com.ifoster.mvp.task.task_detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TaskDetailModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Result")
    @Expose
    private ArrayList<Result> result = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("teacher_name")
        @Expose
        private String teacherName;
        @SerializedName("start_date")
        @Expose
        private String startDate;
        @SerializedName("end_date")
        @Expose
        private String endDate;

//        @SerializedName("class")
//        @Expose
//        private String _class;
//
//        @SerializedName("section")
//        @Expose
//        private String section;

        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("home_type")
        @Expose
        private String homeType;
        @SerializedName("short_descp")
        @Expose
        private String shortDescp;
        @SerializedName("long_descp")
        @Expose
        private String longDescp;

        @SerializedName("attched_text1")
        @Expose
        private String attachedText1;
        @SerializedName("attched_path1")
        @Expose
        private String attachedPath1;

        @SerializedName("attched_text2")
        @Expose
        private String attachedText2;
        @SerializedName("attched_path2")
        @Expose
        private String attachedPath2;

        @SerializedName("attched_text3")
        @Expose
        private String attachedText3;
        @SerializedName("attched_path3")
        @Expose
        private String attachedPath3;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getHomeType() {
            return homeType;
        }

        public void setHomeType(String homeType) {
            this.homeType = homeType;
        }

        public String getShortDescp() {
            return shortDescp;
        }

        public void setShortDescp(String shortDescp) {
            this.shortDescp = shortDescp;
        }

        public String getLongDescp() {
            return longDescp;
        }

        public void setLongDescp(String longDescp) {
            this.longDescp = longDescp;
        }


        public String getAttachedText1() {
            return attachedText1;
        }

        public void setAttachedText1(String attachedText1) {
            this.attachedText1 = attachedText1;
        }

        public String getAttachedPath1() {
            return attachedPath1;
        }

        public void setAttachedPath1(String attachedPath1) {
            this.attachedPath1 = attachedPath1;
        }

        public String getAttachedText2() {
            return attachedText2;
        }

        public void setAttachedText2(String attachedText2) {
            this.attachedText2 = attachedText2;
        }

        public String getAttachedPath2() {
            return attachedPath2;
        }

        public void setAttachedPath2(String attachedPath2) {
            this.attachedPath2 = attachedPath2;
        }

        public String getAttachedText3() {
            return attachedText3;
        }

        public void setAttachedText3(String attachedText3) {
            this.attachedText3 = attachedText3;
        }

        public String getAttachedPath3() {
            return attachedPath3;
        }

        public void setAttachedPath3(String attachedPath3) {
            this.attachedPath3 = attachedPath3;
        }

    }
}