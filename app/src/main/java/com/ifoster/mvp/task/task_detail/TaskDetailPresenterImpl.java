package com.ifoster.mvp.task.task_detail;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class TaskDetailPresenterImpl implements TaskDetailPresenter {

    Activity activity;
    TaskDetailView taskDetailView;
    JSONObject jsonObject;

    public TaskDetailPresenterImpl(Activity activity, TaskDetailView taskDetailView) {
        this.activity = activity;
        this.taskDetailView = taskDetailView;
    }

    @Override
    public void getTaskDetail(int homeworkId, int taskType) {
        try {
            ApiAdapter.getInstance(activity);
            callTaskDetailApi(homeworkId, taskType);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            taskDetailView.onTaskDetailInternetError();
        }
    }

    private void callTaskDetailApi(int homeworkId, int taskType) {
        String token = UserSession.getInstance().getUserToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_HOMWRORK_ID, homeworkId);
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<TaskDetailModel> getLoginOutput;

        if (taskType == 0) {  // current task detail
            getLoginOutput = ApiAdapter.getApiService().getCurrentTaskDetail("application/json", "no-cache", body);
        } else { // archive task detail
            getLoginOutput = ApiAdapter.getApiService().getArchiveTaskDetail("application/json", "no-cache", body);
        }

        getLoginOutput.enqueue(new Callback<TaskDetailModel>() {
            @Override
            public void onResponse(Call<TaskDetailModel> call, Response<TaskDetailModel> response) {

                //Progress.stop();

                try {
                    //getting whole data from response
                    TaskDetailModel taskDetailModel = response.body();
                    String message = taskDetailModel.getMessage();

                    if (taskDetailModel.getStatus()) {

                        taskDetailView.onSuccessTaskDetail(taskDetailModel.getResult(), message);
                    } else {
                        taskDetailView.onUnsuccessTaskDetail(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    taskDetailView.onUnsuccessTaskDetail(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<TaskDetailModel> call, Throwable t) {
                //Progress.stop();
                taskDetailView.onUnsuccessTaskDetail(activity.getString(R.string.error_server));
            }
        });
    }


}
