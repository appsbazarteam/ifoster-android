package com.ifoster.mvp.task.task_dashboard.view;

import com.ifoster.mvp.task.task_dashboard.model.CurrentTaskModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface TaskView {

    // current task
    public void onSuccessCurrentTask(ArrayList<CurrentTaskModel.User> message, String s);

    public void onUnsuccessCurrentTask(String message);

    public void onCurrentTaskInternetError();


    // archieve task
    public void onSuccessArchievTask(ArrayList<CurrentTaskModel.User> message, String s);

    public void onUnsuccessArchievTask(String message);

    public void onArchievTaskInternetError();
}
