package com.ifoster.mvp.task.task_detail;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface TaskDetailPresenter {

    public void getTaskDetail(int homeworkId, int homeWorkId);

}
