package com.ifoster.mvp.task.task_dashboard.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.utility.Utils;
import com.ifoster.mvp.task.task_dashboard.model.CurrentTaskModel;
import com.ifoster.mvp.task.task_detail.TaskDetailActivity;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alam on 15-May-18.
 */

public class CurrentTaskAdapter extends RecyclerView.Adapter<CurrentTaskAdapter.ViewHolderList> {

    Activity activity;
    ArrayList<CurrentTaskModel.User> listData;

    public CurrentTaskAdapter(Activity activity, ArrayList<CurrentTaskModel.User> listData) {
        this.activity = activity;
        this.listData = listData;
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public ViewHolderList onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(activity).inflate(R.layout.item_current_task_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolderList holder, int position) {

        final CurrentTaskModel.User user = listData.get(position);

        Date date = Utils.convertToSimpleDateToDateFormate(user.getStartDate());

        String dayOfTheWeek = (String) DateFormat.format("EEEE", date.getTime()); // Thursday
        String day = (String) DateFormat.format("dd", date); // 20
        String monthString = (String) DateFormat.format("MMM", date); // Jun
        String monthNumber = (String) DateFormat.format("MM", date); // 06
        String year = (String) DateFormat.format("yyyy", date); // 2013

        holder.txtViewDate.setText(day);
        holder.txtViewDay.setText(monthString);
        holder.txtViewTaskTitle.setText(user.getHomeType());
        holder.txtViewTaskSubject.setText(user.getSubject());
        holder.txtViewTeacherName.setText(user.getTeacherName());
        holder.txtViewTaskDesc.setText(user.getShortDescp());


        // set color combination
        if (user.getColorType() == 0) {

            // set color on position zero
            holder.cardViewRoundDate.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.color1_text));

        } else if (user.getColorType() == 1) {

            // set color on position one
            holder.cardViewRoundDate.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.color2_text));

        } else if (user.getColorType() == 2) {

            // set color on position two
            holder.cardViewRoundDate.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.color3_text));

        } else if (user.getColorType() == 3) {

            // set color on position three
            holder.cardViewRoundDate.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.color4_text));
        }

        holder.relLayContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, TaskDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(ConstIntent.KEY_TASK_DETAIL, user);
                bundle.putInt(ConstIntent.KEY_TASK_TYPE, 0);
                intent.putExtras(bundle);
                activity.startActivity(intent);
            }
        });
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayContainer)
        RelativeLayout relLayContainer;

        @BindView(R.id.cardViewRoundDate)
        CardView cardViewRoundDate;

        @BindView(R.id.txtViewDate)
        TextView txtViewDate;

        @BindView(R.id.txtViewDay)
        TextView txtViewDay;

        @BindView(R.id.txtViewTaskTitle)
        TextView txtViewTaskTitle;

        @BindView(R.id.txtViewTaskSubject)
        TextView txtViewTaskSubject;

        @BindView(R.id.txtViewTeacherName)
        TextView txtViewTeacherName;

        @BindView(R.id.txtViewTaskDesc)
        TextView txtViewTaskDesc;

        public ViewHolderList(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            // set font
            FontHelper.applyFont(activity, txtViewTaskTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(activity, txtViewTaskSubject, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(activity, txtViewTeacherName, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
            FontHelper.applyFont(activity, txtViewTaskDesc, FontHelper.FontType.FONT_QUICKSAND_REGULAR);

        }
    }
}
