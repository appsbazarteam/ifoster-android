package com.ifoster.mvp.task.task_dashboard.presenter;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.task.task_dashboard.model.CurrentTaskModel;
import com.ifoster.mvp.task.task_dashboard.view.TaskView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class TaskPresenterImpl implements TaskPresenter {

    Activity activity;
    TaskView taskView;
    JSONObject jsonObject;

    public TaskPresenterImpl(Activity activity, TaskView taskView) {
        this.activity = activity;
        this.taskView = taskView;
    }

    @Override
    public void getCurrentTask() {
        try {
            ApiAdapter.getInstance(activity);
            callCurrentTaskApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            taskView.onCurrentTaskInternetError();
        }
    }

    @Override
    public void getArchieveTask() {
        try {
            ApiAdapter.getInstance(activity);
            callCompletedTaskApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            taskView.onArchievTaskInternetError();
        }
    }

    private void callCurrentTaskApi() {

        int studentId = UserSession.getInstance().getStudentId();
        String token = UserSession.getInstance().getUserToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_STUDENT_ID, studentId);
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CurrentTaskModel> getLoginOutput = ApiAdapter.getApiService().getCutrrentTask("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<CurrentTaskModel>() {
            @Override
            public void onResponse(Call<CurrentTaskModel> call, Response<CurrentTaskModel> response) {
                try {
                    //getting whole data from response
                    CurrentTaskModel loginModel = response.body();
                    String message = loginModel.getMessage();

                    if (loginModel.getStatus()) {

                        taskView.onSuccessCurrentTask(loginModel.getUser(), message);
                    } else {
                        taskView.onUnsuccessCurrentTask(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    taskView.onUnsuccessCurrentTask(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CurrentTaskModel> call, Throwable t) {
                taskView.onUnsuccessCurrentTask(activity.getString(R.string.error_server));
            }
        });
    }

    private void callCompletedTaskApi() {
        int studentId = UserSession.getInstance().getStudentId();
        String token = UserSession.getInstance().getUserToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_STUDENT_ID, studentId);
            jsonObject.put(Const.PARAM_USER_TOKEN, token);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CurrentTaskModel> getLoginOutput = ApiAdapter.getApiService().getArchiveTask("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<CurrentTaskModel>() {
            @Override
            public void onResponse(Call<CurrentTaskModel> call, Response<CurrentTaskModel> response) {

                try {
                    //getting whole data from response
                    CurrentTaskModel loginModel = response.body();
                    String message = loginModel.getMessage();

                    if (loginModel.getStatus()) {

                        taskView.onSuccessArchievTask(loginModel.getUser(), message);
                    } else {
                        taskView.onUnsuccessArchievTask(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    taskView.onUnsuccessArchievTask(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CurrentTaskModel> call, Throwable t) {
                taskView.onUnsuccessArchievTask(activity.getString(R.string.error_server));
            }
        });
    }


}
