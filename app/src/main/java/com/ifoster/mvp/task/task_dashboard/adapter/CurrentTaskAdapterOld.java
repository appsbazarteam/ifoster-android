package com.ifoster.mvp.task.task_dashboard.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.mvp.task.task_dashboard.model.CurrentTaskModelOld;
import com.ifoster.mvp.task.task_detail.TaskDetailActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class CurrentTaskAdapterOld extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ONE = 1;
    private static final int TYPE_TWO = 2;

    Context context;
    private ArrayList<CurrentTaskModelOld> itemList;

    // Constructor of the class
    public CurrentTaskAdapterOld(Context context, ArrayList<CurrentTaskModelOld> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    // get the size of the list
    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    // determine which layout to use for the row
    @Override
    public int getItemViewType(int position) {
        CurrentTaskModelOld item = itemList.get(position);
        if (item.getItemType() == CurrentTaskAdapterOld.TYPE_ONE) {
            return TYPE_ONE;
        } else if (item.getItemType() == CurrentTaskAdapterOld.TYPE_TWO) {
            return TYPE_TWO;
        } else {
            return -1;
        }
    }

    // specify the row layout file and click for each row
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ONE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_current_task_list, parent, false);
            return new ViewHolderOne(view);
        } else if (viewType == TYPE_TWO) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_seperator, parent, false);
            return new ViewHolderTwo(view);
        } else {
            throw new RuntimeException("The type has to be ONE or TWO");
        }
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int listPosition) {
        switch (holder.getItemViewType()) {
            case TYPE_ONE:
                initLayoutOne((ViewHolderOne) holder, listPosition);
                break;
            case TYPE_TWO:
                initLayoutTwo((ViewHolderTwo) holder, listPosition);
                break;
            default:
                break;
        }
    }

    private void initLayoutOne(ViewHolderOne holder, final int position) {

        holder.txtViewDate.setText(itemList.get(position).getDate());
        holder.txtViewDay.setText(itemList.get(position).getDay());
        holder.txtViewTaskTitle.setText(itemList.get(position).getTaskTitle());
        holder.txtViewTaskSubject.setText(itemList.get(position).getTaskSubject());
        holder.txtViewTaskDesc.setText(itemList.get(position).getTaskDescription());

        // set font
        FontHelper.applyFont(context, holder.txtViewTaskTitle, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
        FontHelper.applyFont(context, holder.txtViewTaskSubject, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
        FontHelper.applyFont(context, holder.txtViewTaskDesc, FontHelper.FontType.FONT_QUICKSAND_REGULAR);


        // set color combination
        if (itemList.get(position).getColorType() == 0) {

            // set color on position zero
            holder.cardViewRoundDate.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color1_text));

        } else if (itemList.get(position).getColorType() == 1) {

            // set color on position one
            holder.cardViewRoundDate.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color2_text));

        } else if (itemList.get(position).getColorType() == 2) {

            // set color on position two
            holder.cardViewRoundDate.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color3_text));

        } else if (itemList.get(position).getColorType() == 3) {

            // set color on position three
            holder.cardViewRoundDate.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color4_text));
        }

        holder.relLayContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, TaskDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(ConstIntent.KEY_TASK_DETAIL, itemList.get(position));
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    private void initLayoutTwo(ViewHolderTwo holder, int position) {
        holder.textViewSeperator.setText(itemList.get(position).getSeperatorDetail());

        FontHelper.applyFont(context, holder.textViewSeperator, FontHelper.FontType.FONT_QUICKSAND_BOLD);

    }


    // Static inner class to initialize the views of rows
    static class ViewHolderOne extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayContainer)
        RelativeLayout relLayContainer;

        @BindView(R.id.cardViewRoundDate)
        CardView cardViewRoundDate;

        @BindView(R.id.txtViewDate)
        TextView txtViewDate;

        @BindView(R.id.txtViewDay)
        TextView txtViewDay;

        @BindView(R.id.txtViewTaskTitle)
        TextView txtViewTaskTitle;

        @BindView(R.id.txtViewTaskSubject)
        TextView txtViewTaskSubject;

        @BindView(R.id.txtViewTaskDesc)
        TextView txtViewTaskDesc;

        public ViewHolderOne(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static class ViewHolderTwo extends RecyclerView.ViewHolder {

        @BindView(R.id.textViewSeperator)
        TextView textViewSeperator;

        public ViewHolderTwo(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
