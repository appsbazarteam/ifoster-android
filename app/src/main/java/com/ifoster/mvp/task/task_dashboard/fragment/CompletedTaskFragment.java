package com.ifoster.mvp.task.task_dashboard.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ifoster.R;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.task.task_dashboard.HomeWorkActivity;
import com.ifoster.mvp.task.task_dashboard.adapter.CompletedTaskAdapter;
import com.ifoster.mvp.task.task_dashboard.model.CurrentTaskModel;
import com.ifoster.mvp.task.task_dashboard.presenter.TaskPresenterImpl;
import com.ifoster.mvp.task.task_dashboard.view.TaskView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alam on 02-Oct-17.
 */

public class CompletedTaskFragment extends Fragment implements TaskView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.relLayMainContainer)
    RelativeLayout relLayMainContainer;

    /*---------Class Presenter--------*/
    TaskPresenterImpl taskPresenterImpl;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_completed_task, container, false);
        ButterKnife.bind(this, view);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getData();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



    }

    private void getData() {
        taskPresenterImpl = new TaskPresenterImpl(getActivity(), this);
        ((HomeWorkActivity)getActivity()).showProgressBar();
        taskPresenterImpl.getArchieveTask();
    }

    @Override
    public void onSuccessCurrentTask(ArrayList<CurrentTaskModel.User> message, String s) {
    }

    @Override
    public void onUnsuccessCurrentTask(String message) {
    }

    @Override
    public void onCurrentTaskInternetError() {
    }

    @Override
    public void onSuccessArchievTask(ArrayList<CurrentTaskModel.User> result, String s) {
        hideProgressBar();
        if (result != null) {

            int colorCodeType = 0;

            if (result.size() > 0) {

                for (CurrentTaskModel.User user : result) {

                    // set color
                    user.setColorType(colorCodeType);

                    // increase color code value
                    colorCodeType++;

                    // reset color form strating again
                    if (colorCodeType == 4) {
                        colorCodeType = 0;
                    }
                }
            }

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            CompletedTaskAdapter completedTaskAdapter = new CompletedTaskAdapter(getActivity(), result);
            recyclerView.setAdapter(completedTaskAdapter);
        }

    }

    @Override
    public void onUnsuccessArchievTask(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    @Override
    public void onArchievTaskInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryGetArchiveData, relLayMainContainer);
    }

    OnClickInterface onRetryGetArchiveData = new OnClickInterface() {
        @Override
        public void onClick() {

            getData();
        }
    };

    private void hideProgressBar(){
        ((HomeWorkActivity)getActivity()).hideProgressBar();
    }
}
