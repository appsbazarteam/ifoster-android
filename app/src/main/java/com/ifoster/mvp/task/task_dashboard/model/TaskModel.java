package com.ifoster.mvp.task.task_dashboard.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Alam on 15-May-18.
 */

public class TaskModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user")
    @Expose
    private ArrayList<User> user = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<User> getUser() {
        return user;
    }

    public void setUser(ArrayList<User> user) {
        this.user = user;
    }

    public class User {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("teacher_name")
        @Expose
        private String teacherName;
        @SerializedName("start_date")
        @Expose
        private String startDate;
        @SerializedName("end_date")
        @Expose
        private String endDate;
        @SerializedName("class")
        @Expose
        private String _class;
        @SerializedName("section")
        @Expose
        private String section;
        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("home_type")
        @Expose
        private String homeType;
        @SerializedName("short_descp")
        @Expose
        private String shortDescp;
        @SerializedName("long_descp")
        @Expose
        private String longDescp;
        @SerializedName("attatext1")
        @Expose
        private String attatext1;
        @SerializedName("attatext2")
        @Expose
        private String attatext2;
        @SerializedName("attatext3")
        @Expose
        private String attatext3;
        @SerializedName("home_attched")
        @Expose
        private String homeAttched;
        @SerializedName("home_attched1")
        @Expose
        private String homeAttched1;
        @SerializedName("home_attched2")
        @Expose
        private String homeAttched2;
        @SerializedName("createddate")
        @Expose
        private String createddate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getClass_() {
            return _class;
        }

        public void setClass_(String _class) {
            this._class = _class;
        }

        public String getSection() {
            return section;
        }

        public void setSection(String section) {
            this.section = section;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getHomeType() {
            return homeType;
        }

        public void setHomeType(String homeType) {
            this.homeType = homeType;
        }

        public String getShortDescp() {
            return shortDescp;
        }

        public void setShortDescp(String shortDescp) {
            this.shortDescp = shortDescp;
        }

        public String getLongDescp() {
            return longDescp;
        }

        public void setLongDescp(String longDescp) {
            this.longDescp = longDescp;
        }

        public String getAttatext1() {
            return attatext1;
        }

        public void setAttatext1(String attatext1) {
            this.attatext1 = attatext1;
        }

        public String getAttatext2() {
            return attatext2;
        }

        public void setAttatext2(String attatext2) {
            this.attatext2 = attatext2;
        }

        public String getAttatext3() {
            return attatext3;
        }

        public void setAttatext3(String attatext3) {
            this.attatext3 = attatext3;
        }

        public String getHomeAttched() {
            return homeAttched;
        }

        public void setHomeAttched(String homeAttched) {
            this.homeAttched = homeAttched;
        }

        public String getHomeAttched1() {
            return homeAttched1;
        }

        public void setHomeAttched1(String homeAttched1) {
            this.homeAttched1 = homeAttched1;
        }

        public String getHomeAttched2() {
            return homeAttched2;
        }

        public void setHomeAttched2(String homeAttched2) {
            this.homeAttched2 = homeAttched2;
        }

        public String getCreateddate() {
            return createddate;
        }

        public void setCreateddate(String createddate) {
            this.createddate = createddate;
        }

    }
}
