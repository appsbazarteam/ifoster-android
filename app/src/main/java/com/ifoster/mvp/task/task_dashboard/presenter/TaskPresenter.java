package com.ifoster.mvp.task.task_dashboard.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface TaskPresenter {

    public void getCurrentTask();

    public void getArchieveTask();
}
