package com.ifoster.mvp.task.task_dashboard.model;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentTaskModel implements Serializable {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user")
    @Expose
    private ArrayList<User> user = null;
    @SerializedName("Attachment")
    @Expose
    private String attachment;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<User> getUser() {
        return user;
    }

    public void setUser(ArrayList<User> user) {
        this.user = user;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }


    public class User implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("teacher_name")
        @Expose
        private String teacherName;

        @SerializedName("start_date")
        @Expose
        private String startDate;

        @SerializedName("subject")
        @Expose
        private String subject;

        @SerializedName("home_type")
        @Expose
        private String homeType;

        @SerializedName("short_descp")
        @Expose
        private String shortDescp;

//        @SerializedName("home_attched1")
//        @Expose
//        private String homeAttched1;
//        @SerializedName("home_attched2")
//        @Expose
//        private String homeAttched2;
//        @SerializedName("home_attched3")
//        @Expose
//        private String homeAttched3;

        int colorType;

        public int getColorType() {
            return colorType;
        }

        public void setColorType(int colorType) {
            this.colorType = colorType;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getHomeType() {
            return homeType;
        }

        public void setHomeType(String homeType) {
            this.homeType = homeType;
        }

        public String getShortDescp() {
            return shortDescp;
        }

        public void setShortDescp(String shortDescp) {
            this.shortDescp = shortDescp;
        }

    }
}