package com.ifoster.mvp.task.task_detail;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.format.DateFormat;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.helpers.AlertDialogHelper;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.utility.Resource;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.notice_board.view_more_text.ExpandableTextView;
import com.ifoster.mvp.task.task_dashboard.model.CurrentTaskModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;

public class TaskDetailActivity extends BaseActivity implements TaskDetailView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @BindView(R.id.txtViewDate)
    TextView txtViewDate;

    @BindView(R.id.txtViewDay)
    TextView txtViewDay;

    @BindView(R.id.txtViewTaskTitle)
    TextView txtViewTaskTitle;

    @BindView(R.id.txtViewSubject)
    TextView txtViewSubject;

    @BindView(R.id.txtViewDescription)
    ExpandableTextView txtViewDescription;

    @BindView(R.id.txtViewMore)
    TextView txtViewMore;

    @BindView(R.id.txtViewShortDescription)
    TextView txtViewShortDescription;

    @BindView(R.id.txtViewShortDescriptionTitle)
    TextView txtViewShortDescriptionTitle;

    @BindView(R.id.txtViewFromSubjectTitle)
    TextView txtViewFromSubjectTitle;

    @BindView(R.id.txtViewDescriptionTitle)
    TextView txtViewDescriptionTitle;

    @BindView(R.id.txtViewTeacherNameTitle)
    TextView txtViewTeacherNameTitle;

    @BindView(R.id.txtViewTeacherName)
    TextView txtViewTeacherName;

    @BindView(R.id.linLayAttach1)
    LinearLayout linLayAttach1;

    @BindView(R.id.linLayAttach2)
    LinearLayout linLayAttach2;

    @BindView(R.id.linLayAttach3)
    LinearLayout linLayAttach3;

    @BindView(R.id.txtViewAttach1Title)
    TextView txtViewAttach1Title;

    @BindView(R.id.txtViewAttach1Download)
    TextView txtViewAttach1Download;

    @BindView(R.id.txtViewAttach2Title)
    TextView txtViewAttach2Title;

    @BindView(R.id.txtViewAttach2Download)
    TextView txtViewAttach2Download;

    @BindView(R.id.txtViewAttach3Title)
    TextView txtViewAttach3Title;

    @BindView(R.id.txtViewAttach3Download)
    TextView txtViewAttach3Download;

    @BindView(R.id.txtViewAttachFileTitle)
    TextView txtViewAttachFileTitle;


    CurrentTaskModel.User user;
    int taskType = 0;

    /*----------Class Presenter---------*/
    TaskDetailPresenterImpl taskDetailPresenterImpl;


    TaskDetailModel.Result result;

    int attachType = 0;

    ProgressBar pb;
    Dialog dialog;
    TextView cur_val;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_detail);

        setFont();

        taskDetailPresenterImpl = new TaskDetailPresenterImpl(this, this);

        // set toolbar
        setToolBar();

        getIntentData();

    }

    private void getIntentData() {

        Intent intent = getIntent();

        if (intent.hasExtra(ConstIntent.KEY_TASK_DETAIL)) {

            user = (CurrentTaskModel.User) intent.getSerializableExtra(ConstIntent.KEY_TASK_DETAIL);
            taskType = intent.getIntExtra(ConstIntent.KEY_TASK_TYPE, 0);

            getData();
        }

    }

    private void getData() {

        if (user.getId() != null) {

            int homeworkId = Integer.parseInt(user.getId());
            showProgressBar();
            taskDetailPresenterImpl.getTaskDetail(homeworkId, taskType);
        }
    }

    @OnClick(R.id.txtViewAttach1Download)
    public void downloadAttach1File() {

        attachType = 1;

        if (setRunTimePermission()) {

            showProgress();

            new Thread(new Runnable() {
                public void run() {
                    downloadFile(result.getAttachedPath1());
                }
            }).start();

        } else {
            // request runtime permission
            if (!Settings.System.canWrite(this)) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 2909);
            }
        }
    }

    @OnClick(R.id.txtViewAttach2Download)
    public void downloadAttach2File() {

        attachType = 2;

        if (setRunTimePermission()) {
            showProgress();

            new Thread(new Runnable() {
                public void run() {
                    downloadFile(result.getAttachedPath2());
                }
            }).start();

        } else {
            // request runtime permission
            if (!Settings.System.canWrite(this)) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 2909);
            }
        }
    }

    @OnClick(R.id.txtViewAttach3Download)
    public void downloadAttach3File() {

        attachType = 3;

        if (setRunTimePermission()) {
            showProgress();

            new Thread(new Runnable() {
                public void run() {

                    downloadFile(result.getAttachedPath3());
                }
            }).start();
        } else {
            // request runtime permission
            if (!Settings.System.canWrite(this)) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 2909);
            }
        }
    }

    private boolean setRunTimePermission() {

        int permissionCheckRead = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if ((permissionCheckRead == -1) || (permissionCheckWrite == -1)) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 2909: {

                int permissionCheckRead = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE);

                int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);


                if ((permissionCheckRead == 0) && (permissionCheckWrite == 0)) {

                    if (attachType == 1) {
                        downloadAttach1File();
                    } else if (attachType == 2) {
                        downloadAttach2File();
                    } else if (attachType == 3) {
                        downloadAttach3File();
                    }

                } else {

                    AlertDialogHelper.alertEnableAppSetting(this);
                }

                return;
            }

        }
    }


    private void setFont() {

        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewFromSubjectTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewTeacherNameTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewDescriptionTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewShortDescription, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        //FontHelper.applyFont(this, txtViewDone, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        //FontHelper.applyFont(this, txtViewCancel, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewAttachFileTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewTaskTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewAttach1Download, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewAttach2Download, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewAttach3Download, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        //FontHelper.applyFont(this, edtTextComments, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
        FontHelper.applyFont(this, txtViewSubject, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
        FontHelper.applyFont(this, txtViewTeacherName, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
        FontHelper.applyFont(this, txtViewShortDescription, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
        FontHelper.applyFont(this, txtViewDescription, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
        FontHelper.applyFont(this, txtViewAttach1Title, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
        FontHelper.applyFont(this, txtViewAttach2Title, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
        FontHelper.applyFont(this, txtViewAttach3Title, FontHelper.FontType.FONT_QUICKSAND_REGULAR);

    }

    private void setToolBar() {

        txtViewToolbarTitle.setText(getString(R.string.title_home_work));
        txtViewAttachFileTitle.setPaintFlags(txtViewAttachFileTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtViewShortDescriptionTitle.setPaintFlags(txtViewAttachFileTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtViewDescriptionTitle.setPaintFlags(txtViewAttachFileTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @OnClick(R.id.txtViewMore)
    public void onViewMoreClick() {

        txtViewDescription.performClick();

        if (txtViewDescription.getIsCollapsed()) {
            txtViewMore.setText("View More");
        } else {
            txtViewMore.setText("View Less");
        }
    }

    @OnClick(R.id.imgViewBack)
    public void back() {
        finish();
    }


    @Override
    public void onSuccessTaskDetail(ArrayList<TaskDetailModel.Result> results, String message) {
        hideProgressBar();
        if (results != null) {

            if (results.size() > 0) {

                result = results.get(0);

                setData();
            }
        }
    }


    private void setData() {

        if (result != null) {

            Date date = Utils.convertToSimpleDateToDateFormate(user.getStartDate());

            String dayOfTheWeek = (String) DateFormat.format("EEEE", date.getTime()); // Thursday
            String day = (String) DateFormat.format("dd", date); // 20
            String monthString = (String) DateFormat.format("MMM", date); // Jun
            String monthNumber = (String) DateFormat.format("MM", date); // 06
            String year = (String) DateFormat.format("yyyy", date); // 2013

            txtViewDate.setText(day);
            txtViewDay.setText(monthString);
            txtViewTaskTitle.setText(result.getHomeType());
            txtViewSubject.setText(result.getSubject());
            txtViewTeacherName.setText(result.getTeacherName());
            txtViewShortDescription.setText(result.getShortDescp());
            txtViewDescription.setText(result.getLongDescp());

            if ((result.getAttachedPath1() != null && result.getAttachedPath1().length() > 0) ||
                    (result.getAttachedPath2() != null && result.getAttachedPath2().length() > 0) ||
                    (result.getAttachedPath3() != null && result.getAttachedPath3().length() > 0)) {
                txtViewAttachFileTitle.setVisibility(View.VISIBLE);
            } else {
                txtViewAttachFileTitle.setVisibility(View.GONE);
            }

            showAttachment1();
            showAttachment2();
            showAttachment3();
        }
    }

    private void showAttachment1(){
        if (result.getAttachedPath1() != null && result.getAttachedPath1().length() > 0) {
            linLayAttach1.setVisibility(View.VISIBLE);
            if (result.getAttachedText1() != null && result.getAttachedText1().length() > 0)
                txtViewAttach1Title.setText(result.getAttachedText1());
            else
                txtViewAttach1Title.setText(Resource.toString(R.string.attachment1));
        } else {
            linLayAttach1.setVisibility(View.GONE);
        }
    }

    private void showAttachment2(){
        if (result.getAttachedPath2() != null && result.getAttachedPath2().length() > 0) {
            linLayAttach2.setVisibility(View.VISIBLE);
            if (result.getAttachedText2() != null && result.getAttachedText2().length() > 0)
                txtViewAttach2Title.setText(result.getAttachedText2());
            else
                txtViewAttach2Title.setText(Resource.toString(R.string.attachment2));
        } else {
            linLayAttach2.setVisibility(View.GONE);
        }
    }

    private void showAttachment3(){
        if (result.getAttachedPath3() != null && result.getAttachedPath3().length() > 0) {
            linLayAttach3.setVisibility(View.VISIBLE);
            if (result.getAttachedText3() != null && result.getAttachedText3().length() > 0)
                txtViewAttach3Title.setText(result.getAttachedText3());
            else
                txtViewAttach3Title.setText(Resource.toString(R.string.attachment3));
        } else {
            linLayAttach3.setVisibility(View.GONE);
        }
    }

    @Override
    public void onUnsuccessTaskDetail(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onTaskDetailInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryDetail, coordinateLayout);
    }

    OnClickInterface onRetryDetail = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };


    void downloadFile(String urlDown) {

        if (urlDown != null && urlDown.length() > 0) {

            int downloadedSize = 0;
            int totalSize = 0;

            try {
                URL url = new URL(urlDown);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);

                //connect
                urlConnection.connect();

                //set the path where we want to save the file
                File SDCardRoot = Environment.getExternalStorageDirectory();
                //create a new file, to save the downloaded file
                final File file = new File(SDCardRoot, "downloaded_file.png");

                FileOutputStream fileOutput = new FileOutputStream(file);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                totalSize = urlConnection.getContentLength();

                final int finalTotalSize = totalSize;
                runOnUiThread(new Runnable() {
                    public void run() {
                        pb.setMax(finalTotalSize);
                    }
                });

                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;

                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    fileOutput.write(buffer, 0, bufferLength);
                    downloadedSize += bufferLength;
                    // update the progressbar //
                    final int finalDownloadedSize = downloadedSize;
                    final int finalTotalSize1 = totalSize;
                    runOnUiThread(new Runnable() {
                        public void run() {
                            pb.setProgress(finalDownloadedSize);
                            float per = ((float) finalDownloadedSize / finalTotalSize1) * 100;
                            cur_val.setText("Downloaded " + finalDownloadedSize + "KB / " + finalTotalSize1 + "KB (" + (int) per + "%)");
                        }
                    });
                }
                //close the output stream when complete //
                fileOutput.close();
                runOnUiThread(new Runnable() {
                    public void run() {
                        //pb.dismiss(); // if you want close it..
                        dialog.dismiss();
                        openFileNew(file);
                    }
                });

            } catch (final MalformedURLException e) {
                showError("Error : MalformedURLException " + e);
                e.printStackTrace();
            } catch (final IOException e) {
                showError("Error : IOException " + e);
                e.printStackTrace();
            } catch (final Exception e) {
                showError("Error : Please check your internet connection " + e);
            }
        } else {
            SnackNotify.showMessage("File url not found.", coordinateLayout);
        }
    }

    private void openFileNew(File url) {

        MimeTypeMap map = MimeTypeMap.getSingleton();
        String ext = MimeTypeMap.getFileExtensionFromUrl(url.getName());
        String type = map.getMimeTypeFromExtension(ext);
        if (type == null)
            type = "*/*";

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // only for gingerbread and newer versions
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            Uri photoUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", url);
            intent.setDataAndType(photoUri, type);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        } else {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri data = Uri.fromFile(url);
            intent.setDataAndType(data, type);
            startActivity(intent);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //   mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private void openFile(File url) {

        try {

            // Uri uri = Uri.fromFile(url);

            Uri uri = null;
            try {
                uri = FileProvider.getUriForFile(TaskDetailActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        createImageFile());
            } catch (IOException e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(uri, "application/x-wav");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") ||
                    url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No application found which can open the file", Toast.LENGTH_SHORT).show();
        }
    }

    void showError(final String err) {
        runOnUiThread(new Runnable() {
            public void run() {

                SnackNotify.showMessage(err, coordinateLayout);
            }
        });
    }

    void showProgress() {
        dialog = new Dialog(TaskDetailActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.myprogressdialog);
        dialog.setTitle("Download Progress");

        TextView text = (TextView) dialog.findViewById(R.id.tv1);
        text.setText("Downloading file ... ");
        cur_val = (TextView) dialog.findViewById(R.id.cur_pg_tv);
        cur_val.setText("Starting download...");
        dialog.show();

        pb = (ProgressBar) dialog.findViewById(R.id.progress_bar);
        pb.setProgress(0);
        pb.setProgressDrawable(getResources().getDrawable(R.drawable.green_progress));
    }

}
