package com.ifoster.mvp.task.task_dashboard.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ifoster.R;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.task.task_dashboard.HomeWorkActivity;
import com.ifoster.mvp.task.task_dashboard.adapter.CurrentTaskAdapter;
import com.ifoster.mvp.task.task_dashboard.adapter.CurrentTaskAdapterOld;
import com.ifoster.mvp.task.task_dashboard.model.CurrentTaskModel;
import com.ifoster.mvp.task.task_dashboard.model.CurrentTaskModelOld;
import com.ifoster.mvp.task.task_dashboard.presenter.TaskPresenterImpl;
import com.ifoster.mvp.task.task_dashboard.view.TaskView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alam on 02-Oct-17.
 */

public class CurrentTaskFragment extends Fragment implements TaskView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.relLayMainContainer)
    RelativeLayout relLayMainContainer;

    CurrentTaskAdapterOld currentTaskAdapter;

    /*-----------Class Presenter-----------*/
    TaskPresenterImpl taskPresenterImpl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_current_task, container, false);
        ButterKnife.bind(this, view);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        taskPresenterImpl = new TaskPresenterImpl(getActivity(), this);
        getData();
    }

    private void getData() {
        ((HomeWorkActivity)getActivity()).showProgressBar();
        taskPresenterImpl.getCurrentTask();
    }

    private ArrayList<CurrentTaskModelOld> getCurrentTaskList() {

        int colorCodeType = 0;

        ArrayList<CurrentTaskModelOld> arrayListTask = new ArrayList<>();

        for (int i = 1; i <= 9; i++) {
            CurrentTaskModelOld currentTaskModel = new CurrentTaskModelOld();
            currentTaskModel.setDate("0" + i);
            currentTaskModel.setDay("Jan");
            currentTaskModel.setTaskTitle("Java");
            currentTaskModel.setTaskSubject("My New Task");
            currentTaskModel.setTaskDescription("Your description will be here.");
            currentTaskModel.setSeperatorDetail("This week");

            currentTaskModel.setColorType(colorCodeType);

            // increase color code value
            colorCodeType++;

            // reset color form strating again
            if (colorCodeType == 4) {
                colorCodeType = 0;
            }

            if (i == 1) {
                currentTaskModel.setDay("Oct");
                currentTaskModel.setTaskTitle("Math");
                currentTaskModel.setTaskSubject("Multiplying fractions word problems");
                currentTaskModel.setTaskDescription("Multiplying two numbers and find its fraction.");
            }

            if (i == 2) {
                currentTaskModel.setDay("Oct");
                currentTaskModel.setTaskTitle("Math");
                currentTaskModel.setTaskSubject("Substraction and Addition");
                currentTaskModel.setTaskDescription("Substraction two numbers and add 100 to the result.");
            }

            if (i == 3) {
                currentTaskModel.setDay("Oct");
                currentTaskModel.setTaskTitle("Math");
                currentTaskModel.setTaskSubject("Division with two number.");
                currentTaskModel.setTaskDescription("Divide a by b and find its result.");
            }

            if (i == 4) {
                currentTaskModel.setDay("Nov");
                currentTaskModel.setTaskTitle("General Science");
                currentTaskModel.setTaskSubject("Living and Non-living Things");
                currentTaskModel.setTaskDescription("Write the name of the living and non living thing.");
            }

            if (i == 5) {
                currentTaskModel.setDay("Nov");
                currentTaskModel.setTaskTitle("General Science");
                currentTaskModel.setTaskSubject("Plants");
                currentTaskModel.setTaskDescription("Writes 10 plants name.");
            }


            if (i == 6) {
                currentTaskModel.setDay("Dec");
                currentTaskModel.setTaskTitle("General Science");
                currentTaskModel.setTaskSubject("Wild Animals");
                currentTaskModel.setTaskDescription("Writes 10 Wild Animals name.");
            }

            if (i == 7) {
                currentTaskModel.setDay("Dec");
                currentTaskModel.setTaskTitle("English");
                currentTaskModel.setTaskSubject("Nouns");
                currentTaskModel.setTaskDescription("Describe nouns and its type.");
            }

            if (i == 8) {
                currentTaskModel.setDay("Dec");
                currentTaskModel.setTaskTitle("English");
                currentTaskModel.setTaskSubject("Articles");
                currentTaskModel.setTaskDescription("What is Articles and difference between Definite and InDefinite Articles.");
            }

            if (i == 9) {
                currentTaskModel.setDay("Dec");
                currentTaskModel.setTaskTitle("English");
                currentTaskModel.setTaskSubject("Verbs");
                currentTaskModel.setTaskDescription("What is verb.");
            }


            if (i == 1 || i == 3 || i == 7) {
                currentTaskModel.setItemType(2);
            } else {
                currentTaskModel.setItemType(1);
            }

            if (i == 1) {
                currentTaskModel.setSeperatorDetail("Today");
            }
            if (i == 3) {
                currentTaskModel.setSeperatorDetail("This week");
            }
            if (i == 7) {
                currentTaskModel.setSeperatorDetail("One week from now");
            }

            arrayListTask.add(currentTaskModel);
        }

        return arrayListTask;
    }

    @Override
    public void onSuccessCurrentTask(ArrayList<CurrentTaskModel.User> result, String s) {
        hideProgressBar();
        if (result != null) {

            int colorCodeType = 0;

            if (result.size() > 0) {

                for (CurrentTaskModel.User user : result) {

                    // set color
                    user.setColorType(colorCodeType);

                    // increase color code value
                    colorCodeType++;

                    // reset color form strating again
                    if (colorCodeType == 4) {
                        colorCodeType = 0;
                    }
                }
            }

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            CurrentTaskAdapter currentTaskAdapter = new CurrentTaskAdapter(getActivity(), result);
            recyclerView.setAdapter(currentTaskAdapter);
        }

    }

    @Override
    public void onUnsuccessCurrentTask(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    @Override
    public void onCurrentTaskInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryCurrentTask, relLayMainContainer);
    }

    OnClickInterface onRetryCurrentTask = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    @Override
    public void onSuccessArchievTask(ArrayList<CurrentTaskModel.User> message, String s) {
        /*--------------------NA--------------------*/
    }

    @Override
    public void onUnsuccessArchievTask(String message) {
         /*--------------------NA--------------------*/
    }

    @Override
    public void onArchievTaskInternetError() {
          /*--------------------NA--------------------*/
    }

    private void hideProgressBar(){
        ((HomeWorkActivity)getActivity()).hideProgressBar();
    }
}
