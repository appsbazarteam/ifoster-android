package com.ifoster.mvp.dashboard.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.mvp.achievement.AchievementActivity;
import com.ifoster.mvp.attendance_teacher.activity.TeacherAttendanceDashboardActivity;
import com.ifoster.mvp.class_time_table_teacher.TeacherClassTimeTableActivity;
import com.ifoster.mvp.events_program.EventsAndProgramActivity;
import com.ifoster.mvp.exam_schedule_teacher.TeacherExamScheduleActivity;
import com.ifoster.mvp.gallery.activity.GalleryActivity;
import com.ifoster.mvp.holiday_calender.HolidayCalenderActivity;
import com.ifoster.mvp.leave.leave_approval.LeaveApprovalActivity;
import com.ifoster.mvp.leave.leave_dashboard.LeaveDashboardActivity;
import com.ifoster.mvp.medical.StudentMedicalIdActivity;
import com.ifoster.mvp.notice_board.NoticeboardActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Alam on 30-Sep-17.
 */

public class TeacherFragment extends Fragment {

    @BindView(R.id.cardViewAttendance)
    CardView cardViewAttendance;

    @BindView(R.id.cardViewAchievement)
    CardView cardViewAchievement;

    @BindView(R.id.cardViewGallery)
    CardView cardViewGallery;

    @BindView(R.id.cardViewLeave)
    CardView cardViewLeave;

    @BindView(R.id.cardViewCommunication)
    CardView cardViewCommunication;

    @BindView(R.id.cardViewTodayActivity)
    CardView cardViewTodayActivity;


    @BindView(R.id.txtViewAttendance)
    TextView txtViewAttendance;

    @BindView(R.id.txtViewLeave)
    TextView txtViewLeave;

    @BindView(R.id.txtViewCommunication)
    TextView txtViewCommunication;

    @BindView(R.id.txtViewAchievement)
    TextView txtViewAchievement;

    @BindView(R.id.txtViewGallery)
    TextView txtViewGallery;

    @BindView(R.id.txtViewTodaysActivity)
    TextView txtViewTodaysActivity;

    @BindView(R.id.txtViewApproval)
    TextView txtViewApproval;

    @BindView(R.id.txtViewNoticeBoard)
    TextView txtViewNoticeBoard;

    @BindView(R.id.txtViewUpcomingSchedule)
    TextView txtViewUpcomingSchedule;

    @BindView(R.id.relLayMainContainer)
    RelativeLayout relLayMainContainer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teacher, container, false);
        ButterKnife.bind(this, view);
        setFont();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @OnClick(R.id.cardViewGallery)
    public void gallery() {
        startActivity(new Intent(getActivity(), GalleryActivity.class));
    }

    @OnClick({R.id.cardViewCommunication, R.id.cardViewGallery})
    public void communicationClick() {
        SnackNotify.showMessage("This feature is coming soon.", relLayMainContainer);
    }

    @OnClick(R.id.txtViewApproval)
    public void approval() {
        startActivity(new Intent(getActivity(), LeaveApprovalActivity.class));
    }

    @OnClick(R.id.txtViewNoticeBoard)
    public void noticeBoard() {
        Intent intent = new Intent(getActivity(), NoticeboardActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cardViewTimeTable)
    public void OnTimeTableClick() {
        startActivity(new Intent(getActivity(), TeacherClassTimeTableActivity.class));
    }

    @OnClick(R.id.cardViewMedicalID)
    public void OnMedicalIdClick() {
        startActivity(new Intent(getActivity(), StudentMedicalIdActivity.class));
    }


    @OnClick(R.id.cardViewAttendance)
    public void attendance() {
        Intent intent = new Intent(getActivity(), TeacherAttendanceDashboardActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cardViewLeave)
    public void leave() {
        Intent intent = new Intent(getActivity(), LeaveDashboardActivity.class);
        intent.putExtra(ConstIntent.KEY_USER_TYPE, Const.KEY_SCHOOL);
        startActivity(intent);
    }

    @OnClick(R.id.cardViewExamSchedule)
    public void examScheduleClick() {
        Intent intent = new Intent(getActivity(), TeacherExamScheduleActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cardViewAchievement)
    public void achievement() {
        startActivity(new Intent(getActivity(), AchievementActivity.class));
    }

    @OnClick(R.id.cardViewHoliday)
    public void onHolidayClick() {
        startActivity(new Intent(getActivity(), HolidayCalenderActivity.class));
    }


    @OnClick(R.id.cardViewCommunication)
    public void communication() {
        //startActivity(new Intent(getActivity(), GalleryActivity.class));
    }

    @OnClick(R.id.txtViewUpcomingSchedule)
    public void upcomingSchedule() {
        startActivity(new Intent(getActivity(), EventsAndProgramActivity.class));
    }

    private void setFont() {
        FontHelper.applyFont(getActivity(), txtViewAttendance, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewLeave, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewCommunication, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewAchievement, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewGallery, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        FontHelper.applyFont(getActivity(), txtViewTodaysActivity, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewApproval, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewNoticeBoard, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewUpcomingSchedule, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }
}
