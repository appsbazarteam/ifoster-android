package com.ifoster.mvp.dashboard.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.mvp.achievement.AchievementActivity;
import com.ifoster.mvp.attendance.AttendanceDetailActivity;
import com.ifoster.mvp.class_time_table.ClassTimeTableActivity;
import com.ifoster.mvp.events_program.EventsAndProgramActivity;
import com.ifoster.mvp.exam_schedule.ExamScheduleActivity;
import com.ifoster.mvp.fee.FeeDetailActivity;
import com.ifoster.mvp.gallery.activity.GalleryActivity;
import com.ifoster.mvp.holiday_calender.HolidayCalenderActivity;
import com.ifoster.mvp.leave.leave_dashboard.LeaveDashboardActivity;
import com.ifoster.mvp.medical.StudentMedicalIdActivity;
import com.ifoster.mvp.notice_board.NoticeboardActivity;
import com.ifoster.mvp.result.exam_list.ResultActivity;
import com.ifoster.mvp.task.task_dashboard.HomeWorkActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Alam on 30-Sep-17.
 */

public class ParentFragment extends Fragment {

    @BindView(R.id.cardViewAttendance)
    CardView cardViewAttendance;

    @BindView(R.id.cardViewFee)
    CardView cardViewFee;

    @BindView(R.id.cardViewResult)
    CardView cardViewResult;

    @BindView(R.id.cardViewLeave)
    CardView cardViewLeave;

    @BindView(R.id.cardViewCommunication)
    CardView cardViewCommunication;

    @BindView(R.id.cardViewRouteLocator)
    CardView cardViewRouteLocator;

    @BindView(R.id.cardViewTodayActivity)
    CardView cardViewTodayActivity;

    @BindView(R.id.txtViewAttendance)
    TextView txtViewAttendance;

    @BindView(R.id.txtViewResult)
    TextView txtViewResult;

    @BindView(R.id.txtViewFeeTitle)
    TextView txtViewFeeTitle;

    @BindView(R.id.txtViewLeave)
    TextView txtViewLeave;

    @BindView(R.id.txtViewCommunication)
    TextView txtViewCommunication;

    @BindView(R.id.txtViewRouteLocator)
    TextView txtViewRouteLocator;

    @BindView(R.id.txtViewTodaysActivity)
    TextView txtViewTodaysActivity;

    @BindView(R.id.txtViewHomeWork)
    TextView txtViewHomeWork;

    @BindView(R.id.txtViewNoticeBoard)
    TextView txtViewNoticeBoard;

    @BindView(R.id.txtViewUpcomingSchedule)
    TextView txtViewUpcomingSchedule;

    @BindView(R.id.relLayMainContainer)
    RelativeLayout relLayMainContainer;

    @BindView(R.id.lilLayCenter)
    LinearLayout lilLayCenter;
    @BindView(R.id.imgAttendance)
    ImageView imgAttendance;
    @BindView(R.id.rlCircleAttentance)
    RelativeLayout rlCircleAttentance;
    @BindView(R.id.imgFee)
    ImageView imgFee;
    @BindView(R.id.rlCircleFee)
    RelativeLayout rlCircleFee;
    @BindView(R.id.imgResult)
    ImageView imgResult;
    @BindView(R.id.rlCircleResult)
    RelativeLayout rlCircleResult;
    @BindView(R.id.rlCircleLeave)
    RelativeLayout rlCircleLeave;
    @BindView(R.id.imgExamSchedule)
    ImageView imgExamSchedule;
    @BindView(R.id.rlCircleExamSchedule)
    RelativeLayout rlCircleExamSchedule;
    @BindView(R.id.txtViewExamSchedule)
    TextView txtViewExamSchedule;
    @BindView(R.id.cardViewExamSchedule)
    CardView cardViewExamSchedule;
    @BindView(R.id.imgTimeTable)
    ImageView imgTimeTable;
    @BindView(R.id.rlCircle6)
    RelativeLayout rlCircle6;
    @BindView(R.id.txtViewTimeTable)
    TextView txtViewTimeTable;
    @BindView(R.id.cardViewTimeTable)
    CardView cardViewTimeTable;
    @BindView(R.id.imgHoliday)
    ImageView imgHoliday;
    @BindView(R.id.llCircleHoliday)
    RelativeLayout llCircleHoliday;
    @BindView(R.id.txtViewHoliday)
    TextView txtViewHoliday;
    @BindView(R.id.cardViewHoliday)
    CardView cardViewHoliday;
    @BindView(R.id.imgMedicalID)
    ImageView imgMedicalID;
    @BindView(R.id.llCircleMedicalID)
    RelativeLayout llCircleMedicalID;
    @BindView(R.id.txtViewMedicalID)
    TextView txtViewMedicalID;
    @BindView(R.id.cardViewMedicalID)
    CardView cardViewMedicalID;
    @BindView(R.id.imgAchievement)
    ImageView imgAchievement;
    @BindView(R.id.rlCircleAchievement)
    RelativeLayout rlCircleAchievement;
    @BindView(R.id.txtViewAchievement)
    TextView txtViewAchievement;
    @BindView(R.id.cardViewAchievement)
    CardView cardViewAchievement;
    @BindView(R.id.imgGallery)
    ImageView imgGallery;
    @BindView(R.id.rlCircleGallery)
    RelativeLayout rlCircleGallery;
    @BindView(R.id.txtViewGallery)
    TextView txtViewGallery;
    @BindView(R.id.cardViewGallery)
    CardView cardViewGallery;
    @BindView(R.id.imgCommunication)
    ImageView imgCommunication;
    @BindView(R.id.llCircleCommunication)
    RelativeLayout llCircleCommunication;
    @BindView(R.id.imgRoute)
    ImageView imgRoute;
    @BindView(R.id.rlCircleRoute)
    RelativeLayout rlCircleRoute;
    @BindView(R.id.linLayGrid)
    LinearLayout linLayGrid;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent, container, false);
        ButterKnife.bind(this, view);
        setFont();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @OnClick(R.id.cardViewAttendance)
    public void attendance() {
        startActivity(new Intent(getActivity(), AttendanceDetailActivity.class));
    }

    @OnClick(R.id.cardViewGallery)
    public void gallery() {
        startActivity(new Intent(getActivity(), GalleryActivity.class));
    }

    @OnClick({R.id.cardViewCommunication, R.id.cardViewResult, R.id.cardViewRouteLocator})
    public void communicationClick() {
        SnackNotify.showMessage("This feature is coming soon.", relLayMainContainer);
    }

    @OnClick(R.id.cardViewFee)
    public void fee() {
        startActivity(new Intent(getActivity(), FeeDetailActivity.class));
    }

    @OnClick(R.id.cardViewLeave)
    public void leave() {
        Intent intent = new Intent(getActivity(), LeaveDashboardActivity.class);
        intent.putExtra(ConstIntent.KEY_USER_TYPE, Const.KEY_PARENTS);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewHomeWork)
    public void homeWork() {
        startActivity(new Intent(getActivity(), HomeWorkActivity.class));
    }

    @OnClick(R.id.txtViewUpcomingSchedule)
    public void upcomingSchedule() {
        startActivity(new Intent(getActivity(), EventsAndProgramActivity.class));
    }

    @OnClick(R.id.txtViewNoticeBoard)
    public void noticeBoard() {
        Intent intent = new Intent(getActivity(), NoticeboardActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cardViewMedicalID)
    public void onMedicalId() {
        startActivity(new Intent(getActivity(), StudentMedicalIdActivity.class));
    }

    @OnClick(R.id.cardViewHoliday)
    public void onHolidayClick() {
        startActivity(new Intent(getActivity(), HolidayCalenderActivity.class));
    }

    @OnClick(R.id.cardViewResult)
    public void result() {
        startActivity(new Intent(getActivity(), ResultActivity.class));
    }

    @OnClick(R.id.cardViewExamSchedule)
    public void onExamScheduleClick() {
        startActivity(new Intent(getActivity(), ExamScheduleActivity.class));
    }

    @OnClick(R.id.cardViewTimeTable)
    public void onTimeTableClickClick() {
        startActivity(new Intent(getActivity(), ClassTimeTableActivity.class));
    }

    private void setFont() {
        FontHelper.applyFont(getActivity(), txtViewAttendance, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewFeeTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewResult, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewLeave, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewCommunication, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewRouteLocator, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        FontHelper.applyFont(getActivity(), txtViewTodaysActivity, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        FontHelper.applyFont(getActivity(), txtViewHomeWork, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewNoticeBoard, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewUpcomingSchedule, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    @OnClick(R.id.cardViewAchievement)
    public void onViewClicked() {
        startActivity(new Intent(getActivity(), AchievementActivity.class));
    }
}
