package com.ifoster.mvp.attendance_teacher.view;

import com.ifoster.mvp.attendance_teacher.model.ClassStudentListModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface TakeAttendanceView {

    public void onSuccessClassStudent(ArrayList<ClassStudentListModel.StudentArrayList> result);

    public void onUnsuccessClassStudent(String message);

    public void onClassStudentInternetError();


    public void onSuccessTakeAttendance(String message);

    public void onUnsuccessTakeAttendance(String message);

    public void onTakeAttendanceInternetError();
}
