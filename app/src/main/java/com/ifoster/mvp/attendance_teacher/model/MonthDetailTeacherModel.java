package com.ifoster.mvp.attendance_teacher.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MonthDetailTeacherModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("attedance_date")
    @Expose
    private String attedanceDate;
    @SerializedName("Halfday")
    @Expose
    private String halfday;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAttedanceDate() {
        return attedanceDate;
    }

    public void setAttedanceDate(String attedanceDate) {
        this.attedanceDate = attedanceDate;
    }

    public String getHalfday() {
        return halfday;
    }

    public void setHalfday(String halfday) {
        this.halfday = halfday;
    }
}