package com.ifoster.mvp.attendance_teacher.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

public class ClassStudentListModel implements Serializable {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("List")
    @Expose
    private ArrayList<StudentArrayList> ArrayList = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<StudentArrayList> getArrayList() {
        return ArrayList;
    }

    public void setArrayList(ArrayList<StudentArrayList> ArrayList) {
        this.ArrayList = ArrayList;
    }

    public class StudentArrayList implements Serializable {

        @SerializedName("student_id")
        @Expose
        private String studentId;
        @SerializedName("student_name")
        @Expose
        private String studentName;
        @SerializedName("father_name")
        @Expose
        private String fatherName;
        @SerializedName("admission_no")
        @Expose
        private String admissionNo;
        @SerializedName("image_url")
        @Expose
        private String imageUrl;

        boolean absent;
        boolean present = true;

        public boolean isAbsent() {
            return absent;
        }

        public void setAbsent(boolean absent) {
            this.absent = absent;
        }

        public boolean isPresent() {
            return present;
        }

        public void setPresent(boolean present) {
            this.present = present;
        }

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getStudentName() {
            return studentName;
        }

        public void setStudentName(String studentName) {
            this.studentName = studentName;
        }

        public String getFatherName() {
            return fatherName;
        }

        public void setFatherName(String fatherName) {
            this.fatherName = fatherName;
        }

        public String getAdmissionNo() {
            return admissionNo;
        }

        public void setAdmissionNo(String admissionNo) {
            this.admissionNo = admissionNo;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
    }
}