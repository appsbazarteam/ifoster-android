package com.ifoster.mvp.attendance_teacher.model;

import java.io.Serializable;

/**
 * Created by Alam on 07-Oct-17.
 */

public class TakeAttendanceModel implements Serializable {

    String name;
    int rollNo;
    boolean absent;
    boolean present;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public boolean getAbsent() {
        return absent;
    }

    public void setAbsent(boolean absent) {
        this.absent = absent;
    }

    public boolean getPresent() {
        return present;
    }

    public void setPresent(boolean present) {
        this.present = present;
    }
}
