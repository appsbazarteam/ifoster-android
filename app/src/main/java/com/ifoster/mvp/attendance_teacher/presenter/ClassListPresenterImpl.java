package com.ifoster.mvp.attendance_teacher.presenter;

import android.app.Activity;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.attendance_teacher.model.ClassListModel;
import com.ifoster.mvp.attendance_teacher.view.ClassListView;
import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class ClassListPresenterImpl implements ClassListPresenter {

    private Activity activity;
    private ClassListView classListView;
    private JSONObject jsonObject;

    public ClassListPresenterImpl(Activity activity, ClassListView classListView) {
        this.activity = activity;
        this.classListView = classListView;
    }

    @Override
    public void getTeacherAttendanceList() {
        try {
            ApiAdapter.getInstance(activity);
            callTeacherAttendanceList();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            classListView.onTeacherAttendanceInternetError();
        }
    }

    private void callTeacherAttendanceList() {
        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_EMPLOYEE_ID, UserSession.getInstance().getEmployeeId());
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<ClassListModel> getLoginOutput = ApiAdapter.getApiService().getTeacherAttendance("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<ClassListModel>() {
            @Override
            public void onResponse(Call<ClassListModel> call, Response<ClassListModel> response) {
                try {
                    ClassListModel ClassListModel = response.body();
                    String message = ClassListModel.getMessage();

                    if (ClassListModel.getStatus()) {
                        classListView.onSuccessTeacherAttendance(ClassListModel.getUser());
                    } else {
                        classListView.onUnsuccessTeacherAttendance(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    classListView.onUnsuccessTeacherAttendance(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ClassListModel> call, Throwable t) {
                classListView.onUnsuccessTeacherAttendance(activity.getString(R.string.error_server));
            }
        });
    }

}
