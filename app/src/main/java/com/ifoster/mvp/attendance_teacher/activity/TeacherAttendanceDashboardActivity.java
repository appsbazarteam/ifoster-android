package com.ifoster.mvp.attendance_teacher.activity;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.utility.Resource;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.attendance_teacher.fragment.ClassListFragment;
import com.ifoster.mvp.attendance_teacher.fragment.TeacherAttendanceDetailFragment;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;

public class TeacherAttendanceDashboardActivity extends BaseActivity {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_attendance);

        setFont();
        setToolBar();
        createViewPager();
        tabLayout.setupWithViewPager(viewPager);
        createTabIcons();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    private void createTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText(Resource.toString(R.string.class_list));
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText(Resource.toString(R.string.attendance_detail));
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        FontHelper.applyFont(this, tabOne, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, tabTwo, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    private void createViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ClassListFragment(), Resource.toString(R.string.class_list));
        adapter.addFrag(new TeacherAttendanceDetailFragment(), Resource.toString(R.string.attendance_detail));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @OnClick(R.id.imgViewBack)
    public void goBack() {
        finish();
    }

    private void setToolBar() {
        txtViewToolbarTitle.setText(getString(R.string.title_take_attendace));
    }
}
