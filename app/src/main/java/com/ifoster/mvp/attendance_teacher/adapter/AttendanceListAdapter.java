package com.ifoster.mvp.attendance_teacher.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.utility.BitmapUtils;
import com.ifoster.mvp.attendance_teacher.model.ClassStudentListModel;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class AttendanceListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ClassStudentListModel.StudentArrayList> listData;

    public AttendanceListAdapter(Context context, ArrayList<ClassStudentListModel.StudentArrayList> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_attendance_list, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final ClassStudentListModel.StudentArrayList studentArrayList = listData.get(position);
        if(studentArrayList == null)
            return;

        BitmapUtils.loadImageCircular(((ViewHolderList) holder).imgViewStudentImage,studentArrayList.getImageUrl());
        ((ViewHolderList) holder).txtViewStudentName.setText(studentArrayList.getStudentName() + " ("+studentArrayList.getStudentId()+")");

        // set present check box
        ((ViewHolderList) holder).checkBoxPresent.setChecked(studentArrayList.isPresent());

        // set absent check box
        ((ViewHolderList) holder).checkBoxAbsent.setChecked(studentArrayList.isAbsent());

        ((ViewHolderList) holder).checkBoxPresent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                ((ViewHolderList) holder).checkBoxAbsent.setChecked(!isChecked);

                studentArrayList.setAbsent(!isChecked);
                studentArrayList.setPresent(isChecked);
            }
        });

        ((ViewHolderList) holder).checkBoxAbsent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((ViewHolderList) holder).checkBoxPresent.setChecked(!isChecked);
                studentArrayList.setPresent(!isChecked);
                studentArrayList.setAbsent(isChecked);
            }
        });

    }

    public ArrayList<ClassStudentListModel.StudentArrayList> getAbsentList() {
        ArrayList<ClassStudentListModel.StudentArrayList> absentList = new ArrayList<>();

        if (listData != null) {
            for (int i = 0; i < listData.size(); i++) {
                if (listData.get(i).isAbsent()) {
                    absentList.add(listData.get(i));
                }
            }
        }
        return absentList;
    }

    public ArrayList<ClassStudentListModel.StudentArrayList> getPresentList() {

        ArrayList<ClassStudentListModel.StudentArrayList> presentList = new ArrayList<>();

        if (listData != null) {
            for (int i = 0; i < listData.size(); i++) {
                if (listData.get(i).isPresent()) {
                    presentList.add(listData.get(i));
                }
            }
        }
        return presentList;
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.imgViewStudentImage)
        ImageView imgViewStudentImage;

        @BindView(R.id.txtViewStudentName)
        TextView txtViewStudentName;

        @BindView(R.id.checkBoxPresent)
        AppCompatCheckBox checkBoxPresent;

        @BindView(R.id.checkBoxAbsent)
        AppCompatCheckBox checkBoxAbsent;

        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
