package com.ifoster.mvp.attendance_teacher.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface TeacherAttendanceDetailPresenter {

    public void getAttendanceDetail1(String month);

    public void getAttendanceDetail2(String month, int attendanceType);
}
