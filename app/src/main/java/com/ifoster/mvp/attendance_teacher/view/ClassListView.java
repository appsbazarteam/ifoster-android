package com.ifoster.mvp.attendance_teacher.view;

import com.ifoster.mvp.attendance_teacher.model.ClassListModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ClassListView {

    public void onSuccessTeacherAttendance(ArrayList<ClassListModel.User> result);

    public void onUnsuccessTeacherAttendance(String message);

    public void onTeacherAttendanceInternetError();
}
