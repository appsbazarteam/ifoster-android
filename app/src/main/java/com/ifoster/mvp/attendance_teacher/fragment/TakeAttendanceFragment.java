package com.ifoster.mvp.attendance_teacher.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.attendance_teacher.adapter.AttendanceListAdapter;
import com.ifoster.mvp.attendance_teacher.model.ClassListModel;
import com.ifoster.mvp.attendance_teacher.model.ClassStudentListModel;
import com.ifoster.mvp.attendance_teacher.presenter.TakeAttendancePresenterImpl;
import com.ifoster.mvp.attendance_teacher.view.TakeAttendanceView;
import com.ifoster.mvp.confirm_attendance.ConfirmAttendanceActivity;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Alam on 02-Oct-17.
 */

public class TakeAttendanceFragment extends Fragment implements TakeAttendanceView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.relLayMainContainer)
    RelativeLayout relLayMainContainer;

    private AttendanceListAdapter attendanceListAdapter;
    private TakeAttendancePresenterImpl takeAttendancePresenterImpl;
    private ClassListModel.User user = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_take_attendance, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        takeAttendancePresenterImpl = new TakeAttendancePresenterImpl(getActivity(), this);
        getIntentData();
    }

    private void getIntentData() {
        Intent intent = getActivity().getIntent();
        if (intent.hasExtra(ConstIntent.KEY_CLASS_LIST)) {
            user = (ClassListModel.User) intent.getSerializableExtra(ConstIntent.KEY_CLASS_LIST);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        if (user != null) {
            takeAttendancePresenterImpl.getClassStudentList(user.getStuclass(), user.getSection());
        }
    }

    @Override
    public void onSuccessClassStudent(ArrayList<ClassStudentListModel.StudentArrayList> result) {
        if (result != null && result.size() > 0) {
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                attendanceListAdapter = new AttendanceListAdapter(getActivity(), result);
                recyclerView.setAdapter(attendanceListAdapter);
        }
    }

    @Override
    public void onUnsuccessClassStudent(String message) {
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    @Override
    public void onClassStudentInternetError() {
        SnackNotify.checkConnection(onRetryClassStudent, relLayMainContainer);
    }

    @Override
    public void onSuccessTakeAttendance(String message) {

    }

    @Override
    public void onUnsuccessTakeAttendance(String message) {

    }

    @Override
    public void onTakeAttendanceInternetError() {

    }

    OnClickInterface onRetryClassStudent = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    @OnClick(R.id.cardViewBtnContinue)
    public void btnContinue() {
        if (attendanceListAdapter != null) {
            ArrayList<ClassStudentListModel.StudentArrayList> absentList = attendanceListAdapter.getAbsentList();
            if (absentList.size() > 0) {
                Intent intent = new Intent(getActivity(), ConfirmAttendanceActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(ConstIntent.KEY_ABSENT_LIST, absentList);
                intent.putExtras(bundle);
                startActivity(intent);
            } else {
                ArrayList<ClassStudentListModel.StudentArrayList> presentList = attendanceListAdapter.getPresentList();
                Intent intent = new Intent(getActivity(), ConfirmAttendanceActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(ConstIntent.KEY_ABSENT_LIST, presentList);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }
    }
}
