package com.ifoster.mvp.attendance_teacher.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface TakeAttendancePresenter {

    void getClassStudentList(String className, String section);

    void takeAttendance(String absentList,String presentList,String attendanceData);
}
