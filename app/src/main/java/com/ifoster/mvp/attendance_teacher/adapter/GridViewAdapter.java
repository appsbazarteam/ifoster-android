package com.ifoster.mvp.attendance_teacher.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ifoster.BuildConfig;
import com.ifoster.R;

public class GridViewAdapter extends BaseAdapter {
    private String[] result;
    private static LayoutInflater inflater = null;

    public GridViewAdapter(Activity activity, String[] prgmNameList) {
        result = prgmNameList;
        inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView textViewMonthName;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.item_month, null);
        holder.textViewMonthName = (TextView) rowView.findViewById(R.id.textViewMonthName);
        holder.textViewMonthName.setText(String.valueOf(result[position]));

        return rowView;
    }
} 