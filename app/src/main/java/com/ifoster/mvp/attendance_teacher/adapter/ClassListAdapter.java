package com.ifoster.mvp.attendance_teacher.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.mvp.attendance_teacher.activity.TakeAttendanceActivity;
import com.ifoster.mvp.attendance_teacher.model.ClassListModel;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alam on 15-May-18.
 */

public class ClassListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private ArrayList<ClassListModel.User> listData;

    public ClassListAdapter(Activity activity, ArrayList<ClassListModel.User> listData) {
        this.activity = activity;
        this.listData = listData;
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(activity).inflate(R.layout.item_teacher_attendance, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ClassListModel.User user = listData.get(position);
        if(user == null)
            return;

        if (user.getSection() != null) {
            ((ViewHolderList) holder).txtViewSection.setText(user.getSection());
        } else {
            ((ViewHolderList) holder).txtViewSection.setText("--NA--");
        }

        if (user.getStuclass() != null) {
            ((ViewHolderList) holder).txtViewClass.setText(user.getStuclass());
        } else {
            ((ViewHolderList) holder).txtViewClass.setText("--NA--");
        }

        ((ViewHolderList) holder).cardviewMainContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, TakeAttendanceActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(ConstIntent.KEY_CLASS_LIST, user);
                intent.putExtras(bundle);
                activity.startActivity(intent);
            }
        });
    }

    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.cardviewMainContainer)
        CardView cardviewMainContainer;

        @BindView(R.id.txtViewClass)
        TextView txtViewClass;

        @BindView(R.id.txtViewSection)
        TextView txtViewSection;

        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
