package com.ifoster.mvp.attendance_teacher.fragment;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.Resource;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.attendance.adapter.GridViewAdapter;
import com.ifoster.mvp.attendance.model.AttendanceDetailModel;
import com.ifoster.mvp.attendance_teacher.activity.TeacherAttendanceDashboardActivity;
import com.ifoster.mvp.attendance_teacher.adapter.GridViewHalfDayAdapter;
import com.ifoster.mvp.attendance_teacher.adapter.TeacherAttendanceMonthAdapter;
import com.ifoster.mvp.attendance_teacher.presenter.TeacherAttendanceDetailPresenterImpl;
import com.ifoster.mvp.attendance_teacher.view.TeacherAttendanceDetailView;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import java.util.ArrayList;
import java.util.Calendar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Alam on 03-Jun-18.
 */

public class TeacherAttendanceDetailFragment extends Fragment implements OnChartValueSelectedListener, TeacherAttendanceDetailView {

    @BindView(R.id.relLayMainContainer)
    RelativeLayout relLayMainContainer;

    @BindView(R.id.txtViewStudentName)
    TextView txtViewStudentName;

    @BindView(R.id.txtViewTotalPresent)
    TextView txtViewTotalPresent;

    @BindView(R.id.txtViewTotalLeave)
    TextView txtViewTotalLeave;

    @BindView(R.id.txtViewTotalAbsent)
    TextView txtViewTotalAbsent;

    @BindView(R.id.txtViewAttendanceCycle)
    TextView txtViewAttendanceCycle;

    @BindView(R.id.pieChartCenterText)
    TextView pieChartCenterText;

    @BindView(R.id.pieChart)
    PieChart pieChart;

    @BindView(R.id.relLayAbsent)
    RelativeLayout relLayAbsent;

    @BindView(R.id.relLayPresent)
    RelativeLayout relLayPresent;

    @BindView(R.id.relLayLeave)
    RelativeLayout relLayLeave;

    /*Absent Left Container*/
    @BindView(R.id.llAbsentContainer)
    LinearLayout llAbsentContainer;

    @BindView(R.id.gridViewAbsent)
    GridView gridViewAbsent;

    @BindView(R.id.gridViewHalfDayAbsent)
    GridView gridViewHalfDayAbsent;

    /*Present Left Container*/
    @BindView(R.id.llPresentContainer)
    LinearLayout llPresentContainer;

    @BindView(R.id.gridViewPresent)
    GridView gridViewPresent;

    @BindView(R.id.gridViewHalfDayPresent)
    GridView gridViewHalfDayPresent;

    /*Leave Left Container*/
    @BindView(R.id.llLeaveContainer)
    LinearLayout llLeaveContainer;

    @BindView(R.id.gridViewLeave)
    GridView gridViewLeave;

    @BindView(R.id.gridViewHalfDayLeave)
    GridView gridViewHalfDayLeave;


    @BindView(R.id.txtViewAttendanceSummery)
    TextView txtViewAttendanceSummery;

    @BindView(R.id.imgViewAbsentPlus)
    ImageView imgViewAbsentPlus;

    @BindView(R.id.imgViewPresentPlus)
    ImageView imgViewPresentPlus;

    @BindView(R.id.imgViewLeavePlus)
    ImageView imgViewLeavePlus;

    /*-----Absent Presnt Leave title*/
    @BindView(R.id.txtAbsent)
    TextView txtAbsent;

    @BindView(R.id.txtPresent)
    TextView txtPresent;

    @BindView(R.id.txtLeave)
    TextView txtLeave;

    /*-----Absent Presnt Leave No found title*/
    @BindView(R.id.txtViewLeaveNoFound)
    TextView txtViewLeaveNoFound;

    @BindView(R.id.txtViewPresentNoFound)
    TextView txtViewPresentNoFound;

    @BindView(R.id.txtViewAbsentNoFound)
    TextView txtViewAbsentNoFound;

    /*-----Absent Presnt Leave date are title*/
    @BindView(R.id.txtViewAbsentDateAre)
    TextView txtViewAbsentDateAre;

    @BindView(R.id.txtViewPresentDateAre)
    TextView txtViewPresentDateAre;

    @BindView(R.id.txtViewLeaveDateAre)
    TextView txtViewLeaveDateAre;

    @BindView(R.id.recyclerViewMonth)
    RecyclerView recyclerViewMonth;

    private TeacherAttendanceDetailPresenterImpl teacherAttendanceDetailPresenterImpl;
    private AttendanceDetailModel attendanceDetailModel;
    private String monthSelected;
    private int attendanceType = 0;
    private String currentMonth;
    private int scrollPosition;
    private int userLogedInType = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teacher_attendance_detail, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        teacherAttendanceDetailPresenterImpl = new TeacherAttendanceDetailPresenterImpl(getActivity(), this);

        // get current month
        monthSelected = Utils.getCurrentMonthForFee();
        currentMonth  = Utils.getCurrentMonth();;

        setFont();
        bindMonthToRecyclerView();
        getIntentData();
        getData();
    }

    private void bindMonthToRecyclerView() {
        recyclerViewMonth.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewMonth.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMonth.setNestedScrollingEnabled(false);

        ArrayList<com.ifoster.common.adapter.model.MonthModel> list = getMonthList();
        TeacherAttendanceMonthAdapter noticeboardAdapter = new TeacherAttendanceMonthAdapter(getActivity(), list, this);
        recyclerViewMonth.setAdapter(noticeboardAdapter);
        recyclerViewMonth.scrollToPosition(scrollPosition);
    }

    private void getIntentData() {
        Intent intent = getActivity().getIntent();
        if (intent.hasExtra(ConstIntent.KEY_USER_TYPE)) {
            userLogedInType = intent.getIntExtra(ConstIntent.KEY_USER_TYPE, 0);
            if (userLogedInType == 0) {
                userLogedInType = UserSession.getInstance().getLoginType();
            }
        } else {
            userLogedInType = UserSession.getInstance().getLoginType();
        }
    }

    private void setFont() {
        txtViewAttendanceSummery.setPaintFlags(txtViewAttendanceSummery.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        FontHelper.applyFont(getActivity(), txtViewStudentName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewAttendanceCycle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewAttendanceSummery, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), pieChartCenterText, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        FontHelper.applyFont(getActivity(), txtAbsent, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtPresent, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtLeave, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        FontHelper.applyFont(getActivity(), txtViewTotalAbsent, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewTotalPresent, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewTotalLeave, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        FontHelper.applyFont(getActivity(), txtViewPresentDateAre, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewAbsentDateAre, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewLeaveDateAre, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        FontHelper.applyFont(getActivity(), txtViewAbsentNoFound, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewPresentNoFound, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(getActivity(), txtViewLeaveNoFound, FontHelper.FontType.FONT_QUICKSAND_BOLD);

    }

    @OnClick(R.id.relLayAbsent)
    public void onAbsentClick() {
        attendanceType = 1;
        llPresentContainer.setVisibility(View.GONE);
        llLeaveContainer.setVisibility(View.GONE);

        if (llAbsentContainer.getVisibility() == View.VISIBLE) {
            imgViewAbsentPlus.setImageResource(R.mipmap.ic_plus);
            llAbsentContainer.setVisibility(View.GONE);
        } else {
            llAbsentContainer.setVisibility(View.VISIBLE);
            imgViewAbsentPlus.setImageResource(R.mipmap.ic_minus);

            imgViewPresentPlus.setImageResource(R.mipmap.ic_plus);
            imgViewLeavePlus.setImageResource(R.mipmap.ic_plus);

            callMonthDateApi();
        }
    }

    private void callMonthDateApi() {
        teacherAttendanceDetailPresenterImpl.getAttendanceDetail2(monthSelected, attendanceType);
    }

    @OnClick(R.id.relLayPresent)
    public void onPresentClick() {
        attendanceType = 0;

        llAbsentContainer.setVisibility(View.GONE);
        llLeaveContainer.setVisibility(View.GONE);

        if (llPresentContainer.getVisibility() == View.VISIBLE) {
            imgViewPresentPlus.setImageResource(R.mipmap.ic_plus);
            llPresentContainer.setVisibility(View.GONE);
        } else {

            llPresentContainer.setVisibility(View.VISIBLE);
            imgViewPresentPlus.setImageResource(R.mipmap.ic_minus);

            imgViewAbsentPlus.setImageResource(R.mipmap.ic_plus);
            imgViewLeavePlus.setImageResource(R.mipmap.ic_plus);

            callMonthPresentApi();
        }
    }

    private void callMonthPresentApi() {
        teacherAttendanceDetailPresenterImpl.getEmployeePresentData();
    }

    @OnClick(R.id.relLayLeave)
    public void onLeaveClick() {
        attendanceType = 2;

        llAbsentContainer.setVisibility(View.GONE);
        llPresentContainer.setVisibility(View.GONE);

        if (llLeaveContainer.getVisibility() == View.VISIBLE) {
            imgViewLeavePlus.setImageResource(R.mipmap.ic_plus);
            llLeaveContainer.setVisibility(View.GONE);
        } else {
            llLeaveContainer.setVisibility(View.VISIBLE);
            imgViewLeavePlus.setImageResource(R.mipmap.ic_minus);

            imgViewAbsentPlus.setImageResource(R.mipmap.ic_plus);
            imgViewPresentPlus.setImageResource(R.mipmap.ic_plus);

            callMonthDateApi();
        }
    }

    private void setPiChat() {

        String todayAttendance = "NA";
        pieChartCenterText.setText(todayAttendance);

//        if (attendanceDetailModel.getCurrentDateStatus() != null /*&& attendanceDetailModel.getCurrentDateStatus().size() > 0*/) {
//            if (attendanceDetailModel.getCurrentDateStatus().getAttendence().equals("0")) {
//                todayAttendance = "Present";
//            } else if (attendanceDetailModel.getCurrentDateStatus().getAttendence().equals("1")) {
//                todayAttendance = "Absent";
//            } else if (attendanceDetailModel.getCurrentDateStatus().getAttendence().equals("2")) {
//                todayAttendance = "Leave";
//            }
//        }

        pieChart.setUsePercentValues(true);
        pieChart.setCenterTextSize(15f);

        // get current month
        Calendar calendar = Calendar.getInstance();
        int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        float totalAbsent = (((attendanceDetailModel.getAbsentcount() * 100) / days));
        float totalPresent = (((attendanceDetailModel.getPresentcount() * 100) / days));
        float totalLeave = (((Float.parseFloat(attendanceDetailModel.getLeave()) * 100) / days));

        float remaningSpace = 100 - (totalAbsent + totalPresent + totalLeave);

        ArrayList<Entry> yvalues = new ArrayList<Entry>();
        yvalues.add(new Entry(totalAbsent, 0));
        yvalues.add(new Entry(totalPresent, 1));
        yvalues.add(new Entry(totalLeave, 2));
        yvalues.add(new Entry(remaningSpace, 3));

        PieDataSet dataSet = new PieDataSet(yvalues, "Election Results");

        ArrayList<String> xVals = new ArrayList<String>();

        xVals.add("January");
        xVals.add("February");
        xVals.add("March");
        xVals.add("April");

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        pieChart.setData(data);

        pieChart.setDescription("");

        pieChart.setDrawHoleEnabled(true);
        pieChart.setTransparentCircleRadius(25f);
        pieChart.setHoleRadius(70f);

        // remove circle text
        pieChart.setDrawSliceText(false);
        pieChart.getData().setDrawValues(false);

        // remove bottom indecator
        Legend leg = pieChart.getLegend();
        leg.setEnabled(false);

        // set each set color
        ArrayList<Integer> colors = new ArrayList<Integer>();

        colors.add(Color.parseColor("#777ED8"));
        colors.add(Color.parseColor("#E6E14C"));
        colors.add(Color.parseColor("#E54E4E"));
        colors.add(Color.parseColor("#ffffff"));
        dataSet.setColors(colors);

        data.setValueTextSize(16f);
        data.setValueTextColor(Color.DKGRAY);
        pieChart.setOnChartValueSelectedListener(this);

        pieChart.animateXY(1400, 1400);

    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

        if (e == null)
            return;
        Log.i("VAL SELECTED",
                "Value: " + e.getVal() + ", xIndex: " + e.getXIndex()
                        + ", DataSet index: " + dataSetIndex);
    }

    private void getData() {
        ((TeacherAttendanceDashboardActivity)getActivity()).showProgressBar();
        teacherAttendanceDetailPresenterImpl.getAttendanceDetail1(monthSelected);
    }

    @Override
    public void onNothingSelected() {
        Log.i("PieChart", "nothing selected");
    }

    @Override
    public void onSuccessAttendanceDetail(AttendanceDetailModel attendanceDetailModel) {
        ((TeacherAttendanceDashboardActivity)getActivity()).hideProgressBar();
        this.attendanceDetailModel = attendanceDetailModel;
        setData();
    }

    private void setData() {
        if (attendanceDetailModel != null) {
            setPiChat();

            txtViewStudentName.setText("Teacher");
            txtViewAttendanceCycle.setText(attendanceDetailModel.getMonthRange());
            txtViewTotalAbsent.setText(String.valueOf(attendanceDetailModel.getAbsentcount()));
            txtViewTotalPresent.setText(String.valueOf(attendanceDetailModel.getPresentcount()));
            txtViewTotalLeave.setText(String.valueOf(attendanceDetailModel.getLeave()));
        }
    }

    @Override
    public void onUnsuccessAttendanceDetail(String message) {
        ((TeacherAttendanceDashboardActivity)getActivity()).hideProgressBar();
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    @Override
    public void onAttendanceDetailInternetError() {
        ((TeacherAttendanceDashboardActivity)getActivity()).hideProgressBar();
        SnackNotify.checkConnection(onRetryAttendanceDetail, relLayMainContainer);
    }

    @Override
    public void onSuccessMonthDetail(String data, String totalHaldDay) {
        ((TeacherAttendanceDashboardActivity)getActivity()).hideProgressBar();
        String[] dateArray;
        String[] halfDayArray;

        if (data != null) {

            if (data.contains(",")) {
                dateArray = data.split(",");
            } else {
                dateArray = new String[]{data};
            }

            if (dateArray.length > 0) {

                // remove first item from array
                int n = dateArray.length - 1;
                String[] newArray = new String[n];
                System.arraycopy(dateArray, 1, newArray, 0, n);

                GridViewAdapter gridViewAdapter = new GridViewAdapter(getActivity(), newArray);

                if (attendanceType == 0) { // Present
                    gridViewPresent.setAdapter(gridViewAdapter);

                    if (newArray.length > 0) {
                        txtViewPresentNoFound.setVisibility(View.GONE);
                    } else {
                        txtViewPresentNoFound.setVisibility(View.VISIBLE);
                    }

                } else if (attendanceType == 1) { // Absent

                    gridViewAbsent.setAdapter(gridViewAdapter);

                    if (newArray.length > 0) {
                        txtViewAbsentNoFound.setVisibility(View.GONE);
                    } else {
                        txtViewAbsentNoFound.setVisibility(View.VISIBLE);
                    }

                } else if (attendanceType == 2) { // Leave

                    gridViewLeave.setAdapter(gridViewAdapter);

                    if (newArray.length > 0) {
                        txtViewLeaveNoFound.setVisibility(View.GONE);
                    } else {
                        txtViewLeaveNoFound.setVisibility(View.VISIBLE);
                    }
                }
            }

        }


        // ----------------------------------------

        // Half Day
        if (totalHaldDay != null) {

            if (totalHaldDay.contains(",")) {
                halfDayArray = totalHaldDay.split(",");
            } else {
                halfDayArray = new String[]{totalHaldDay};
            }

            if (halfDayArray.length > 0) {

                // remove first item from array
                int n = halfDayArray.length - 1;
                String[] newArray = new String[n];
                System.arraycopy(halfDayArray, 1, newArray, 0, n);

                GridViewHalfDayAdapter gridViewAdapter = new GridViewHalfDayAdapter(getActivity(), newArray);

                if (attendanceType == 0) { // Present
                    gridViewHalfDayPresent.setAdapter(gridViewAdapter);
                } else if (attendanceType == 1) { // Absent
                    gridViewHalfDayAbsent.setAdapter(gridViewAdapter);
                } else if (attendanceType == 2) { // Leave
                    gridViewHalfDayLeave.setAdapter(gridViewAdapter);
                }
            }
        }
    }

    @Override
    public void onUnsuccessMonthDetail(String message) {
        ((TeacherAttendanceDashboardActivity)getActivity()).hideProgressBar();
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    @Override
    public void onMonthDetailInternetError() {
        ((TeacherAttendanceDashboardActivity)getActivity()).hideProgressBar();
    }

    @Override
    public void onMonthSelect(String month) {
        if (!monthSelected.equalsIgnoreCase(month)) {
            resetSummary();
            monthSelected = month;
            getData();
        }
    }

    private void resetSummary(){
        txtViewPresentNoFound.setText(Resource.toString(R.string.no_record_found));
        imgViewPresentPlus.setImageResource(R.mipmap.ic_plus);
        imgViewAbsentPlus.setImageResource(R.mipmap.ic_plus);
        imgViewLeavePlus.setImageResource(R.mipmap.ic_plus);
        llPresentContainer.setVisibility(View.GONE);
        llAbsentContainer.setVisibility(View.GONE);
        llLeaveContainer.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessAttendancePresent(String data) {
        ((TeacherAttendanceDashboardActivity)getActivity()).hideProgressBar();
        txtViewPresentNoFound.setText(data);
    }

    @Override
    public void onUnSuccessAttendancePresent(String message) {
        ((TeacherAttendanceDashboardActivity)getActivity()).hideProgressBar();
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    OnClickInterface onRetryAttendanceDetail = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    private ArrayList<com.ifoster.common.adapter.model.MonthModel> getMonthList() {

        ArrayList<com.ifoster.common.adapter.model.MonthModel> monthList = new ArrayList<>();

        com.ifoster.common.adapter.model.MonthModel monthModel1 = new com.ifoster.common.adapter.model.MonthModel();
        monthModel1.setMonthName("Jan");

        com.ifoster.common.adapter.model.MonthModel monthModel2 = new com.ifoster.common.adapter.model.MonthModel();
        monthModel2.setMonthName("Feb");

        com.ifoster.common.adapter.model.MonthModel monthModel3 = new com.ifoster.common.adapter.model.MonthModel();
        monthModel3.setMonthName("Mar");

        com.ifoster.common.adapter.model.MonthModel monthModel4 = new com.ifoster.common.adapter.model.MonthModel();
        monthModel4.setMonthName("Apr");

        com.ifoster.common.adapter.model.MonthModel monthModel5 = new com.ifoster.common.adapter.model.MonthModel();
        monthModel5.setMonthName("May");

        com.ifoster.common.adapter.model.MonthModel monthModel6 = new com.ifoster.common.adapter.model.MonthModel();
        monthModel6.setMonthName("Jun");

        com.ifoster.common.adapter.model.MonthModel monthModel7 = new com.ifoster.common.adapter.model.MonthModel();
        monthModel7.setMonthName("Jul");

        com.ifoster.common.adapter.model.MonthModel monthModel8 = new com.ifoster.common.adapter.model.MonthModel();
        monthModel8.setMonthName("Aug");

        com.ifoster.common.adapter.model.MonthModel monthModel9 = new com.ifoster.common.adapter.model.MonthModel();
        monthModel9.setMonthName("Sep");

        com.ifoster.common.adapter.model.MonthModel monthModel10 = new com.ifoster.common.adapter.model.MonthModel();
        monthModel10.setMonthName("Oct");

        com.ifoster.common.adapter.model.MonthModel monthModel11 = new com.ifoster.common.adapter.model.MonthModel();
        monthModel11.setMonthName("Nov");

        com.ifoster.common.adapter.model.MonthModel monthModel12 = new com.ifoster.common.adapter.model.MonthModel();
        monthModel12.setMonthName("Dec");


        monthList.add(monthModel1);
        monthList.add(monthModel2);
        monthList.add(monthModel3);
        monthList.add(monthModel4);
        monthList.add(monthModel5);
        monthList.add(monthModel6);
        monthList.add(monthModel7);
        monthList.add(monthModel8);
        monthList.add(monthModel9);
        monthList.add(monthModel10);
        monthList.add(monthModel11);
        monthList.add(monthModel12);

        for (int i = 0; i < monthList.size(); i++) {
            if (currentMonth.equalsIgnoreCase(monthList.get(i).getMonthName())) {
                monthList.get(i).setSelected(true);
                scrollPosition = i;
                break;
            }
        }
        return monthList;
    }
}


