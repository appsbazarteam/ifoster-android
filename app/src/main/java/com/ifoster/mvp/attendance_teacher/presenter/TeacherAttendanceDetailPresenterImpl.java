package com.ifoster.mvp.attendance_teacher.presenter;

import android.content.Context;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.attendance.model.AttendanceDetailModel;
import com.ifoster.mvp.attendance.model.PresentModel;
import com.ifoster.mvp.attendance_teacher.model.MonthDetailTeacherModel;
import com.ifoster.mvp.attendance_teacher.view.TeacherAttendanceDetailView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class TeacherAttendanceDetailPresenterImpl implements TeacherAttendanceDetailPresenter {

    private Context activity;
    private TeacherAttendanceDetailView attendanceDetailView;
    private JSONObject jsonObject;

    public TeacherAttendanceDetailPresenterImpl(Context activity, TeacherAttendanceDetailView attendanceDetailView) {
        this.activity = activity;
        this.attendanceDetailView = attendanceDetailView;
    }

    @Override
    public void getAttendanceDetail1(String month) {
        try {
            ApiAdapter.getInstance(activity);
            callAttendanceDetailApi(month);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            attendanceDetailView.onAttendanceDetailInternetError();
        }
    }

    public void getAttendanceDetail2(String month, int attendanceType) {

        try {
            ApiAdapter.getInstance(activity);
            callMonthDetailApi(month, attendanceType);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            attendanceDetailView.onAttendanceDetailInternetError();
        }

    }

    public void getEmployeePresentData() {

        try {
            ApiAdapter.getInstance(activity);
            callEmployeePresentApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            attendanceDetailView.onAttendanceDetailInternetError();
        }

    }

    private void callAttendanceDetailApi(String month) {
        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_TEACHER_ID, UserSession.getInstance().getEmployeeId());
            jsonObject.put(Const.PARAM_MONTH, month);
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<AttendanceDetailModel> getLoginOutput = ApiAdapter.getApiService().getTeacherAttendanceDetail("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<AttendanceDetailModel>() {
            @Override
            public void onResponse(Call<AttendanceDetailModel> call, Response<AttendanceDetailModel> response) {
                try {
                    AttendanceDetailModel attendanceDetailModel = response.body();
                    String message = attendanceDetailModel.getMessage();

                    if (attendanceDetailModel.getStatus()) {
                        attendanceDetailView.onSuccessAttendanceDetail(attendanceDetailModel);
                    } else {
                        attendanceDetailView.onUnsuccessAttendanceDetail(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    attendanceDetailView.onUnsuccessAttendanceDetail(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<AttendanceDetailModel> call, Throwable t) {
                attendanceDetailView.onUnsuccessAttendanceDetail(activity.getString(R.string.error_server));
            }
        });
    }


    private void callMonthDetailApi(String month, int attendanceType) {
        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_TEACHER_ID, UserSession.getInstance().getEmployeeId());
            jsonObject.put(Const.PARAM_MONTH, month);
            jsonObject.put(Const.PARAM_ATTENDANCE, attendanceType);
            jsonObject.put(Const.PARAM_HALF_DAY, attendanceType);
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<MonthDetailTeacherModel> getLoginOutput = ApiAdapter.getApiService().getTeacherMonthDetail("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<MonthDetailTeacherModel>() {
            @Override
            public void onResponse(Call<MonthDetailTeacherModel> call, Response<MonthDetailTeacherModel> response) {
                try {
                    MonthDetailTeacherModel attendanceDetailModel = response.body();
                    String message = attendanceDetailModel.getMessage();

                    if (attendanceDetailModel.getStatus()) {
                        attendanceDetailView.onSuccessMonthDetail(attendanceDetailModel.getAttedanceDate(), attendanceDetailModel.getHalfday());
                    } else {
                        attendanceDetailView.onUnsuccessMonthDetail(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    attendanceDetailView.onUnsuccessMonthDetail(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<MonthDetailTeacherModel> call, Throwable t) {
                attendanceDetailView.onUnsuccessMonthDetail(activity.getString(R.string.error_server));
            }
        });
    }

    private void callEmployeePresentApi() {
        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<PresentModel> getLoginOutput = ApiAdapter.getApiService().getEmployeeAttendancePresent("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<PresentModel>() {
            @Override
            public void onResponse(Call<PresentModel> call, Response<PresentModel> response) {
                try {
                    PresentModel attendanceDetailModel = response.body();
                    String message = attendanceDetailModel.getMessage();

                    if (attendanceDetailModel.getStatus()) {
                        attendanceDetailView.onSuccessAttendancePresent(attendanceDetailModel.getResult());
                    } else {
                        attendanceDetailView.onUnSuccessAttendancePresent(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    attendanceDetailView.onUnSuccessAttendancePresent(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<PresentModel> call, Throwable t) {
                attendanceDetailView.onUnSuccessAttendancePresent(activity.getString(R.string.error_server));
            }
        });
    }

}
