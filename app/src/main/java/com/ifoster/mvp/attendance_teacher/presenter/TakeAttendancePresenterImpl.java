package com.ifoster.mvp.attendance_teacher.presenter;

import android.app.Activity;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.attendance_teacher.model.ClassStudentListModel;
import com.ifoster.mvp.attendance_teacher.model.TakeAttendanceResponseModel;
import com.ifoster.mvp.attendance_teacher.view.TakeAttendanceView;
import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class TakeAttendancePresenterImpl implements TakeAttendancePresenter {

    private Activity activity;
    private TakeAttendanceView takeAttendanceView;
    private JSONObject jsonObject;

    public TakeAttendancePresenterImpl(Activity activity, TakeAttendanceView takeAttendanceView) {
        this.activity = activity;
        this.takeAttendanceView = takeAttendanceView;
    }

    @Override
    public void getClassStudentList(String className, String section) {
        try {
            ApiAdapter.getInstance(activity);
            callClassStudent(className, section);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            takeAttendanceView.onClassStudentInternetError();
        }
    }

    @Override
    public void takeAttendance(String absentList, String presentList, String attendanceData) {

        try {
            ApiAdapter.getInstance(activity);
            callAttendanceApi(absentList, presentList, attendanceData);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            takeAttendanceView.onClassStudentInternetError();
        }

    }

    private void callClassStudent(String className, String section) {
        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CLASS_NAME, className);
            jsonObject.put(Const.PARAM_SECTION, section);
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<ClassStudentListModel> getLoginOutput = ApiAdapter.getApiService().getClassStudentList("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<ClassStudentListModel>() {
            @Override
            public void onResponse(Call<ClassStudentListModel> call, Response<ClassStudentListModel> response) {
                try {
                    ClassStudentListModel classStudentListModel = response.body();
                    String message = classStudentListModel.getMessage();

                    if (classStudentListModel.getStatus()) {
                        takeAttendanceView.onSuccessClassStudent(classStudentListModel.getArrayList());
                    } else {
                        takeAttendanceView.onUnsuccessClassStudent(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    takeAttendanceView.onUnsuccessClassStudent(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ClassStudentListModel> call, Throwable t) {
                takeAttendanceView.onUnsuccessClassStudent(activity.getString(R.string.error_server));
            }
        });
    }


    private void callAttendanceApi(String absentList, String presentList, String attendanceData) {
        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_ABSENT_STUDENYT_IDS, absentList);
            jsonObject.put(Const.PARAM_PRESENT_STUDENYT_IDS, presentList);
            jsonObject.put(Const.PARAM_ATTENDANCE_DATE, attendanceData);
            jsonObject.put(Const.PARAM_EMPLOYEE_ID, UserSession.getInstance().getEmployeeId());
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<TakeAttendanceResponseModel> getLoginOutput = ApiAdapter.getApiService().takeAttendance("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<TakeAttendanceResponseModel>() {
            @Override
            public void onResponse(Call<TakeAttendanceResponseModel> call, Response<TakeAttendanceResponseModel> response) {
                try {
                    TakeAttendanceResponseModel classStudentListModel = response.body();
                    String message = classStudentListModel.getMessage();

                    if (classStudentListModel.getStatus()) {
                        takeAttendanceView.onSuccessTakeAttendance(message);
                    } else {
                        takeAttendanceView.onUnsuccessTakeAttendance(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    takeAttendanceView.onUnsuccessTakeAttendance(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<TakeAttendanceResponseModel> call, Throwable t) {
                takeAttendanceView.onUnsuccessTakeAttendance(activity.getString(R.string.error_server));
            }
        });
    }

}
