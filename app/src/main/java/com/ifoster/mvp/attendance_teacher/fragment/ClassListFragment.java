package com.ifoster.mvp.attendance_teacher.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.attendance_teacher.activity.TeacherAttendanceDashboardActivity;
import com.ifoster.mvp.attendance_teacher.adapter.ClassListAdapter;
import com.ifoster.mvp.attendance_teacher.model.ClassListModel;
import com.ifoster.mvp.attendance_teacher.presenter.ClassListPresenterImpl;
import com.ifoster.mvp.attendance_teacher.view.ClassListView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alam on 02-Oct-17.
 */

public class ClassListFragment extends Fragment implements ClassListView {

    @BindView(R.id.relLayMainContainer)
    RelativeLayout relLayMainContainer;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private ClassListPresenterImpl classListPresenterImpl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_class_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        classListPresenterImpl = new ClassListPresenterImpl(getActivity(), this);
        getData();
    }

    private void getData() {
        ((TeacherAttendanceDashboardActivity)getActivity()).showProgressBar();
        classListPresenterImpl.getTeacherAttendanceList();
    }

    @Override
    public void onSuccessTeacherAttendance(ArrayList<ClassListModel.User> result) {
        ((TeacherAttendanceDashboardActivity)getActivity()).hideProgressBar();
        if (result != null && result.size() > 0) {
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            ClassListAdapter classListAdapter = new ClassListAdapter(getActivity(), result);
            recyclerView.setAdapter(classListAdapter);
        } else {
            SnackNotify.showMessage(getString(R.string.no_record_found), relLayMainContainer);
        }
    }

    @Override
    public void onUnsuccessTeacherAttendance(String message) {
        ((TeacherAttendanceDashboardActivity)getActivity()).hideProgressBar();
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    @Override
    public void onTeacherAttendanceInternetError() {
        ((TeacherAttendanceDashboardActivity)getActivity()).hideProgressBar();
        SnackNotify.checkConnection(onRetryTeacherAttendance, relLayMainContainer);
    }

    OnClickInterface onRetryTeacherAttendance = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };
}
