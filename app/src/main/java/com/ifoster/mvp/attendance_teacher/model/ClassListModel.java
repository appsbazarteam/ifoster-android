package com.ifoster.mvp.attendance_teacher.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

public class ClassListModel implements Serializable {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Result")
    @Expose
    private ArrayList<User> user = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<User> getUser() {
        return user;
    }

    public void setUser(ArrayList<User> user) {
        this.user = user;
    }


    public class User implements Serializable {

        @SerializedName("stuclass")
        @Expose
        private String stuclass;
        @SerializedName("section")
        @Expose
        private String section;

        public String getStuclass() {
            return stuclass;
        }

        public void setStuclass(String stuclass) {
            this.stuclass = stuclass;
        }

        public String getSection() {
            return section;
        }

        public void setSection(String section) {
            this.section = section;
        }

    }

}