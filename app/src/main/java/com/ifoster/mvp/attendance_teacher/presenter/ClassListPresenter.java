package com.ifoster.mvp.attendance_teacher.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ClassListPresenter {
    public void getTeacherAttendanceList();
}
