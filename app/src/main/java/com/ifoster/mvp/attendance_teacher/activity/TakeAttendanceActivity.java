package com.ifoster.mvp.attendance_teacher.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.attendance_teacher.adapter.AttendanceListAdapter;
import com.ifoster.mvp.attendance_teacher.model.ClassListModel;
import com.ifoster.mvp.attendance_teacher.model.ClassStudentListModel;
import com.ifoster.mvp.attendance_teacher.presenter.TakeAttendancePresenterImpl;
import com.ifoster.mvp.attendance_teacher.view.TakeAttendanceView;
import com.ifoster.mvp.confirm_attendance.ConfirmAttendanceActivity;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.OnClick;

public class TakeAttendanceActivity extends BaseActivity implements TakeAttendanceView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    private AttendanceListAdapter attendanceListAdapter;
    private TakeAttendancePresenterImpl takeAttendancePresenterImpl;
    private ClassListModel.User user = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_attendance2);

        takeAttendancePresenterImpl = new TakeAttendancePresenterImpl(this, this);
        getIntentData();
        setToolbar();
    }

    private void setToolbar() {
        txtViewToolbarTitle.setText(getString(R.string.title_take_attendace));
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent.hasExtra(ConstIntent.KEY_CLASS_LIST)) {
            user = (ClassListModel.User) intent.getSerializableExtra(ConstIntent.KEY_CLASS_LIST);
            getData();
        }
    }

    private void getData() {
        if (user != null) {
            showProgressBar();
            takeAttendancePresenterImpl.getClassStudentList(user.getStuclass(), user.getSection());
        }
    }

    @OnClick(R.id.imgViewBack)
    public void onBackClick() {
        finish();
    }

    @Override
    public void onSuccessClassStudent(ArrayList<ClassStudentListModel.StudentArrayList> result) {
        hideProgressBar();
        if (result != null) {
            if (result.size() > 0) {
                for (ClassStudentListModel.StudentArrayList studentArrayList : result) {
                    studentArrayList.setPresent(true);
                }

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                attendanceListAdapter = new AttendanceListAdapter(this, result);
                recyclerView.setAdapter(attendanceListAdapter);
            }
        }
    }

    @Override
    public void onUnsuccessClassStudent(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onClassStudentInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryClassStudent, coordinateLayout);
    }

    @Override
    public void onSuccessTakeAttendance(String message) {
        hideProgressBar();
    }

    @Override
    public void onUnsuccessTakeAttendance(String message) {
        hideProgressBar();
    }

    @Override
    public void onTakeAttendanceInternetError() {
        hideProgressBar();
    }

    OnClickInterface onRetryClassStudent = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    @OnClick(R.id.cardViewBtnContinue)
    public void btnContinue() {
        if (attendanceListAdapter != null) {
            ArrayList<ClassStudentListModel.StudentArrayList> absentList = attendanceListAdapter.getAbsentList();
            ArrayList<ClassStudentListModel.StudentArrayList> presentList = attendanceListAdapter.getPresentList();

            // goto absent confirms page
            Intent intent = new Intent(this, ConfirmAttendanceActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(ConstIntent.KEY_ABSENT_LIST, absentList);
            bundle.putSerializable(ConstIntent.KEY_PRESENT_LIST, presentList);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }
}
