package com.ifoster.mvp.fee;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.adapter.model.MonthModel;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.DateTimeUtils;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.fee.adapter.FeeMonthAdapter;
import com.ifoster.mvp.fee.model.FeeResponseModel;
import com.ifoster.mvp.fee.presenter.FeePresenterImpl;
import com.ifoster.mvp.fee.view.FeeView;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class FeeDetailActivity extends BaseActivity implements OnChartValueSelectedListener, FeeView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.pieChart)
    PieChart pieChart;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @BindView(R.id.txtViewStudentName)
    TextView txtViewStudentName;

    @BindView(R.id.txtViewFeeCycle)
    TextView txtViewFeeCycle;

    @BindView(R.id.linLayFeeDetails)
    LinearLayout linLayFeeDetails;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.cardViewStatus)
    CardView cardViewStatus;

    @BindView(R.id.pieChartCenterText)
    TextView pieChartCenterText;
    /*-----------Class Presenter-----------*/
    FeePresenterImpl feePresenterImpl;
    /*------------Response Data------------*/
    FeeResponseModel feeResponseModel;

    @BindView(R.id.recyclerViewMonth)
    RecyclerView recyclerViewMonth;

    String month = "";
    String selectedMonth;

    int userLogedInType = 0;
    int scrollPosition;

    float totalAmount = 0;
    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;
    @BindView(R.id.imgViewUser)
    ImageView imgViewUser;
    @BindView(R.id.imgViewBell)
    ImageView imgViewBell;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;
    @BindView(R.id.txtViewPayNow)
    TextView txtViewPayNow;
    @BindView(R.id.circle)
    RelativeLayout circle;
    @BindView(R.id.relLayPay)
    RelativeLayout relLayPay;
    @BindView(R.id.txtFeeTutionType)
    TextView txtFeeTutionType;
    @BindView(R.id.txtViewTutionTotalFee)
    TextView txtViewTutionTotalFee;
    @BindView(R.id.relLayTutionFee)
    RelativeLayout relLayTutionFee;
    @BindView(R.id.txtFeeAnnualType)
    TextView txtFeeAnnualType;
    @BindView(R.id.txtViewAnnualTotalFee)
    TextView txtViewAnnualTotalFee;
    @BindView(R.id.relLayAnnualFee)
    RelativeLayout relLayAnnualFee;
    @BindView(R.id.txtFeeDevelopmentType)
    TextView txtFeeDevelopmentType;
    @BindView(R.id.txtViewDevelopmentTotalFee)
    TextView txtViewDevelopmentTotalFee;
    @BindView(R.id.relLayDevelopmentFee)
    RelativeLayout relLayDevelopmentFee;
    @BindView(R.id.txtFeeElearningType)
    TextView txtFeeElearningType;
    @BindView(R.id.txtViewElearningTotalFee)
    TextView txtViewElearningTotalFee;
    @BindView(R.id.relLayElearningFee)
    RelativeLayout relLayElearningFee;
    @BindView(R.id.txtFeeTransportationType)
    TextView txtFeeTransportationType;
    @BindView(R.id.txtViewTransportationTotalFee)
    TextView txtViewTransportationTotalFee;
    @BindView(R.id.relLayTransportationFee)
    RelativeLayout relLayTransportationFee;
    @BindView(R.id.txtFeeTotal)
    TextView txtFeeTotal;
    @BindView(R.id.txtViewTotalAmount)
    TextView txtViewTotalAmount;
    @BindView(R.id.relLayTotalFee)
    RelativeLayout relLayTotalFee;
    @BindView(R.id.content_fee_detail)
    RelativeLayout contentFeeDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_detail);
        feePresenterImpl = new FeePresenterImpl(this, this);
        month = Utils.getCurrentMonthForFee();

        selectedMonth = Utils.getCurrentMonth();

        //getData(currentMonth);
        txtViewStudentName.setText(UserSession.getInstance().getStudentName());

        setFont();
        setToolbar();
        bindMonthToRecyclerView();
        setPiChat();
        getStudentFee(month);
    }

    private void bindMonthToRecyclerView() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        recyclerViewMonth.setLayoutManager(linearLayoutManager);
        recyclerViewMonth.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMonth.setNestedScrollingEnabled(false);
        ArrayList<MonthModel> list = getMonthList();
        FeeMonthAdapter feeMonthAdapter = new FeeMonthAdapter(this, list, this);
        recyclerViewMonth.setAdapter(feeMonthAdapter);
        recyclerViewMonth.scrollToPosition(scrollPosition);
    }

    private void setPiChat() {

        pieChart.setUsePercentValues(true);
        pieChart.setCenterText("");
        pieChart.setCenterTextSize(14f);

        float jan = 8.33f;
        float feb = 8.33f;
        float march = 8.33f;
        float april = 8.33f;
        float may = 8.33f;
        float june = 8.33f;
        float july = 8.33f;
        float august = 8.33f;
        float september = 8.33f;
        float october = 8.33f;
        float november = 8.33f;
        float december = 8.33f;

        int janColor = Color.parseColor("#FFFFFF");
        int febColor = Color.parseColor("#FFFFFF");
        int marchColor = Color.parseColor("#FFFFFF");
        int aprilColor = Color.parseColor("#FFFFFF");
        int mayColor = Color.parseColor("#FFFFFF");
        int juneColor = Color.parseColor("#FFFFFF");
        int julyColor = Color.parseColor("#FFFFFF");
        int augustColor = Color.parseColor("#FFFFFF");
        int septemberColor = Color.parseColor("#FFFFFF");
        int octoberColor = Color.parseColor("#FFFFFF");
        int novemberColor = Color.parseColor("#FFFFFF");
        int decemberColor = Color.parseColor("#FFFFFF");

        if (month.equalsIgnoreCase("April")) {
            april = 8.33f;
            janColor = Color.parseColor("#2196F3");
        } else if (month.equalsIgnoreCase("May")) {
            may = 8.33f;
            janColor = Color.parseColor("#2196F3");
            febColor = Color.parseColor("#009688");
        } else if (month.equalsIgnoreCase("June")) {
            june = 8.33f;
            janColor = Color.parseColor("#2196F3");
            febColor = Color.parseColor("#009688");
            marchColor = Color.parseColor("#5A295D");
        } else if (month.equalsIgnoreCase("July")) {
            july = 8.33f;
            janColor = Color.parseColor("#2196F3");
            febColor = Color.parseColor("#009688");
            marchColor = Color.parseColor("#5A295D");
            aprilColor = Color.parseColor("#8BC34A");
        } else if (month.equalsIgnoreCase("August")) {
            august = 8.33f;
            janColor = Color.parseColor("#2196F3");
            febColor = Color.parseColor("#009688");
            marchColor = Color.parseColor("#5A295D");
            aprilColor = Color.parseColor("#8BC34A");
            mayColor = Color.parseColor("#E91E63");
        } else if (month.equalsIgnoreCase("September")) {
            september = 8.33f;
            janColor = Color.parseColor("#2196F3");
            febColor = Color.parseColor("#009688");
            marchColor = Color.parseColor("#5A295D");
            aprilColor = Color.parseColor("#8BC34A");
            mayColor = Color.parseColor("#E91E63");
            juneColor = Color.parseColor("#1F5F7E");
        } else if (month.equalsIgnoreCase("October")) {
            october = 8.33f;
            janColor = Color.parseColor("#2196F3");
            febColor = Color.parseColor("#009688");
            marchColor = Color.parseColor("#5A295D");
            aprilColor = Color.parseColor("#8BC34A");
            mayColor = Color.parseColor("#E91E63");
            juneColor = Color.parseColor("#1F5F7E");
            julyColor = Color.parseColor("#843c9b");
        } else if (month.equalsIgnoreCase("November")) {
            november = 8.33f;
            janColor = Color.parseColor("#2196F3");
            febColor = Color.parseColor("#009688");
            marchColor = Color.parseColor("#5A295D");
            aprilColor = Color.parseColor("#8BC34A");
            mayColor = Color.parseColor("#E91E63");
            juneColor = Color.parseColor("#1F5F7E");
            julyColor = Color.parseColor("#843c9b");
            augustColor = Color.parseColor("#E6E14C");
        } else if (month.equalsIgnoreCase("December")) {
            december = 8.33f;
            janColor = Color.parseColor("#2196F3");
            febColor = Color.parseColor("#009688");
            marchColor = Color.parseColor("#5A295D");
            aprilColor = Color.parseColor("#8BC34A");
            mayColor = Color.parseColor("#E91E63");
            juneColor = Color.parseColor("#1F5F7E");
            julyColor = Color.parseColor("#843c9b");
            augustColor = Color.parseColor("#E6E14C");
            septemberColor = Color.parseColor("#191970");
        } else if (month.equalsIgnoreCase("January")) {
            jan = 8.33f;
            janColor = Color.parseColor("#2196F3");
            febColor = Color.parseColor("#009688");
            marchColor = Color.parseColor("#5A295D");
            aprilColor = Color.parseColor("#8BC34A");
            mayColor = Color.parseColor("#E91E63");
            juneColor = Color.parseColor("#1F5F7E");
            julyColor = Color.parseColor("#843c9b");
            augustColor = Color.parseColor("#E6E14C");
            septemberColor = Color.parseColor("#191970");
            octoberColor = Color.parseColor("#c6c6ff");
        } else if (month.equalsIgnoreCase("February")) {
            feb = 8.33f;
            janColor = Color.parseColor("#2196F3");
            febColor = Color.parseColor("#009688");
            marchColor = Color.parseColor("#5A295D");
            aprilColor = Color.parseColor("#8BC34A");
            mayColor = Color.parseColor("#E91E63");
            juneColor = Color.parseColor("#1F5F7E");
            julyColor = Color.parseColor("#843c9b");
            augustColor = Color.parseColor("#E6E14C");
            septemberColor = Color.parseColor("#191970");
            octoberColor = Color.parseColor("#c6c6ff");
            novemberColor = Color.parseColor("#707862");
        } else if (month.equalsIgnoreCase("March")) {
            march = 8.33f;
            janColor = Color.parseColor("#2196F3");
            febColor = Color.parseColor("#009688");
            marchColor = Color.parseColor("#5A295D");
            aprilColor = Color.parseColor("#8BC34A");
            mayColor = Color.parseColor("#E91E63");
            juneColor = Color.parseColor("#1F5F7E");
            julyColor = Color.parseColor("#843c9b");
            augustColor = Color.parseColor("#E6E14C");
            septemberColor = Color.parseColor("#191970");
            octoberColor = Color.parseColor("#c6c6ff");
            novemberColor = Color.parseColor("#707862");
            decemberColor = Color.parseColor("#fa8072");
        }


        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.
        ArrayList<Entry> yvalues = new ArrayList<Entry>();
        yvalues.add(new Entry(jan, 0));
        yvalues.add(new Entry(feb, 1));
        yvalues.add(new Entry(march, 2));
        yvalues.add(new Entry(april, 3));

        yvalues.add(new Entry(may, 4));
        yvalues.add(new Entry(june, 5));
        yvalues.add(new Entry(july, 6));
        yvalues.add(new Entry(august, 7));

        yvalues.add(new Entry(september, 8));
        yvalues.add(new Entry(october, 9));
        yvalues.add(new Entry(november, 10));
        yvalues.add(new Entry(december, 11));


        PieDataSet dataSet = new PieDataSet(yvalues, "Election Results");

        ArrayList<String> xVals = new ArrayList<String>();

        xVals.add("January");
        xVals.add("February");
        xVals.add("March");
        xVals.add("April");

        xVals.add("May");
        xVals.add("June");
        xVals.add("July");
        xVals.add("August");

        xVals.add("September");
        xVals.add("October");
        xVals.add("November");
        xVals.add("December");

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        pieChart.setData(data);

        pieChart.setDescription("");

        pieChart.setDrawHoleEnabled(true);
        pieChart.setTransparentCircleRadius(8.33f);
        pieChart.setHoleRadius(70f);

        // remove circle text
        pieChart.setDrawSliceText(false);
        pieChart.getData().setDrawValues(false);

        // remove bottom indecator
        Legend leg = pieChart.getLegend();
        leg.setEnabled(false);

        // default cell color
        // dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);

        // set each set color
        ArrayList<Integer> colors = new ArrayList<Integer>();

        colors.add(janColor);
        colors.add(febColor);
        colors.add(marchColor);
        colors.add(aprilColor);

        colors.add(mayColor);
        colors.add(juneColor);
        colors.add(julyColor);
        colors.add(augustColor);

        colors.add(septemberColor);
        colors.add(octoberColor);
        colors.add(novemberColor);
        colors.add(decemberColor);

        dataSet.setColors(colors);

        data.setValueTextSize(16f);
        data.setValueTextColor(Color.DKGRAY);
        pieChart.setOnChartValueSelectedListener(this);

        pieChart.animateXY(1400, 1400);

    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

        if (e == null)
            return;
        Log.i("VAL SELECTED",
                "Value: " + e.getVal() + ", xIndex: " + e.getXIndex()
                        + ", DataSet index: " + dataSetIndex);
    }

    @Override
    public void onNothingSelected() {
        Log.i("PieChart", "nothing selected");
    }

    @OnClick(R.id.imgViewBack)
    public void goBack() {
        finish();
    }

    private void setToolbar() {

        txtViewToolbarTitle.setText(getString(R.string.title_fee1));
    }

    @Override
    public void onSuccessFee(FeeResponseModel feeResponseModel) {
        hideProgressBar();
        this.feeResponseModel = feeResponseModel;
        Log.e("feeeeeeeeeeeeee",feeResponseModel.getResult().size()+"");

        if (feeResponseModel != null) {

            // month range
            txtViewFeeCycle.setText(feeResponseModel.getMonthRange());

            if (feeResponseModel.getResult() != null && feeResponseModel.getResult().size() > 0) {

                // set student name
               // txtViewStudentName.setText(feeResponseModel.getResult().get(0).getStudentName());
                pieChartCenterText.setText(feeResponseModel.getResult().get(0).getStatus());

                // hide/show pay now button
                if (feeResponseModel.getResult().get(0).getStatus().equalsIgnoreCase("Due")) {
                    cardViewStatus.setVisibility(View.VISIBLE);
                } else {
                    cardViewStatus.setVisibility(View.GONE);
                }

                // add fee for current month

                setFeesForCurrentMonth(feeResponseModel.getResultFee());
            } else {
                setFeesForCurrentMonth(feeResponseModel.getResultFee());
            }
            // set all month
        }
    }

    private void setFeesForCurrentMonth(ArrayList<FeeResponseModel.ResultFee> feeList) {

        totalAmount = 0;

        if (feeList != null && feeList.size() > 0) {

            FeeResponseModel.ResultFee resultFee = feeList.get(0);

           // for (int i = 0; i < feeList.size(); i++) {
                // set tution fee value
                if (resultFee.getTutionFee() != null && resultFee.getTutionFee().length() > 0) {
                    txtViewTutionTotalFee.setText("Rs." + resultFee.getTutionFee());
                    totalAmount = totalAmount + Float.parseFloat(resultFee.getTutionFee());
                } else {
                    txtViewTutionTotalFee.setText("Rs." + 0.0);
                    //totalAmount = totalAmount + Float.parseFloat(resultFee.getTutionFee());
                }
                // set annual fee value
                if (resultFee.getAnnualFee() != null && resultFee.getAnnualFee().length() > 0) {
                    txtViewAnnualTotalFee.setText("Rs." + resultFee.getAnnualFee());
                    totalAmount = totalAmount + Float.parseFloat(resultFee.getAnnualFee());
                } else {
                    txtViewAnnualTotalFee.setText("Rs." + 0.0);
                    //totalAmount = totalAmount + Float.parseFloat(resultFee.getTutionFee());
                }
                // set development charge value
                if (resultFee.getDevelopmentCharge() != null && resultFee.getDevelopmentCharge().length() > 0) {
                    txtViewDevelopmentTotalFee.setText("Rs." + resultFee.getDevelopmentCharge());
                    totalAmount = totalAmount + Float.parseFloat(resultFee.getDevelopmentCharge());
                } else {
                    txtViewDevelopmentTotalFee.setText("Rs." + 0.0);
                    //totalAmount = totalAmount + Float.parseFloat(resultFee.getTutionFee());
                }
                // set e-learning fee value
                if (resultFee.getElearning() != null && resultFee.getElearning().length() > 0) {
                    txtViewElearningTotalFee.setText("Rs." + resultFee.getElearning());
                    totalAmount = totalAmount + Float.parseFloat(resultFee.getElearning());
                } else {
                    txtViewElearningTotalFee.setText("Rs." + 0.0);
                    //totalAmount = totalAmount + Float.parseFloat(resultFee.getTutionFee());
                }
                // set transportation fee value
                if (resultFee.getTransportation() != null && resultFee.getTransportation().length() > 0) {
                    txtViewTransportationTotalFee.setText("Rs." + resultFee.getTransportation());
                    totalAmount = totalAmount + Float.parseFloat(resultFee.getTransportation());

                } else {
                    txtViewTransportationTotalFee.setText("Rs." + 0.0);
                    //totalAmount = totalAmount + Float.parseFloat(resultFee.getTutionFee());
                }
                // set total amount
                txtViewTotalAmount.setText("Rs." + String.valueOf(totalAmount));
            //}
        }
        else {
            cardViewStatus.setVisibility(View.GONE);
            txtViewTutionTotalFee.setText("Rs." + 0.0);
            totalAmount = 0;
            txtViewAnnualTotalFee.setText("Rs." + 0.0);
            txtViewDevelopmentTotalFee.setText("Rs." + 0.0);
            txtViewElearningTotalFee.setText("Rs." + 0.0);
            txtViewTransportationTotalFee.setText("Rs." + 0.0);
            txtViewTotalAmount.setText("Rs." + 0.0);
        }
    }

    @Override
    public void onUnsuccessFee(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onFeeInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryStudentFee, coordinateLayout);
    }

    @Override
    public void onMonthSelect(String monthly) {
        String month = DateTimeUtils.convertDate(monthly, DateTimeUtils.DATE_FORMAT_MMM,DateTimeUtils.DATE_FORMAT_MMMM);
        if(!selectedMonth.equalsIgnoreCase(month)){
            this.selectedMonth = month;
            getStudentFee(selectedMonth);
        }
    }

    OnClickInterface onRetryStudentFee = new OnClickInterface() {
        @Override
        public void onClick() {
            getStudentFee(month);
        }
    };

    private void getStudentFee(String month) {
        showProgressBar();
        feePresenterImpl.getStudentMonthlyFee(month);
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewFeeCycle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }


    public ArrayList<MonthModel> getMonthList() {


        ArrayList<MonthModel> monthList = new ArrayList<>();

        MonthModel monthModel1 = new MonthModel();
        monthModel1.setMonthName("Jan");

        MonthModel monthModel2 = new MonthModel();
        monthModel2.setMonthName("Feb");

        MonthModel monthModel3 = new MonthModel();
        monthModel3.setMonthName("Mar");

        MonthModel monthModel4 = new MonthModel();
        monthModel4.setMonthName("Apr");

        MonthModel monthModel5 = new MonthModel();
        monthModel5.setMonthName("May");

        MonthModel monthModel6 = new MonthModel();
        monthModel6.setMonthName("Jun");

        MonthModel monthModel7 = new MonthModel();
        monthModel7.setMonthName("Jul");

        MonthModel monthModel8 = new MonthModel();
        monthModel8.setMonthName("Aug");

        MonthModel monthModel9 = new MonthModel();
        monthModel9.setMonthName("Sep");

        MonthModel monthModel10 = new MonthModel();
        monthModel10.setMonthName("Oct");

        MonthModel monthModel11 = new MonthModel();
        monthModel11.setMonthName("Nov");

        MonthModel monthModel12 = new MonthModel();
        monthModel12.setMonthName("Dec");


        monthList.add(monthModel1);
        monthList.add(monthModel2);
        monthList.add(monthModel3);
        monthList.add(monthModel4);
        monthList.add(monthModel5);
        monthList.add(monthModel6);
        monthList.add(monthModel7);
        monthList.add(monthModel8);
        monthList.add(monthModel9);
        monthList.add(monthModel10);
        monthList.add(monthModel11);
        monthList.add(monthModel12);

        for (int i = 0; i < monthList.size(); i++) {

            if (selectedMonth.equalsIgnoreCase(monthList.get(i).getMonthName())) {

                monthList.get(i).setSelected(true);
                scrollPosition = i;

                break;
            }
        }

        return monthList;
    }

}
