package com.ifoster.mvp.fee.presenter;

import android.content.Context;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.fee.model.FeeResponseModel;
import com.ifoster.mvp.fee.view.FeeView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class FeePresenterImpl implements FeePresenter {

    Context activity;
    FeeView feeView;
    JSONObject jsonObject;

    public FeePresenterImpl(Context activity, FeeView feeView) {
        this.activity = activity;
        this.feeView = feeView;
    }

    @Override
    public void getStudentMonthlyFee(String month) {
        try {
            ApiAdapter.getInstance(activity);
            callStudentMonthlyApi(month);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            feeView.onFeeInternetError();
        }
    }

    private void callStudentMonthlyApi(String month) {

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_STUDENT_ID, UserSession.getInstance().getStudentId());
            jsonObject.put(Const.PARAM_MONTH, month);
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<FeeResponseModel> getLoginOutput = ApiAdapter.getApiService().getStudentFee("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<FeeResponseModel>() {
            @Override
            public void onResponse(Call<FeeResponseModel> call, Response<FeeResponseModel> response) {
                try {
                    //getting whole data from response
                    FeeResponseModel feeResponseModel = response.body();
                    String message = feeResponseModel.getMessage();

                    if (feeResponseModel.getStatus()) {
                        feeView.onSuccessFee(feeResponseModel);
                    } else {
                        feeView.onUnsuccessFee(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    feeView.onUnsuccessFee(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<FeeResponseModel> call, Throwable t) {
                feeView.onUnsuccessFee(activity.getString(R.string.error_server));
            }
        });
    }


}
