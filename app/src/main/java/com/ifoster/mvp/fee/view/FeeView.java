package com.ifoster.mvp.fee.view;

import com.ifoster.mvp.fee.model.FeeResponseModel;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface FeeView {

    public void onSuccessFee(FeeResponseModel feeResponseModel);

    public void onUnsuccessFee(String message);

    public void onFeeInternetError();

    public void onMonthSelect(String monthSelected);
}
