package com.ifoster.mvp.fee.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface FeePresenter {

    public void getStudentMonthlyFee(String month);
}
