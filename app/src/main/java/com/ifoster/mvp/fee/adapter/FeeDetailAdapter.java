package com.ifoster.mvp.fee.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.fee.model.FeeModel;
import com.ifoster.mvp.fee.model.FeeResponseModel;
import com.ifoster.mvp.fee.presenter.FeePresenterImpl;
import com.ifoster.mvp.fee.view.FeeView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class FeeDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements FeeView {

    Context context;
    ArrayList<FeeModel> listData;

    int pos = 0;

    FeePresenterImpl feePresenterImpl;

    public FeeDetailAdapter(Context context, ArrayList<FeeModel> listData) {

        this.context = context;
        this.listData = listData;

        feePresenterImpl = new FeePresenterImpl(context, this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_fee_header, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        // get fee list
        ArrayList<FeeResponseModel.ResultFee> feeList = listData.get(position).getFeeList();

        ((ViewHolderList) holder).txtViewMonthName.setText(listData.get(position).getMonthName());

        // hide/show leave list
        if (listData.get(position).isExpand()) {
            ((ViewHolderList) holder).imgViewPlus.setImageResource(R.mipmap.ic_minus);
            ((ViewHolderList) holder).linLayFeeDetails.setVisibility(View.VISIBLE);
        } else {
            ((ViewHolderList) holder).imgViewPlus.setImageResource(R.mipmap.ic_plus);
            ((ViewHolderList) holder).linLayFeeDetails.setVisibility(View.GONE);
        }

        if (feeList != null && feeList.size() > 0) {

            float totalAmount = 0;
            
            ((ViewHolderList) holder).linLayFeeDetails.removeAllViews();

            for (int i = 0; i < feeList.size(); i++) {

                LayoutInflater inflater = null;
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mLinearView = inflater.inflate(R.layout.item_fee, null);

                // tution fee
                final RelativeLayout relLayTutionFee = (RelativeLayout) mLinearView.findViewById(R.id.relLayTutionFee);
                final TextView txtViewTutionTotalFee = (TextView) mLinearView.findViewById(R.id.txtViewTutionTotalFee);

                // annual fee
                final RelativeLayout relLayAnnualFee = (RelativeLayout) mLinearView.findViewById(R.id.relLayAnnualFee);
                final TextView txtViewAnnualTotalFee = (TextView) mLinearView.findViewById(R.id.txtViewAnnualTotalFee);

                // development charge
                final RelativeLayout relLayDevelopmentFee = (RelativeLayout) mLinearView.findViewById(R.id.relLayDevelopmentFee);
                final TextView txtViewDevelopmentTotalFee = (TextView) mLinearView.findViewById(R.id.txtViewDevelopmentTotalFee);


                // e-learning
                final RelativeLayout relLayElearningFee = (RelativeLayout) mLinearView.findViewById(R.id.relLayElearningFee);
                final TextView txtViewElearningTotalFee = (TextView) mLinearView.findViewById(R.id.txtViewElearningTotalFee);


                // transportation fee
                final RelativeLayout relLayTransportationFee = (RelativeLayout) mLinearView.findViewById(R.id.relLayTransportationFee);
                final TextView txtViewTransportationTotalFee = (TextView) mLinearView.findViewById(R.id.txtViewTransportationTotalFee);


                // total fee
                final RelativeLayout relLayTotalFee = (RelativeLayout) mLinearView.findViewById(R.id.relLayTotalFee);
                final TextView txtViewTotalAmount = (TextView) mLinearView.findViewById(R.id.txtViewTotalAmount);


                // set tution fee value
                if (feeList.get(i).getTutionFee() != null && feeList.get(i).getTutionFee().length() > 0) {
                    relLayTutionFee.setVisibility(View.VISIBLE);
                    txtViewTutionTotalFee.setText("Rs." + feeList.get(i).getTutionFee());

                    // add amount to total
                    totalAmount = totalAmount + Float.parseFloat(feeList.get(i).getTutionFee());

                } else {
                    relLayTutionFee.setVisibility(View.GONE);
                }

                // set annual fee value
                if (feeList.get(i).getAnnualFee() != null && feeList.get(i).getAnnualFee().length() > 0) {
                    relLayAnnualFee.setVisibility(View.VISIBLE);
                    txtViewAnnualTotalFee.setText("Rs." + feeList.get(i).getAnnualFee());

                    // add amount to total
                    totalAmount = totalAmount + Float.parseFloat(feeList.get(i).getAnnualFee());


                } else {
                    relLayAnnualFee.setVisibility(View.GONE);
                }

                // set development charge value
                if (feeList.get(i).getDevelopmentCharge() != null && feeList.get(i).getDevelopmentCharge().length() > 0) {
                    relLayDevelopmentFee.setVisibility(View.VISIBLE);
                    txtViewDevelopmentTotalFee.setText("Rs." + feeList.get(i).getDevelopmentCharge());

                    // add amount to total
                    totalAmount = totalAmount + Float.parseFloat(feeList.get(i).getDevelopmentCharge());


                } else {
                    relLayDevelopmentFee.setVisibility(View.GONE);
                }

                // set e-learning fee value
                if (feeList.get(i).getElearning() != null && feeList.get(i).getElearning().length() > 0) {
                    relLayElearningFee.setVisibility(View.VISIBLE);
                    txtViewElearningTotalFee.setText("Rs." + feeList.get(i).getElearning());

                    // add amount to total
                    totalAmount = totalAmount + Float.parseFloat(feeList.get(i).getElearning());


                } else {
                    relLayElearningFee.setVisibility(View.GONE);
                }

                // set transportation fee value
                if (feeList.get(i).getTransportation() != null && feeList.get(i).getTransportation().length() > 0) {
                    relLayTransportationFee.setVisibility(View.VISIBLE);
                    txtViewTransportationTotalFee.setText("Rs." + feeList.get(i).getTransportation());

                    // add amount to total
                    totalAmount = totalAmount + Float.parseFloat(feeList.get(i).getTransportation());

                } else {
                    relLayTransportationFee.setVisibility(View.GONE);
                }


                // set total amount
                txtViewTotalAmount.setText("Rs." + String.valueOf(totalAmount));

                // add view to linearlayout
                ((ViewHolderList) holder).linLayFeeDetails.addView(mLinearView);
            }
        }

        // parent item click
        ((ViewHolderList) holder).cardViewMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String monthName = listData.get(position).getMonthName();


                pos = position;
                boolean isCallAPI;

                if (listData.get(position).isExpand()) {
                    listData.get(position).setExpand(false);

                    for (int i = 0; i < listData.size(); i++) {
                        listData.get(i).setExpand(false);
                    }
                    isCallAPI = false;

                } else {

                    listData.get(position).setExpand(true);

                    for (int i = 0; i < listData.size(); i++) {
                        if (position != i) {
                            ((ViewHolderList) holder).imgViewPlus.setImageResource(R.mipmap.ic_plus);
                            listData.get(i).setExpand(false);
                        }
                    }
                    isCallAPI = true;
                }

                notifyDataSetChanged();

                if (isCallAPI) {
                    getStudentFee(monthName);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    /*---------------Api Callback---------------*/
    @Override
    public void onSuccessFee(FeeResponseModel feeResponseModel) {

        if (feeResponseModel != null &&
                feeResponseModel.getResultFee() != null &&
                feeResponseModel.getResultFee().size() > 0) {

            setFeesForCurrentMonth(feeResponseModel.getResultFee());
        }

    }

    private void setFeesForCurrentMonth(ArrayList<FeeResponseModel.ResultFee> feeList) {

        if (feeList != null && feeList.size() > 0) {

            listData.get(pos).setFeeList(feeList);

            // refresh list
            notifyDataSetChanged();
        }
    }

    @Override
    public void onUnsuccessFee(String message) {
        //SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onFeeInternetError() {
        //SnackNotify.checkConnection(onRetryStudentFee, coordinateLayout);
    }

    @Override
    public void onMonthSelect(String monthSelected) {

    }

    OnClickInterface onRetryStudentFee = new OnClickInterface() {
        @Override
        public void onClick() {
            // getStudentFee();
        }
    };

    private void getStudentFee(String month) {

        feePresenterImpl.getStudentMonthlyFee(month);
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        // parent view
        @BindView(R.id.cardViewMonth)
        CardView cardViewMonth;

        @BindView(R.id.txtViewMonthName)
        TextView txtViewMonthName;

        @BindView(R.id.imgViewPlus)
        ImageView imgViewPlus;

        @BindView(R.id.linLayFeeDetails)
        LinearLayout linLayFeeDetails;


        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            FontHelper.applyFont(context, txtViewMonthName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        }
    }


}
