package com.ifoster.mvp.fee.model;

import java.util.ArrayList;

/**
 * Created by Alam on 23-Oct-17.
 */

public class FeeModel {

    public String monthName;

    public boolean isExpand;

    ArrayList<FeeResponseModel.ResultFee> feeList;

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    public ArrayList<FeeResponseModel.ResultFee> getFeeList() {
        return feeList;
    }

    public void setFeeList(ArrayList<FeeResponseModel.ResultFee> feeList) {
        this.feeList = feeList;
    }
}
