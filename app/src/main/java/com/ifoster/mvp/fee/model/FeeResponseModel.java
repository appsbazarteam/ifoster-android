package com.ifoster.mvp.fee.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeeResponseModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("MonthRange")
    @Expose
    private String monthRange;
    @SerializedName("Result")
    @Expose
    private ArrayList<Result> result = null;
    @SerializedName("Result_fee")
    @Expose
    private ArrayList<ResultFee> resultFee = null;

//    @SerializedName("Month_Details")
//    @Expose
//    private String monthDetails;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMonthRange() {
        return monthRange;
    }

    public void setMonthRange(String monthRange) {
        this.monthRange = monthRange;
    }

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }

    public ArrayList<ResultFee> getResultFee() {
        return resultFee;
    }

    public void setResultFee(ArrayList<ResultFee> resultFee) {
        this.resultFee = resultFee;
    }

//    public String getMonthDetails() {
//        return monthDetails;
//    }
//
//    public void setMonthDetails(String monthDetails) {
//        this.monthDetails = monthDetails;
//    }


    public class Result {

//        @SerializedName("student_name")
//        @Expose
//        private String studentName;
//        @SerializedName("class")
//        @Expose
//        private String _class;
//        @SerializedName("section")
//        @Expose
//        private String section;
        @SerializedName("status")
        @Expose
        private String status;

//        public String getStudentName() {
//            return studentName;
//        }
//
//        public void setStudentName(String studentName) {
//            this.studentName = studentName;
//        }
//
//        public String getClass_() {
//            return _class;
//        }
//
//        public void setClass_(String _class) {
//            this._class = _class;
//        }
//
//        public String getSection() {
//            return section;
//        }
//
//        public void setSection(String section) {
//            this.section = section;
//        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }

    public class ResultFee {

        @SerializedName("tution_fee")
        @Expose
        private String tutionFee;
        @SerializedName("annual_fee")
        @Expose
        private String annualFee;
        @SerializedName("development_charge")
        @Expose
        private String developmentCharge;
        @SerializedName("elearning")
        @Expose
        private String elearning;
        @SerializedName("transportation")
        @Expose
        private String transportation;

        public String getTutionFee() {
            return tutionFee;
        }

        public void setTutionFee(String tutionFee) {
            this.tutionFee = tutionFee;
        }

        public String getAnnualFee() {
            return annualFee;
        }

        public void setAnnualFee(String annualFee) {
            this.annualFee = annualFee;
        }

        public String getDevelopmentCharge() {
            return developmentCharge;
        }

        public void setDevelopmentCharge(String developmentCharge) {
            this.developmentCharge = developmentCharge;
        }

        public String getElearning() {
            return elearning;
        }

        public void setElearning(String elearning) {
            this.elearning = elearning;
        }

        public String getTransportation() {
            return transportation;
        }

        public void setTransportation(String transportation) {
            this.transportation = transportation;
        }
    }
}