package com.ifoster.mvp.achievement;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.achievement.adapter.AchievementAdapter;
import com.ifoster.mvp.achievement.model.AchievementResponseModel;
import com.ifoster.mvp.achievement.presenter.AchievementPresenterImpl;
import com.ifoster.mvp.achievement.view.AchievementView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.OnClick;

public class AchievementActivity extends BaseActivity implements AchievementView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    private  AchievementPresenterImpl achievementPresenterImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achievement);

        achievementPresenterImpl = new AchievementPresenterImpl(this, this);

        setFont();

        setToolBar();

        getAchievement();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    @OnClick(R.id.imgViewBack)
    public void goBack() {
        finish();
    }

    private void setToolBar() {
        txtViewToolbarTitle.setText(getString(R.string.title_achievements));
    }

    @Override
    public void onSuccessAchievement(ArrayList<AchievementResponseModel.Result> result) {
        hideProgressBar();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        if (result != null && result.size() > 0) {
            int colorCodeType = 0;

            for (AchievementResponseModel.Result resultItem : result) {
                resultItem.setColorCodeType(colorCodeType);
                colorCodeType++;
                if (colorCodeType == 4) {
                    colorCodeType = 0;
                }
            }

            AchievementAdapter pendingLeaveAdapter = new AchievementAdapter(this, result);
            recyclerView.setAdapter(pendingLeaveAdapter);
        } else {
            SnackNotify.showMessage(getString(R.string.no_record_found), coordinateLayout);
        }
    }

    @Override
    public void onUnsuccessAchievement(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onAchievementInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryAchievment, coordinateLayout);
    }

    OnClickInterface onRetryAchievment = new OnClickInterface() {
        @Override
        public void onClick() {
            getAchievement();
        }
    };

    private void getAchievement() {
        showProgressBar();
        achievementPresenterImpl.getAchievements();
    }
}
