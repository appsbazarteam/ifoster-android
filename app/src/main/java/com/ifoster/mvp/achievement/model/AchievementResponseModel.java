package com.ifoster.mvp.achievement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AchievementResponseModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Result")
    @Expose
    private ArrayList<Result> result = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("date_of_event")
        @Expose
        private String dateOfEvent;
        @SerializedName("description")
        @Expose
        private String description;

        int colorCodeType;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDateOfEvent() {
            return dateOfEvent;
        }

        public void setDateOfEvent(String dateOfEvent) {
            this.dateOfEvent = dateOfEvent;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getColorCodeType() {
            return colorCodeType;
        }

        public void setColorCodeType(int colorCodeType) {
            this.colorCodeType = colorCodeType;
        }
    }

}