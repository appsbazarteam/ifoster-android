package com.ifoster.mvp.achievement.presenter;

import android.content.Context;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.mvp.achievement.model.AchievementResponseModel;
import com.ifoster.mvp.achievement.view.AchievementView;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class AchievementPresenterImpl implements AchievementPresenter {
    private Context activity;
    private AchievementView achievementView;

    public AchievementPresenterImpl(Context activity, AchievementView achievementView) {
        this.activity = activity;
        this.achievementView = achievementView;
    }

    @Override
    public void getAchievements() {
        try {
            ApiAdapter.getInstance(activity);
            callAchievmentApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            achievementView.onAchievementInternetError();
        }
    }

    private void callAchievmentApi() {
        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), "");
        Call<AchievementResponseModel> getLoginOutput = ApiAdapter.getApiService().getAchievement("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<AchievementResponseModel>() {
            @Override
            public void onResponse(Call<AchievementResponseModel> call, Response<AchievementResponseModel> response) {
                try {
                    AchievementResponseModel attendanceDetailModel = response.body();
                    String message = attendanceDetailModel.getMessage();

                    if (attendanceDetailModel.getStatus()) {
                        achievementView.onSuccessAchievement(attendanceDetailModel.getResult());
                    } else {
                        achievementView.onUnsuccessAchievement(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    achievementView.onUnsuccessAchievement(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<AchievementResponseModel> call, Throwable t) {
                achievementView.onUnsuccessAchievement(activity.getString(R.string.error_server));
            }
        });
    }
}
