package com.ifoster.mvp.achievement.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface AchievementPresenter {
    public void getAchievements();
}
