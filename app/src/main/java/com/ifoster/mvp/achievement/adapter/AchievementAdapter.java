package com.ifoster.mvp.achievement.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.utility.Utils;
import com.ifoster.mvp.achievement.model.AchievementResponseModel;
import com.ifoster.mvp.notice_board.view_more_text.ExpandableTextViewWith10Line;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class AchievementAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<AchievementResponseModel.Result> listData;

    public AchievementAdapter(Context context, ArrayList<AchievementResponseModel.Result> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_achievement, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        AchievementResponseModel.Result result = listData.get(position);

        if(result == null)
            return;

        if (result.getTitle() != null)
            ((ViewHolderList) holder).txtViewName.setText(result.getTitle());
        else
            ((ViewHolderList) holder).txtViewName.setText("--NA--");


        if (result.getDescription() != null)
            ((ViewHolderList) holder).txtViewDesc.setText(result.getDescription());
        else
            ((ViewHolderList) holder).txtViewDesc.setText("--NA--");

        if (result.getDateOfEvent() != null)
            ((ViewHolderList) holder).txtViewDate.setText(Utils.convertToDateFormate(result.getDateOfEvent()));
        else
            ((ViewHolderList) holder).txtViewDate.setText("--NA--");

        // view more click event
        ((ViewHolderList) holder).txtViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewHolderList) holder).txtViewDesc.performClick();

                if (((ViewHolderList) holder).txtViewDesc.getIsCollapsed()) {
                    ((ViewHolderList) holder).txtViewMore.setText("View More");
                } else {
                    ((ViewHolderList) holder).txtViewMore.setText("View Less");
                }
            }
        });

        ((ViewHolderList) holder).imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.coordinateLayoutContainer)
        RelativeLayout coordinateLayoutContainer;

        @BindView(R.id.cardViewTodayActivity)
        CardView cardViewTodayActivity;

        @BindView(R.id.imgProfile)
        ImageView imgProfile;

        @BindView(R.id.txtViewName)
        TextView txtViewName;

        @BindView(R.id.txtViewDate)
        TextView txtViewDate;

        @BindView(R.id.txtViewDesc)
        ExpandableTextViewWith10Line txtViewDesc;

        @BindView(R.id.txtViewMore)
        TextView txtViewMore;

        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            FontHelper.applyFont(context, txtViewName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewDesc, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewMore, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        }
    }
}
