package com.ifoster.mvp.achievement;

/**
 * Created by Alam on 24-Oct-17.
 */

public class AchievementModel {

    private String name;
    private String description;
    private String image;
    private int colorCodeType;

    public int getColorCodeType() {
        return colorCodeType;
    }

    public void setColorCodeType(int colorCodeType) {
        this.colorCodeType = colorCodeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
