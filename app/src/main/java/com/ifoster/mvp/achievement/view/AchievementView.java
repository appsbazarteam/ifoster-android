package com.ifoster.mvp.achievement.view;

import com.ifoster.mvp.achievement.model.AchievementResponseModel;
import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface AchievementView {

    public void onSuccessAchievement(ArrayList<AchievementResponseModel.Result> result);

    public void onUnsuccessAchievement(String message);

    public void onAchievementInternetError();
}
