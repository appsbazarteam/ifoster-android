package com.ifoster.mvp.exam_schedule_teacher;

import com.ifoster.mvp.exam_schedule.ExamScheduleModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface TeacherExamScheduleView {

    public void onSuccessGetExamSchedule(ArrayList<ExamScheduleModel.Result> results);

    public void onUnsuccessGetExamSchedule(String message);

    public void onGetExamScheduleInternetError();
}
