package com.ifoster.mvp.exam_schedule_teacher;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.exam_schedule.ExamScheduleModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class TeacherExamSchedulePresenterImpl implements TeacherExamSchedulePresenter {

    Activity activity;
    TeacherExamScheduleView examScheduleView;
    JSONObject jsonObject;

    public TeacherExamSchedulePresenterImpl(Activity activity, TeacherExamScheduleView examScheduleView) {
        this.activity = activity;
        this.examScheduleView = examScheduleView;
    }

    @Override
    public void getTeacherExamSchedule() {
        try {
            ApiAdapter.getInstance(activity);
            callExamScheduleApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            examScheduleView.onGetExamScheduleInternetError();
        }
    }

    private void callExamScheduleApi() {

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_TEACHER_ID, UserSession.getInstance().getEmployeeId());
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<ExamScheduleModel> getLoginOutput = ApiAdapter.getApiService().getTeacherExamSchedule("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<ExamScheduleModel>() {
            @Override
            public void onResponse(Call<ExamScheduleModel> call, Response<ExamScheduleModel> response) {

                try {
                    //getting whole data from response
                    ExamScheduleModel examScheduleModel = response.body();
                    String message = examScheduleModel.getMessage();

                    if (examScheduleModel.getStatus()) {
                        examScheduleView.onSuccessGetExamSchedule(examScheduleModel.getResult());
                    } else {
                        examScheduleView.onUnsuccessGetExamSchedule(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    examScheduleView.onUnsuccessGetExamSchedule(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ExamScheduleModel> call, Throwable t) {
                examScheduleView.onUnsuccessGetExamSchedule(activity.getString(R.string.error_server));
            }
        });
    }

}
