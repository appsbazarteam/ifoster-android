package com.ifoster.mvp.exam_schedule_teacher;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface TeacherExamSchedulePresenter {

    public void getTeacherExamSchedule();
}
