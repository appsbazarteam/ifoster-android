package com.ifoster.mvp.navigation;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifoster.App;
import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.CircleTransform;
import com.ifoster.common.utility.Resource;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.change_mobile_no.ChangeMobileNoActivity;
import com.ifoster.mvp.dashboard.fragment.ParentFragment;
import com.ifoster.mvp.dashboard.fragment.TeacherFragment;
import com.ifoster.mvp.login.LoginActivity;
import com.ifoster.mvp.login.LoginModel;
import com.ifoster.mvp.navigation.presenter.LogoutPresenterImpl;
import com.ifoster.mvp.navigation.view.LogoutView;
import com.ifoster.mvp.notification.NotificationActivity;
import com.ifoster.mvp.reset_password.ResetPasswordActivity;
import com.ifoster.mvp.show_student_list.ShowStudentListActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;

public class NavigationActivity extends BaseActivity implements LogoutView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.imgViewBell)
    ImageView imgViewBell;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.imgUserProfile)
    ImageView imgUserProfile;

    @BindView(R.id.txtViewUserName)
    TextView txtViewUserName;

    @BindView(R.id.txtViewClass)
    TextView txtViewClass;

    @BindView(R.id.txtViewDashboard)
    TextView txtViewDashboard;

    @BindView(R.id.txtViewLogout)
    TextView txtViewLogout;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @BindView(R.id.txtViewChangePassword)
    TextView txtViewChangePassword;

    @BindView(R.id.tv_title)
    TextView mTitle;

    @BindView(R.id.txtViewChangeMobileNo)
    TextView txtViewChangeMobileNo;
    @BindView(R.id.imgViewUser)
    ImageView imgViewUser;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;
    @BindView(R.id.txtViewRate)
    TextView txtViewRate;
    @BindView(R.id.txtViewShare)
    TextView txtViewShare;
    @BindView(R.id.txtViewHelp)
    TextView txtViewHelp;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.content_dashboard)
    RelativeLayout contentDashboard;
    @BindView(R.id.txtViewSwitchUser)
    TextView txtViewSwitchUser;
    @BindView(R.id.linLayotSwitchUser)
    LinearLayout linLayotSwitchUser;
    @BindView(R.id.viewSwitchUser)
    View viewSwitchUser;

    public static final String IS_FROM_DASHBOARD = "IS_FROM_DASHBOARD";
    private LogoutPresenterImpl logoutPresenterImpl;
    private LoginModel.Result result;
    private int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        //ButterKnife.bind(this);

        logoutPresenterImpl = new LogoutPresenterImpl(this, this);
        setToolbar();
        setFont();

        imgViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                } else {
                    drawer_layout.openDrawer(GravityCompat.START);
                }
            }
        });

        setFont();
        createViewPager();
        tabLayout.setupWithViewPager(viewPager);
        getIntentData();
        setProfileImage();
        tabLayout.setVisibility(View.GONE);
    }

    private void setProfileImage() {
        if (App.getApplicationInstance().getProfileImageURL() != null && App.getApplicationInstance().getProfileImageURL().length() > 0) {
            Glide.with(this).load(App.getApplicationInstance().getProfileImageURL())
                    .error(R.drawable.user)
                    .placeholder(R.drawable.user)
                    .thumbnail(0.5f)
                    .crossFade()
                    .transform(new CircleTransform(this))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgUserProfile);

        } else {
            imgUserProfile.setBackgroundResource(R.drawable.user);
        }
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent.hasExtra(ConstIntent.KEY_USER_TYPE)) {
            type = intent.getIntExtra(ConstIntent.KEY_USER_TYPE, 0);
            if (type == 2) {
                viewPager.setCurrentItem(1, true);
                txtViewUserName.setText(Resource.toString(R.string.teacher));
            }
        }
        if(App.getApplicationInstance().isSingleStudent()){
            linLayotSwitchUser.setVisibility(View.GONE);
            viewSwitchUser.setVisibility(View.GONE);
        }

    }

    @OnClick(R.id.txtViewChangePassword)
    public void onChangePasswordClick() {
        startActivity(new Intent(this, ResetPasswordActivity.class));
    }

    @OnClick(R.id.txtViewChangeMobileNo)
    public void onChangeMobileNoClick() {
        startActivity(new Intent(this, ChangeMobileNoActivity.class));
    }

    @OnClick(R.id.txtViewLogout)
    public void onLogoutClick() {
        showAlertLogout();
    }

    public void showAlertLogout() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(NavigationActivity.this);
        alertDialog.setCancelable(false);
        alertDialog.setTitle(Resource.toString(R.string.logout));
        alertDialog.setMessage(Resource.toString(R.string.are_you_sure_you_want_to_logout));

        alertDialog.setPositiveButton(Resource.toString(R.string.logout), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                logoutUser();
            }
        });

        alertDialog.setNegativeButton(Resource.toString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                drawer_layout.closeDrawer(GravityCompat.START);
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void logoutUser() {
        logoutPresenterImpl.logout();
    }

    private void createViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        if (App.getApplicationInstance().isShowParentTab())
            adapter.addFrag(new ParentFragment(), Resource.toString(R.string.parent));
        else
            adapter.addFrag(new TeacherFragment(), Resource.toString(R.string.employee));

        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        result = UserSession.getInstance().getLoginModelObject();

        if (UserSession.getInstance().getLoginType() == 2 && result != null) {
            viewPager.setCurrentItem(1, true);
            txtViewUserName.setText(result.getEid().getEmpName());
            mTitle.setText(result.getEid().getEmpName());
        } else {
            txtViewUserName.setText(UserSession.getInstance().getStudentName());
            mTitle.setText(UserSession.getInstance().getStudentName() + " - " + UserSession.getInstance().getStudentClass() + " (" + UserSession.getInstance().getStudentSection() + ")");
        }

        if (type == 2 && result != null && result.getEid() != null) {
            txtViewUserName.setText(result.getEid().getEmpName());
            mTitle.setText(result.getEid().getEmpName());
        }

        if (App.getApplicationInstance().isShowParentTab()) {
            txtViewClass.setText(UserSession.getInstance().getStudentClass() + " (" + UserSession.getInstance().getStudentSection() + ")");
        } else {
            txtViewClass.setVisibility(View.GONE);
        }
    }


    @OnClick({R.id.txtViewRate, R.id.txtViewShare, R.id.imgViewBell, R.id.txtViewSwitchUser})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtViewRate:
                NavigationActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("market://details?id=com.ifoster")));
                break;
            case R.id.txtViewShare:
                shareWithIntent("iFoster App");
                break;
            case R.id.txtViewSwitchUser:
                drawer_layout.closeDrawer(GravityCompat.START);
                Intent in = new Intent(this, ShowStudentListActivity.class);
                in.putExtra(IS_FROM_DASHBOARD,true);
                startActivity(in);
                break;
            case R.id.imgViewBell:
                startActivity(new Intent(this, NotificationActivity.class));
                break;
        }
    }


    private void shareWithIntent(String shareableText) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "market://details?id=com.ifoster");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, shareableText);
        startActivity(Intent.createChooser(shareIntent, "Share with"));
    }

    @Override
    public void onBackPressed() {
        SnackNotify.showSnakeBarForBackPress(coordinateLayout);
    }


    @Override
    public void onSuccessLogout(String message) {
        UserSession.getInstance().clearUserSession();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finishAffinity();
    }

    @Override
    public void onUnsuccessLogout(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onLogoutInternetError() {
        SnackNotify.checkConnection(onRetryLogout, coordinateLayout);
    }

    OnClickInterface onRetryLogout = new OnClickInterface() {
        @Override
        public void onClick() {
            logoutUser();
        }
    };

    private void setToolbar() {
        imgViewBell.setVisibility(View.VISIBLE);
        txtViewToolbarTitle.setText(getString(R.string.title_dashboard));
        imgViewBack.setImageResource(R.mipmap.ic_menu);
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewUserName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewClass, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtViewDashboard, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
        FontHelper.applyFont(this, txtViewLogout, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
        FontHelper.applyFont(this, txtViewChangeMobileNo, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
        FontHelper.applyFont(this, txtViewChangePassword, FontHelper.FontType.FONT_QUICKSAND_REGULAR);
    }
}
