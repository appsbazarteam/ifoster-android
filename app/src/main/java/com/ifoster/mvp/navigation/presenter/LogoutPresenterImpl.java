package com.ifoster.mvp.navigation.presenter;

import android.app.Activity;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.helpers.Progress;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.navigation.model.LogoutModel;
import com.ifoster.mvp.navigation.view.LogoutView;
import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class LogoutPresenterImpl implements LogoutPresenter {

    private Activity activity;
    private LogoutView logoutView;
    private JSONObject jsonObject;

    public LogoutPresenterImpl(Activity activity, LogoutView logoutView) {
        this.activity = activity;
        this.logoutView = logoutView;
    }

    @Override
    public void logout() {
        try {
            ApiAdapter.getInstance(activity);
            callLogoutApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            logoutView.onLogoutInternetError();
        }
    }

    private void callLogoutApi() {
        Progress.start(activity);
        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_ID, UserSession.getInstance().getStudentId());
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<LogoutModel> getLoginOutput = ApiAdapter.getApiService().logout("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<LogoutModel>() {
            @Override
            public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {
                Progress.stop();
                try {
                    LogoutModel examScheduleModel = response.body();
                    String message = examScheduleModel.getMessage();

                    if (examScheduleModel.getStatus()) {
                        logoutView.onSuccessLogout(examScheduleModel.getMessage());
                    } else {
                        logoutView.onSuccessLogout(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    logoutView.onSuccessLogout(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<LogoutModel> call, Throwable t) {
                Progress.stop();
                logoutView.onUnsuccessLogout(activity.getString(R.string.error_server));
            }
        });
    }

}
