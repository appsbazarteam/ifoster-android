package com.ifoster.mvp.navigation.view;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface LogoutView {

    public void onSuccessLogout(String message);

    public void onUnsuccessLogout(String message);

    public void onLogoutInternetError();
}
