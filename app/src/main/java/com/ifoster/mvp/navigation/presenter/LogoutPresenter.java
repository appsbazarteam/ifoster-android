package com.ifoster.mvp.navigation.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface LogoutPresenter {

    public void logout();
}
