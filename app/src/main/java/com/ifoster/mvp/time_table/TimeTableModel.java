package com.ifoster.mvp.time_table;

/**
 * Created by Alam on 20-Oct-17.
 */

public class TimeTableModel {

    Sunday sonday;
    Monday monday;
    Tuesday tuesday;
    Wednessday wednessday;
    Thursday thursday;
    Friday friday;

    public Sunday getSonday() {
        return sonday;
    }

    public void setSonday(Sunday sonday) {
        this.sonday = sonday;
    }

    public Monday getMonday() {
        return monday;
    }

    public void setMonday(Monday monday) {
        this.monday = monday;
    }

    public Tuesday getTuesday() {
        return tuesday;
    }

    public void setTuesday(Tuesday tuesday) {
        this.tuesday = tuesday;
    }

    public Wednessday getWednessday() {
        return wednessday;
    }

    public void setWednessday(Wednessday wednessday) {
        this.wednessday = wednessday;
    }

    public Thursday getThursday() {
        return thursday;
    }

    public void setThursday(Thursday thursday) {
        this.thursday = thursday;
    }

    public Friday getFriday() {
        return friday;
    }

    public void setFriday(Friday friday) {
        this.friday = friday;
    }

    public class Sunday {

        String totalClasses;
        String className;

        public String getTotalClasses() {
            return totalClasses;
        }

        public void setTotalClasses(String totalClasses) {
            this.totalClasses = totalClasses;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }
    }

    public class Monday {

        String totalClasses;
        String className;

        public String getTotalClasses() {
            return totalClasses;
        }

        public void setTotalClasses(String totalClasses) {
            this.totalClasses = totalClasses;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }
    }

    public class Tuesday {

        String totalClasses;
        String className;

        public String getTotalClasses() {
            return totalClasses;
        }

        public void setTotalClasses(String totalClasses) {
            this.totalClasses = totalClasses;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }
    }

    public class Wednessday {

        String totalClasses;
        String className;

        public String getTotalClasses() {
            return totalClasses;
        }

        public void setTotalClasses(String totalClasses) {
            this.totalClasses = totalClasses;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }
    }

    public class Thursday {

        String totalClasses;
        String className;

        public String getTotalClasses() {
            return totalClasses;
        }

        public void setTotalClasses(String totalClasses) {
            this.totalClasses = totalClasses;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }
    }

    public class Friday {

        String totalClasses;
        String className;

        public String getTotalClasses() {
            return totalClasses;
        }

        public void setTotalClasses(String totalClasses) {
            this.totalClasses = totalClasses;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }
    }
}
