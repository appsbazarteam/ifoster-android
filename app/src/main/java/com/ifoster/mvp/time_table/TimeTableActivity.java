package com.ifoster.mvp.time_table;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.mvp.BaseActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class TimeTableActivity extends BaseActivity {

    @BindView(R.id.linLayMondayColumn)
    LinearLayout linLayMondayColumn;

    @BindView(R.id.linLayTuesdayColumn)
    LinearLayout linLayTuesdayColumn;

    @BindView(R.id.linLayWednessColumn)
    LinearLayout linLayWednessColumn;

    @BindView(R.id.linLayThursdayColumn)
    LinearLayout linLayThursdayColumn;

    @BindView(R.id.linLayFridayColumn)
    LinearLayout linLayFridayColumn;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_table);

        //ButterKnife.bind(this);

        setFont();

        setToolBar();

        setTimeTable();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }


    private void setTimeTable() {

        ArrayList<TimeTableModel> arrayListTimeTable = getTimeTableList();

        for (int i = 0; i < arrayListTimeTable.size(); i++) {

            LinearLayout linearLayout1 = new LinearLayout(this);
            LinearLayout linearLayout2 = new LinearLayout(this);
            LinearLayout linearLayout3 = new LinearLayout(this);
            LinearLayout linearLayout4 = new LinearLayout(this);
            LinearLayout linearLayout5 = new LinearLayout(this);

            TextView textView1 = new TextView(this);
            TextView textView2 = new TextView(this);
            TextView textView3 = new TextView(this);
            TextView textView4 = new TextView(this);
            TextView textView5 = new TextView(this);

            // set text color
            textView1.setTextColor(ContextCompat.getColor(this, R.color.color_white));
            textView2.setTextColor(ContextCompat.getColor(this, R.color.color_white));
            textView3.setTextColor(ContextCompat.getColor(this, R.color.color_white));
            textView4.setTextColor(ContextCompat.getColor(this, R.color.color_white));
            textView5.setTextColor(ContextCompat.getColor(this, R.color.color_white));

            // set textview param
            textView1.setLayoutParams(new Toolbar.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT));
            textView2.setLayoutParams(new Toolbar.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT));
            textView3.setLayoutParams(new Toolbar.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT));
            textView4.setLayoutParams(new Toolbar.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT));
            textView5.setLayoutParams(new Toolbar.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT));

            textView1.setGravity(Gravity.CENTER_HORIZONTAL);
            textView2.setGravity(Gravity.CENTER_HORIZONTAL);
            textView3.setGravity(Gravity.CENTER_HORIZONTAL);
            textView4.setGravity(Gravity.CENTER_HORIZONTAL);
            textView5.setGravity(Gravity.CENTER_HORIZONTAL);


            // set Color code
            linearLayout1.setBackgroundColor(getColorCode(arrayListTimeTable.get(i).getMonday().className));
            linearLayout2.setBackgroundColor(getColorCode(arrayListTimeTable.get(i).getTuesday().className));
            linearLayout3.setBackgroundColor(getColorCode(arrayListTimeTable.get(i).getWednessday().className));
            linearLayout4.setBackgroundColor(getColorCode(arrayListTimeTable.get(i).getThursday().className));
            linearLayout5.setBackgroundColor(getColorCode(arrayListTimeTable.get(i).getFriday().className));


            // set Monday Column
            textView1.setText(arrayListTimeTable.get(i).getMonday().getClassName());
            if (arrayListTimeTable.get(i).getMonday().getTotalClasses().equals("1")) {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT,
                        0, 6);
                params.setMargins(0, 2, 0, 2);
                linearLayout1.setLayoutParams(params);
                //linearLayout1.removeAllViews();
                linearLayout1.addView(textView1);
                linLayMondayColumn.addView(linearLayout1);

            } else {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT,
                        0, 12);
                params.setMargins(0, 2, 0, 2);
                linearLayout1.setLayoutParams(params);
                linearLayout1.removeAllViews();
                linearLayout1.addView(textView1);
                linLayMondayColumn.addView(linearLayout1);
            }

            // set Tuesday Column
            textView2.setText(arrayListTimeTable.get(i).getTuesday().getClassName());
            if (arrayListTimeTable.get(i).getTuesday().getTotalClasses().equals("1")) {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT,
                        0, 6);
                params.setMargins(0, 2, 0, 2);
                linearLayout2.setLayoutParams(params);
                //linearLayout2.removeAllViews();
                linearLayout2.addView(textView2);
                linLayTuesdayColumn.addView(linearLayout2);

            } else {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT,
                        0, 12);
                params.setMargins(0, 2, 0, 2);
                linearLayout2.setLayoutParams(params);
                linearLayout2.removeAllViews();
                linearLayout2.addView(textView2);
                linLayTuesdayColumn.addView(linearLayout2);
            }

            // set Wednessday Column
            textView3.setText(arrayListTimeTable.get(i).getWednessday().getClassName());
            if (arrayListTimeTable.get(i).getWednessday().getTotalClasses().equals("1")) {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT,
                        0, 6);
                params.setMargins(0, 2, 0, 2);
                linearLayout3.setLayoutParams(params);
                // linearLayout3.removeAllViews();
                linearLayout3.addView(textView3);
                linLayWednessColumn.addView(linearLayout3);

            } else {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT,
                        0, 12);
                params.setMargins(0, 2, 0, 2);
                linearLayout3.setLayoutParams(params);
                linearLayout3.removeAllViews();
                linearLayout3.addView(textView3);
                linLayWednessColumn.addView(linearLayout3);
            }

            // set Thursday Column
            textView4.setText(arrayListTimeTable.get(i).getThursday().getClassName());
            if (arrayListTimeTable.get(i).getThursday().getTotalClasses().equals("1")) {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT,
                        0, 6);
                params.setMargins(0, 2, 0, 2);
                linearLayout4.setLayoutParams(params);
                // linearLayout4.removeAllViews();
                linearLayout4.addView(textView4);
                linLayThursdayColumn.addView(linearLayout4);

            } else {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT,
                        0, 12);
                params.setMargins(0, 2, 0, 2);
                linearLayout4.setLayoutParams(params);
                linearLayout4.removeAllViews();
                linearLayout4.addView(textView4);
                linLayThursdayColumn.addView(linearLayout4);
            }

            // set Friday Column
            textView5.setText(arrayListTimeTable.get(i).getFriday().getClassName());
            if (arrayListTimeTable.get(i).getFriday().getTotalClasses().equals("1")) {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT,
                        0, 6);
                params.setMargins(0, 2, 0, 2);
                linearLayout5.setLayoutParams(params);
                //linearLayout5.removeAllViews();
                linearLayout5.addView(textView5);
                linLayFridayColumn.addView(linearLayout5);

            } else {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT,
                        0, 12);
                params.setMargins(0, 2, 0, 2);
                linearLayout5.setLayoutParams(params);
                linearLayout5.removeAllViews();
                linearLayout5.addView(textView5);
                linLayFridayColumn.addView(linearLayout5);
            }

        }


    }

    private int getColorCode(String subject) {

        if (subject.equals("Hindi")) {
            return ContextCompat.getColor(this, R.color.color_time_table1);
        }

        if (subject.equals("Science")) {
            return ContextCompat.getColor(this, R.color.color_time_table2);
        }

        if (subject.equals("Math")) {
            return ContextCompat.getColor(this, R.color.color_time_table3);
        }

        if (subject.equals("Computer")) {
            return ContextCompat.getColor(this, R.color.color_time_table5);
        }

        return 0;
    }

    private ArrayList<TimeTableModel> getTimeTableList() {

        ArrayList<TimeTableModel> arrayListTimeTable = new ArrayList<>();

        /*------------------------1-------------------------*/
        TimeTableModel timeTableModel1 = new TimeTableModel();
        // Mon
        TimeTableModel.Monday monday = timeTableModel1.new Monday();
        monday.setClassName("Hindi");
        monday.setTotalClasses("2");
        timeTableModel1.setMonday(monday);

        //Tue
        TimeTableModel.Tuesday tuesday = timeTableModel1.new Tuesday();
        tuesday.setClassName("Hindi");
        tuesday.setTotalClasses("1");
        timeTableModel1.setTuesday(tuesday);

        //Wed
        TimeTableModel.Wednessday wednessday = timeTableModel1.new Wednessday();
        wednessday.setClassName("Computer");
        wednessday.setTotalClasses("1");
        timeTableModel1.setWednessday(wednessday);

        //Thu
        TimeTableModel.Thursday thursday = timeTableModel1.new Thursday();
        thursday.setClassName("Science");
        thursday.setTotalClasses("2");
        timeTableModel1.setThursday(thursday);

        //Fri
        TimeTableModel.Friday friday = timeTableModel1.new Friday();
        friday.setClassName("Hindi");
        friday.setTotalClasses("1");
        timeTableModel1.setFriday(friday);

         /*------------------------2-------------------------*/
        TimeTableModel timeTableModel2 = new TimeTableModel();
        // Mon
        TimeTableModel.Monday monday2 = timeTableModel2.new Monday();
        monday2.setClassName("Math");
        monday2.setTotalClasses("2");
        timeTableModel2.setMonday(monday2);

        //Tue
        TimeTableModel.Tuesday tuesday2 = timeTableModel2.new Tuesday();
        tuesday2.setClassName("Science");
        tuesday2.setTotalClasses("2");
        timeTableModel2.setTuesday(tuesday2);

        //Wed
        TimeTableModel.Wednessday wednessday2 = timeTableModel2.new Wednessday();
        wednessday2.setClassName("Math");
        wednessday2.setTotalClasses("1");
        timeTableModel2.setWednessday(wednessday2);

        //Thu
        TimeTableModel.Thursday thursday2 = timeTableModel2.new Thursday();
        thursday2.setClassName("Hindi");
        thursday2.setTotalClasses("2");
        timeTableModel2.setThursday(thursday2);

        //Fri
        TimeTableModel.Friday friday2 = timeTableModel2.new Friday();
        friday2.setClassName("Math");
        friday2.setTotalClasses("1");
        timeTableModel2.setFriday(friday2);

         /*------------------------3-------------------------*/
        TimeTableModel timeTableModel3 = new TimeTableModel();
        // Mon
        TimeTableModel.Monday monday3 = timeTableModel3.new Monday();
        monday3.setClassName("Science");
        monday3.setTotalClasses("1");
        timeTableModel3.setMonday(monday3);

        //Tue
        TimeTableModel.Tuesday tuesday3 = timeTableModel3.new Tuesday();
        tuesday3.setClassName("Science");
        tuesday3.setTotalClasses("2");
        timeTableModel3.setTuesday(tuesday3);

        //Wed
        TimeTableModel.Wednessday wednessday3 = timeTableModel3.new Wednessday();
        wednessday3.setClassName("Science");
        wednessday3.setTotalClasses("1");
        timeTableModel3.setWednessday(wednessday3);

        //Thu
        TimeTableModel.Thursday thursday3 = timeTableModel3.new Thursday();
        thursday3.setClassName("Computer");
        thursday3.setTotalClasses("1");
        timeTableModel3.setThursday(thursday3);

        //Fri
        TimeTableModel.Friday friday3 = timeTableModel3.new Friday();
        friday3.setClassName("Hindi");
        friday3.setTotalClasses("1");
        timeTableModel3.setFriday(friday3);

         /*------------------------4-------------------------*/
        TimeTableModel timeTableModel4 = new TimeTableModel();

        // Mon
        TimeTableModel.Monday monday4 = timeTableModel4.new Monday();
        monday4.setClassName("Computer");
        monday4.setTotalClasses("1");
        timeTableModel4.setMonday(monday4);

        //Tue
        TimeTableModel.Tuesday tuesday4 = timeTableModel4.new Tuesday();
        tuesday4.setClassName("Hindi");
        tuesday4.setTotalClasses("1");
        timeTableModel4.setTuesday(tuesday4);

        //Wed
        TimeTableModel.Wednessday wednessday4 = timeTableModel4.new Wednessday();
        wednessday4.setClassName("Hindi");
        wednessday4.setTotalClasses("2");
        timeTableModel4.setWednessday(wednessday4);

        //Thu
        TimeTableModel.Thursday thursday4 = timeTableModel4.new Thursday();
        thursday4.setClassName("Computer");
        thursday4.setTotalClasses("1");
        timeTableModel4.setThursday(thursday4);

        //Fri
        TimeTableModel.Friday friday4 = timeTableModel4.new Friday();
        friday4.setClassName("Computer");
        friday4.setTotalClasses("1");
        timeTableModel4.setFriday(friday4);


        // add model to list
        arrayListTimeTable.add(timeTableModel1);
        arrayListTimeTable.add(timeTableModel2);
        arrayListTimeTable.add(timeTableModel3);
        arrayListTimeTable.add(timeTableModel4);

        return arrayListTimeTable;

    }

    @OnClick(R.id.imgViewBack)
    public void goBack() {
        finish();
    }

    private void setToolBar() {

        txtViewToolbarTitle.setText(getString(R.string.title_time_table));
    }

}
