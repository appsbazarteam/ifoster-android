package com.ifoster.mvp.splash;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

import com.ifoster.App;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.Resource;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.login.LoginActivity;
import com.ifoster.mvp.login.LoginModel;
import com.ifoster.mvp.navigation.NavigationActivity;
import com.ifoster.mvp.show_student_list.ShowStudentListActivity;
import com.ifoster.mvp.show_student_list.ShowStudentListModel;
import com.ifoster.mvp.show_student_list.ShowStudentListPresenterImpl;
import com.ifoster.mvp.show_student_list.ShowStudentListView;

import java.util.ArrayList;

import butterknife.BindView;

public class SplashActivity extends BaseActivity implements ShowStudentListView {
    private static final int SPLASH_TIME_OUT = 3000;

    @BindView(R.id.content_splash)
    RelativeLayout contentSplash;

    private ShowStudentListPresenterImpl showStudentListPresenterImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate();
            }
        }, SPLASH_TIME_OUT);
    }

    private void navigate() {
        if (UserSession.getInstance().isUserLoggedIn()) {
            if (UserSession.getInstance().getLoginType() == 1) {
                callStudentListAPI();
            } else if (UserSession.getInstance().getLoginType() == 2) {
                if (UserSession.getInstance().getLoginModelObject() != null && UserSession.getInstance().getLoginModelObject().getEid() != null) {
                    App.getApplicationInstance().setEid(UserSession.getInstance().getLoginModelObject().getEid());
                    App.getApplicationInstance().setProfileImageURL(UserSession.getInstance().getLoginModelObject().getEid().getImagePath());
                }
                startActivity(new Intent(SplashActivity.this, NavigationActivity.class));
                finish();
            } else if (UserSession.getInstance().getLoginType() == 3) {
                showDialog();
            }
        } else {
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public RelativeLayout getContentSplash() {
        return contentSplash;
    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.mipmap.ic_foster_louncher)
                .setTitle(Resource.toString(R.string.login_choice))
                .setMessage(Resource.toString(R.string.you_want_to_login_as_a))
                .setCancelable(false)
                .setPositiveButton(Resource.toString(R.string.teacher), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        onTeacherSelect();
                    }
                }).setNegativeButton(Resource.toString(R.string.parent), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                callStudentListAPI();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void onTeacherSelect() {
        LoginModel.Result result = UserSession.getInstance().getLoginModelObject();
        App.getApplicationInstance().setShowParentTab(false);
        App.getApplicationInstance().setSingleStudent(true);

        if (result != null && result.getEid() != null) {
            setEmpDetailsToSession(result.getEid().getEmpId(), result);
            App.getApplicationInstance().setProfileImageURL(result.getEid().getImagePath());
            App.getApplicationInstance().setEid(result.getEid());
        }

        Intent intent = new Intent(this, NavigationActivity.class);
        intent.putExtra(ConstIntent.KEY_USER_TYPE, 2);
        startActivity(intent);
        finish();
    }

    private void callStudentListAPI() {
        showProgressBar();
        showStudentListPresenterImpl = new ShowStudentListPresenterImpl(this, this);
        showStudentListPresenterImpl.callingStudentListApi(UserSession.getInstance().getUserToken(), 0);
    }

    @Override
    public void onSuccessStudentList(ArrayList<ShowStudentListModel.User> userArrayList) {
        hideProgressBar();
        if (userArrayList != null && userArrayList.size() >0) {
            if (userArrayList.size() == 1) {
                navigateToDashboard(userArrayList.get(0));
            }else {
                navigateToStudentList();
            }
        } else {
            onUnsuccessStudentList(Resource.toString(R.string.error_server));
        }
    }

    @Override
    public void onUnsuccessStudentList(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, contentSplash);
    }

    @Override
    public void onStudentListInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryGetStudentList, contentSplash);
    }

    OnClickInterface onRetryGetStudentList = new OnClickInterface() {
        @Override
        public void onClick() {
            callStudentListAPI();
        }
    };

    private void navigateToDashboard(ShowStudentListModel.User user) {
        App.getApplicationInstance().setSingleStudent(true);
        App.getApplicationInstance().setShowParentTab(true);
        App.getApplicationInstance().setProfileImageURL(user.getProfileImage());
        UserSession.getInstance().saveStudentId(Integer.parseInt(user.getStudentId()), user.getStudentName(), user.getClass_(), user.getSection());
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.putExtra(ConstIntent.KEY_USER_TYPE, UserSession.getInstance().getLoginType());
        startActivity(intent);
        finish();
    }

    private void navigateToStudentList() {
        LoginModel.Result result = UserSession.getInstance().getLoginModelObject();
        if (result != null && result.getEid() != null) {
            setEmpDetailsToSession(result.getEid().getEmpId(), result);
        }
        Intent intent = new Intent(this, ShowStudentListActivity.class);
        intent.putExtra(ConstIntent.KEY_USER_TYPE, 1);
        startActivity(intent);
        finish();
    }

    private void setEmpDetailsToSession(int empId, LoginModel.Result result) {
        UserSession.getInstance().saveEmployeeDetails(empId, result.getEid().getEmpName(), result.getEid().getImagePath());
    }
}
