package com.ifoster.mvp.login;

import android.app.Activity;
import android.provider.Settings;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.FcmSession;
import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class LoginPresenterImpl implements LoginPresenter {

    private Activity activity;
    private LoginView loginView;
    private JSONObject jsonObject;

    public LoginPresenterImpl(Activity activity, LoginView loginView) {
        this.activity = activity;
        this.loginView = loginView;
    }

    @Override
    public void callingLoginApi(String mobileNo, String password) {
        try {
            ApiAdapter.getInstance(activity);
            gettingResultOfLogin(mobileNo, password);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            loginView.onInternetError();
        }
    }

    private void gettingResultOfLogin(String mobileNo, String password) {
        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceToken = new FcmSession(activity).getFcmToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_MOBILE_NO, mobileNo);
            jsonObject.put(Const.PARAM_PASSWORD, password);
            jsonObject.put(Const.KEY_DEVICE_ID, deviceId);
            jsonObject.put(Const.KEY_DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.KEY_DEVICE_TYPE, Const.KEY_DEVICE_TYPE_VALUE);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<LoginModel> getLoginOutput = ApiAdapter.getApiService().login("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                try {
                    LoginModel loginModel = response.body();
                    String message = loginModel.getMessage();

                    if (loginModel.getStatus()) {
                        loginView.onSuccessLogin(loginModel.getResult(), message);
                    } else {
                        loginView.onUnsuccessLogin(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    loginView.onUnsuccessLogin(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                loginView.onUnsuccessLogin(activity.getString(R.string.error_server));
            }
        });
    }
}
