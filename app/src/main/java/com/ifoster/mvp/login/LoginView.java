package com.ifoster.mvp.login;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface LoginView {

    public void onSuccessLogin(LoginModel.Result result, String message);

    public void onUnsuccessLogin(String message);

    public void onInternetError();
}
