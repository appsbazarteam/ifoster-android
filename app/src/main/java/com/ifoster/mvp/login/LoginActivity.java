package com.ifoster.mvp.login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifoster.App;
import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.Resource;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.change_mobile_no.ChangeMobileNoActivity;
import com.ifoster.mvp.forgot_password.ForgotPasswordActivity;
import com.ifoster.mvp.navigation.NavigationActivity;
import com.ifoster.mvp.show_student_list.ShowStudentListActivity;
import com.ifoster.mvp.show_student_list.ShowStudentListModel;
import com.ifoster.mvp.show_student_list.ShowStudentListPresenterImpl;
import com.ifoster.mvp.show_student_list.ShowStudentListView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class LoginActivity extends BaseActivity implements LoginView, ShowStudentListView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;
    @BindView(R.id.textViewForgotPassword)
    TextView textViewForgotPassword;
    @BindView(R.id.textViewChangeMobileNo)
    TextView textViewChangeMobileNo;
    @BindView(R.id.edtTextMobileNo)
    AppCompatEditText edtTextMobileNo;
    @BindView(R.id.edtTextPassword)
    AppCompatEditText edtTextPassword;
    @BindView(R.id.txtView_login_submitTxt)
    TextView txtView_login_submitTxt;
    @BindView(R.id.or)
    TextView or;
    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;
    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    private LoginPresenterImpl loginPresenterImpl;
    private ShowStudentListPresenterImpl showStudentListPresenterImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginPresenterImpl = new LoginPresenterImpl(this, this);

        setFont();
        setToolbar();
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, edtTextMobileNo, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, edtTextPassword, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, txtView_login_submitTxt, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, textViewChangeMobileNo, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, textViewForgotPassword, FontHelper.FontType.FONT_QUICKSAND_BOLD);
        FontHelper.applyFont(this, or, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    private void setToolbar() {
        imgViewBack.setVisibility(View.GONE);
        txtViewToolbarTitle.setText(getString(R.string.title_login));
        textViewForgotPassword.setPaintFlags(textViewForgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        textViewChangeMobileNo.setPaintFlags(textViewChangeMobileNo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @OnEditorAction(R.id.edtTextPassword)
    public boolean setEdTextPassword(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            getData();
            return true;
        }
        return false;
    }

    @OnClick(R.id.cardViewBtnContinue)
    public void btnSubmit() {
        getData();
    }

    @OnClick(R.id.textViewForgotPassword)
    public void forgotPassword() {
        if(!isProgressBarShowing())
            startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

    @OnClick(R.id.textViewChangeMobileNo)
    public void changeMobileNo() {
        startActivity(new Intent(this, ChangeMobileNoActivity.class));
    }

    private void getData() {
        String mobileNo = edtTextMobileNo.getText().toString();
        String password = edtTextPassword.getText().toString();

        Utils.hideKeyboardIfOpen(this);
        if (isValid(mobileNo, password)) {
            if(!isProgressBarShowing()){
                showProgressBar();
                loginPresenterImpl.callingLoginApi(mobileNo, password);
            }
        }
    }

    private boolean isValid(String mobileNo, String password) {
        if (TextUtils.isEmpty(mobileNo)) {
            edtTextMobileNo.setError(getString(R.string.empty_mobile_no));
            return false;
        } else if (TextUtils.isEmpty(password)) {
            edtTextMobileNo.setError(getString(R.string.empty_pass));
            return false;
        }
        return true;
    }

    @Override
    public void onSuccessLogin(LoginModel.Result result, String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
        // save user detail to sessin
        setUserDetailToSession(result);
        UserSession.getInstance().saveLoginModelObject(result);

        if (result.getData() == 1) {
            callStudentListAPI();
        } else if (result.getData() == 2) {
            App.getApplicationInstance().setShowParentTab(false);

            if (result != null && result.getEid() !=null && result.getEid().getEmpId() >0 ) {
                App.getApplicationInstance().setEid(result.getEid());
                App.getApplicationInstance().setProfileImageURL(UserSession.getInstance().getLoginModelObject().getEid().getImagePath());
                setEmpIdToSession(result.getEid().getEmpId(),result);
            }
            startActivity(new Intent(this, NavigationActivity.class));
            finish();
        } else if (result.getData() == 3) {
            showDialog();
        }
    }

    private void setUserDetailToSession(LoginModel.Result result) {
        UserSession.getInstance().createUserSession(edtTextMobileNo.getText().toString(), result.getToken(), result.getData());
    }

    private void setEmpIdToSession(int empId, LoginModel.Result result) {
        UserSession.getInstance().saveEmployeeDetails(empId, result.getEid().getEmpName(), result.getEid().getImagePath());
    }

    @Override
    public void onUnsuccessLogin(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryLogin, coordinateLayout);
    }

    OnClickInterface onRetryLogin = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    //dialog
    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.mipmap.ic_foster_louncher)
                .setTitle(Resource.toString(R.string.login_choice))
                .setMessage(Resource.toString(R.string.you_want_to_login_as_a))
                .setCancelable(false)
                .setPositiveButton(Resource.toString(R.string.teacher), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        onTeacherSelect();
                    }
                }).setNegativeButton(Resource.toString(R.string.parent), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                callStudentListAPI();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void onTeacherSelect() {
        LoginModel.Result result = UserSession.getInstance().getLoginModelObject();
        App.getApplicationInstance().setShowParentTab(false);
        App.getApplicationInstance().setSingleStudent(true);

        if (result != null && result.getEid() != null) {
            setEmpDetailsToSession(result.getEid().getEmpId(), result);
            App.getApplicationInstance().setProfileImageURL(result.getEid().getImagePath());
            App.getApplicationInstance().setEid(result.getEid());
        }

        Intent intent = new Intent(this, NavigationActivity.class);
        intent.putExtra(ConstIntent.KEY_USER_TYPE, 2);
        startActivity(intent);
        finish();
    }

    private void callStudentListAPI() {
        showProgressBar();
        showStudentListPresenterImpl = new ShowStudentListPresenterImpl(this, this);
        showStudentListPresenterImpl.callingStudentListApi(UserSession.getInstance().getUserToken(), 0);
    }

    @Override
    public void onSuccessStudentList(ArrayList<ShowStudentListModel.User> userArrayList) {
        hideProgressBar();
        if (userArrayList != null && userArrayList.size() >0) {
            if (userArrayList.size() == 1) {
                navigateToDashboard(userArrayList.get(0));
            }else {
                navigateToStudentList();
            }
        } else {
            onUnsuccessStudentList(Resource.toString(R.string.error_server));
        }
    }

    @Override
    public void onUnsuccessStudentList(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onStudentListInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryGetStudentList, coordinateLayout);
    }

    OnClickInterface onRetryGetStudentList = new OnClickInterface() {
        @Override
        public void onClick() {
            callStudentListAPI();
        }
    };

    private void navigateToDashboard(ShowStudentListModel.User user) {
        App.getApplicationInstance().setSingleStudent(true);
        App.getApplicationInstance().setShowParentTab(true);
        App.getApplicationInstance().setProfileImageURL(user.getProfileImage());
        UserSession.getInstance().saveStudentId(Integer.parseInt(user.getStudentId()), user.getStudentName(), user.getClass_(), user.getSection());
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.putExtra(ConstIntent.KEY_USER_TYPE, UserSession.getInstance().getLoginType());
        startActivity(intent);
        finish();
    }

    private void navigateToStudentList() {
        LoginModel.Result result = UserSession.getInstance().getLoginModelObject();
        if (result != null && result.getEid() != null) {
            setEmpDetailsToSession(result.getEid().getEmpId(), result);
        }
        Intent intent = new Intent(this, ShowStudentListActivity.class);
        intent.putExtra(ConstIntent.KEY_USER_TYPE, 1);
        startActivity(intent);
        finish();
    }

    private void setEmpDetailsToSession(int empId, LoginModel.Result result) {
        UserSession.getInstance().saveEmployeeDetails(empId, result.getEid().getEmpName(), result.getEid().getImagePath());
    }
}
