package com.ifoster.mvp.login;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface LoginPresenter {
    public void callingLoginApi(String mobileNo, String password);
}
