package com.ifoster.mvp.medical;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedicalIdModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Result")
    @Expose
    private ArrayList<Result> result = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("height")
        @Expose
        private String height;
        @SerializedName("weight")
        @Expose
        private String weight;
        @SerializedName("blood_group")
        @Expose
        private String bloodGroup;
        @SerializedName("contact")
        @Expose
        private String contact;
        @SerializedName("eye_left")
        @Expose
        private String eyeVisionLeft;
        @SerializedName("eye_right")
        @Expose
        private String eyeVisionRight;
        @SerializedName("allergy_type")
        @Expose
        private String allergyType;
        @SerializedName("allergy")
        @Expose
        private String allergy;

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getBloodGroup() {
            return bloodGroup;
        }

        public void setBloodGroup(String bloodGroup) {
            this.bloodGroup = bloodGroup;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getEyeVisionLeft() {
            return eyeVisionLeft;
        }

        public void setEyeVisionLeft(String eyeVisionLeft) {
            this.eyeVisionLeft = eyeVisionLeft;
        }

        public String getEyeVisionRight() {
            return eyeVisionRight;
        }

        public void setEyeVisionRight(String eyeVisionRight) {
            this.eyeVisionRight = eyeVisionRight;
        }

        public String getAllergyType() {
            return allergyType;
        }

        public void setAllergyType(String allergyType) {
            this.allergyType = allergyType;
        }

        public String getAllergy() {
            return allergy;
        }

        public void setAllergy(String allergy) {
            this.allergy = allergy;
        }

    }
}