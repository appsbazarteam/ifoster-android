package com.ifoster.mvp.medical;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ifoster.App;
import com.ifoster.R;
import com.ifoster.common.adapter.AdapterSpinnerBlack;
import com.ifoster.common.helpers.AlertDialogHelper;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.CircleTransform;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.login.LoginModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnItemSelected;

public class StudentMedicalIdActivity extends BaseActivity implements MedicalIdView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @BindView(R.id.txtViewUserName)
    TextView txtViewUserName;

    @BindView(R.id.txtViewDOB)
    TextView txtViewDOB;

    @BindView(R.id.txtViewUpdateInfo)
    TextView txtViewUpdateInfo;

    @BindView(R.id.txtViewHeight)
    TextView txtViewHeight;

    @BindView(R.id.edtTxtHeight)
    AppCompatEditText edtTxtHeight;

    @BindView(R.id.txtViewWeight)
    TextView txtViewWeight;

    @BindView(R.id.edtTxtWeight)
    AppCompatEditText edtTxtWeight;

    @BindView(R.id.txtViewBloodGroup)
    TextView txtViewBloodGroup;

    @BindView(R.id.edtTxtBloodGroup)
    AppCompatEditText edtTxtBloodGroup;

    @BindView(R.id.txtViewEyeVision)
    TextView txtViewEyeVision;

    @BindView(R.id.edtTxtEyeVisionLeft)
    AppCompatEditText edtTxtEyeVisionLeft;

    @BindView(R.id.txtViewAllergy)
    TextView txtViewAllergy;

   /* @BindView(R.id.edtTxtAllergy)
    AppCompatEditText edtTxtAllergy;*/

    @BindView(R.id.txtViewAllergyDetails)
    TextView txtViewAllergyDetails;

    @BindView(R.id.edtTxtAllergyDetails)
    AppCompatEditText edtTxtAllergyDetails;

    @BindView(R.id.txtViewSOS)
    TextView txtViewSOS;

    @BindView(R.id.edtTxtSOS)
    AppCompatEditText edtTxtSOS;

    @BindView(R.id.edtTxtEyeVisionRight)
    AppCompatEditText edtTxtEyeVisionRight;

    @BindView(R.id.rLayoutOk)
    RelativeLayout rLayoutOk;

    @BindView(R.id.rLayoutCancel)
    RelativeLayout rLayoutCancel;

    @BindView(R.id.txtViewCall)
    TextView txtViewCall;

    @BindView(R.id.spinnerAllergy)
    AppCompatSpinner spinnerAllergy;

    @BindView(R.id.llAllergyDetails)
    LinearLayout llAllergyDetails;

    /*----------Class Presenter------------*/
    MedicalIdPresenterImpl medicalIdPresenterImpl;

    /*------UserSession--------*/


    /*-----------Hash Map Leave--------*/
    ArrayList<HashMap<String, String>> hashMapArrayListAllergy;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_id);

        medicalIdPresenterImpl = new MedicalIdPresenterImpl(this, this);

        //setFont();

        // set toolbar
        setToolBar();

        bindStudentLeaveTypeSpinner();

        getData();

        setProfileImage();

    }

    private void setProfileImage() {
        if (App.getApplicationInstance().getProfileImageURL() != null && App.getApplicationInstance().getProfileImageURL().length() > 0) {
            Glide.with(this).load(App.getApplicationInstance().getProfileImageURL())
                    .error(R.drawable.user)
                    .placeholder(R.drawable.user)
                    .thumbnail(0.5f)
                    .crossFade()
                    .transform(new CircleTransform(this))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfile);

        } else {
            imgProfile.setBackgroundResource(R.drawable.user);
        }

        if(App.getApplicationInstance().isShowParentTab()){
            txtViewUserName.setText(UserSession.getInstance().getStudentName());
        }else{
            LoginModel.Result result = UserSession.getInstance().getLoginModelObject();
            if(result != null && result.getEid() != null && result.getEid().getEmpName() !=null && result.getEid().getEmpName().length() > 0){
                txtViewUserName.setText(result.getEid().getEmpName());
            }
        }

    }

    @OnItemSelected(R.id.spinnerAllergy)
    public void spinnerItemSelected(Spinner spinner, int position) {

        String selectedAllergy = hashMapArrayListAllergy.get(spinnerAllergy.getSelectedItemPosition()).get(Const.KEY_NAME);

        if (selectedAllergy.equalsIgnoreCase("Yes")) {
            llAllergyDetails.setVisibility(View.VISIBLE);
        } else {
            llAllergyDetails.setVisibility(View.GONE);
        }
    }

    private void bindStudentLeaveTypeSpinner() {

        hashMapArrayListAllergy = new ArrayList<>();

        //add spinner item
        HashMap<String, String> hashMap1 = new HashMap<>();
        hashMap1.put(Const.KEY_ID, "1");
        hashMap1.put(Const.KEY_NAME, "No");

        hashMapArrayListAllergy.add(hashMap1);

        HashMap<String, String> hashMap2 = new HashMap<>();
        hashMap2.put(Const.KEY_ID, "2");
        hashMap2.put(Const.KEY_NAME, "Yes");

        hashMapArrayListAllergy.add(hashMap2);

        // bind spiiner item
        bindAllrgyToSpinner(hashMapArrayListAllergy);
    }

    private void bindAllrgyToSpinner(ArrayList<HashMap<String, String>> hashMapArrayListAllergy) {

        AdapterSpinnerBlack adapterSpinner = new AdapterSpinnerBlack(this, R.layout.layout_spinner_dropdown, hashMapArrayListAllergy);
        spinnerAllergy.setAdapter(adapterSpinner);
    }

    private void setToolBar() {
        txtViewToolbarTitle.setText(getString(R.string.title_activity_medical_id_update));
    }

    @Override
    protected void onResume() {
        super.onResume();
        txtViewUpdateInfo.setPaintFlags(txtViewUpdateInfo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @OnClick(R.id.imgViewBack)
    public void back() {
        finish();
    }

    @OnClick(R.id.rLayoutOk)
    public void okButton() {

        updateMedicalId();
    }

    private void updateMedicalId() {

        Utils.hideKeyboardIfOpen(this);

        String height = edtTxtHeight.getText().toString();
        String weight = edtTxtWeight.getText().toString();
        String bloodGroup = edtTxtBloodGroup.getText().toString();
        String eyeLeftVision = edtTxtEyeVisionLeft.getText().toString();
        String eyeRightVision = edtTxtEyeVisionRight.getText().toString();
        // String allergy = edtTxtAllergy.getText().toString();
        String allergy = hashMapArrayListAllergy.get(spinnerAllergy.getSelectedItemPosition()).get(Const.KEY_NAME);
        String allergyDetail = edtTxtAllergyDetails.getText().toString();
        String contact = edtTxtSOS.getText().toString();

        if (isValid(height, weight, bloodGroup, eyeLeftVision, eyeRightVision, allergy, allergyDetail, contact)) {
            if(!isProgressBarShowing()){
                showProgressBar();
                medicalIdPresenterImpl.updateMedicalDetail(height, weight, bloodGroup, contact, eyeLeftVision, eyeRightVision,
                        allergy, allergyDetail);
            }


        }
    }

    private boolean isValid(String height, String weight, String bloodGroup, String eyeVision, String eyeRightVision
            , String allergy, String allergyDetail, String emergncySos) {

        if (TextUtils.isEmpty(height)) {
            showMessage(getString(R.string.empty_height), edtTxtHeight);
            return false;
        } else if (TextUtils.isEmpty(weight)) {
            showMessage(getString(R.string.empty_weight), edtTxtWeight);
            return false;
        } else if (TextUtils.isEmpty(emergncySos)) {
            showMessage(getString(R.string.empty_emergncy_sos), edtTxtSOS);
            return false;
        }

        return true;
    }

    private void showMessage(String msg, AppCompatEditText edtText) {
        edtText.setError(msg);
        edtText.requestFocus();
    }


    @OnClick(R.id.txtViewCall)
    public void call() {

        String mobileNo = edtTxtSOS.getText().toString();

        if (mobileNo.length() > 0) {
            alertForCall(mobileNo);
        } else {
            SnackNotify.showMessage("Please enter SOS number first to call.", coordinateLayout);
        }

    }

    public void alertForCall(final String mobileNo) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(StudentMedicalIdActivity.this);
        alertDialog.setCancelable(true);

        alertDialog.setMessage(mobileNo);

        alertDialog.setPositiveButton("Call", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                int permissionCheckCamera = ContextCompat.checkSelfPermission(StudentMedicalIdActivity.this,
                        Manifest.permission.CALL_PHONE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if ((permissionCheckCamera == -1)) {

                        if (!Settings.System.canWrite(StudentMedicalIdActivity.this)) {
                            requestPermissions(new String[]{Manifest.permission.CALL_PHONE
                            }, 2909);
                        } else {
                            callTo000(mobileNo);
                        }
                    } else {
                        callTo000(mobileNo);
                    }
                } else {
                    callTo000(mobileNo);
                }
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();

    }

    @OnEditorAction(R.id.edtTxtSOS)
    public boolean setEdTextPassword(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            updateMedicalId();
            return true;
        }
        return false;
    }

    private void callTo000(String mobileNo) {

        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mobileNo));
        if (ActivityCompat.checkSelfPermission(StudentMedicalIdActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case 2909: { // calling permission

                int permissionCheckRead = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CALL_PHONE);

                if ((permissionCheckRead == 0)) {
                    callTo000(edtTxtSOS.getText().toString());
                } else {
                    AlertDialogHelper.alertEnableAppSetting(this);
                }
                return;
            }
        }
    }


    @OnClick(R.id.rLayoutCancel)
    public void cancelButton() {
        finish();
    }

    @Override
    public void onSuccessMedicalId(ArrayList<MedicalIdModel.Result> results) {
        hideProgressBar();
        if (results != null) {

            // student Name
            //txtViewUserName.setText(UserSession.getInstance().getStudentName());

            if (results.size() > 0) {
                if (results.size() > 0) {

                    MedicalIdModel.Result result = results.get(0);

                    // Height
                    if (result.getHeight() != null) {
                        edtTxtHeight.setText(result.getHeight());
                        edtTxtHeight.setSelection(result.getHeight().length());
                    } else {
                        edtTxtHeight.setText("");
                    }
                    // Weight
                    if (result.getWeight() != null)
                        edtTxtWeight.setText(result.getWeight());
                    else
                        edtTxtWeight.setText("");

                    // Blood group
                    if (result.getBloodGroup() != null)
                        edtTxtBloodGroup.setText(result.getBloodGroup());
                    else
                        edtTxtBloodGroup.setText("");

                    // Eye Vision
                    if (result.getEyeVisionLeft() != null)
                        edtTxtEyeVisionLeft.setText(result.getEyeVisionLeft());
                    else
                        edtTxtEyeVisionLeft.setText("");

                    // Eye Vision
                    if (result.getEyeVisionRight() != null)
                        edtTxtEyeVisionRight.setText(result.getEyeVisionRight());
                    else
                        edtTxtEyeVisionRight.setText("");

                    //Allergy Detail
                    if (result.getAllergy() != null)
                        edtTxtAllergyDetails.setText(result.getAllergy());
                    else
                        edtTxtAllergyDetails.setText("");

                    //Allergy Detail
                    if (result.getContact() != null)
                        edtTxtSOS.setText(result.getContact());
                    else
                        edtTxtSOS.setText("");

                    if (result.getAllergyType() != null && result.getAllergyType().equalsIgnoreCase("Yes")) {
                        spinnerAllergy.setSelection(1);
                        llAllergyDetails.setVisibility(View.VISIBLE);
                    } else if (result.getAllergyType() != null && result.getAllergyType().equalsIgnoreCase("No")) {
                        spinnerAllergy.setSelection(0);
                        llAllergyDetails.setVisibility(View.GONE);
                    }
                }
            } else {

                SnackNotify.showMessage(getString(R.string.no_record_found), coordinateLayout);
            }
        }
    }

    @Override
    public void onUnsuccessMedicalId(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onMedicalIdInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryMedical, coordinateLayout);
    }

    @Override
    public void onSuccessMedicalIdUpdate(String message) {
        hideProgressBar();
        AlertDialogHelper.showAlertAndFinishedActivity(this, message);
    }

    @Override
    public void onUnsuccessMedicalIdUpdate(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onMedicalIdUpdateInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryMedicalUpdate, coordinateLayout);
    }


    OnClickInterface onRetryMedicalUpdate = new OnClickInterface() {
        @Override
        public void onClick() {
            updateMedicalId();
        }
    };


    OnClickInterface onRetryMedical = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    private void getData() {
        showProgressBar();
        medicalIdPresenterImpl.getMedicalIdDetail();
    }
}
