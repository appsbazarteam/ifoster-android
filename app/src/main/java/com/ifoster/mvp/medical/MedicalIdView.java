package com.ifoster.mvp.medical;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface MedicalIdView {

    public void onSuccessMedicalId(ArrayList<MedicalIdModel.Result> results);

    public void onUnsuccessMedicalId(String message);

    public void onMedicalIdInternetError();


    public void onSuccessMedicalIdUpdate(String message);

    public void onUnsuccessMedicalIdUpdate(String message);

    public void onMedicalIdUpdateInternetError();
}
