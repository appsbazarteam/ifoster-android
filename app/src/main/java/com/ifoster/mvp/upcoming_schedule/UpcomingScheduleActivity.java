package com.ifoster.mvp.upcoming_schedule;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.common.scroll_date_picker.DayScrollDatePicker;
import com.ifoster.mvp.common.scroll_date_picker.OnDateSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;

public class UpcomingScheduleActivity extends BaseActivity {

    @BindView(R.id.datePicker)
    DayScrollDatePicker datePicker;

    @BindView(R.id.txtViewSelectedDate)
    TextView txtViewSelectedDate;

    @BindView(R.id.linLayTimeInterval)
    LinearLayout linLayTimeInterval;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcoming_schedule);

        setFont();

        setToolBar();

        setUpPicker();

        getValue();

        setSchedule();

    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }

    private void setUpPicker() {
        datePicker.setStartDate(01, 04, 2017); // set start date for the picker
        datePicker.setEndDate(30, 03, 2018); // set end date for the picker
    }

    private void getValue() {
        datePicker.getSelectedDate(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@Nullable Date date) {
                if (date != null) {
                    //txtViewSelectedDate.setText(date.toString()); // set picker selected value onto textView

                    SimpleDateFormat postFormater = new SimpleDateFormat("MMM dd, yyyy");
                    String newDateStr = postFormater.format(date);

                    txtViewSelectedDate.setText(newDateStr.toString());
                }
            }
        });
    }

    @OnClick(R.id.imgViewBack)
    public void goBack() {
        finish();
    }

    private void setToolBar() {

        txtViewToolbarTitle.setText(getString(R.string.title_upcoming_schedule));
    }

    private void setSchedule() {

        int colorCodeType = 0;

        ArrayList<UpcomingScheduleModel> upcomingList = getScheduleList();

        for (int i = 0; i < upcomingList.size(); i++) {

            LayoutInflater inflater = null;
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mLinearView = inflater.inflate(R.layout.item_upcoming_schedule, null);

            final TextView txtViewStartTime = (TextView) mLinearView.findViewById(R.id.txtViewStartTime);
            final TextView txtViewEndTime = (TextView) mLinearView.findViewById(R.id.txtViewEndTime);
            final TextView txtViewEvent = (TextView) mLinearView.findViewById(R.id.txtViewEvent);
            final View viewLine = (View) mLinearView.findViewById(R.id.viewLine);
            final RelativeLayout relLayColorIndecator = (RelativeLayout) mLinearView.findViewById(R.id.relLayColorIndecator);


            // set start and end time
            txtViewStartTime.setText(upcomingList.get(i).getStartTime());
            txtViewEndTime.setText(upcomingList.get(i).getEndTime());
            txtViewEvent.setText(upcomingList.get(i).getDescription());

            FontHelper.applyFont(this, txtViewEvent, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(this, txtViewEndTime, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(this, txtViewStartTime, FontHelper.FontType.FONT_QUICKSAND_BOLD);

            // set color combination
            if (colorCodeType == 0) {

                // set color on position zero
                relLayColorIndecator.setBackgroundColor(ContextCompat.getColor(this, R.color.color1));
                viewLine.setBackgroundColor(ContextCompat.getColor(this, R.color.color1_text));
                txtViewEvent.setTextColor(ContextCompat.getColor(this, R.color.color1_text));

            } else if (colorCodeType == 1) {

                // set color on position one
                relLayColorIndecator.setBackgroundColor(ContextCompat.getColor(this, R.color.color2));
                viewLine.setBackgroundColor(ContextCompat.getColor(this, R.color.color2_text));
                txtViewEvent.setTextColor(ContextCompat.getColor(this, R.color.color2_text));

            } else if (colorCodeType == 2) {

                // set color on position two
                relLayColorIndecator.setBackgroundColor(ContextCompat.getColor(this, R.color.color3));
                viewLine.setBackgroundColor(ContextCompat.getColor(this, R.color.color3_text));
                txtViewEvent.setTextColor(ContextCompat.getColor(this, R.color.color3_text));

            } else if (colorCodeType == 3) {

                // set color on position three
                relLayColorIndecator.setBackgroundColor(ContextCompat.getColor(this, R.color.color4));
                viewLine.setBackgroundColor(ContextCompat.getColor(this, R.color.color4_text));
                txtViewEvent.setTextColor(ContextCompat.getColor(this, R.color.color4_text));

            }

            // increase color clde value
            colorCodeType++;

            // reset color form strating again
            if (colorCodeType == 4) {
                colorCodeType = 0;
            }

            // add layout
            linLayTimeInterval.addView(mLinearView);

        }
    }

    private ArrayList<UpcomingScheduleModel> getScheduleList() {


        ArrayList<UpcomingScheduleModel> upcomingList = new ArrayList<>();

        for (int i = 0; i < 5; i++) {

            UpcomingScheduleModel upcomingScheduleModel = new UpcomingScheduleModel();

            if (i == 0) {
                upcomingScheduleModel.setStartTime("9:00 AM");
                upcomingScheduleModel.setEndTime("10:00 AM");
                upcomingScheduleModel.setTitle("Dentist appoinment");
                upcomingScheduleModel.setDescription("Dentist appoinment");
            }

            if (i == 1) {
                upcomingScheduleModel.setStartTime("10:30 AM");
                upcomingScheduleModel.setEndTime("11:00 AM");
                upcomingScheduleModel.setTitle("Coffee with Allison");
                upcomingScheduleModel.setDescription("Coffee with Allison");
            }

            if (i == 2) {
                upcomingScheduleModel.setStartTime("11:10 AM");
                upcomingScheduleModel.setEndTime("12:00 PM");
                upcomingScheduleModel.setTitle("Team briefing in main conference room");
                upcomingScheduleModel.setDescription("Team briefing in main conference room");
            }

            if (i == 3) {
                upcomingScheduleModel.setStartTime("12:15 PM");
                upcomingScheduleModel.setEndTime("12:30 AM");
                upcomingScheduleModel.setTitle("Call with Wayne");
                upcomingScheduleModel.setDescription("Call with Wayne");
            }

            if (i == 4) {
                upcomingScheduleModel.setStartTime("1:00 PM");
                upcomingScheduleModel.setEndTime("02:00 PM");
                upcomingScheduleModel.setTitle("Lunch with Nancy");
                upcomingScheduleModel.setDescription("Lunch with Nancy");
            }

            upcomingList.add(upcomingScheduleModel);
        }

        return upcomingList;

    }


}
