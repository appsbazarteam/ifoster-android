package com.ifoster.mvp.holiday_calender;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface HolidayCalenderView {

    public void onSuccessHolidayList(ArrayList<HoliDayCalenderModel.Result> results);

    public void onUnsuccessHolidayList(String message);

    public void onHolidayListInternetError();
}
