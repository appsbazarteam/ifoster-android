package com.ifoster.mvp.holiday_calender;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HoliDayCalenderModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Result")
    @Expose
    private ArrayList<Result> result = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }


    public class Result {

        @SerializedName("Date")
        @Expose
        private String date;
        @SerializedName("holiday_type")
        @Expose
        private String holidayType;
        @SerializedName("holiday_descp")
        @Expose
        private String holidayDescp;

        int colorType;

        public int getColorType() {
            return colorType;
        }

        public void setColorType(int colorType) {
            this.colorType = colorType;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getHolidayType() {
            return holidayType;
        }

        public void setHolidayType(String holidayType) {
            this.holidayType = holidayType;
        }

        public String getHolidayDescp() {
            return holidayDescp;
        }

        public void setHolidayDescp(String holidayDescp) {
            this.holidayDescp = holidayDescp;
        }

    }
}