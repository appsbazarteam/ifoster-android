package com.ifoster.mvp.holiday_calender;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface HolidayCalenderPresenter {
    public void getHolidayList();
}
