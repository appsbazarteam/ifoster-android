package com.ifoster.mvp.holiday_calender;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class HolidayCalenderPresenterImpl implements HolidayCalenderPresenter {

    Activity activity;
    HolidayCalenderView holidayCalenderView;

    public HolidayCalenderPresenterImpl(Activity activity, HolidayCalenderView holidayCalenderView) {
        this.activity = activity;
        this.holidayCalenderView = holidayCalenderView;
    }

    @Override
    public void getHolidayList() {
        try {
            ApiAdapter.getInstance(activity);
            callHolidayApi();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            holidayCalenderView.onHolidayListInternetError();
        }
    }

    private void callHolidayApi() {

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), "");

        Call<HoliDayCalenderModel> getLoginOutput = ApiAdapter.getApiService().getHolidayList("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<HoliDayCalenderModel>() {
            @Override
            public void onResponse(Call<HoliDayCalenderModel> call, Response<HoliDayCalenderModel> response) {

                try {
                    //getting whole data from response
                    HoliDayCalenderModel holiDayCalenderModel = response.body();
                    String message = holiDayCalenderModel.getMessage();

                    if (holiDayCalenderModel.getStatus()) {
                        holidayCalenderView.onSuccessHolidayList(holiDayCalenderModel.getResult());
                    } else {
                        holidayCalenderView.onUnsuccessHolidayList(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    holidayCalenderView.onUnsuccessHolidayList(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<HoliDayCalenderModel> call, Throwable t) {
                holidayCalenderView.onUnsuccessHolidayList(activity.getString(R.string.error_server));
            }
        });
    }
}
