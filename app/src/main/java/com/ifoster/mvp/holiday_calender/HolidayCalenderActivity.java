package com.ifoster.mvp.holiday_calender;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class HolidayCalenderActivity extends BaseActivity implements HolidayCalenderView {

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    HolidayCalenderPresenterImpl holidayCalenderPresenterImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holiday_calender);

        holidayCalenderPresenterImpl = new HolidayCalenderPresenterImpl(this, this);

        setToolbar();
        setFont();
    }

    private void setToolbar() {
        txtViewToolbarTitle.setText(getString(R.string.title_holiday_calender));
    }

    private void setFont() {
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getData();
    }

    @OnClick(R.id.imgViewBack)
    public void back() {
        finish();
    }

    @Override
    public void onSuccessHolidayList(ArrayList<HoliDayCalenderModel.Result> results) {
        hideProgressBar();
        if (results != null && results.size() > 0) {

            int colorCodeType = 0;

            if (results.size() > 0) {

                for (HoliDayCalenderModel.Result user : results) {
                    // set color
                    user.setColorType(colorCodeType);

                    // increase color code value
                    colorCodeType++;

                    // reset color form strating again
                    if (colorCodeType == 4) {
                        colorCodeType = 0;
                    }
                }
            }

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setNestedScrollingEnabled(false);

            HolidayCalenderAdapter holidayCalenderAdapter = new HolidayCalenderAdapter(this, results);
            recyclerView.setAdapter(holidayCalenderAdapter);
        }
    }

    @Override
    public void onUnsuccessHolidayList(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onHolidayListInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryHolidayList, coordinateLayout);
    }

    OnClickInterface onRetryHolidayList = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    private void getData() {
        showProgressBar();
        holidayCalenderPresenterImpl.getHolidayList();
    }
}
