package com.ifoster.mvp.holiday_calender;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-03-2017.
 */

public class HolidayCalenderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<HoliDayCalenderModel.Result> listData;

    public HolidayCalenderAdapter(Context context, ArrayList<HoliDayCalenderModel.Result> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(context).inflate(R.layout.item_holiday_calender, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final HoliDayCalenderModel.Result result = listData.get(position);

        if (result.getDate() != null) {
            String[] date = result.getDate().split(" ");

            if (date.length >= 2) {
                ((ViewHolderList) holder).txtViewCircleDate.setText(date[0]);
                ((ViewHolderList) holder).txtViewCircleMonth.setText(date[1]);
            }
        }

        if (result.getHolidayType() != null) {
            ((ViewHolderList) holder).txtViewHolidayType.setText(result.getHolidayType());
        }

        if (result.getHolidayDescp() != null) {
            ((ViewHolderList) holder).txtViewHolidayDesc.setText(result.getHolidayDescp());
        }

        // set color combination
        if (result.getColorType() == 0) {
            // set color on position zero
            ((ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color1_text));

        } else if (result.getColorType() == 1) {
            // set color on position one
            ((ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color2_text));

        } else if (result.getColorType() == 2) {
            // set color on position two
            ((ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color3_text));

        } else if (result.getColorType() == 3) {
            // set color on position three
            ((ViewHolderList) holder).cardViewRoundType.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color4_text));
        }

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.cardViewRoundType)
        CardView cardViewRoundType;

        @BindView(R.id.txtViewCircleDate)
        TextView txtViewCircleDate;

        @BindView(R.id.txtViewCircleMonth)
        TextView txtViewCircleMonth;

        @BindView(R.id.txtViewHolidayType)
        TextView txtViewHolidayType;

        @BindView(R.id.txtViewHolidayDesc)
        TextView txtViewHolidayDesc;

        /*@BindView(R.id.txtViewMore)
        TextView txtViewMore;*/

        public ViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            // set font
            FontHelper.applyFont(context, txtViewCircleDate, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewCircleMonth, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewHolidayType, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(context, txtViewHolidayDesc, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        }
    }
}
