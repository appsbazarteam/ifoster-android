package com.ifoster.mvp;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.ifoster.R;
import com.android.progressbar.SmoothProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by A1VSZHT2 on 7/13/2018.
 */

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.progress_bar)
    SmoothProgressBar mProgressBar;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    public void showProgressBar(){
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar(){
        mProgressBar.setVisibility(View.GONE);
    }

    public boolean isProgressBarShowing(){
        return mProgressBar.getVisibility() == View.VISIBLE? true:false;
    }
}
