package com.ifoster.mvp.events_program.view;

import com.ifoster.mvp.notice_board.model.EventListModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface EventProgramView {

    public void onSuccessEventList(ArrayList<EventListModel.Result> message);

    public void onUnsuccessEventList(String message);

    public void onEventListInternetError();

    public void onAttachmentDownload(String path);
}
