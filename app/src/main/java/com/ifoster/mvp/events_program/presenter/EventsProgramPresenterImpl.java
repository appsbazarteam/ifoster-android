package com.ifoster.mvp.events_program.presenter;

import android.app.Activity;

import com.ifoster.App;
import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import com.ifoster.mvp.events_program.view.EventProgramView;
import com.ifoster.mvp.notice_board.model.EventListModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class EventsProgramPresenterImpl implements EventsProgramPresenter {

    private Activity activity;
    private EventProgramView noticeView;
    private JSONObject jsonObject;

    public EventsProgramPresenterImpl(Activity activity, EventProgramView noticeView) {
        this.activity = activity;
        this.noticeView = noticeView;
    }

    @Override
    public void getClassWiseEventList(String date, int userLogedInType) {
        try {
            ApiAdapter.getInstance(activity);
            callClassWiseEventListApi(date, userLogedInType);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            noticeView.onEventListInternetError();
        }
    }


    @Override
    public void getGlobalEventList(String date, int userLogedInType) {
        try {
            ApiAdapter.getInstance(activity);
            callGlobalEventListApi(date, userLogedInType);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            noticeView.onEventListInternetError();
        }
    }


    private void callClassWiseEventListApi(String date, int userLogedInType) {

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.STUDENT_ID, UserSession.getInstance().getStudentId()+"");
            jsonObject.put(Const.PARAM_MONTH, date);
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<EventListModel> getLoginOutput = null;

        getLoginOutput = ApiAdapter.getApiService().getClasswiseEventList("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<EventListModel>() {
            @Override
            public void onResponse(Call<EventListModel> call, Response<EventListModel> response) {
                try {

                    //getting whole data from response
                    EventListModel eventListModel = response.body();
                    String message = eventListModel.getMessage();

                    if (eventListModel.getStatus()) {
                        noticeView.onSuccessEventList(eventListModel.getResult());
                    } else {
                        noticeView.onUnsuccessEventList(message);
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    noticeView.onUnsuccessEventList(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<EventListModel> call, Throwable t) {
                noticeView.onUnsuccessEventList(activity.getString(R.string.error_server));
            }
        });
    }

    private void callGlobalEventListApi(String date, int userLogedInType) {

        String logedData = "";

        if(App.getApplicationInstance().isShowParentTab()){
            logedData = "1";
        }else{
            logedData = "2";
        }

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.LOGED_DATA, logedData);
            jsonObject.put(Const.PARAM_MONTH, date);
            jsonObject.put(Const.PARAM_USER_TOKEN, UserSession.getInstance().getUserToken());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<EventListModel> getLoginOutput = null;

        getLoginOutput = ApiAdapter.getApiService().getGlobalEventList("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<EventListModel>() {
            @Override
            public void onResponse(Call<EventListModel> call, Response<EventListModel> response) {

                try {

                    //getting whole data from response
                    EventListModel eventListModel = response.body();
                    String message = eventListModel.getMessage();

                    if (eventListModel.getStatus()) {
                        noticeView.onSuccessEventList(eventListModel.getResult());
                    } else {
                        noticeView.onUnsuccessEventList(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    noticeView.onUnsuccessEventList(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<EventListModel> call, Throwable t) {
                noticeView.onUnsuccessEventList(activity.getString(R.string.error_server));
            }
        });
    }


}
