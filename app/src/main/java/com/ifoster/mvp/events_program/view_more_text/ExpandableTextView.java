package com.ifoster.mvp.events_program.view_more_text;

/**
 * Created by Braintech on 03-04-2017.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.ifoster.R;


public class ExpandableTextView extends AppCompatTextView {


    boolean isCollapsed = true;

    public ExpandableTextView(Context context) {
        this(context, null);
    }

    public ExpandableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ExpandableTextView);

        typedArray.recycle();

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isCollapsed) {
                    expand(v);
                } else {
                    collapse(v);
                }

            }
        });
    }


    public void collapse(final View v) {
        isCollapsed = true;

        ((TextView) v).setSingleLine(true);
        ((TextView) v).setEllipsize(TextUtils.TruncateAt.END);
        int n = 2; // the exact number of lines you want to display
        ((TextView) v).setMaxLines(n);

    }

    public void expand(final View v) {
        isCollapsed = false;

        ((TextView) v).setSingleLine(false);
        ((TextView) v).setEllipsize(TextUtils.TruncateAt.END);
        int n = 10; // the exact number of lines you want to display
        ((TextView) v).setMaxLines(n);


    }

    public boolean getIsCollapsed() {
        return isCollapsed;
    }

}