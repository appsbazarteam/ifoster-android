package com.ifoster.mvp.events_program.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ifoster.R;
import com.ifoster.common.adapter.model.MonthModel;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.common.utility.Utils;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.events_program.EventsAndProgramActivity;
import com.ifoster.mvp.events_program.adapter.GlobalEventMonthAdapter;
import com.ifoster.mvp.events_program.presenter.EventsProgramPresenterImpl;
import com.ifoster.mvp.events_program.view.EventProgramView;
import com.ifoster.mvp.notice_board.adapter.EventAdapter;
import com.ifoster.mvp.notice_board.model.EventListModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alam on 30-Sep-17.
 */

public class GlobalEventsFragment extends Fragment implements EventProgramView {


    @BindView(R.id.recyclerViewMonth)
    RecyclerView recyclerViewMonth;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.relLayMainContainer)
    RelativeLayout relLayMainContainer;

    String currentMonth;

    /*-----------Class Presenter-----------*/
    EventsProgramPresenterImpl eventsProgramPresenter;

    /*--------UserSession-----*/


    int userLogedInType = 0;

    int scrollPosition;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_event_and_program, container, false);
        ButterKnife.bind(this, view);


        // Inflate the layout for getActivity() fragment
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        eventsProgramPresenter = new EventsProgramPresenterImpl(getActivity(), this);



        getIntentData();

        currentMonth = Utils.getCurrentMonth();

        getData(currentMonth);

        bindMonthToRecyclerView();

    }

    private void bindMonthToRecyclerView() {

        recyclerViewMonth.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewMonth.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMonth.setNestedScrollingEnabled(false);

        ArrayList<MonthModel> list = getMonthList();

        GlobalEventMonthAdapter noticeboardAdapter = new GlobalEventMonthAdapter(getActivity(), list, this);
        recyclerViewMonth.setAdapter(noticeboardAdapter);

        recyclerViewMonth.scrollToPosition(scrollPosition);
    }


    private void getIntentData() {

        Intent intent = getActivity().getIntent();

        if (intent.hasExtra(ConstIntent.KEY_USER_TYPE)) {

            userLogedInType = intent.getIntExtra(ConstIntent.KEY_USER_TYPE, 0);

            if (userLogedInType == 0) {
                userLogedInType = UserSession.getInstance().getLoginType();
            }
        } else {
            userLogedInType = UserSession.getInstance().getLoginType();
        }
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    public void getData(String date) {
        ((EventsAndProgramActivity)getActivity()).showProgressBar();
        eventsProgramPresenter.getGlobalEventList(date, userLogedInType);
    }

    @Override
    public void onSuccessEventList(ArrayList<EventListModel.Result> results) {
        ((EventsAndProgramActivity)getActivity()).hideProgressBar();
        if (results != null && results.size() > 0) {

            int colorCodeType = 0;

            for (int i = 0; i < results.size(); i++) {
                results.get(i).setColoType(colorCodeType);

                // increase color clde value
                colorCodeType++;

                // reset color form starting again
                if (colorCodeType == 4) {
                    colorCodeType = 0;
                }
            }


        } else {

            results = new ArrayList<>();

            SnackNotify.showMessage("No record found.", relLayMainContainer);
        }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        EventAdapter eventAdapter = new EventAdapter(getActivity(), results, 1,this);
        recyclerView.setAdapter(eventAdapter);

    }

    @Override
    public void onUnsuccessEventList(String message) {
        ((EventsAndProgramActivity)getActivity()).hideProgressBar();
        SnackNotify.showMessage(message, relLayMainContainer);
    }

    @Override
    public void onEventListInternetError() {
        ((EventsAndProgramActivity)getActivity()).hideProgressBar();
        SnackNotify.checkConnection(onRetryNotice, relLayMainContainer);
    }

    @Override
    public void onAttachmentDownload(String path) {
        EventsAndProgramActivity noticeboardActivity = (EventsAndProgramActivity)getActivity();
        if(noticeboardActivity != null)
            noticeboardActivity.downloadAttachment(path);
    }

    OnClickInterface onRetryNotice = new OnClickInterface() {
        @Override
        public void onClick() {
            getData(currentMonth);
        }
    };

    private ArrayList<MonthModel> getMonthList() {


        ArrayList<MonthModel> monthList = new ArrayList<>();

        MonthModel monthModel1 = new MonthModel();
        monthModel1.setMonthName("Jan");

        MonthModel monthModel2 = new MonthModel();
        monthModel2.setMonthName("Feb");

        MonthModel monthModel3 = new MonthModel();
        monthModel3.setMonthName("Mar");

        MonthModel monthModel4 = new MonthModel();
        monthModel4.setMonthName("Apr");

        MonthModel monthModel5 = new MonthModel();
        monthModel5.setMonthName("May");

        MonthModel monthModel6 = new MonthModel();
        monthModel6.setMonthName("Jun");

        MonthModel monthModel7 = new MonthModel();
        monthModel7.setMonthName("Jul");

        MonthModel monthModel8 = new MonthModel();
        monthModel8.setMonthName("Aug");

        MonthModel monthModel9 = new MonthModel();
        monthModel9.setMonthName("Sep");

        MonthModel monthModel10 = new MonthModel();
        monthModel10.setMonthName("Oct");

        MonthModel monthModel11 = new MonthModel();
        monthModel11.setMonthName("Nov");

        MonthModel monthModel12 = new MonthModel();
        monthModel12.setMonthName("Dec");


        monthList.add(monthModel1);
        monthList.add(monthModel2);
        monthList.add(monthModel3);
        monthList.add(monthModel4);
        monthList.add(monthModel5);
        monthList.add(monthModel6);
        monthList.add(monthModel7);
        monthList.add(monthModel8);
        monthList.add(monthModel9);
        monthList.add(monthModel10);
        monthList.add(monthModel11);
        monthList.add(monthModel12);

        for (int i = 0; i < monthList.size(); i++) {

            if (currentMonth.equalsIgnoreCase(monthList.get(i).getMonthName())) {

                monthList.get(i).setSelected(true);

                scrollPosition = i;

                break;
            }
        }

        return monthList;
    }

}
