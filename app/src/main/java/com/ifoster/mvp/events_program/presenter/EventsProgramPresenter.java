package com.ifoster.mvp.events_program.presenter;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface EventsProgramPresenter {

    public void getClassWiseEventList(String date, int userLogedInType);
    public void getGlobalEventList(String date, int userLogedInType);
}
