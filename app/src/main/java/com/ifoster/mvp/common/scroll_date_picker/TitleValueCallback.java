package com.ifoster.mvp.common.scroll_date_picker;

import org.joda.time.LocalDate;

public interface TitleValueCallback {
    void onTitleValueReturned(LocalDate date);
}
