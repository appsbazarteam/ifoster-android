package com.ifoster.mvp.common.scroll_date_picker;

public interface OnChildClickedListener {
    void onChildClick(boolean clicked);
}
