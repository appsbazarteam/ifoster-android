package com.ifoster.mvp.common.scroll_date_picker;

import android.support.annotation.Nullable;

import java.util.Date;

public interface OnDateSelectedListener {
    void onDateSelected(@Nullable Date date);
}
