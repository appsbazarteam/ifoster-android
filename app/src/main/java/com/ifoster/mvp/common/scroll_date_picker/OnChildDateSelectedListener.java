package com.ifoster.mvp.common.scroll_date_picker;

import android.support.annotation.Nullable;

import org.joda.time.LocalDate;

public interface OnChildDateSelectedListener {
    void onDateSelectedChild(@Nullable LocalDate date);
}
