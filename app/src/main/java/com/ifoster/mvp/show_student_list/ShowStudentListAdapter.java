package com.ifoster.mvp.show_student_list;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ifoster.App;
import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.CircleTransform;
import com.ifoster.mvp.navigation.NavigationActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alam on 15-May-18.
 */

public class ShowStudentListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity activity;
    ArrayList<ShowStudentListModel.User> listData;
    int userType = 0;

    public ShowStudentListAdapter(Activity activity, ArrayList<ShowStudentListModel.User> listData, int userType) {
        this.activity = activity;
        this.listData = listData;
        this.userType = userType;
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderList(LayoutInflater.from(activity).inflate(R.layout.item_student_list, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ShowStudentListModel.User user = listData.get(position);

        Glide.with(activity).load(user.getProfileImage())
                .error(R.drawable.user)
                .placeholder(R.drawable.user)
                .thumbnail(0.5f)
                .crossFade()
                .transform(new CircleTransform(activity))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(((ViewHolderList) holder).imgViewStudent);

        if (user.getStudentName() != null) {
            ((ViewHolderList) holder).txtViewName.setText(user.getStudentName());
        } else {
            ((ViewHolderList) holder).txtViewName.setText("--NA--");
        }


        if (user.getSection() != null) {
            ((ViewHolderList) holder).txtViewSection.setText(user.getSection());
        } else {
            ((ViewHolderList) holder).txtViewSection.setText("--NA--");
        }


        if (user.getClass_() != null) {
            ((ViewHolderList) holder).txtViewClass.setText(user.getClass_());
        } else {
            ((ViewHolderList) holder).txtViewClass.setText("--NA--");
        }

        ((ViewHolderList) holder).cardviewMainContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user.getStudentId() != null) {
                    App.getApplicationInstance().setShowParentTab(true);
                    App.getApplicationInstance().setProfileImageURL(user.getProfileImage());
                    UserSession.getInstance().saveStudentId(Integer.parseInt(user.getStudentId()), user.getStudentName(),user.getClass_(),user.getSection());
                    Intent intent = new Intent(activity, NavigationActivity.class);
                    intent.putExtra(ConstIntent.KEY_USER_TYPE, userType);
                    activity.startActivity(intent);
                    activity.finish();
                }
            }
        });
    }


    public class ViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.txtName)
        TextView txtName;

        @BindView(R.id.txtViewName)
        TextView txtViewName;

        @BindView(R.id.imgViewStudent)
        ImageView imgViewStudent;

        @BindView(R.id.cardviewMainContainer)
        CardView cardviewMainContainer;

        @BindView(R.id.txtClass)
        TextView txtClass;

        @BindView(R.id.txtViewClass)
        TextView txtViewClass;

        @BindView(R.id.txtSection)
        TextView txtSection;

        @BindView(R.id.txtViewSection)
        TextView txtViewSection;

        public ViewHolderList(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            FontHelper.applyFont(activity, txtName, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(activity, txtViewName, FontHelper.FontType.FONT_QUICKSAND_BOLD);

            FontHelper.applyFont(activity, txtClass, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(activity, txtViewClass, FontHelper.FontType.FONT_QUICKSAND_BOLD);

            FontHelper.applyFont(activity, txtSection, FontHelper.FontType.FONT_QUICKSAND_BOLD);
            FontHelper.applyFont(activity, txtViewSection, FontHelper.FontType.FONT_QUICKSAND_BOLD);

        }
    }
}
