package com.ifoster.mvp.show_student_list;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ifoster.App;
import com.ifoster.R;
import com.ifoster.common.helpers.FontHelper;
import com.ifoster.common.requestresponse.ConstIntent;
import com.ifoster.common.session.UserSession;
import com.ifoster.common.utility.Resource;
import com.ifoster.common.utility.SnackNotify;
import com.ifoster.interfaces.OnClickInterface;
import com.ifoster.mvp.BaseActivity;
import com.ifoster.mvp.navigation.NavigationActivity;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.OnClick;

public class ShowStudentListActivity extends BaseActivity implements ShowStudentListView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewToolbarTitle)
    TextView txtViewToolbarTitle;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.headerImg)
    RelativeLayout headerImg;

    private ShowStudentListPresenterImpl showStudentListPresenterImpl;
    private int userType = 0;
    private boolean isFromdashboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_student_list);
        setToolbarTitle();

        showStudentListPresenterImpl = new ShowStudentListPresenterImpl(this, this);
        getData();
        getIntentData();
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent.hasExtra(ConstIntent.KEY_USER_TYPE)) {
            userType = intent.getIntExtra(ConstIntent.KEY_USER_TYPE, 0);
            if (userType == 0)
                userType = UserSession.getInstance().getLoginType();
        } else
            userType = UserSession.getInstance().getLoginType();

        Bundle bundle = intent.getExtras();
        if(bundle != null)
            isFromdashboard = bundle.getBoolean(NavigationActivity.IS_FROM_DASHBOARD);
    }

    private void getData() {
        showProgressBar();
        showStudentListPresenterImpl.callingStudentListApi(UserSession.getInstance().getUserToken(), 0);
    }

    @OnClick(R.id.imgViewBack)
    public void onBackClick() {
        if(isFromdashboard)
            finish();
        else
            SnackNotify.showSnakeBarForBackPress(coordinateLayout);
    }

    @Override
    public void onBackPressed() {
        if(isFromdashboard)
            finish();
        else
            SnackNotify.showSnakeBarForBackPress(coordinateLayout);
    }

    @Override
    public void onSuccessStudentList(ArrayList<ShowStudentListModel.User> userArrayList) {
        hideProgressBar();
        if (userArrayList != null && userArrayList.size() > 0) {
            App.getApplicationInstance().setSingleStudent(false);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            ShowStudentListAdapter showStudentListAdapter = new ShowStudentListAdapter(this, userArrayList, userType);
            recyclerView.setAdapter(showStudentListAdapter);
        } else {
            onUnsuccessStudentList(Resource.toString(R.string.error_server));
        }
    }

    @Override
    public void onUnsuccessStudentList(String message) {
        hideProgressBar();
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onStudentListInternetError() {
        hideProgressBar();
        SnackNotify.checkConnection(onRetryGetStudentList, coordinateLayout);
    }

    OnClickInterface onRetryGetStudentList = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    private void setToolbarTitle() {
        txtViewToolbarTitle.setText(Resource.toString(R.string.student_list));
        FontHelper.applyFont(this, txtViewToolbarTitle, FontHelper.FontType.FONT_QUICKSAND_BOLD);
    }
}
