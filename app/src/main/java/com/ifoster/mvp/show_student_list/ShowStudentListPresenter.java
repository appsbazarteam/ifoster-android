package com.ifoster.mvp.show_student_list;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ShowStudentListPresenter {
    public void callingStudentListApi(String userToken, int userLoginType);
}
