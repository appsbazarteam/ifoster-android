package com.ifoster.mvp.show_student_list;

import java.util.ArrayList;

/**
 * Created by Braintech on 13-09-2017.
 */

public interface ShowStudentListView {

    public void onSuccessStudentList(ArrayList<ShowStudentListModel.User> message);

    public void onUnsuccessStudentList(String message);

    public void onStudentListInternetError();
}
