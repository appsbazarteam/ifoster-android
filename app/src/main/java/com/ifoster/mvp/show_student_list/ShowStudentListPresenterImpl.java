package com.ifoster.mvp.show_student_list;

import android.app.Activity;

import com.ifoster.BuildConfig;
import com.ifoster.R;
import com.ifoster.common.requestresponse.ApiAdapter;
import com.ifoster.common.requestresponse.Const;
import com.ifoster.common.session.UserSession;
import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 13-09-2017.
 */

public class ShowStudentListPresenterImpl implements ShowStudentListPresenter {

    Activity activity;
    ShowStudentListView showStudentListView;
    JSONObject jsonObject;

    public ShowStudentListPresenterImpl(Activity activity, ShowStudentListView showStudentListView) {
        this.activity = activity;
        this.showStudentListView = showStudentListView;
    }

    @Override
    public void callingStudentListApi(String userToken, int userLoginType) {
        try {
            ApiAdapter.getInstance(activity);
            getStudentList(userToken);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            showStudentListView.onStudentListInternetError();
        }
    }

    private void getStudentList(String userToken) {

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_USER_TOKEN, userToken);
            jsonObject.put("mobileNumber", UserSession.getInstance().getMobileNo());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<ShowStudentListModel> getLoginOutput = ApiAdapter.getApiService().getStudentList("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<ShowStudentListModel>() {
            @Override
            public void onResponse(Call<ShowStudentListModel> call, Response<ShowStudentListModel> response) {

                try {

                    //getting whole data from response
                    ShowStudentListModel showStudentListModel = response.body();
                    String message = showStudentListModel.getMessage();

                    if (showStudentListModel.getStatus()) {
                        showStudentListView.onSuccessStudentList(showStudentListModel.getUser());
                    } else {
                        showStudentListView.onUnsuccessStudentList(message);
                    }
                } catch (Exception exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    showStudentListView.onUnsuccessStudentList(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ShowStudentListModel> call, Throwable t) {
                showStudentListView.onUnsuccessStudentList(activity.getString(R.string.error_server));
            }
        });
    }


}
